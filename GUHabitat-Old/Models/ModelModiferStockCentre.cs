﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModiferStockCentre
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public int reference { get; set; }
        public DateTime Date { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }


        public decimal StockTheorie { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        public decimal Stock { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        public decimal StockFin { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        public decimal Depotage { get; set; }

        public decimal GainPerte { get; set; }

        public string Commentaire { get; set; }

        public int etat { get; set; }

        public List<CentreDto> ListeCentre { get; set; }

       


    }
}