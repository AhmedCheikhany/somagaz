﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelAjouterBonEnlevement
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int CodeClient { get; set; }
        public string NomClient { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int idMouvementBon { get; set; }
        public string MouvementBon { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int idTypeBonEnlevement { get; set; }
        public string TypeBonEnlevement { get; set; }
        public string ReferenceDepotage { get; set; }
        public string ReferenceBonPesee { get; set; }

        public string ImmatriculeCamion { get; set; }
        public string ChauffeurCamion { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal Tonnage { get; set; }

        public string Commentaires { get; set; }

        public int etat { get; set; }

        public string MouvementBonString
        {
            get
            {
                string str = "";
                if (idMouvementBon == 1)
                    str = "Entrees";
                else if (idMouvementBon == 2)
                    str = "Sorties";
                return str;
            }
        }


        public List<TypeBonEnlevementDto> ListeTypeBonEnlevement { get; set; }
    }
}