﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{

    public class ModelListeFicheConsignationParClient
    {

        public int? page { get; set; }
        public string filtreMotCle { get; set; }
        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }
        public PagedList.IPagedList<FicheConsignationDto> ListeFicheConsignationParClient { get; set; }

    }

}