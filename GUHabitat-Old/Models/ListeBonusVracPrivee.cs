﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ListeBonusVracPrivee
    {

        public int? page { get; set; }
        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }
        public int CodeClient { get; set; }

        public PagedList.IPagedList<BonusVracPriveeDto> ListeBonusPrivee { get; set; }

    }
}