﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelStatistiques
    {


        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }
        public DateTime filtreDate { get; set; }

       
        public decimal StockNKTT { get; set; }
        public decimal StockTheorieNKTT { get; set; }
        public decimal StockNKTTFin { get; set; }
        public decimal GainPerteNktt { get; set; }
        public decimal StockActuelNktt { get; set; }
        public decimal TonnageAvoirNktt { get; set; }

        public decimal Depotage { get; set; }

       

        public decimal TotalRetour { get; set; }

        public decimal TonnageCollaborateurEnvoie { get; set; }
        public decimal TonnageCollaborateurEnvoiePipes { get; set; }
        public decimal TonnageCollaborateurReception { get; set; }

        public decimal TonnageCollaborateurEnvoieInventaire { get; set; }
        public decimal TonnageCollaborateurReceptionInventaire { get; set; }

        public decimal TonnageCollaborateurActuel { get; set; }

        public int B12Beev { get; set; }
        public int B9Beev { get; set; }
        public int B6RBeev { get; set; }
        public int B6VBeev { get; set; }
        public int B3Beev { get; set; }
        public int B35Beev { get; set; }


        public int B12Arb { get; set; }
        public int B9Arb { get; set; }
        public int B6RArb { get; set; }
        public int B6VArb { get; set; }
        public int B3Arb { get; set; }
        public int B35Arb { get; set; }

        public int B12Beep { get; set; }
        public int B9Beep { get; set; }
        public int B6RBeep { get; set; }
        public int B6VBeep { get; set; }
        public int B3Beep { get; set; }
        public int B35Beep { get; set; }


        public decimal TonnageSomagaz { get; set; }
        public decimal TonnagePrivee { get; set; }
        public decimal TonnagePartenaire { get; set; }
        public decimal AutresVrac { get; set; }


        public decimal TotalProduction { get; set; }
        public decimal TotalCommerciale { get; set; }
        public decimal TotalConditionneeProduction { get; set; }
        public decimal TotalConditionneeCommerciale { get; set; }

        public decimal TotalRetourLivrer { get; set; }


        public decimal TotalPoidsManquantAredp { get; set; }
        public decimal TotalPoidsRestantAredp { get; set; }

        public decimal TotalPoidsManquantAvoir { get; set; }
        public decimal TotalPoidsRestantAvoir { get; set; }



        public decimal B12PoidManquantAvoir { get; set; }
        public decimal B9PoidManquantAvoir { get; set; }
        public decimal B6RPoidManquantAvoir { get; set; }
        public decimal B6VPoidManquantAvoir { get; set; }
        public decimal B3PoidManquantAvoir { get; set; }
        public decimal B35PoidManquantAvoir { get; set; }


        public decimal B12PoidRestantAvoir { get; set; }
        public decimal B9PoidRestantAvoir { get; set; }
        public decimal B6RPoidRestantAvoir { get; set; }
        public decimal B6VPoidRestantAvoir { get; set; }
        public decimal B3PoidRestantAvoir { get; set; }
        public decimal B35PoidRestantAvoir { get; set; }


        public int B12Vente { get; set; }

        public int B9Vente { get; set; }

        public int B6RVente { get; set; }

        public int B6VVente { get; set; }

        public int B3Vente { get; set; }

        public int B35Vente { get; set; }




        public int B12VenteDirecte { get; set; }

        public int B9VenteDirecte { get; set; }

        public int B6RVenteDirecte { get; set; }

        public int B6VVenteDirecte { get; set; }

        public int B3VenteDirecte { get; set; }

        public int B35VenteDirecte { get; set; }



        public int B12Dotation { get; set; }

        public int B9Dotation { get; set; }

        public int B6RDotation { get; set; }

        public int B6VDotation { get; set; }

        public int B3Dotation { get; set; }

        public int B35Dotation { get; set; }


        public int B12Consignation { get; set; }

        public int B9Consignation { get; set; }

        public int B6RConsignation { get; set; }

        public int B6VConsignation { get; set; }

        public int B3Consignation { get; set; }

        public int B35Consignation { get; set; }

        public int PresentoirConsignation { get; set; }



        public int B12CorpsEtCharges { get; set; }

        public int B9CorpsEtCharges { get; set; }

        public int B6RCorpsEtCharges { get; set; }

        public int B6VCorpsEtCharges { get; set; }

        public int B3CorpsEtCharges { get; set; }

        public int B35CorpsEtCharges { get; set; }


        public int B12Corps { get; set; }

        public int B9Corps { get; set; }

        public int B6RCorps { get; set; }

        public int B6VCorps { get; set; }

        public int B3Corps { get; set; }

        public int B35Corps { get; set; }





        public int B12Avoir { get; set; }

        public int B9Avoir { get; set; }

        public int B6RAvoir { get; set; }

        public int B6VAvoir { get; set; }

        public int B3Avoir { get; set; }

        public int B35Avoir { get; set; }



        public int B12Aredp { get; set; }

        public int B9Aredp { get; set; }

        public int B6RAredp { get; set; }

        public int B6VAredp { get; set; }

        public int B3Aredp { get; set; }

        public int B35Aredp { get; set; }



        public int B12Client { get; set; }

        public int B9Client { get; set; }

        public int B6RClient { get; set; }

        public int B6VClient { get; set; }

        public int B3Client { get; set; }

        public int B35Client { get; set; }


       

        public int B12Afact { get; set; }

        public int B9Afact { get; set; }

        public int B6RAfact { get; set; }

        public int B6VAfact { get; set; }

        public int B3Afact { get; set; }

        public int B35Afact { get; set; }



        public int B12Production { get; set; }

        public int B9Production { get; set; }

        public int B6RProduction { get; set; }

        public int B6VProduction { get; set; }

        public int B3Production { get; set; }

        public int B35Production { get; set; }




    }
}