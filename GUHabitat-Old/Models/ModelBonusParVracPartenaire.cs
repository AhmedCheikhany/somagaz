﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelBonusParVracPartenaire
    {

        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }

        public int CodeClient { get; set; }
        public string NomClient { get; set; }

        public decimal Tonnage { get; set; }

        public decimal MontantMru { get; set; }

        public decimal MontantMro { get; set; }




    }
}