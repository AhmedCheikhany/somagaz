﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModifierConsignationCentre
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int CodeCentre { get; set; }

        public string LibelleCentre { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12 { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9 { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6R { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6V { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3 { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35 { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B12Prix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B9Prix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B6RPrix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B6VPrix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B3Prix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B35Prix { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB12 { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB9 { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB6R { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB6V { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB3 { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB35 { get; set; }


        public int etat { get; set; }

        public List<CentreDto> ListeCentre { get; set; }

        public string Commentaire { get; set; }

      



    }
}