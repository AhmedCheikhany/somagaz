﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelSituationStatistique
    {

        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }

        public decimal StockNKTT { get; set; }
        public decimal Depotage { get; set; }

        public decimal StockInterieur { get; set; }
        public decimal TotalStock { get; set; }

        public decimal VenteConditionneeNKTT { get; set; }


        public decimal VenteCentresInterieur { get; set; }
        public decimal VenteVrac { get; set; }
        public decimal TotalVente { get; set; }
        public decimal TransfertCentres { get; set; }
        public decimal TransfertPartenaires { get; set; }

        public decimal TotalTransfert { get; set; }
        public decimal GainPerte { get; set; }
        public decimal RecetteNKTT { get; set; }
        public decimal RecetteInterieur { get; set; }
        public decimal TotalRecette { get; set; }
        public decimal Versement { get; set; }
        public decimal VersementInterieur { get; set; }
        public decimal TotalVersement { get; set; }
        public decimal AutreReglement { get; set; }

        public decimal Pret { get; set; }
        public decimal PretsAceJour { get; set; }

        public decimal MoyenneJournaliere { get; set; }
        public decimal MinJournaliere { get; set; }
        public decimal MaxJournaliere { get; set; }
        public decimal VenteAceJour { get; set; }


    }
}