﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelStatistiqueCentreNKTT
    {
        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }



        public decimal TotalRetourLivrer { get; set; }


        public decimal TotalRetour { get; set; }

        public decimal TotalRetourSomagaz { get; set; }

        public decimal TotalAutreRetour { get; set; }

        public decimal TotalRetourPartenaire { get; set; }

        public decimal TotalRetourCollaborateurs { get; set; }

        public decimal TotalStock { get; set; }

        public decimal TotalProductions { get; set; }

        public decimal TotalFacturations { get; set; }


        public decimal TonnageSomagaz { get; set; }
        public decimal TonnagePrivee { get; set; }
        public decimal TonnagePartenaire { get; set; }
        public decimal AutresVrac { get; set; }



        public decimal TotalProduction { get; set; }
        public decimal TotalFacturation { get; set; }

        public decimal TotalConditionneeProduction { get; set; }
        public decimal TotalConditionneeCommerciale { get; set; }


        public decimal TonnageCollaborateurEnvoie { get; set; }
        public decimal TonnageCollaborateurReception { get; set; }
        public decimal TonnageCollaborateurActuel { get; set; }

        public decimal TonnageCollaborateurEnvoieCiterne { get; set; }
        public decimal TonnageCollaborateurReceptionCiterne { get; set; }

        public decimal TonnageCollaborateurEnvoieInventaire { get; set; }
        public decimal TonnageCollaborateurReceptionInventaire { get; set; }


        public decimal TonnageCollaborateurEnvoiePipes { get; set; }
        public decimal TonnageCollaborateurReceptionPipes { get; set; }


        public decimal StockNKTT { get; set; }
        public decimal StockTheorieNKTT { get; set; }
        public decimal StockFinNKTT { get; set; }
        public decimal StockActuelNktt { get; set; }
        public decimal TonnageAvoirNktt { get; set; }
        public decimal Depotage { get; set; }

        public decimal GainPerteNktt { get; set; }



        public int B12Vente { get; set; }

        public int B9Vente { get; set; }

        public int B6RVente { get; set; }

        public int B6VVente { get; set; }

        public int B3Vente { get; set; }

        public int B35Vente { get; set; }


        public int B12VenteDirecte { get; set; }

        public int B9VenteDirecte { get; set; }

        public int B6RVenteDirecte { get; set; }

        public int B6VVenteDirecte { get; set; }

        public int B3VenteDirecte { get; set; }

        public int B35VenteDirecte { get; set; }






        public int B12Dotation { get; set; }

        public int B9Dotation { get; set; }

        public int B6RDotation { get; set; }

        public int B6VDotation { get; set; }

        public int B3Dotation { get; set; }

        public int B35Dotation { get; set; }


        public int B12Consignation { get; set; }

        public int B9Consignation { get; set; }

        public int B6RConsignation { get; set; }

        public int B6VConsignation { get; set; }

        public int B3Consignation { get; set; }

        public int B35Consignation { get; set; }




        public int B12Avoir { get; set; }

        public int B9Avoir { get; set; }

        public int B6RAvoir { get; set; }

        public int B6VAvoir { get; set; }

        public int B3Avoir { get; set; }

        public int B35Avoir { get; set; }



        public int B12Afact { get; set; }

        public int B9Afact { get; set; }

        public int B6RAfact { get; set; }

        public int B6VAfact { get; set; }

        public int B3Afact { get; set; }

        public int B35Afact { get; set; }



        public int B12Production { get; set; }

        public int B9Production { get; set; }

        public int B6RProduction { get; set; }

        public int B6VProduction { get; set; }

        public int B3Production { get; set; }

        public int B35Production { get; set; }




        public int B12Emballage { get; set; }
        public int B9Emballage { get; set; }
        public int B6REmballage { get; set; }
        public int B6VEmballage { get; set; }
        public int B3Emballage { get; set; }
        public int B35Emballage { get; set; }





    }
}