﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelStatistiqueRetourVrac
    {


        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }

        public decimal TonnageSomagaz { get; set; }
        public decimal TonnagePartenaires { get; set; }


    }
}