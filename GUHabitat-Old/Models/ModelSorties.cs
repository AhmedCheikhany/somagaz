﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelSorties
    {


        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }
        public DateTime filtreDate { get; set; }

        public int B12TotalSortie { get; set; }
        public int B9TotalSortie { get; set; }
        public int B6RTotalSortie { get; set; }
        public int B6VTotalSortie { get; set; }
        public int B3TotalSortie { get; set; }
        public int B35TotalSortie { get; set; }
        


        public int B12TotalSortieVente { get; set; }
        public int B9TotalSortieVente { get; set; }
        public int B6RTotalSortieVente { get; set; }
        public int B6VTotalSortieVente { get; set; }
        public int B3TotalSortieVente { get; set; }
        public int B35TotalSortieVente { get; set; }


        public int B12TotalSortieVenteDirecte { get; set; }
        public int B9TotalSortieVenteDirecte { get; set; }
        public int B6RTotalSortieVenteDirecte { get; set; }
        public int B6VTotalSortieVenteDirecte { get; set; }
        public int B3TotalSortieVenteDirecte { get; set; }
        public int B35TotalSortieVenteDirecte { get; set; }


        public int B12TotalSortieConsignation { get; set; }
        public int B9TotalSortieConsignation { get; set; }
        public int B6RTotalSortieConsignation { get; set; }
        public int B6VTotalSortieConsignation { get; set; }
        public int B3TotalSortieConsignation { get; set; }
        public int B35TotalSortieConsignation { get; set; }
        public int PresentoirTotalSortieConsignation { get; set; }

        public int B12TotalSortieDotation { get; set; }
        public int B9TotalSortieDotation { get; set; }
        public int B6RTotalSortieDotation { get; set; }
        public int B6VTotalSortieDotation { get; set; }
        public int B3TotalSortieDotation { get; set; }
        public int B35TotalSortieDotation { get; set; }

        public int B12TotalSortieAredp { get; set; }
        public int B9TotalSortieAredp { get; set; }
        public int B6RTotalSortieAredp { get; set; }
        public int B6VTotalSortieAredp { get; set; }
        public int B3TotalSortieAredp { get; set; }
        public int B35TotalSortieAredp { get; set; }


    }
}