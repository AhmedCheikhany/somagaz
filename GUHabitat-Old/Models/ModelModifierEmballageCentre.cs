﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModifierEmballageCentre
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int idTypeMouvement { get; set; }
        public string LibelleMouvement { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int idDestinationSource { get; set; }
        public string LibelleDestinationSource { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int idEtatEmballage { get; set; }
        public string LibelleEtatEmballage { get; set; }


        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int B12 { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int B9 { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int B6R { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int B6V { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int B3 { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int B35 { get; set; }

        public string Commentaire { get; set; }

        public int etat { get; set; }


        public List<CentreDto> ListeCentre { get; set; }
        public List<TypeMouvementEmballageDto> ListeMouvement { get; set; }
        public List<SourceDestinationEmballageDto> ListeSourceDestination { get; set; }
        public List<EtatEmballageDto> ListeEtatEmballages { get; set; }





    }
}