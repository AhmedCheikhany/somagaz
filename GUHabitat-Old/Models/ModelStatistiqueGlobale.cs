﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelStatistiqueGlobale
    {

        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }


        public decimal TotalProductionGlobale { get; set; }
        public decimal TotalFacturationGlobale { get; set; }
        public decimal TotalStockGlobale { get; set; }
        public decimal TotalStockGloalActuel { get; set; }
        public decimal TotalStockGloalTheorique { get; set; }
        public decimal TotalStockGloalFin { get; set; }

        public decimal GainPerteGlobale { get; set; }


        public decimal TotalRetourLivrer { get; set; }


        public decimal TotalRetour { get; set; }

        public decimal TotalRetourSomagaz { get; set; }

        public decimal TotalAutreRetour { get; set; }

        public decimal TotalRetourPartenaire { get; set; }

        public decimal TotalRetourCollaborateurs { get; set; }

        public decimal TotalStock { get; set; }

        public decimal TotalProductions { get; set; }

        public decimal TotalFacturations { get; set; }


        public decimal TonnageSomagaz { get; set; }
        public decimal TonnagePrivee { get; set; }
        public decimal TonnagePartenaire { get; set; }
        public decimal AutresVrac { get; set; }


        public decimal TonnageCollaborateurEnvoie { get; set; }
        public decimal TonnageCollaborateurReception { get; set; }

        public decimal TonnageCollaborateurEnvoieInventaire { get; set; }
        public decimal TonnageCollaborateurReceptionInventaire { get; set; }

        public decimal TonnageCollaborateurActuel { get; set; }

        public decimal TonnageCollaborateurEnvoieCiterne { get; set; }
        public decimal TonnageCollaborateurReceptionCiterne { get; set; }

        public decimal TonnageCollaborateurEnvoiePipes { get; set; }
        public decimal TonnageCollaborateurReceptionPipes { get; set; }


        public decimal TotalProduction { get; set; }
        public decimal TotalFacturation { get; set; }

        public decimal TotalConditionneeProduction { get; set; }
        public decimal TotalConditionneeCommerciale { get; set; }



        public decimal StockNKTT { get; set; }
        public decimal StockTheorieNKTT { get; set; }
        public decimal StockFinNKTT { get; set; }
        public decimal StockActuelNktt { get; set; }
        public decimal TonnageAvoirNktt { get; set; }
        public decimal Depotage { get; set; }

        public decimal GainPerteNktt { get; set; }



        public int B12Dotation { get; set; }

        public int B9Dotation { get; set; }

        public int B6RDotation { get; set; }

        public int B6VDotation { get; set; }

        public int B3Dotation { get; set; }

        public int B35Dotation { get; set; }


        public int B12Consignation { get; set; }

        public int B9Consignation { get; set; }

        public int B6RConsignation { get; set; }

        public int B6VConsignation { get; set; }

        public int B3Consignation { get; set; }

        public int B35Consignation { get; set; }




        public int B12Avoir { get; set; }

        public int B9Avoir { get; set; }

        public int B6RAvoir { get; set; }

        public int B6VAvoir { get; set; }

        public int B3Avoir { get; set; }

        public int B35Avoir { get; set; }



        public int B12Afact { get; set; }

        public int B9Afact { get; set; }

        public int B6RAfact { get; set; }

        public int B6VAfact { get; set; }

        public int B3Afact { get; set; }

        public int B35Afact { get; set; }



        public int B12Production { get; set; }

        public int B9Production { get; set; }

        public int B6RProduction { get; set; }

        public int B6VProduction { get; set; }

        public int B3Production { get; set; }

        public int B35Production { get; set; }




        public int B12Emballage{ get; set; }
        public int B9Emballage{ get; set; }
        public int B6REmballage { get; set; }
        public int B6VEmballage{ get; set; }
        public int B3Emballage { get; set; }
        public int B35Emballage { get; set; }





        public decimal TotalFacturationInterieur { get; set; }

        public decimal TotalProductionInterieur { get; set; }



        public decimal StockInterieur { get; set; }
        public decimal StockTheorieInterieur { get; set; }
        public decimal StockFinInterieur { get; set; }
        public decimal StockActuelInterieur { get; set; }
        public decimal GainPerteInterieur { get; set; }


        public int B12VenteInterieur { get; set; }

        public int B9VenteInterieur { get; set; }

        public int B6RVenteInterieur { get; set; }

        public int B6VVenteInterieur { get; set; }

        public int B3VenteInterieur { get; set; }

        public int B35VenteInterieur { get; set; }




        public int B12AfactInterieur { get; set; }

        public int B9AfactInterieur { get; set; }

        public int B6RAfactInterieur { get; set; }

        public int B6VAfactInterieur { get; set; }

        public int B3AfactInterieur { get; set; }

        public int B35AfactInterieur { get; set; }



        public int B12ProductionInterieur { get; set; }

        public int B9ProductionInterieur { get; set; }

        public int B6RProductionInterieur { get; set; }

        public int B6VProductionInterieur { get; set; }

        public int B3ProductionInterieur { get; set; }

        public int B35ProductionInterieur { get; set; }







        public int B12DotationInterieur { get; set; }

        public int B9DotationInterieur { get; set; }

        public int B6RDotationInterieur { get; set; }

        public int B6VDotationInterieur { get; set; }

        public int B3DotationInterieur { get; set; }

        public int B35DotationInterieur { get; set; }




        public int B12ConsignationInterieur { get; set; }

        public int B9ConsignationInterieur { get; set; }

        public int B6RConsignationInterieur { get; set; }

        public int B6VConsignationInterieur { get; set; }

        public int B3ConsignationInterieur { get; set; }

        public int B35ConsignationInterieur { get; set; }



        public int B12EmballageInterieur { get; set; }
        public int B9EmballageInterieur { get; set; }
        public int B6REmballageInterieur { get; set; }
        public int B6VEmballageInterieur { get; set; }
        public int B3EmballageInterieur { get; set; }
        public int B35EmballageInterieur { get; set; }





    }
}