﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelAjouterAredp
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int CodeClient { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string NomClient { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string NumeroFacture { get; set; }

        public int B12Afact { get; set; }
        public int B9Afact { get; set; }
        public int B6RAfact { get; set; }
        public int B6VAfact { get; set; }
        public int B3Afact { get; set; }
        public int B35Afact { get; set; }

        public int B12Nonfact { get; set; }
        public int B9Nonfact { get; set; }
        public int B6RNonfact { get; set; }
        public int B6VNonfact { get; set; }
        public int B3Nonfact { get; set; }
        public int B35Nonfact { get; set; }

        public string Commentaire { get; set; }
        public int etat { get; set; }

        //public int idProduit { get; set; }
        //public decimal poidsLu { get; set; }
        //public decimal Tare { get; set; }

        public List<AredpDetailsDto> ListeAredpDetails { get; set; }
        //public List<ProduitDto> ListeProduit { get; set; }


    }
}