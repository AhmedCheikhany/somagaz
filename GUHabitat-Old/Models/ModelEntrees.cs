﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelEntrees
    {

        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }
        public DateTime filtreDate { get; set; }

        public int B12TotalEntree { get; set; }
        public int B9TotalEntree { get; set; }
        public int B6RTotalEntree { get; set; }
        public int B6VTotalEntree { get; set; }
        public int B3TotalEntree { get; set; }
        public int B35TotalEntree { get; set; }


        public int B12TotalEntreeVente { get; set; }
        public int B9TotalEntreeVente { get; set; }
        public int B6RTotalEntreeVente { get; set; }
        public int B6VTotalEntreeVente { get; set; }
        public int B3TotalEntreeVente { get; set; }
        public int B35TotalEntreeVente { get; set; }


        public int B12TotalEntreeVenteDirecte { get; set; }
        public int B9TotalEntreeVenteDirecte { get; set; }
        public int B6RTotalEntreeVenteDirecte { get; set; }
        public int B6VTotalEntreeVenteDirecte { get; set; }
        public int B3TotalEntreeVenteDirecte { get; set; }
        public int B35TotalEntreeVenteDirecte { get; set; }



        public int B12TotalEntreeDotation { get; set; }
        public int B9TotalEntreeDotation { get; set; }
        public int B6RTotalEntreeDotation { get; set; }
        public int B6VTotalEntreeDotation { get; set; }
        public int B3TotalEntreeDotation { get; set; }
        public int B35TotalEntreeDotation { get; set; }

        public int B12TotalEntreeAredp { get; set; }
        public int B9TotalEntreeAredp { get; set; }
        public int B6RTotalEntreeAredp { get; set; }
        public int B6VTotalEntreeAredp { get; set; }
        public int B3TotalEntreeAredp { get; set; }
        public int B35TotalEntreeAredp { get; set; }



        public int B12Client { get; set; }

        public int B9Client { get; set; }

        public int B6RClient { get; set; }

        public int B6VClient { get; set; }

        public int B3Client { get; set; }

        public int B35Client { get; set; }





        public int B12VideRec { get; set; }

        public int B9VideRec { get; set; }

        public int B6RVideRec { get; set; }

        public int B6VVideRec { get; set; }

        public int B3VideRec { get; set; }

        public int B35VideRec { get; set; }


        public int B12VideDef { get; set; }

        public int B9VideDef { get; set; }

        public int B6RVideDef { get; set; }

        public int B6VVideDef { get; set; }

        public int B3VideDef { get; set; }

        public int B35VideDef { get; set; }


        public int B12PleinRemp { get; set; }

        public int B9PleinRemp { get; set; }

        public int B6RPleinRemp { get; set; }

        public int B6VPleinRemp { get; set; }

        public int B3PleinRemp { get; set; }

        public int B35PleinRemp { get; set; }


        public int B12PleinDef { get; set; }

        public int B9PleinDef { get; set; }

        public int B6RPleinDef { get; set; }

        public int B6VPleinDef { get; set; }

        public int B3PleinDef { get; set; }

        public int B35PleinDef { get; set; }


        public int B12DefAremp { get; set; }

        public int B9DefAremp { get; set; }

        public int B6RDefAremp { get; set; }

        public int B6VDefAremp { get; set; }

        public int B3DefAremp { get; set; }

        public int B35DefAremp { get; set; }


        public int B12DefRemp { get; set; }

        public int B9DefRemp { get; set; }

        public int B6RDefRemp { get; set; }

        public int B6VDefRemp { get; set; }

        public int B3DefRemp { get; set; }

        public int B35DefRemp { get; set; }



        public int B12AredpAfact { get; set; }

        public int B9AredpAfact { get; set; }

        public int B6RAredpAfact { get; set; }

        public int B6VAredpAfact { get; set; }

        public int B3AredpAfact { get; set; }

        public int B35AredpAfact { get; set; }


        public int B12AredpNonfact { get; set; }

        public int B9AredpNonfact { get; set; }

        public int B6RAredpNonfact { get; set; }

        public int B6VAredpNonfact { get; set; }

        public int B3AredpNonfact { get; set; }

        public int B35AredpNonfact { get; set; }


    }
}