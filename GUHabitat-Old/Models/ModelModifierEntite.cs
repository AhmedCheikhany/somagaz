﻿using GUHabitat.DTO;
using GUHabitat.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModifierEntite
    {
        public int id { get; set; }
        public string libelle { get; set; }
    }
}