﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModifierAredpDetails
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public int idAredp { get; set; }
        
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int idProduit { get; set; }
        public string NomProduit { get; set; }
        [Range(typeof(decimal), "0", "79228162514264337593543950335")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal poidsLu { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal Tare { get; set; }
        public decimal poidsManquant { get; set; }
        public decimal poidsRestant { get; set; }

        public List<ProduitDto> ListeProduit { get; set; }

    }
}