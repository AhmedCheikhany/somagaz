﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModifierFicheConsignation
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public int idTypeFicheConsignation { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int CodeClient { get; set; }  
        public string NomClient { get; set; }

        public string TypeFicheConsignation { get; set; }
        public string NumeroSerie { get; set; }

        public string VehiculeImmatricule { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12DC { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9DC { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RDC { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VDC { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3DC { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35DC { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int PresentoirDC { get; set; }


        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float B12Prix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float B9Prix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float B6RPrix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float B6VPrix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float B3Prix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float B35Prix { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float PresentoirPrix { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float TotalPrixB12 { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float TotalPrixB9 { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float TotalPrixB6R { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float TotalPrixB6V { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float TotalPrixB3 { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float TotalPrixB35 { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public float TotalPrixPresentoir { get; set; }


        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12DE { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9DE { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RDE { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VDE { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3DE { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35DE { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int PresentoirDE { get; set; }

        public string Commentaire { get; set; }
        public int etat { get; set; }

        public decimal Tonnage { get; set; }

        public List<TypeFicheConsignationDto> ListeFiches { get; set; }
    }
}