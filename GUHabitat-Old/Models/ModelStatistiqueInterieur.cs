﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelStatistiqueInterieur
    {

        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }

        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }

        public List<CentreDto> ListeCentre { get; set; }


        public decimal TotalFacturation{ get; set; }

        public decimal TotalProduction { get; set; }



        public decimal Stock{ get; set; }
        public decimal StockTheorie { get; set; }
        public decimal StockFin { get; set; }
        public decimal StockActuel { get; set; }
        public decimal Reception { get; set; }
        public decimal Retour { get; set; }


        public decimal TonnageVente { get; set; }
        public decimal TonnageVenteConsommateur { get; set; }
        public decimal TonnageVenteRevendeur { get; set; }

        public decimal GainPerte { get; set; }

        public int B12Vente { get; set; }

        public int B9Vente { get; set; }

        public int B6RVente { get; set; }

        public int B6VVente { get; set; }

        public int B3Vente { get; set; }

        public int B35Vente { get; set; }



        public int B12VenteConsomateur { get; set; }

        public int B9VenteConsomateur { get; set; }

        public int B6RVenteConsomateur { get; set; }

        public int B6VVenteConsomateur { get; set; }

        public int B3VenteConsomateur { get; set; }

        public int B35VenteConsomateur { get; set; }


        public int B12PrixConsomateur { get; set; }

        public int B9PrixConsomateur { get; set; }

        public int B6RPrixConsomateur { get; set; }

        public int B6VPrixConsomateur { get; set; }

        public int B3PrixConsomateur { get; set; }

        public int B35PrixConsomateur { get; set; }


        public int B12VenteRevendeur { get; set; }

        public int B9VenteRevendeur { get; set; }

        public int B6RVenteRevendeur { get; set; }

        public int B6VVenteRevendeur { get; set; }

        public int B3VenteRevendeur { get; set; }

        public int B35VenteRevendeur { get; set; }


        public int B12PrixRevendeur { get; set; }

        public int B9PrixRevendeur { get; set; }

        public int B6RPrixRevendeur { get; set; }

        public int B6VPrixRevendeur { get; set; }

        public int B3PrixRevendeur { get; set; }

        public int B35PrixRevendeur { get; set; }


        public int B12TotalPrixConsomateur { get; set; }

        public int B9TotalPrixConsomateur { get; set; }

        public int B6RTotalPrixConsomateur { get; set; }

        public int B6VTotalPrixConsomateur { get; set; }

        public int B3TotalPrixConsomateur { get; set; }

        public int B35TotalPrixConsomateur { get; set; }


        public int B12TotalPrixRevendeur { get; set; }

        public int B9TotalPrixRevendeur { get; set; }

        public int B6RTotalPrixRevendeur { get; set; }

        public int B6VTotalPrixRevendeur { get; set; }

        public int B3TotalPrixRevendeur { get; set; }

        public int B35TotalPrixRevendeur { get; set; }




        public int B12Afact { get; set; }

        public int B9Afact { get; set; }

        public int B6RAfact { get; set; }

        public int B6VAfact { get; set; }

        public int B3Afact { get; set; }

        public int B35Afact { get; set; }



        public int B12Production { get; set; }

        public int B9Production { get; set; }

        public int B6RProduction { get; set; }

        public int B6VProduction { get; set; }

        public int B3Production { get; set; }

        public int B35Production { get; set; }







        public int B12Dotation { get; set; }

        public int B9Dotation { get; set; }

        public int B6RDotation { get; set; }

        public int B6VDotation { get; set; }

        public int B3Dotation { get; set; }

        public int B35Dotation { get; set; }




        public int B12Consignation { get; set; }

        public int B9Consignation { get; set; }

        public int B6RConsignation { get; set; }

        public int B6VConsignation { get; set; }

        public int B3Consignation { get; set; }

        public int B35Consignation { get; set; }



        public int B12Emballage { get; set; }
        public int B9Emballage { get; set; }
        public int B6REmballage { get; set; }
        public int B6VEmballage { get; set; }
        public int B3Emballage { get; set; }
        public int B35Emballage { get; set; }



    }
}