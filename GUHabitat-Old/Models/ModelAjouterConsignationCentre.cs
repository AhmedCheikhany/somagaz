﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelAjouterConsignationCentre
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int CodeCentre { get; set; }

        public string LibelleCentre { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12 { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9 { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6R { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6V { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3 { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35 { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B12Prix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B9Prix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B6RPrix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B6VPrix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B3Prix { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B35Prix { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB12 { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB9 { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB6R { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB6V { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB3 { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB35 { get; set; }


        public int etat { get; set; }

        public string Commentaire { get; set; }

        public string libelleCentreString
        {
            get
            {
                string str = "";
                if (CodeCentre == 1)
                    str = "NOUADHIBOU";
                else if (CodeCentre == 2)
                    str = "ATAR";
                else if (CodeCentre == 3)
                    str = "ZOUERATE";
                else if (CodeCentre == 4)
                    str = "ALEG";
                else if (CodeCentre == 5)
                    str = "SEILIBABY";
                else if (CodeCentre == 6)
                    str = "KIFFA";
                else if (CodeCentre == 7)
                    str = "AIOUN";
                else if (CodeCentre == 8)
                    str = "NEMA";

                return str;
            }
        }

    }
}