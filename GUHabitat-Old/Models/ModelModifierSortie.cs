﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModifierSortie
    {

        public int id { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string Reference { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string ReferenceTypeSortie { get; set; }
       
        public string NumeroFacture { get; set; }
        public string Concerne { get; set; }
        public int Client { get; set; }
        public int Employe { get; set; }
        public int idTypeSortie { get; set; }
        public string TypeSortie { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12 { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9 { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6R { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6V { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3 { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35 { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int Presentoir { get; set; }

        public string Commentaire { get; set; }
        public int etat { get; set; }

        public List<TypeSortieDto> ListeFiches { get; set; }


    }
}