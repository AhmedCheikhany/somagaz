﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelAvoirParClient
    {

        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }

        public int CodeClient { get; set; }
        public string NomClient { get; set; }

        public int B12Avoir { get; set; }

        public int B9Avoir { get; set; }

        public int B6RAvoir { get; set; }

        public int B6VAvoir { get; set; }

        public int B3Avoir { get; set; }

        public int B35Avoir { get; set; }


        public decimal B12PoidManquantAvoir { get; set; }
        public decimal B9PoidManquantAvoir { get; set; }
        public decimal B6RPoidManquantAvoir { get; set; }
        public decimal B6VPoidManquantAvoir { get; set; }
        public decimal B3PoidManquantAvoir { get; set; }
        public decimal B35PoidManquantAvoir { get; set; }


        public decimal B12PoidRestantAvoir { get; set; }
        public decimal B9PoidRestantAvoir { get; set; }
        public decimal B6RPoidRestantAvoir { get; set; }
        public decimal B6VPoidRestantAvoir { get; set; }
        public decimal B3PoidRestantAvoir { get; set; }
        public decimal B35PoidRestantAvoir { get; set; }




        public decimal TotalPoidsManquantAvoir { get; set; }
        public decimal TotalPoidsRestantAvoir { get; set; }
        public decimal TotalTonnageAvoir { get; set; }
       

    }
}