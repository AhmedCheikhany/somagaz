﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class AjouterBeevModel
    {

        public int id { get; set; }
        [Required]
        public string Serie { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12 { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9 { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6R { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6V { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3 { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35 { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int CodeClient { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string NomClient { get; set; }
        public string Commentaire { get; set; }
        public int etat { get; set; }

    }
}