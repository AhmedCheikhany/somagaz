﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ListeBeevModel
    {

        public int? page { get; set; }
        public string filtreMotCle { get; set; }
        //public int? filtreTypeDemande { get; set; }
        //public int? filtreDecision { get; set; }
        public PagedList.IPagedList<BeevDto> ListeBeev { get; set; }
    }
}