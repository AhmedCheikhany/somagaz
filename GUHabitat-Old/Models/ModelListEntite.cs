﻿using GUHabitat.DTO;
using GUHabitat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelListEntite
    {
        public string filtreMotCle { get; set; }
        public List<EntiteDto> ListeEntites { get; set; }
    }
    
}