﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelAjouterFicheVenteBouteille
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int CodeClient { get; set; }
        public int idTypeFiche { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int matriculeEmploye { get; set; }
       
        public string NomEmploye { get; set; }
        
        public string NomClient { get; set; }
        public string VehiculeImmatricule { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int NbrePersonnes { get; set; }
        public string DotationMatricule { get; set; }
        public string TypeFiche { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12Client { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9Client { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RClient { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VClient { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3Client { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35Client { get; set; }


        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12VideRec { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9VideRec { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RVideRec { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VVideRec { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3VideRec { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35VideRec { get; set; }


        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12VideDef { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9VideDef { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RVideDef { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VVideDef { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3VideDef { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35VideDef { get; set; }


        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12PleinRemp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9PleinRemp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RPleinRemp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VPleinRemp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3PleinRemp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35PleinRemp { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12PleinDef { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9PleinDef { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RPleinDef { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VPleinDef { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3PleinDef { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35PleinDef { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12DefAremp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9DefAremp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RDefAremp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VDefAremp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3DefAremp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35DefAremp { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12DefRemp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9DefRemp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RDefRemp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VDefRemp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3DefRemp { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35DefRemp { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12Afact { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9Afact { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RAfact { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VAfact { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3Afact { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35Afact { get; set; }

        public string Commentaire { get; set; }
        public int etat { get; set; }
        public decimal Tonnage { get; set; }


        public string TypeFicheString
        {
            get
            {
                string str = "";
                if (idTypeFiche == 1)
                    str = "Fiche Vente";
                else if (idTypeFiche == 2)
                    str = "Fiche Dotation";
                else if (idTypeFiche == 3)
                    str = "Vente Directe";
                return str;
            }
        }

    }
}