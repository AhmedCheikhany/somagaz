﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelStatistiquesVrac
    {

        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }

        public decimal TonnageSomagaz { get; set; }
        public decimal TonnageCollaborateurs { get; set; }
        public decimal TonnagePrivee { get; set; }
        public decimal TonnageStar { get; set; }
        public decimal TonnagePartenaires { get; set; }
    }
}