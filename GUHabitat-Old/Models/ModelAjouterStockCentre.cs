﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelAjouterStockCentre
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public int reference { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }


        public decimal StockTheorie { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        public decimal Stock { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        public decimal StockFin { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        public decimal Depotage { get; set; }

        public decimal GainPerte { get; set; }

        public string Commentaire { get; set; }

        public int etat { get; set; }

        public string libelleCentreString
        {
            get
            {
                string str = "";
                if (CodeCentre == 1)
                    str = "NOUADHIBOU";
                else if (CodeCentre == 2)
                    str = "ATAR";
                else if (CodeCentre == 3)
                    str = "ZOUERATE";
                else if (CodeCentre == 4)
                    str = "ALEG";
                else if (CodeCentre == 5)
                    str = "SEILIBABY";
                else if (CodeCentre == 6)
                    str = "KIFFA";
                else if (CodeCentre == 7)
                    str = "AIOUN";
                else if (CodeCentre == 8)
                    str = "NEMA";
                else if (CodeCentre == 9)
                    str = "NOUAKCHOTT";
                return str;
            }
        }

    }
}