﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModifierBonChargement
    {


        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public int idTypeBon { get; set; }
        public string TypeBon { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string ImmtriculeCamion { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int NbrePersonnes { get; set; }
        public string ResponsableCamion { get; set; }
        public string ChauffeurCamion { get; set; }
        public int CodeClient { get; set; }
        public string NomClient { get; set; }
        public float Tonnage { get; set; }
        public int CodeDestination { get; set; }
        public string libelleDestination { get; set; }
        public string Demmarage { get; set; }
        public string EtatPneus { get; set; }
        public string Freinage { get; set; }
        public string Extincteur { get; set; }
        public string Observation { get; set; }
        public string Numeropermis { get; set; }
        public string NpoliceRC { get; set; }
        public string NpoliceRCGaz { get; set; }
        public string AutoriserEntree { get; set; }
        public string Facturer { get; set; }
        public string QuantiteFactQuantiteChargee { get; set; }
        public string AutoriserSortie { get; set; }
        public float PoidDebut { get; set; }
        public string NumeroPontBascule { get; set; }
        public string ClapetVanne { get; set; }
        public string CalageVehicule { get; set; }
        public string MiseTerre { get; set; }
        public string ObservationExploitation { get; set; }
        public string AutoriserCharger { get; set; }

        public DateTime heureChargement { get; set; }
        public DateTime heureFinChargement { get; set; }
        public float PoidsApresChargement { get; set; }
        public float TonnageCharger { get; set; }
        public string NumeroFacture { get; set; }
        public string TempsEstime { get; set; }
        public int etat { get; set; }


        public List<TypeBonChargementDto> ListeTypeBons { get; set; }


    }
}