﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelAjouterFicheSituation
    {
        public int id { get; set; }
        public int Reference { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal StockNKTT { get; set; }

        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal Depotage { get; set; }

        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal StockInterieur { get; set; }
        
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalStock { get; set; }
        
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal VenteConditionneeNKTT { get; set; }
       
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal VenteCentresInterieur { get; set; }
       
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal VenteVrac { get; set; }
        
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalVente { get; set; }
        
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TransfertCentres { get; set; }
        
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TransfertPartenaires { get; set; }
        
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalTransfert { get; set; }

       
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal GainPerte { get; set; }
        
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal RecetteNKTT { get; set; }
     
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal RecetteInterieur { get; set; }
     
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalRecette { get; set; }
       
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal Versement { get; set; }

        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal VersementInterieur { get; set; }

        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalVersement { get; set; }


        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal AutreReglement { get; set; }


        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal PretsAceJour { get; set; }

        public string Commentaire { get; set; }
        public int etat { get; set; }
        public string etatString { get; set; }
    }
}