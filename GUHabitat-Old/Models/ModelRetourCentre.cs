﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelRetourCentre
    {

        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }

        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }


        public decimal Tonnage { get; set; }


    }
}