﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelImprimerDetailsAredp
    {

        public string Reference { get; set; }
        public string NomClient { get; set; }
        public DateTime Date { get; set; }
        public int CodeClient { get; set; }
        public string NumeroFacture { get; set; }

        public int idAredp { get; set; }
        public int idProduit { get; set; }
        public string NomProduit { get; set; }
        public decimal poidsLu { get; set; }
        public decimal Tare { get; set; }
        public decimal poidsManquant { get; set; }
        public decimal poidsRestant { get; set; }

    }
}