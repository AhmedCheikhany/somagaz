﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModifierVenteCentre
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int CodeCentre { get; set; }
       
        public string LibelleCentre { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12Revendeur { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9Revendeur { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RRevendeur { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VRevendeur { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3Revendeur { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35Revendeur { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B12PrixRevendeur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B9PrixRevendeur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B6RPrixRevendeur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B6VPrixRevendeur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B3PrixRevendeur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B35PrixRevendeur { get; set; }


        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B12Consommateur { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B9Consommateur { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6RConsommateur { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B6VConsommateur { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B3Consommateur { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre entier.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int B35Consommateur { get; set; }


        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B12PrixConsommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B9PrixConsommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B6RPrixConsommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B6VPrixConsommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B3PrixConsommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal B35PrixConsommateur { get; set; }


        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB12Consommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB9Consommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB6RConsommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB6VConsommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB3Consommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB35Consommateur { get; set; }


        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB12Revendeur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB9Revendeur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB6RRevendeur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB6VRevendeur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB3Revendeur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalPrixB35Revendeur { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalMontantConsommateur { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "s'il vous plaît entrez un nombre décimal.")]
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal TotalMontantRevendeur { get; set; }

        public string Commentaire { get; set; }


        public List<CentreDto> ListeCentre { get; set; }

        public int etat { get; set; }

    }
}