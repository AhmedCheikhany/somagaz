﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelSituationCentreStatistique
    {

        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }
        public DateTime filtreDate { get; set; }

        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }


        public decimal TotalCommerciale { get; set; }


        public int B12Dotation { get; set; }

        public int B9Dotation { get; set; }

        public int B6RDotation { get; set; }

        public int B6VDotation { get; set; }

        public int B3Dotation { get; set; }

        public int B35Dotation { get; set; }


        public int B12Consignation { get; set; }

        public int B9Consignation { get; set; }

        public int B6RConsignation { get; set; }

        public int B6VConsignation { get; set; }

        public int B3Consignation { get; set; }

        public int B35Consignation { get; set; }




        public int B12Avoir { get; set; }

        public int B9Avoir { get; set; }

        public int B6RAvoir { get; set; }

        public int B6VAvoir { get; set; }

        public int B3Avoir { get; set; }

        public int B35Avoir { get; set; }



        public int B12Afact { get; set; }

        public int B9Afact { get; set; }

        public int B6RAfact { get; set; }

        public int B6VAfact { get; set; }

        public int B3Afact { get; set; }

        public int B35Afact { get; set; }



        public int B12Production { get; set; }

        public int B9Production { get; set; }

        public int B6RProduction { get; set; }

        public int B6VProduction { get; set; }

        public int B3Production { get; set; }

        public int B35Production { get; set; }






        public decimal TotalRetourLivrer { get; set; }

        public decimal TotalRetour { get; set; }

        public decimal TotalStock { get; set; }

        public decimal TotalProductions { get; set; }

        public decimal TotalFacturations { get; set; }


        public decimal TonnageSomagaz { get; set; }
        public decimal TonnagePrivee { get; set; }
        public decimal TonnagePartenaire { get; set; }
        public decimal AutresVrac { get; set; }



        public decimal TotalProduction { get; set; }
        public decimal TotalFacturation { get; set; }

        public decimal TotalConditionneeProduction { get; set; }
        public decimal TotalConditionneeCommerciale { get; set; }





        public decimal TotalProductionNktt { get; set; }
        public decimal TotalFacturationNktt { get; set; }

        public decimal TotalConditionneeProductionNktt { get; set; }
        public decimal TotalConditionneeCommercialeNktt { get; set; }

        public decimal TotalProductionNOUADHIBOU { get; set; }
        public decimal TotalFacturationNOUADHIBOU { get; set; }

        public decimal TotalProductionATAR { get; set; }
        public decimal TotalFacturationATAR { get; set; }

        public decimal TotalProductionZOUERATE { get; set; }
        public decimal TotalFacturationZOUERATE { get; set; }


        public decimal TotalProductionALEG { get; set; }
        public decimal TotalFacturationALEG { get; set; }


        public decimal TotalProductionSEILIBABY { get; set; }
        public decimal TotalFacturationSEILIBABY { get; set; }


        public decimal TotalProductionKIFFA { get; set; }
        public decimal TotalFacturationKIFFA { get; set; }


        public decimal TotalProductionAIOUN { get; set; }
        public decimal TotalFacturationAIOUN { get; set; }


        public decimal TotalProductionNEMA { get; set; }
        public decimal TotalFacturationNEMA { get; set; }

        



        public decimal StockNktt { get; set; }
        public decimal DepotageNktt { get; set; }

        public decimal StockNOUADHIBOU { get; set; }
        public decimal StockATAR { get; set; }
        public decimal StockZOUERATE { get; set; }
        public decimal StockALEG { get; set; }
        public decimal StockSEILIBABY { get; set; }
        public decimal StockKIFFA { get; set; }
        public decimal StockAIOUN { get; set; }
        public decimal StockNEMA { get; set; }




        public decimal MoyenneJournaliere { get; set; }
        public decimal MinJournaliere { get; set; }
        public decimal MaxJournaliere { get; set; }
        public decimal VenteAceJour { get; set; }




        public int B12TotalProductionNouakchott { get; set; }
        public int B9TotalProductionNouakchott { get; set; }
        public int B6RTotalProductionNouakchott { get; set; }
        public int B6VTotalProductionNouakchott { get; set; }
        public int B3TotalProductionNouakchott { get; set; }
        public int B35TotalProductionNouakchott { get; set; }

        public int B12TotalProductionNOUADHIBOU { get; set; }
        public int B9TotalProductionNOUADHIBOU { get; set; }
        public int B6RTotalProductionNOUADHIBOU { get; set; }
        public int B6VTotalProductionNOUADHIBOU { get; set; }
        public int B3TotalProductionNOUADHIBOU { get; set; }
        public int B35TotalProductionNOUADHIBOU { get; set; }

        public int B12TotalProductionATAR { get; set; }
        public int B9TotalProductionATAR { get; set; }
        public int B6RTotalProductionATAR { get; set; }
        public int B6VTotalProductionATAR { get; set; }
        public int B3TotalProductionATAR { get; set; }
        public int B35TotalProductionATAR { get; set; }

        public int B12TotalProductionZOUERATE { get; set; }
        public int B9TotalProductionZOUERATE { get; set; }
        public int B6RTotalProductionZOUERATE { get; set; }
        public int B6VTotalProductionZOUERATE { get; set; }
        public int B3TotalProductionZOUERATE { get; set; }
        public int B35TotalProductionZOUERATE { get; set; }



        public int B12TotalProductionALEG { get; set; }
        public int B9TotalProductionALEG { get; set; }
        public int B6RTotalProductionALEG { get; set; }
        public int B6VTotalProductionALEG { get; set; }
        public int B3TotalProductionALEG { get; set; }
        public int B35TotalProductionALEG { get; set; }


        public int B12TotalProductionSEILIBABY { get; set; }
        public int B9TotalProductionSEILIBABY { get; set; }
        public int B6RTotalProductionSEILIBABY { get; set; }
        public int B6VTotalProductionSEILIBABY { get; set; }
        public int B3TotalProductionSEILIBABY { get; set; }
        public int B35TotalProductionSEILIBABY { get; set; }



        public int B12TotalProductionKIFFA { get; set; }
        public int B9TotalProductionKIFFA { get; set; }
        public int B6RTotalProductionKIFFA { get; set; }
        public int B6VTotalProductionKIFFA { get; set; }
        public int B3TotalProductionKIFFA { get; set; }
        public int B35TotalProductionKIFFA { get; set; }



        public int B12TotalProductionAIOUN { get; set; }
        public int B9TotalProductionAIOUN { get; set; }
        public int B6RTotalProductionAIOUN { get; set; }
        public int B6VTotalProductionAIOUN { get; set; }
        public int B3TotalProductionAIOUN { get; set; }
        public int B35TotalProductionAIOUN { get; set; }


        public int B12TotalProductionNEMA { get; set; }
        public int B9TotalProductionNEMA { get; set; }
        public int B6RTotalProductionNEMA { get; set; }
        public int B6VTotalProductionNEMA { get; set; }
        public int B3TotalProductionNEMA { get; set; }
        public int B35TotalProductionNEMA { get; set; }








        public int B12TotalFacturationNouakchott { get; set; }
        public int B9TotalFacturationNouakchott { get; set; }
        public int B6RTotalFacturationNouakchott { get; set; }
        public int B6VTotalFacturationNouakchott { get; set; }
        public int B3TotalFacturationNouakchott { get; set; }
        public int B35TotalFacturationNouakchott { get; set; }

        public int B12TotalFacturationNOUADHIBOU { get; set; }
        public int B9TotalFacturationNOUADHIBOU { get; set; }
        public int B6RTotalFacturationNOUADHIBOU { get; set; }
        public int B6VTotalFacturationNOUADHIBOU { get; set; }
        public int B3TotalFacturationNOUADHIBOU { get; set; }
        public int B35TotalFacturationNOUADHIBOU { get; set; }

        public int B12TotalFacturationATAR { get; set; }
        public int B9TotalFacturationATAR { get; set; }
        public int B6RTotalFacturationATAR { get; set; }
        public int B6VTotalFacturationATAR { get; set; }
        public int B3TotalFacturationATAR { get; set; }
        public int B35TotalFacturationATAR { get; set; }

        public int B12TotalFacturationZOUERATE { get; set; }
        public int B9TotalFacturationZOUERATE { get; set; }
        public int B6RTotalFacturationZOUERATE { get; set; }
        public int B6VTotalFacturationZOUERATE { get; set; }
        public int B3TotalFacturationZOUERATE { get; set; }
        public int B35TotalFacturationZOUERATE { get; set; }



        public int B12TotalFacturationALEG { get; set; }
        public int B9TotalFacturationALEG { get; set; }
        public int B6RTotalFacturationALEG { get; set; }
        public int B6VTotalFacturationALEG { get; set; }
        public int B3TotalFacturationALEG { get; set; }
        public int B35TotalFacturationALEG { get; set; }


        public int B12TotalFacturationSEILIBABY { get; set; }
        public int B9TotalFacturationSEILIBABY { get; set; }
        public int B6RTotalFacturationSEILIBABY { get; set; }
        public int B6VTotalFacturationSEILIBABY { get; set; }
        public int B3TotalFacturationSEILIBABY { get; set; }
        public int B35TotalFacturationSEILIBABY { get; set; }



        public int B12TotalFacturationKIFFA { get; set; }
        public int B9TotalFacturationKIFFA { get; set; }
        public int B6RTotalFacturationKIFFA { get; set; }
        public int B6VTotalFacturationKIFFA { get; set; }
        public int B3TotalFacturationKIFFA { get; set; }
        public int B35TotalFacturationKIFFA { get; set; }



        public int B12TotalFacturationAIOUN { get; set; }
        public int B9TotalFacturationAIOUN { get; set; }
        public int B6RTotalFacturationAIOUN { get; set; }
        public int B6VTotalFacturationAIOUN { get; set; }
        public int B3TotalFacturationAIOUN { get; set; }
        public int B35TotalFacturationAIOUN { get; set; }


        public int B12TotalFacturationNEMA { get; set; }
        public int B9TotalFacturationNEMA { get; set; }
        public int B6RTotalFacturationNEMA { get; set; }
        public int B6VTotalFacturationNEMA { get; set; }
        public int B3TotalFacturationNEMA { get; set; }
        public int B35TotalFacturationNEMA { get; set; }




        public int B12VenteNOUADHIBOU { get; set; }
        public int B9VenteNOUADHIBOU { get; set; }
        public int B6RVenteNOUADHIBOU { get; set; }
        public int B6VVenteNOUADHIBOU { get; set; }
        public int B3VenteNOUADHIBOU { get; set; }
        public int B35VenteNOUADHIBOU { get; set; }




        public int B12EmballageNOUADHIBOU { get; set; }
        public int B9EmballageNOUADHIBOU { get; set; }
        public int B6REmballageNOUADHIBOU { get; set; }
        public int B6VEmballageNOUADHIBOU { get; set; }
        public int B3EmballageNOUADHIBOU { get; set; }
        public int B35EmballageNOUADHIBOU { get; set; }



        public int B12DotationNOUADHIBOU { get; set; }
        public int B9DotationNOUADHIBOU { get; set; }
        public int B6RDotationNOUADHIBOU { get; set; }
        public int B6VDotationNOUADHIBOU { get; set; }
        public int B3DotationNOUADHIBOU { get; set; }
        public int B35DotationNOUADHIBOU { get; set; }

        public int B12ConsignationNOUADHIBOU { get; set; }
        public int B9ConsignationNOUADHIBOU { get; set; }
        public int B6RConsignationNOUADHIBOU { get; set; }
        public int B6VConsignationNOUADHIBOU { get; set; }
        public int B3ConsignationNOUADHIBOU { get; set; }
        public int B35ConsignationNOUADHIBOU { get; set; }




        public int B12VenteNouakchott { get; set; }
        public int B9VenteNouakchott { get; set; }
        public int B6RVenteNouakchott { get; set; }
        public int B6VVenteNouakchott { get; set; }
        public int B3VenteNouakchott { get; set; }
        public int B35VenteNouakchott { get; set; }



        public int B12AvoirNouakchott { get; set; }
        public int B9AvoirNouakchott { get; set; }
        public int B6RAvoirNouakchott { get; set; }
        public int B6VAvoirNouakchott { get; set; }
        public int B3AvoirNouakchott { get; set; }
        public int B35AvoirNouakchott { get; set; }




        public int B12EmballageNouakchott { get; set; }
        public int B9EmballageNouakchott { get; set; }
        public int B6REmballageNouakchott { get; set; }
        public int B6VEmballageNouakchott { get; set; }
        public int B3EmballageNouakchott { get; set; }
        public int B35EmballageNouakchott { get; set; }



        public int B12DotationNouakchott { get; set; }
        public int B9DotationNouakchott { get; set; }
        public int B6RDotationNouakchott { get; set; }
        public int B6VDotationNouakchott { get; set; }
        public int B3DotationNouakchott { get; set; }
        public int B35DotationNouakchott { get; set; }

        public int B12ConsignationNouakchott { get; set; }
        public int B9ConsignationNouakchott { get; set; }
        public int B6RConsignationNouakchott { get; set; }
        public int B6VConsignationNouakchott { get; set; }
        public int B3ConsignationNouakchott { get; set; }
        public int B35ConsignationNouakchott { get; set; }

        public int B12VenteATAR { get; set; }
        public int B9VenteATAR { get; set; }
        public int B6RVenteATAR { get; set; }
        public int B6VVenteATAR { get; set; }
        public int B3VenteATAR { get; set; }
        public int B35VenteATAR { get; set; }




        public int B12EmballageATAR { get; set; }
        public int B9EmballageATAR { get; set; }
        public int B6REmballageATAR { get; set; }
        public int B6VEmballageATAR { get; set; }
        public int B3EmballageATAR { get; set; }
        public int B35EmballageATAR { get; set; }



        public int B12DotationATAR { get; set; }
        public int B9DotationATAR { get; set; }
        public int B6RDotationATAR { get; set; }
        public int B6VDotationATAR { get; set; }
        public int B3DotationATAR { get; set; }
        public int B35DotationATAR { get; set; }

        public int B12ConsignationATAR { get; set; }
        public int B9ConsignationATAR { get; set; }
        public int B6RConsignationATAR { get; set; }
        public int B6VConsignationATAR { get; set; }
        public int B3ConsignationATAR { get; set; }
        public int B35ConsignationATAR { get; set; }



        public int B12VenteZOUERATE { get; set; }
        public int B9VenteZOUERATE { get; set; }
        public int B6RVenteZOUERATE { get; set; }
        public int B6VVenteZOUERATE { get; set; }
        public int B3VenteZOUERATE { get; set; }
        public int B35VenteZOUERATE { get; set; }




        public int B12EmballageZOUERATE { get; set; }
        public int B9EmballageZOUERATE { get; set; }
        public int B6REmballageZOUERATE { get; set; }
        public int B6VEmballageZOUERATE { get; set; }
        public int B3EmballageZOUERATE { get; set; }
        public int B35EmballageZOUERATE { get; set; }



        public int B12DotationZOUERATE { get; set; }
        public int B9DotationZOUERATE { get; set; }
        public int B6RDotationZOUERATE { get; set; }
        public int B6VDotationZOUERATE { get; set; }
        public int B3DotationZOUERATE { get; set; }
        public int B35DotationZOUERATE { get; set; }

        public int B12ConsignationZOUERATE { get; set; }
        public int B9ConsignationZOUERATE { get; set; }
        public int B6RConsignationZOUERATE { get; set; }
        public int B6VConsignationZOUERATE { get; set; }
        public int B3ConsignationZOUERATE { get; set; }
        public int B35ConsignationZOUERATE { get; set; }


        public int B12VenteALEG { get; set; }
        public int B9VenteALEG { get; set; }
        public int B6RVenteALEG { get; set; }
        public int B6VVenteALEG { get; set; }
        public int B3VenteALEG { get; set; }
        public int B35VenteALEG { get; set; }




        public int B12EmballageALEG { get; set; }
        public int B9EmballageALEG { get; set; }
        public int B6REmballageALEG { get; set; }
        public int B6VEmballageALEG { get; set; }
        public int B3EmballageALEG { get; set; }
        public int B35EmballageALEG { get; set; }



        public int B12DotationALEG { get; set; }
        public int B9DotationALEG { get; set; }
        public int B6RDotationALEG { get; set; }
        public int B6VDotationALEG { get; set; }
        public int B3DotationALEG { get; set; }
        public int B35DotationALEG { get; set; }

        public int B12ConsignationALEG { get; set; }
        public int B9ConsignationALEG { get; set; }
        public int B6RConsignationALEG { get; set; }
        public int B6VConsignationALEG { get; set; }
        public int B3ConsignationALEG { get; set; }
        public int B35ConsignationALEG { get; set; }


        public int B12VenteSEILIBABY { get; set; }
        public int B9VenteSEILIBABY { get; set; }
        public int B6RVenteSEILIBABY { get; set; }
        public int B6VVenteSEILIBABY { get; set; }
        public int B3VenteSEILIBABY { get; set; }
        public int B35VenteSEILIBABY { get; set; }




        public int B12EmballageSEILIBABY { get; set; }
        public int B9EmballageSEILIBABY { get; set; }
        public int B6REmballageSEILIBABY { get; set; }
        public int B6VEmballageSEILIBABY { get; set; }
        public int B3EmballageSEILIBABY { get; set; }
        public int B35EmballageSEILIBABY { get; set; }



        public int B12DotationSEILIBABY { get; set; }
        public int B9DotationSEILIBABY { get; set; }
        public int B6RDotationSEILIBABY { get; set; }
        public int B6VDotationSEILIBABY { get; set; }
        public int B3DotationSEILIBABY { get; set; }
        public int B35DotationSEILIBABY { get; set; }

        public int B12ConsignationSEILIBABY { get; set; }
        public int B9ConsignationSEILIBABY { get; set; }
        public int B6RConsignationSEILIBABY { get; set; }
        public int B6VConsignationSEILIBABY { get; set; }
        public int B3ConsignationSEILIBABY { get; set; }
        public int B35ConsignationSEILIBABY { get; set; }


        public int B12VenteKIFFA { get; set; }
        public int B9VenteKIFFA { get; set; }
        public int B6RVenteKIFFA { get; set; }
        public int B6VVenteKIFFA { get; set; }
        public int B3VenteKIFFA { get; set; }
        public int B35VenteKIFFA { get; set; }




        public int B12EmballageKIFFA { get; set; }
        public int B9EmballageKIFFA { get; set; }
        public int B6REmballageKIFFA { get; set; }
        public int B6VEmballageKIFFA { get; set; }
        public int B3EmballageKIFFA { get; set; }
        public int B35EmballageKIFFA { get; set; }



        public int B12DotationKIFFA { get; set; }
        public int B9DotationKIFFA { get; set; }
        public int B6RDotationKIFFA { get; set; }
        public int B6VDotationKIFFA { get; set; }
        public int B3DotationKIFFA { get; set; }
        public int B35DotationKIFFA { get; set; }

        public int B12ConsignationKIFFA { get; set; }
        public int B9ConsignationKIFFA { get; set; }
        public int B6RConsignationKIFFA { get; set; }
        public int B6VConsignationKIFFA { get; set; }
        public int B3ConsignationKIFFA { get; set; }
        public int B35ConsignationKIFFA { get; set; }




        public int B12VenteAIOUN { get; set; }
        public int B9VenteAIOUN { get; set; }
        public int B6RVenteAIOUN { get; set; }
        public int B6VVenteAIOUN { get; set; }
        public int B3VenteAIOUN { get; set; }
        public int B35VenteAIOUN { get; set; }




        public int B12EmballageAIOUN { get; set; }
        public int B9EmballageAIOUN { get; set; }
        public int B6REmballageAIOUN { get; set; }
        public int B6VEmballageAIOUN { get; set; }
        public int B3EmballageAIOUN { get; set; }
        public int B35EmballageAIOUN { get; set; }



        public int B12DotationAIOUN { get; set; }
        public int B9DotationAIOUN { get; set; }
        public int B6RDotationAIOUN { get; set; }
        public int B6VDotationAIOUN { get; set; }
        public int B3DotationAIOUN { get; set; }
        public int B35DotationAIOUN { get; set; }

        public int B12ConsignationAIOUN { get; set; }
        public int B9ConsignationAIOUN { get; set; }
        public int B6RConsignationAIOUN { get; set; }
        public int B6VConsignationAIOUN { get; set; }
        public int B3ConsignationAIOUN { get; set; }
        public int B35ConsignationAIOUN { get; set; }


        public int B12VenteNEMA { get; set; }
        public int B9VenteNEMA { get; set; }
        public int B6RVenteNEMA { get; set; }
        public int B6VVenteNEMA { get; set; }
        public int B3VenteNEMA { get; set; }
        public int B35VenteNEMA { get; set; }




        public int B12EmballageNEMA { get; set; }
        public int B9EmballageNEMA { get; set; }
        public int B6REmballageNEMA { get; set; }
        public int B6VEmballageNEMA { get; set; }
        public int B3EmballageNEMA { get; set; }
        public int B35EmballageNEMA { get; set; }



        public int B12DotationNEMA { get; set; }
        public int B9DotationNEMA { get; set; }
        public int B6RDotationNEMA { get; set; }
        public int B6VDotationNEMA { get; set; }
        public int B3DotationNEMA { get; set; }
        public int B35DotationNEMA { get; set; }

        public int B12ConsignationNEMA { get; set; }
        public int B9ConsignationNEMA { get; set; }
        public int B6RConsignationNEMA { get; set; }
        public int B6VConsignationNEMA { get; set; }
        public int B3ConsignationNEMA { get; set; }
        public int B35ConsignationNEMA { get; set; }


    }
}