﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModifierBonRetourCiterne
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }

        public int idTypeBon { get; set; }
        public string TypeBon { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int CodeClient { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public int CodeCentre { get; set; }
        public string NomClient { get; set; }
        public string libelleCentre { get; set; }

        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string ImmtriculeCamion { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateEnvoie { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string ReferenceBonChargement { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal PoidsVide { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal PoidsPlein { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal PoidsGaz { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string ReferencePVPesee { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateRetour { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal PoidsRetour { get; set; }

        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal PoidsVideRetour { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal PoidsPleinRetour { get; set; }

        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal PoidsGazRestant { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public decimal PoidsReceptionnerCentre { get; set; }

        public string Commentaire { get; set; }
        public int etat { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateLivraison { get; set; }
        public string DateLivraisonString { get; set; }
        public int idBonRetour { get; set; }
        public string CommentaireLivraison { get; set; }


        public List<TypeBonRetourDto> ListeTypeBons { get; set; }

        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Visa DC";
                else if (etat == 2)
                    str = "Visa DE";
                else if (etat == 3)
                    str = "Valider";
                else if (etat == 4)
                    str = "Livrer";
                else if (etat == 5)
                    str = "Clôturer";
                return str;
            }
        }

    }
}