﻿using GUHabitat.DTO;
using GUHabitat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelListUtilisateur
    {

        public string filtreMotCle { get; set; }
        public int? filtreEntite{ get; set; }
        public int? page { get; set; }
     
        public List<EntiteDto> ListeEntites { get; set; }
        public List<RoleDto> ListeRoles { get; set; }
        public PagedList.IPagedList<UtilisateurDto> ListeUtilisateurs { get; set; }
    }
    
}