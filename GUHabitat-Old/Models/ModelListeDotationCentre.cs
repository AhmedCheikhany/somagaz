﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelListeDotationCentre
    {

        public int? page { get; set; }
        public string filtreMotCle { get; set; }
        public PagedList.IPagedList<DotationCentreDto> ListeDotationCentre { get; set; }



    }
}