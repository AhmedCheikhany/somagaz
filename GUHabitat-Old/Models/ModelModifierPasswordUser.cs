﻿using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelModifierPasswordUser
    {


        public int id { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string login { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string Oldpassword { get; set; }
        [Required(ErrorMessage = "Ce champ est obligatoire.")]
        public string password { get; set; }
       
        public string Nom { get; set; }
        
        public int idEntite { get; set; }
      
        public int idRole { get; set; }
        //public string OldPassword { get; set; }
        public List<EntiteDto> ListeEntites { get; set; }
        public List<RoleDto> ListeRoles { get; set; }
        public HttpPostedFileBase signature { get; set; }

    }
}