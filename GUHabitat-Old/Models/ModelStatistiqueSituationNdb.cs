﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.Models
{
    public class ModelStatistiqueSituationNdb
    {

        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }
        public DateTime filtreDate { get; set; }

        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }



        public decimal TotalFacturationNOUADHIBOU { get; set; }

        public decimal TotalProductionNOUADHIBOU { get; set; }



        public decimal StockNOUADHIBOU { get; set; }

        public int B12TotalProductionNOUADHIBOU { get; set; }
        public int B9TotalProductionNOUADHIBOU { get; set; }
        public int B6RTotalProductionNOUADHIBOU { get; set; }
        public int B6VTotalProductionNOUADHIBOU { get; set; }
        public int B3TotalProductionNOUADHIBOU { get; set; }
        public int B35TotalProductionNOUADHIBOU { get; set; }


        public int B12TotalFacturationNOUADHIBOU { get; set; }
        public int B9TotalFacturationNOUADHIBOU { get; set; }
        public int B6RTotalFacturationNOUADHIBOU { get; set; }
        public int B6VTotalFacturationNOUADHIBOU { get; set; }
        public int B3TotalFacturationNOUADHIBOU { get; set; }
        public int B35TotalFacturationNOUADHIBOU { get; set; }


        public int B12VenteNOUADHIBOU { get; set; }
        public int B9VenteNOUADHIBOU { get; set; }
        public int B6RVenteNOUADHIBOU { get; set; }
        public int B6VVenteNOUADHIBOU { get; set; }
        public int B3VenteNOUADHIBOU { get; set; }
        public int B35VenteNOUADHIBOU { get; set; }



        public int B12ConsignationNOUADHIBOU { get; set; }
        public int B9ConsignationNOUADHIBOU { get; set; }
        public int B6RConsignationNOUADHIBOU { get; set; }
        public int B6VConsignationNOUADHIBOU { get; set; }
        public int B3ConsignationNOUADHIBOU { get; set; }
        public int B35ConsignationNOUADHIBOU { get; set; }


        public int B12DotationNOUADHIBOU { get; set; }
        public int B9DotationNOUADHIBOU { get; set; }
        public int B6RDotationNOUADHIBOU { get; set; }
        public int B6VDotationNOUADHIBOU { get; set; }
        public int B3DotationNOUADHIBOU { get; set; }
        public int B35DotationNOUADHIBOU { get; set; }



        public int B12EmballageNOUADHIBOU { get; set; }
        public int B9EmballageNOUADHIBOU { get; set; }
        public int B6REmballageNOUADHIBOU { get; set; }
        public int B6VEmballageNOUADHIBOU { get; set; }
        public int B3EmballageNOUADHIBOU { get; set; }
        public int B35EmballageNOUADHIBOU { get; set; }




    }
}