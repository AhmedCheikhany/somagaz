﻿
using GUHabitat.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Filters
{
    public class NavigationLogFilter : ActionFilterAttribute
    {
        //avant
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var username = filterContext.HttpContext.User.Identity.Name;
            var ip = filterContext.HttpContext.Request.UserHostAddress;
            var url = filterContext.HttpContext.Request.RawUrl;
            var msg = $"{username};{ip};{url}";

            LogHelper.WriteInfo(msg);

        }

        //après
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
           
        }
    }
}