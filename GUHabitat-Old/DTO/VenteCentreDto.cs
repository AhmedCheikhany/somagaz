﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class VenteCentreDto
    {
        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }

        public int B12Revendeur { get; set; }
        public int B9Revendeur { get; set; }
        public int B6RRevendeur { get; set; }
        public int B6VRevendeur { get; set; }
        public int B3Revendeur { get; set; }
        public int B35Revendeur { get; set; }

        public decimal B12PrixRevendeur { get; set; }
        public decimal B9PrixRevendeur { get; set; }
        public decimal B6RPrixRevendeur { get; set; }
        public decimal B6VPrixRevendeur { get; set; }
        public decimal B3PrixRevendeur { get; set; }
        public decimal B35PrixRevendeur { get; set; }

       
        public int B12Consommateur { get; set; }
        public int B9Consommateur { get; set; }
        public int B6RConsommateur { get; set; }
        public int B6VConsommateur { get; set; }
        public int B3Consommateur { get; set; }
        public int B35Consommateur { get; set; }


        public decimal B12PrixConsommateur { get; set; }
        public decimal B9PrixConsommateur { get; set; }
        public decimal B6RPrixConsommateur { get; set; }
        public decimal B6VPrixConsommateur { get; set; }
        public decimal B3PrixConsommateur { get; set; }
        public decimal B35PrixConsommateur { get; set; }





        public int TotalNbreB12 { get; set; }
        public int TotalNbreB9 { get; set; }
        public int TotalNbreB6R{ get; set; }
        public int TotalNbreB6V { get; set; }
        public int TotalNbreB3{ get; set; }
        public int TotalNbreB35 { get; set; }

        public decimal TotalPrixB12Consommateur { get; set; }
        public decimal TotalPrixB9Consommateur { get; set; }
        public decimal TotalPrixB6RConsommateur { get; set; }
        public decimal TotalPrixB6VConsommateur { get; set; }
        public decimal TotalPrixB3Consommateur { get; set; }
        public decimal TotalPrixB35Consommateur { get; set; }

        public decimal TotalPrixB12Revendeur { get; set; }
        public decimal TotalPrixB9Revendeur { get; set; }
        public decimal TotalPrixB6RRevendeur { get; set; }
        public decimal TotalPrixB6VRevendeur { get; set; }
        public decimal TotalPrixB3Revendeur { get; set; }
        public decimal TotalPrixB35Revendeur { get; set; }


        public decimal TotalMontantConsommateur { get; set; }
        public decimal TotalMontantRevendeur { get; set; }



        public string Commentaire { get; set; }


        public byte[] qrCodeImage { get; set; }
        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Visa DC";
                else if (etat == 2)
                    str = "Valider";
                else if (etat == 3)
                    str = "Clôturer";
                return str;
            }
        }


    }
}