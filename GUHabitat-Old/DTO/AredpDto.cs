﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class AredpDto
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        public string NomClient { get; set; }
        public DateTime Date { get; set; }
        public int CodeClient { get; set; }
        public string NumeroFacture { get; set; }

        public int B12Afact { get; set; }
        public int B9Afact { get; set; }
        public int B6RAfact { get; set; }
        public int B6VAfact { get; set; }
        public int B3Afact { get; set; }
        public int B35Afact { get; set; }

        public int B12Nonfact { get; set; }
        public int B9Nonfact { get; set; }
        public int B6RNonfact { get; set; }
        public int B6VNonfact { get; set; }
        public int B3Nonfact { get; set; }
        public int B35Nonfact { get; set; }

        public int TotalB12 { get; set; }
        public int TotalB9 { get; set; }
        public int TotalB6R { get; set; }
        public int TotalB6V { get; set; }
        public int TotalB3 { get; set; }
        public int TotalB35 { get; set; }

        public string Commentaire { get; set; }
        public int etat { get; set; }
        public byte[] qrCodeImage { get; set; }

        public List<AredpDetailsDto> ListeAredpDetails { get; set; }

        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Visa DC";
                else if (etat == 2)
                    str = "Valider";
                else if (etat == 3)
                    str = "Clôturer";
                return str;
            }
        }
    }
}