﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class SortieDto
    {

        public int id { get; set; }
        public string Reference { get; set; }
        public string ReferenceTypeSortie { get; set; }
        public string NumeroFacture { get; set; }
        public int Client { get; set; }
        public int Employe { get; set; }
        public DateTime Date { get; set; }
        public int idTypeSortie { get; set; }
        public string TypeSortie { get; set; }
        public int B12 { get; set; }
        public int B9 { get; set; }
        public int B6R { get; set; }
        public int B6V { get; set; }
        public int B3 { get; set; }
        public int B35 { get; set; }
        public int Presentoir { get; set; }
        public string Commentaires { get; set; }
      
        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Visa DC";
                else if (etat == 2)
                    str = "Valider";
                else if (etat == 3)
                    str = "Clôturer";
                return str;
            }
        }


    }
}