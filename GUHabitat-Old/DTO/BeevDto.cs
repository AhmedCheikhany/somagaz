﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class BeevDto
    {

        public int id { get; set; }
        public string Serie { get; set; }
        public DateTime Date { get; set; }
        public int B12 { get; set; }
        public int B9 { get; set; }
        public int B6R { get; set; }
        public int B6V { get; set; }
        public int B3 { get; set; }
        public int B35 { get; set; }
        public int CodeClient { get; set; }
        public string NomClient { get; set; }
        public string Commentaire { get; set; }
        public byte[] qrCodeImage { get; set; }
        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "En Instance";
                else
                    str = "Clôturer ";
                return str;
            }
        }
    }
}