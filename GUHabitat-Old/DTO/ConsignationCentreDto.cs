﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class ConsignationCentreDto
    {
        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }

        public int B12 { get; set; }
        public int B9 { get; set; }
        public int B6R { get; set; }
        public int B6V { get; set; }
        public int B3 { get; set; }
        public int B35 { get; set; }

        public decimal B12Prix{ get; set; }
        public decimal B9Prix { get; set; }
        public decimal B6RPrix { get; set; }
        public decimal B6VPrix { get; set; }
        public decimal B3Prix { get; set; }
        public decimal B35Prix { get; set; }

        public decimal TotalPrixB12 { get; set; }
        public decimal TotalPrixB9 { get; set; }
        public decimal TotalPrixB6R { get; set; }
        public decimal TotalPrixB6V { get; set; }
        public decimal TotalPrixB3 { get; set; }
        public decimal TotalPrixB35 { get; set; }



        public string Commentaire { get; set; }


        public byte[] qrCodeImage { get; set; }
        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Visa DC";
                else if (etat == 2)
                    str = "Valider";
                else if (etat == 3)
                    str = "Clôturer";
                return str;
            }
        }



    }
}