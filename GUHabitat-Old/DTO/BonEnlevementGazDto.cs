﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class BonEnlevementGazDto
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        public string ReferenceDepotage { get; set; }
        public string ReferenceBonPesee { get; set; }
        public string ImmatriculeCamion { get; set; }
        public string ChauffeurCamion { get; set; }
        public DateTime Date { get; set; }
        public int CodeClient { get; set; }
        public string NomClient { get; set; }
        public int idMouvementBon { get; set; }
        public string MouvementBon { get; set; }
        public int idTypeBonEnlevement { get; set; }
        public string TypeBonEnlevement { get; set; }

        public decimal Tonnage { get; set; }
        public string Commentaires { get; set; }

        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Visa DE";
                else if (etat == 2)
                    str = "Valider";
                else if (etat == 3)
                    str = "Clôturer";
                return str;
            }
        }
    }
}