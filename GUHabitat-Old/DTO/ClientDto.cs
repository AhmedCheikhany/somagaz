﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class ClientDto
    {
        public int Code { get; set; }
        public string nomComplet { get; set; }
    }
}