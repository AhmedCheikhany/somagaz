﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class StockCentreDto
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public int reference { get; set; }
        public DateTime Date { get; set; }
        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }


        public decimal StockTheorie { get; set; }
        public decimal Stock { get; set; }
        public decimal StockFin { get; set; }
        public decimal Depotage { get; set; }
        public decimal GainPerte { get; set; }

        public string Commentaire { get; set; }


        public byte[] qrCodeImage { get; set; }
        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Saisie";
                else if (etat == 2)
                    str = "Valider";             
                else if (etat == 3)
                    str = "Clôturer";
                return str;
            }
        }



    }
}