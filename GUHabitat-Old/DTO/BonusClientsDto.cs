﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class BonusClientsDto
    {

        public int Numero { get; set; }
        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }

        public int CodeClient { get; set; }
        public string NomClient { get; set; }

        public int B12Afact { get; set; }

        public int B9Afact { get; set; }

        public int B6RAfact { get; set; }

        public int B6VAfact { get; set; }

        public int B3Afact { get; set; }

        public int B35Afact { get; set; }

        public decimal Tonnage { get; set; }

        public decimal MontantMru { get; set; }

        public decimal MontantMro { get; set; }



    }
}