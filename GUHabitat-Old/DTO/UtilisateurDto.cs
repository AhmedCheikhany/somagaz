﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class UtilisateurDto
    {

        public int id { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string Nom { get; set; }
        public int idEntite { get; set; }
        public int idRole { get; set; }
        public string entite { get; set; }
        public string role { get; set; }
        public byte[] signature { get; set; }
    }
}