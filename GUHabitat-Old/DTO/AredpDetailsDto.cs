﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class AredpDetailsDto
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public int idAredp { get; set; }
        public int idProduit { get; set; }
        public string NomProduit { get; set; }
        public decimal poidsLu { get; set; }
        public decimal Tare { get; set; }
        public decimal poidsManquant { get; set; }
        public decimal poidsRestant { get; set; }
       

    }
}