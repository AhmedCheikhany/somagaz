﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class BonRetourCiterneDto
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
       
        public int idTypeBon { get; set; }
        public string TypeBon { get; set; }

        public int CodeConcerne { get; set; }
        public string Concrne { get; set; }

        public int CodeClient { get; set; }
        public string NomClient { get; set; }

        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }

        public string ImmtriculeCamion { get; set; }
        public DateTime DateEnvoie { get; set; }
        public string ReferenceBonChargement { get; set; }
        public decimal PoidsVide { get; set; }
        public decimal PoidsPlein { get; set; }
        public decimal PoidsGaz { get; set; }
        public string ReferencePVPesee { get; set; }

        public DateTime DateRetour { get; set; }
        public decimal PoidsRetour { get; set; }

        public decimal PoidsVideRetour { get; set; }
        public decimal PoidsPleinRetour { get; set; }

        public decimal PoidsGazRestant{ get; set; }
        public decimal PoidsReceptionnerCentre { get; set; }

        public string Commentaire { get; set; }
        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Visa DC";
                else if (etat == 2)
                    str = "Visa DE";
                else if (etat == 3)
                    str = "Valider";
                else if (etat == 4)
                    str = "Livrer";
                else if (etat == 5)
                    str = "Clôturer";
                return str;
            }
        }
    }
}