﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class DestinationDto
    {
        public int Code { get; set; }
        public string libelle { get; set; }

    }
}