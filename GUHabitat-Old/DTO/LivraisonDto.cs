﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class LivraisonDto
    {

        public int idEntite { get; set; }
        public int id { get; set; }
        
        public int idBonRetour { get; set; }
        public DateTime DateLivraison { get; set; }
        public decimal PoidsRetour { get; set; }
        public string commentaire { get; set; }

    }
}