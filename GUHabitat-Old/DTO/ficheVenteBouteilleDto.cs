﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class FicheVenteBouteilleDto
    {


        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public int CodeClient { get; set; }

        public int idTypeFiche { get; set; }
        public int matriculeEmploye { get; set; }
        public string NomEmploye { get; set; }
        public string NomClient { get; set; }
        public string VehiculeImmatricule { get; set; }
        public int NbrePersonnes { get; set; }
        public string DotationMatricule { get; set; }
        public string TypeFiche { get; set; }

        public int B12Client { get; set; }
        public int B9Client { get; set; }
        public int B6RClient { get; set; }
        public int B6VClient { get; set; }
        public int B3Client { get; set; }
        public int B35Client { get; set; }



        public int B12VideRec { get; set; }
        public int B9VideRec { get; set; }
        public int B6RVideRec { get; set; }
        public int B6VVideRec { get; set; }
        public int B3VideRec { get; set; }
        public int B35VideRec { get; set; }

        public int B12VideDef { get; set; }
        public int B9VideDef { get; set; }
        public int B6RVideDef { get; set; }
        public int B6VVideDef { get; set; }
        public int B3VideDef { get; set; }
        public int B35VideDef { get; set; }

        public int B12PleinRemp { get; set; }
        public int B9PleinRemp { get; set; }
        public int B6RPleinRemp { get; set; }
        public int B6VPleinRemp { get; set; }
        public int B3PleinRemp { get; set; }
        public int B35PleinRemp { get; set; }

        public int B12PleinDef { get; set; }
        public int B9PleinDef { get; set; }
        public int B6RPleinDef { get; set; }
        public int B6VPleinDef { get; set; }
        public int B3PleinDef { get; set; }
        public int B35PleinDef { get; set; }

        public int B12DefAremp { get; set; }
        public int B9DefAremp { get; set; }
        public int B6RDefAremp { get; set; }
        public int B6VDefAremp { get; set; }
        public int B3DefAremp { get; set; }
        public int B35DefAremp { get; set; }


        public int B12DefRemp { get; set; }
        public int B9DefRemp { get; set; }
        public int B6RDefRemp { get; set; }
        public int B6VDefRemp { get; set; }
        public int B3DefRemp { get; set; }
        public int B35DefRemp { get; set; }

        public int B12Afact { get; set; }
        public int B9Afact { get; set; }
        public int B6RAfact { get; set; }
        public int B6VAfact { get; set; }
        public int B3Afact { get; set; }
        public int B35Afact { get; set; }

        public decimal Tonnage { get; set; }

        public string Commentaire { get; set; }
        public byte[] qrCodeImage { get; set; }
        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Visa DC";
                else if (etat == 2)
                    str = "Visa DE";
                else if (etat == 3)
                    str = "Valider";
                else if (etat == 4)
                    str = "Clôturer";
                return str;
            }
        }

    }
}