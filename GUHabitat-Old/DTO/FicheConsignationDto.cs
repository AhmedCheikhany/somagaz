﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class FicheConsignationDto
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public int CodeClient { get; set; }
        public string NomClient { get; set; }
        public string VehiculeImmatricule { get; set; }
        public int idTypeFicheConsignation { get; set; }
        public string TypeFicheConsignation { get; set; }
        public string NumeroSerie { get; set; }

        public int B12DC { get; set; }
        public int B9DC { get; set; }
        public int B6RDC { get; set; }
        public int B6VDC { get; set; }
        public int B3DC { get; set; }
        public int B35DC { get; set; }
        public int PresentoirDC { get; set; }


        public float B12Prix { get; set; }
        public float B9Prix { get; set; }
        public float B6RPrix { get; set; }
        public float B6VPrix { get; set; }
        public float B3Prix { get; set; }
        public float B35Prix { get; set; }
        public float PresentoirPrix { get; set; }


        public float TotalPrixB12 { get; set; }
        public float TotalPrixB9 { get; set; }
        public float TotalPrixB6R { get; set; }
        public float TotalPrixB6V { get; set; }
        public float TotalPrixB3 { get; set; }
        public float TotalPrixB35 { get; set; }
        public float TotalPrixPresentoir { get; set; }


        public int B12DE { get; set; }
        public int B9DE { get; set; }
        public int B6RDE { get; set; }
        public int B6VDE { get; set; }
        public int B3DE { get; set; }
        public int B35DE { get; set; }
        public int PresentoirDE { get; set; }


        public decimal Tonnage { get; set; }

        public float Total { get; set; }

        public string Commentaire { get; set; }


        public byte[] qrCodeImage { get; set; }
        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Visa DC";
                else if (etat == 2)
                    str = "Visa DE";
                else if (etat == 3)
                    str = "Valider";
                else if (etat == 4)
                    str = "Clôturer";
                return str;
            }
        }


    }
}