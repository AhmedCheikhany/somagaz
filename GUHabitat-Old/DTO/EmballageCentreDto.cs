﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class EmballageCentreDto
    {

        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        public DateTime Date { get; set; }

        public int CodeCentre { get; set; }
        public string LibelleCentre { get; set; }

        public int idTypeMouvement { get; set; }
        public string LibelleMouvement { get; set; }
        public int idDestinationSource { get; set; }
        public string LibelleDestinationSource { get; set; }
        public int idEtatEmballage { get; set; }
        public string LibelleEtatEmballage { get; set; }


        public int B12 { get; set; }
        public int B9 { get; set; }
        public int B6R { get; set; }
        public int B6V { get; set; }
        public int B3 { get; set; }
        public int B35 { get; set; }

        public string Commentaire { get; set; }


        public byte[] qrCodeImage { get; set; }
        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Saisie";
                else if (etat == 2)
                    str = "Valider";
                else if (etat == 3)
                    str = "Clôturer";
                return str;
            }
        }




    }
}