﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class TypeMouvementEmballageDto
    {

        public int id { get; set; }
        public string libelle { get; set; }

    }
}