﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class BonChargementCiterneDto
    {


        public int id { get; set; }
        public int idEntite { get; set; }
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public string DateString { get; set; }
        public int idTypeBon { get; set; }
        public string TypeBon { get; set; }
        public string ImmtriculeCamion { get; set; }
        public int NbrePersonnes { get; set; }
        public string ResponsableCamion { get; set; }
        public string ChauffeurCamion { get; set; }
        public int CodeClient { get; set; }
        public string NomClient { get; set; }
        public float Tonnage { get; set; }
        public int CodeDestination { get; set; }
        public string libelleDestination { get; set; }
        public string Demmarage { get; set; }
        public string EtatPneus { get; set; }
        public string Freinage { get; set; }
        public string Extincteur { get; set; }
        public string Observation { get; set; }
        public string Numeropermis { get; set; }
        public string NpoliceRC { get; set; }
        public string NpoliceRCGaz { get; set; }
        public string AutoriserEntree { get; set; }
        public string Facturer { get; set; }
        public string QuantiteFactQuantiteChargee { get; set; }
        public string AutoriserSortie { get; set; }
        public float PoidDebut { get; set; }
        public string NumeroPontBascule { get; set; }
        public string ClapetVanne { get; set; }
        public string CalageVehicule { get; set; }
        public string MiseTerre { get; set; }
        public string ObservationExploitation { get; set; }
        public string AutoriserCharger { get; set; }

        public string PoidDebutString { get; set; }
        public string PoidsApresChargementString { get; set; }
        public string TonnageChargerString { get; set; }



        public DateTime heureChargement { get; set; }
        public DateTime heureFinChargement { get; set; }
        public float PoidsApresChargement { get; set; }
        public float TonnageCharger { get; set; }
        public string NumeroFacture { get; set; }
        public string TempsEstime { get; set; }
        public int etat { get; set; }
        public string etatString
        {
            get
            {
                string str = "";
                if (etat == 1)
                    str = "Visa DC";
                else if (etat == 2)
                    str = "Visa DE";
                else if (etat == 3)
                    str = "Valider";
                else if (etat == 4)
                    str = "Clôturer";
                return str;
            }
        }


    }
}