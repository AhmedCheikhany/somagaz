﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.DTO
{
    public class ProduitDto
    {
        public int id { get; set; }
        public string libelle { get; set; }
        public decimal tare { get; set; }
    }
}