﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUHabitat.Common
{
    public static class LogHelper
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();


        public static void WriteError(string msg)
        {
            Logger.Error(msg);
        }

        public static void WriteError(Exception ex)
        {
            Logger.Error(ex);
        }

        public static void WriteInfo(string msg)
        {
            Logger.Info(msg);
        }
        public static bool In<T>(this T source, params T[] list)
        {
            return list.Contains(source);
        }
    }
}
