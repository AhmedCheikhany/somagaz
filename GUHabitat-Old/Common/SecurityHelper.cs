﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GUHabitat.Common
{
    public static class SecurityHelper
    {
        public static string GetConnectedUserName()
        {
           // string login = (IUser.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            return HttpContext.Current.User.Identity.Name;
        }
    }
}
