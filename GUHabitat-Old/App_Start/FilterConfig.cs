﻿using GUHabitat.Filters;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new AuthorizeAttribute());
            filters.Add(new NavigationLogFilter());
            filters.Add(new MaxJsonSizeAttribute());
        }
    }
}
