﻿
$(function () {
    $("#add").click(function () {
        var isValid = true;

        if (document.getElementById("idProduit").selectedIndex == 0) {
            $('#idProduit').siblings('span.error').text('Please select item');
            isValid = false;
        }
        else {
            $('#idProduit').siblings('span.error').text('');
        }

        if (!($("#poidsLu").val().trim() != '' && (parseInt($('#poidsLu').val()) >= 0))) {
            $('#poidsLu').siblings('span.error').text('Please enter poidsLu');
            isValid = false;
        }
        else {
            $('#poidsLu').siblings('span.error').text('');
        }

        if (!($("#Tare").val().trim() != '' && (parseInt($('#Tare').val()) || 0))) {
            $('#Tare').siblings('span.error').text('Please enter Tare');
            isValid = false;
        }
        else {
            $('#Tare').siblings('span.error').text('');
        }

       


        if (isValid) {


            var ProductID = document.getElementById("idProduit").value;

            var $newRow = $("#MainRow").clone().removeAttr('id');

            $('.idProduit', $newRow).val(ProductID);

            $('#add', $newRow).addClass('Supprimer').html('Supprimer').removeClass('btn-success').addClass('btn-danger');

            $('#idProduit, #poidsLu', "#Tare", $newRow).attr('disabled', true);

            $("#idProduit, #poidsLu","#Tare", $newRow).removeAttr("id");
            $("span.error", $newRow).remove();

            
            $("#OrderItems").append($newRow[0]);

            document.getElementById("idProduit").selectedIndex = 0;
            $("#poidsLu").val('');
            $("#Tare").val('');
        }
    });

    $("#OrderItems").on("click", ".Supprimer", function () {
        $(this).parents("tr").remove();
    });

    $("#submit").click(function () {
        var isValid = true;

        var itemsList = [];

        $("#OrderItems tr").each(function () {
            var item = {
                idProduit: $('select.idProduit', this).val(),
                poidsLu: $('.poidsLu', this).val(),
                Tare: $('.Tare', this).val()

            }
            
            itemsList.push(item);
        });

        if (itemsList.length == 0) {
            $('#orderMessage').text('');
            isValid = false;


           



        }


        if ($('#Reference').val().trim() == '') {
            $('#Reference').siblings('span.error').css('visibility', 'visible');
            isValid = false;
        }
        else {
            $('#Reference').siblings('span.error').css('visibility', 'hidden');
        }

        if ($('#Date').val().trim() == '') {
            $('#Date').siblings('span.error').css('visibility', 'visible');
            isValid = false;
        }
        else {
            $('#Date').siblings('span.error').css('visibility', 'hidden');
        }
        if ($('#CodeClient').val().trim() == '') {
            $('#CodeClient').siblings('span.error').css('visibility', 'visible');
            isValid = false;
        }
        else {
            $('#CodeClient').siblings('span.error').css('visibility', 'hidden');
        }
        if ($('#NomClient').val().trim() == '') {
            $('#NomClient').siblings('span.error').css('visibility', 'visible');
            isValid = false;
        }
        else {
            $('#NomClient').siblings('span.error').css('visibility', 'hidden');
        }
        
      

        if (isValid) {
            var data = {
                Reference: $('#Reference').val(),
                Date: $('#Date').val(),
                CodeClient: $('#CodeClient').val(),
                NomClient: $('#NomClient').val(),
                Commentaire: $('#Commentaire').val(),
                
                ListeAredpDetails: itemsList
                
            }

            $("#submit").attr('disabled', 'disabled');
            $("#submit").html('Attendez  Svp ...');
           
           
          
           
            $.ajax({
                type: 'POST',
                url: MyAppUrlSettings.MyUsefulUrl,
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function (data) {
                    if (data.status) {
                      
 
                        window.location.href = MyAppUrlSettingsIndex.MyUsefulUrlIndex
                    }
                    else {
                        
                        alert('Error');
                        window.location.href = MyAppUrlSettings.MyUsefulUrl
                    }
                  
                }

            });
        }
    });
});
