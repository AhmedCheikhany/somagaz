﻿using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class EmballageCentreController : Controller
    {
       
        [HttpPost]
        public ActionResult ValiderEmballageCentre(int CodeCentreEmballage)
        {
            return RedirectToAction("Ajouter", new { CodeCentreEmballage = CodeCentreEmballage });
        }



        // GET: EmballageCentre
        public ActionResult Index(string filtreMotCle, int? page)
        {

            ModelListeEmballageCentre model = new ModelListeEmballageCentre();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeEmballageCentre = BusinessGestionEmballageCentre.Instance.ChercherListeEmballageCentre(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);
        }


        public ActionResult EnInstance(int id)
        {


            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionEmballageCentre.Instance.EnInstance(idEntite, id);

            return RedirectToAction("Index");
        }



        public ActionResult Valider(int id)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionEmballageCentre.Instance.Valider(idEntite, id);


            return RedirectToAction("Index");
        }







        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelAjouterEmballageCentre model = new ModelAjouterEmballageCentre();


            EmballageCentreDto dto = BusinessGestionEmballageCentre.Instance.GetEmballageCentreParId(id);

            model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvements();
            model.ListeSourceDestination = BusinessGestionEmballageCentre.Instance.GetListeSourceDestination();  
            model.ListeEtatEmballages = BusinessGestionEmballageCentre.Instance.GetListeEtatEmballages();


            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;

                model.CodeCentre = dto.CodeCentre;
                model.LibelleCentre = dto.LibelleCentre;

                model.idTypeMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementParId(dto.idTypeMouvement).id;
                model.LibelleMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementParId(dto.idTypeMouvement).libelle;

                model.idDestinationSource = BusinessGestionEmballageCentre.Instance.GetSourceDestinationParId(dto.idDestinationSource).id;
                model.LibelleDestinationSource = BusinessGestionEmballageCentre.Instance.GetSourceDestinationParId(dto.idDestinationSource).libelle;

                model.idEtatEmballage = BusinessGestionEmballageCentre.Instance.GetEtatEmballageParId(dto.idEtatEmballage).id;
                model.LibelleEtatEmballage = BusinessGestionEmballageCentre.Instance.GetEtatEmballageParId(dto.idEtatEmballage).libelle;

                model.B12 = dto.B12;
                model.B9 = dto.B9;
                model.B6R = dto.B6R;
                model.B6V = dto.B6V;
                model.B3 = dto.B3;
                model.B35 = dto.B35;



                model.Commentaire = dto.Commentaire;

                model.etat = dto.etat;



            };
            return View(model);

        }


        [HttpGet]
        public ActionResult Ajouter(int CodeCentreEmballage)
        {
            ModelAjouterEmballageCentre model = new ModelAjouterEmballageCentre();

             
            if(CodeCentreEmballage == 9) model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvements();
            if (CodeCentreEmballage < 9) model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvementsInterieurs();


            model.ListeSourceDestination = BusinessGestionEmballageCentre.Instance.GetListeSourceDestination(CodeCentreEmballage);
            model.ListeEtatEmballages = BusinessGestionEmballageCentre.Instance.GetListeEtatEmballages();



            model.Date = DateTime.Now;

            string reference = GetNewCode();

            model.CodeCentre = BusinessGestionEmballageCentre.Instance.GetCentreParCode(CodeCentreEmballage).Code;

            model.Reference = (reference == "1") ? "000001" : reference;

            return View(model);
        }



        [HttpPost]
        public ActionResult Ajouter(ModelAjouterEmballageCentre model)
        {



            if (ModelState.IsValid)
            {

                EmballageCentreDto dto = new EmballageCentreDto
                {
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),

                    Reference = model.Reference,
                    Date = model.Date,
                    CodeCentre = model.CodeCentre,
                    idTypeMouvement = model.idTypeMouvement,
                    idDestinationSource = model.idDestinationSource,
                    idEtatEmballage = model.idEtatEmballage,


                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,


                    Commentaire = model.Commentaire,

                    etat = model.etat

                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);


                //decimal Tonnage = (decimal)(dto.B12 * 12.5 + dto.B9 * 9 + dto.B6R * 6 + dto.B6V * 6 + dto.B3 * 3 + dto.B35 * 35) / 1000;

                //var StockActuel = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).StockActuel;

                //var EmballageB12 = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B12Emballage;
                //var EmballageB9 = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B9Emballage;
                //var EmballageB6R = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B6REmballage;
                //var EmballageB6V = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B6VEmballage;
                //var EmballageB3 = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B3Emballage;
                //var EmballageB35 = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B35Emballage;

             
                //if (model.idTypeMouvement == 2 && ( model.B12 > EmballageB12 || model.B9 > EmballageB9 || model.B6R > EmballageB6R || model.B6V > EmballageB6V || model.B3 > EmballageB3 || model.B35 > EmballageB35))
                //{
                //    if (model.CodeCentre == 9)
                //    {
                //        model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvements();
                //        model.idTypeMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementParId(model.idTypeMouvement).id;
                //        model.LibelleMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementParId(model.idTypeMouvement).libelle;

                //    }
                //    if (model.CodeCentre < 9)
                //    {
                //        model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvementsInterieurs();
                //        model.idTypeMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementInterieursParId(model.idTypeMouvement).id;
                //        model.LibelleMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementInterieursParId(model.idTypeMouvement).libelle;

                //    }

                //    model.ListeSourceDestination = BusinessGestionEmballageCentre.Instance.GetListeSourceDestination(model.CodeCentre);
                //    model.idDestinationSource = BusinessGestionEmballageCentre.Instance.GetSourceDestinationParId(model.idDestinationSource).id;
                //    model.LibelleDestinationSource = BusinessGestionEmballageCentre.Instance.GetSourceDestinationParId(model.idDestinationSource).libelle;

                //    model.ListeEtatEmballages = BusinessGestionEmballageCentre.Instance.GetListeEtatEmballages();
                //    model.idEtatEmballage = BusinessGestionEmballageCentre.Instance.GetEtatEmballageParId(model.idEtatEmballage).id;
                //    model.LibelleEtatEmballage = BusinessGestionEmballageCentre.Instance.GetCentreParCode(model.idEtatEmballage).libelle;



                //    ViewBag.MessageEmballageAjout = "Quantite non disponible";
                //    model.Date = DateTime.Now;
                //    return View(model);
                //}




                BusinessGestionEmballageCentre.Instance.Ajouter(dto);





                return RedirectToAction("Index");
            }
            else
            {
                if(model.CodeCentre == 9)
                {
                    model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvements();
                    model.idTypeMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementParId(model.idTypeMouvement).id;
                    model.LibelleMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementParId(model.id).libelle;

                }
                if (model.CodeCentre < 9)
                {
                    model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvementsInterieurs();
                    model.idTypeMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementInterieursParId(model.idTypeMouvement).id;
                    model.LibelleMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementInterieursParId(model.id).libelle;

                }

                model.ListeSourceDestination = BusinessGestionEmballageCentre.Instance.GetListeSourceDestination(model.CodeCentre);
                model.idDestinationSource = BusinessGestionEmballageCentre.Instance.GetSourceDestinationParId(model.idDestinationSource).id;
                model.LibelleDestinationSource = BusinessGestionEmballageCentre.Instance.GetSourceDestinationParId(model.idDestinationSource).libelle;

                model.ListeEtatEmballages = BusinessGestionEmballageCentre.Instance.GetListeEtatEmballages();
                model.idEtatEmballage = BusinessGestionEmballageCentre.Instance.GetEtatEmballageParId(model.idEtatEmballage).id;
                model.LibelleEtatEmballage = BusinessGestionEmballageCentre.Instance.GetCentreParCode(model.idEtatEmballage).libelle;


                model.Date = DateTime.Now;
                return View(model);
            }

        }



        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModifierEmballageCentre model = new ModelModifierEmballageCentre();

           

            model.ListeCentre = BusinessGestionEmballageCentre.Instance.GetListeCentre();
            ////model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvements();
            model.ListeEtatEmballages = BusinessGestionEmballageCentre.Instance.GetListeEtatEmballages();


            EmballageCentreDto dto = BusinessGestionEmballageCentre.Instance.GetEmballageCentreParId(id);

            if (dto.CodeCentre == 9) model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvements();
            if (dto.CodeCentre < 9) model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvementsInterieurs();

            model.ListeSourceDestination = BusinessGestionEmballageCentre.Instance.GetListeSourceDestination(dto.CodeCentre);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.CodeCentre = dto.CodeCentre;
                model.LibelleCentre = dto.LibelleCentre;

                model.idTypeMouvement = dto.idTypeMouvement;
                model.LibelleMouvement = dto.LibelleMouvement;

                model.idDestinationSource = dto.idDestinationSource;
                model.LibelleDestinationSource = dto.LibelleDestinationSource;

                model.idEtatEmballage = dto.idEtatEmballage;
                model.LibelleEtatEmballage = dto.LibelleEtatEmballage;



                model.B12 = dto.B12;
                model.B9 = dto.B9;
                model.B6R = dto.B6R;
                model.B6V = dto.B6V;
                model.B3 = dto.B3;
                model.B35 = dto.B35;

                model.Commentaire = dto.Commentaire;

                model.etat = dto.etat;



            };

            return View(model);


        }



        [HttpPost]
        public ActionResult Modifier(ModelModifierEmballageCentre model)
        {


            if (ModelState.IsValid)
            {

                EmballageCentreDto dto = new EmballageCentreDto
                {

                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    id = model.id,
                    Reference = model.Reference,
                    Date = model.Date,
                    CodeCentre = model.CodeCentre,
                    LibelleCentre = model.LibelleCentre,
                    idTypeMouvement = model.idTypeMouvement,
                    idDestinationSource = model.idDestinationSource,
                    idEtatEmballage = model.idEtatEmballage,

                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,


                    Commentaire = model.Commentaire,

                    etat = model.etat

                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);



                //var EmballageB12 = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B12Emballage;
                //var EmballageB9 = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B9Emballage;
                //var EmballageB6R = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B6REmballage;
                //var EmballageB6V = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B6VEmballage;
                //var EmballageB3 = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B3Emballage;
                //var EmballageB35 = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).B35Emballage;


                //if (model.B12 > EmballageB12 || model.B9 > EmballageB9 || model.B6R > EmballageB6R || model.B6V > EmballageB6V || model.B3 > EmballageB3 || model.B35 > EmballageB35)
                //{
                //    if (model.CodeCentre == 9)
                //    {
                //        model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvements();
                //        model.idTypeMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementParId(model.idTypeMouvement).id;
                //        model.LibelleMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementParId(model.id).libelle;

                //    }
                //    if (model.CodeCentre < 9)
                //    {
                //        model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvementsInterieurs();
                //        model.idTypeMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementInterieursParId(model.idTypeMouvement).id;
                //        model.LibelleMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementInterieursParId(model.id).libelle;

                //    }

                //    model.ListeSourceDestination = BusinessGestionEmballageCentre.Instance.GetListeSourceDestination(model.CodeCentre);
                //    model.idDestinationSource = BusinessGestionEmballageCentre.Instance.GetSourceDestinationParId(model.idDestinationSource).id;
                //    model.LibelleDestinationSource = BusinessGestionEmballageCentre.Instance.GetSourceDestinationParId(model.idDestinationSource).libelle;

                //    model.ListeEtatEmballages = BusinessGestionEmballageCentre.Instance.GetListeEtatEmballages();
                //    model.idEtatEmballage = BusinessGestionEmballageCentre.Instance.GetEtatEmballageParId(model.idEtatEmballage).id;
                //    model.LibelleEtatEmballage = BusinessGestionEmballageCentre.Instance.GetCentreParCode(model.idEtatEmballage).libelle;




                //    ViewBag.MessageEmballageModifier = "Quantite non disponible";
                //    model.Date = DateTime.Now;
                //    return View(model);
                //}



                BusinessGestionEmballageCentre.Instance.Modifier(dto);





                return RedirectToAction("Index");
            }
            else
            {

                model.ListeCentre = BusinessGestionEmballageCentre.Instance.GetListeCentre();
                model.CodeCentre = BusinessGestionEmballageCentre.Instance.GetCentreParCode(model.CodeCentre).Code;
                model.LibelleCentre = BusinessGestionEmballageCentre.Instance.GetCentreParCode(model.CodeCentre).libelle;

                if (model.CodeCentre == 9)
                {
                    model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvements();
                    model.idTypeMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementParId(model.idTypeMouvement).id;
                    model.LibelleMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementParId(model.id).libelle;

                }
                if (model.CodeCentre < 9)
                {
                    model.ListeMouvement = BusinessGestionEmballageCentre.Instance.GetListeMouvementsInterieurs();
                    model.idTypeMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementInterieursParId(model.idTypeMouvement).id;
                    model.LibelleMouvement = BusinessGestionEmballageCentre.Instance.GetTypeMouvementInterieursParId(model.id).libelle;

                }
                model.ListeSourceDestination = BusinessGestionEmballageCentre.Instance.GetListeSourceDestination();
                model.idDestinationSource = BusinessGestionEmballageCentre.Instance.GetSourceDestinationParId(model.idDestinationSource).id;
                model.LibelleDestinationSource = BusinessGestionEmballageCentre.Instance.GetSourceDestinationParId(model.idDestinationSource).libelle;

                model.ListeEtatEmballages = BusinessGestionEmballageCentre.Instance.GetListeEtatEmballages();
                model.idEtatEmballage = BusinessGestionEmballageCentre.Instance.GetEtatEmballageParId(model.idEtatEmballage).id;
                model.LibelleEtatEmballage = BusinessGestionEmballageCentre.Instance.GetCentreParCode(model.idEtatEmballage).libelle;


                model.Date = DateTime.Now;
                return View(model);
            }

        }




        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {

                maxCode = db.EmballageCentres.OrderByDescending(s => s.id).Select(x => x.Reference).FirstOrDefault();


            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }


    }
}