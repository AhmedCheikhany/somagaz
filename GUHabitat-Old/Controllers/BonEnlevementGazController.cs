﻿using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class BonEnlevementGazController : Controller
    {
        [HttpPost]
        public ActionResult ValiderMouvementBon(int idMouvementBon)
        {
            return RedirectToAction("Ajouter", new { idMouvementBon = idMouvementBon });
        }



      
        [HttpGet]
        public ActionResult Index(string filtreMotCle, int? page)
        {
            ModelListeBonEnlevementGaz model = new ModelListeBonEnlevementGaz();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeBonEnlevement = BusinessGestionBonEnlevementGaz.Instance.ChercherListeBonEnlevement(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);


        }


        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelAjouterBonEnlevement model = new ModelAjouterBonEnlevement();
           
            BonEnlevementGazDto dto = BusinessGestionBonEnlevementGaz.Instance.GetBonEnlevementParId(id);

            model.ListeTypeBonEnlevement = BusinessGestionBonEnlevementGaz.Instance.GetListeTypeBonEnlevement();

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.idMouvementBon = dto.idMouvementBon;
                model.MouvementBon = dto.MouvementBon;
                model.idTypeBonEnlevement = dto.idTypeBonEnlevement;
                model.TypeBonEnlevement = dto.TypeBonEnlevement;
                model.CodeClient = dto.CodeClient;
                model.NomClient = dto.NomClient;
                
                model.ImmatriculeCamion = dto.ImmatriculeCamion;
                model.ChauffeurCamion = dto.ChauffeurCamion;
                model.ReferenceBonPesee = dto.ReferenceBonPesee;
                model.ReferenceDepotage = dto.ReferenceDepotage;
                model.Tonnage = dto.Tonnage;

                model.Commentaires = dto.Commentaires;
                model.etat = dto.etat;


            };
            return View(model);

        }


        [HttpGet]
        public ActionResult Ajouter(int idMouvementBon)
        {
            ModelAjouterBonEnlevement model = new ModelAjouterBonEnlevement();
            
            model.ListeTypeBonEnlevement = (idMouvementBon == 1) ? BusinessGestionBonEnlevementGaz.Instance.GetListeTypeBonEnlevement() : BusinessGestionBonEnlevementGaz.Instance.GetListeTypeBonEnlevement(2);

            model.Date = DateTime.Now;
            string reference = GetNewCode();
            model.Reference = (reference == "1") ? "000001" : reference;

            model.idMouvementBon = BusinessGestionBonEnlevementGaz.Instance.GetMouvementParId(idMouvementBon).id;


            return View(model);
        }


        [HttpPost]
        public ActionResult Ajouter(ModelAjouterBonEnlevement model)
        {



            if (ModelState.IsValid)
            {

                BonEnlevementGazDto dto = new BonEnlevementGazDto
                {
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),

                    Reference = model.Reference,
                    Date = model.Date,
                    CodeClient = model.CodeClient,
                    idMouvementBon = model.idMouvementBon,
                    idTypeBonEnlevement = model.idTypeBonEnlevement,
                    ImmatriculeCamion = model.ImmatriculeCamion,
                    ChauffeurCamion = model.ChauffeurCamion,
                    ReferenceBonPesee = model.ReferenceBonPesee,
                    ReferenceDepotage = model.ReferenceDepotage,
                    Tonnage = model.Tonnage,

                    Commentaires = model.Commentaires,
                    etat = model.etat

                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

                BusinessGestionBonEnlevementGaz.Instance.Ajouter(dto);

                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }


        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModifierBonEnlevement model = new ModelModifierBonEnlevement();
           
            BonEnlevementGazDto dto = BusinessGestionBonEnlevementGaz.Instance.GetBonEnlevementParId(id);

            model.ListeMouvementBon = BusinessGestionBonEnlevementGaz.Instance.GetListeMouvementBonEnlevement();
            model.ListeTypeBon = (dto.idMouvementBon == 1) ? BusinessGestionBonEnlevementGaz.Instance.GetListeTypeBonEnlevement() : BusinessGestionBonEnlevementGaz.Instance.GetListeTypeBonEnlevement(2);



            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.idMouvementBon = dto.idMouvementBon;
                model.MouvementBon = dto.MouvementBon;
                model.idTypeBonEnlevement = dto.idTypeBonEnlevement;
                model.TypeBonEnlevement = dto.TypeBonEnlevement;
                model.CodeClient = dto.CodeClient;
                model.NomClient = dto.NomClient;

                model.ImmatriculeCamion = dto.ImmatriculeCamion;
                model.ChauffeurCamion = dto.ChauffeurCamion;
                model.ReferenceBonPesee = dto.ReferenceBonPesee;
                model.ReferenceDepotage = dto.ReferenceDepotage;
                model.Tonnage = dto.Tonnage;

                model.Commentaires = dto.Commentaires;
                model.etat = dto.etat;



            };
           

            return View(model);


        }



        [HttpPost]
        public ActionResult Modifier(ModelModifierBonEnlevement model)
        {


            if (ModelState.IsValid)
            {

                BonEnlevementGazDto dto = new BonEnlevementGazDto
                {

                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    id = model.id,
                    Reference = model.Reference,
                    Date = model.Date,
                    CodeClient = model.CodeClient,
                    idMouvementBon = model.idMouvementBon,
                    idTypeBonEnlevement = model.idTypeBonEnlevement,
                    ImmatriculeCamion = model.ImmatriculeCamion,
                    ChauffeurCamion = model.ChauffeurCamion,
                    ReferenceBonPesee = model.ReferenceBonPesee,
                    ReferenceDepotage = model.ReferenceDepotage,
                    Tonnage = model.Tonnage,

                    Commentaires = model.Commentaires,
                    etat = model.etat
                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

                BusinessGestionBonEnlevementGaz.Instance.Modifier(dto);

                return RedirectToAction("Index");
            }
            else
            {
                model.ListeMouvementBon = BusinessGestionBonEnlevementGaz.Instance.GetListeMouvementBonEnlevement();
                model.idMouvementBon = BusinessGestionBonEnlevementGaz.Instance.GetMouvementParId(model.idMouvementBon).id;
                model.MouvementBon = BusinessGestionBonEnlevementGaz.Instance.GetMouvementParId(model.idMouvementBon).libelle;

                model.ListeTypeBon = BusinessGestionBonEnlevementGaz.Instance.GetListeTypeBonEnlevement();
                model.idTypeBonEnlevement = BusinessGestionBonEnlevementGaz.Instance.GetTypeBonEnlevementParId(model.idTypeBonEnlevement).id;
                model.TypeBonEnlevement = BusinessGestionBonEnlevementGaz.Instance.GetTypeBonEnlevementParId(model.idTypeBonEnlevement).libelle;


                model.Date = DateTime.Now;
                return View(model);
            }

        }









        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {
                maxCode = db.BonEnlevementGazs.OrderByDescending(s => s.id).Select(x => x.Reference).FirstOrDefault();
                //maxCode = db.FicheConsignations.Select(x => x.Reference).Max();
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }

    }
}