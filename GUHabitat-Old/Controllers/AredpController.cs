﻿using BLL;
using CrystalDecisions.CrystalReports.Engine;
using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using GUHabitat.Reports;
using PagedList;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class AredpController : Controller
    {

        [HttpGet]
        public ActionResult Index(string filtreMotCle, int? page)
        {
            ModelListeAredp model = new ModelListeAredp();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeAredp = BusinessGestionAredp.Instance.ChercherListeAredp(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);


        }


        public ActionResult IndexParDate(string filtreMotCle, DateTime? dateDebut, DateTime? dateFin, int? page)
        {

            ModelListeAredpParClient model = new ModelListeAredpParClient();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            if (dateDebut == null) dateDebut = DateTime.Now.Date;
            if (dateFin == null) dateFin = DateTime.Now.Date;

            model.filtreMotCle = filtreMotCle;
            model.dateDebut = (DateTime)dateDebut;
            model.dateFin = (DateTime)dateFin;


            int pageSize = 20;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeAredpParClient = BusinessGestionAredp.Instance.ChercherListeAredp(model.filtreMotCle, model.dateDebut, model.dateFin, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);
        }



        [HttpGet]
        public ActionResult Acloturer(string filtreMotCle, int? page)
        {
            ModelListeAredp model = new ModelListeAredp();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeAredp = BusinessGestionAredp.Instance.ChercherListeAredpCloturer(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            
            ViewBag.nbInstance = BusinessGestionAredp.Instance.TotalCloturer();

            return View(model);


        }


        public void Selected(int[] ids)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            BusinessGestionAredp.Instance.Cloturer(ids);
        }

        public ActionResult EnInstance(int id)
        {


            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionAredp.Instance.EnInstance(idEntite, id);

            return RedirectToAction("Index");
        }


        public ActionResult Valider(int id)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionAredp.Instance.Valider(idEntite, id);

            return RedirectToAction("Index");
        }


        public ActionResult CloturerParDate(string date)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionAredp.Instance.CloturerParDate(idEntite, Convert.ToDateTime(date));

            return RedirectToAction("Index");
        }





        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelAjouterAredp model = new ModelAjouterAredp();


            AredpDto dto = BusinessGestionAredp.Instance.GetAredpParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.CodeClient = dto.CodeClient;
                model.NomClient = dto.NomClient;
              
                model.B12Afact = dto.B12Afact;
                model.B9Afact = dto.B9Afact;
                model.B6RAfact = dto.B6RAfact;
                model.B6VAfact = dto.B6VAfact;
                model.B3Afact = dto.B3Afact;
                model.B35Afact = dto.B35Afact;

                model.B12Nonfact = dto.B12Nonfact;
                model.B9Nonfact = dto.B9Nonfact;
                model.B6RNonfact = dto.B6RNonfact;
                model.B6VNonfact = dto.B6VNonfact;
                model.B3Nonfact = dto.B3Nonfact;
                model.B35Nonfact = dto.B35Nonfact;


                model.Commentaire = dto.Commentaire;
                model.etat = dto.etat;


            };
            return View(model);

        }







        [HttpGet]
        public ActionResult Ajouter()
        {
            ModelAjouterAredp model = new ModelAjouterAredp();
            string reference = GetNewCode();
            model.Reference = (reference == "1") ? "000001" : reference;
            model.Date = DateTime.Now;
            //model.ListeProduit = BusinessGestionAredp.Instance.GetListeProduit();
            return View(model);
        }


        [HttpPost]
        public JsonResult Ajouter(ModelAjouterAredp model)
        {
            bool status = true;
          
                AredpDto dto = new AredpDto
                {
                    Date = model.Date,
                    Reference = model.Reference,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                   
                    B12Afact = 0,
                    B9Afact =  0,
                    B6RAfact = 0,
                    B6VAfact = 0,
                    B3Afact =  0,
                    B35Afact = 0,

                    B12Nonfact = 0,
                    B9Nonfact =  0,
                    B6RNonfact = 0,
                    B6VNonfact = 0,
                    B3Nonfact =  0,
                    B35Nonfact = 0,

                    Commentaire = model.Commentaire,
                    ListeAredpDetails = model.ListeAredpDetails
                };

                
                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

                BusinessGestionAredp.Instance.Ajouter(dto, status);


                return new JsonResult { Data = new { status = status, message = "" } };
        }



        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModifierAredp model = new ModelModifierAredp();


            AredpDto dto = BusinessGestionAredp.Instance.GetAredpParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.CodeClient = dto.CodeClient;
                model.NomClient = dto.NomClient;
                
                model.B12Afact = dto.B12Afact;
                model.B9Afact = dto.B9Afact;
                model.B6RAfact = dto.B6RAfact;
                model.B6VAfact = dto.B6VAfact;
                model.B3Afact = dto.B3Afact;
                model.B35Afact = dto.B35Afact;

                model.B12Nonfact = dto.B12Nonfact;
                model.B9Nonfact = dto.B9Nonfact;
                model.B6RNonfact = dto.B6RNonfact;
                model.B6VNonfact = dto.B6VNonfact;
                model.B3Nonfact = dto.B3Nonfact;
                model.B35Nonfact = dto.B35Nonfact;


                model.Commentaire = dto.Commentaire;
                model.etat = dto.etat;


            };
            return View(model);

        }



        [HttpPost]
        public ActionResult Modifier(ModelModifierAredp model)
        {



            if (ModelState.IsValid)
            {

                AredpDto dto = new AredpDto
                {
                    id = model.id,
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    Date = model.Date,
                    Reference = model.Reference,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    
                    Commentaire = model.Commentaire




                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

              

                BusinessGestionAredp.Instance.Modifier(dto);


                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }



        }



        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {
                maxCode = db.AREDPs.OrderByDescending(s => s.id).Select(x => x.Reference).FirstOrDefault();
                //maxCode = db.FicheConsignations.Select(x => x.Reference).Max();
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }


        public ActionResult Imprimer(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {




                //rpt = new crpAredpA5();
                rpt = crpAredpA5.Instance;
                var dto = BusinessGestionAredp.Instance.GetAredpImprimerParId(id);

                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    //QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.com" + "\n" + dto.Reference, QRCodeGenerator.ECCLevel.Q);
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode("==> AREDP " + "\n\n" + "Référence :" + "\n\n" + dto.Reference + "\n\n" + "Date :" + "\n\n" + dto.Date.Date.ToShortDateString() + "\n\n" + "Code Client :" + "\n\n" + dto.CodeClient + "\n\n" + "Nom Client :" + "\n\n" + dto.NomClient + "\n\n" + "B12 :" + "\n\n" + dto.TotalB12 + "\n\n" + "B9 :" + "\n\n" + dto.TotalB9 + "\n\n" + "B6R :" + "\n\n" + dto.TotalB6R + "\n\n" + "B6V :" + "\n\n" + dto.TotalB6V + "\n\n" + "B3 :" + "\n\n" + dto.TotalB3 + "\n\n" + "B35 :" + "\n\n" + dto.TotalB35, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeImage.Save(ms, ImageFormat.Bmp);
                        dto.qrCodeImage = ms.ToArray();
                    }

                    List<AredpDto> list = new List<AredpDto>();
                    list.Add(dto);
                    rpt.SetDataSource(list);
                    Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

                    if (idEntite == 4 && (dto.TotalB12 != 0 || dto.TotalB9 != 0 || dto.TotalB6R != 0 || dto.TotalB6V != 0 || dto.TotalB3 != 0 || dto.TotalB35 != 0))
                    {
                        BusinessGestionAredp.Instance.Valider(idEntite, id);
                    }

                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            catch (Exception) { }
            return null;
        }



        public ActionResult ImprimerDetailsAredp(int id)
        {
            ReportClass rpt = null;
            try
            {

                //rpt = new crpAredpDetails();
                rpt =  crpAredpDetails.Instance;


                var dto = BusinessGestionAredp.Instance.GetAredpImprimerParId(id);
                var detailsDto = BusinessGestionAredp.Instance.GetAredpDetailsImprimerParId(id);
                List<ModelImprimerDetailsAredp> list = new List<ModelImprimerDetailsAredp>();
                foreach (AredpDetailsDto detailDto in detailsDto)
                {
                    ModelImprimerDetailsAredp item = new ModelImprimerDetailsAredp();

                    item.Reference = dto.Reference;
                    item.Date = dto.Date;
                    item.NumeroFacture = dto.NumeroFacture;
                    item.CodeClient = dto.CodeClient;
                    item.NomClient = dto.NomClient;
                    item.NomProduit = detailDto.NomProduit;
                    item.poidsLu = detailDto.poidsLu;
                    item.Tare = detailDto.Tare;
                    item.poidsManquant = detailDto.poidsManquant;
                    item.poidsRestant = detailDto.poidsRestant;
                    list.Add(item);
                }

                rpt.SetDataSource(list);
                Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception) { }
            return null;
        }
    }
}