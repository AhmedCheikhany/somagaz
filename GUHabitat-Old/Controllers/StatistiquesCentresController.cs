﻿using CrystalDecisions.CrystalReports.Engine;
using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using GUHabitat.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class StatistiquesCentresController : Controller
    {



        [HttpGet]
        public ActionResult SituationInterieurParCentre()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiqueInterieur> list = new List<ModelStatistiqueInterieur>();

                ModelStatistiqueInterieur model = new ModelStatistiqueInterieur();
                //model.ListeCentre = BusinessGestionStockCentre.Instance.GetListeCentre();

                //ViewBag.ListeCentre = new SelectList(model.ListeCentre, "Code", "libelle");


                return View(list);

            }


        }
        [HttpPost]
        public ActionResult SituationInterieurParCentre(int? CodeCentre, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiqueInterieur> list = new List<ModelStatistiqueInterieur>();

                ModelStatistiqueInterieur model = new ModelStatistiqueInterieur();

             

                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.CodeCentre = CodeCentre;
                var code = ViewBag.Code;



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;



                


                var dateStock = _context.StockCentres.Where(x => x.Date == dateDebut && x.CodeCentre != 0 && x.CodeCentre == CodeCentre).Select(x => x.Date).SingleOrDefault();


                model = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(CodeCentre,dateDebut, dateFin);



                list.Add(model);
                return View(list);
            }




        }





        [HttpGet]
        public ActionResult SituationGlobale()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiqueGlobale> list = new List<ModelStatistiqueGlobale>();

                ModelStatistiqueGlobale model = new ModelStatistiqueGlobale();



                return View(list);

            }


        }



        [HttpPost]
        public ActionResult SituationGlobale(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiqueGlobale> list = new List<ModelStatistiqueGlobale>();

                ModelStatistiqueGlobale model = new ModelStatistiqueGlobale();





               
                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
               




                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model = BusinessGestionStatistiques.Instance.GetStatistiqueGlobale(dateDebut, dateFin);


                //var dateStockInterieur = _context.StockCentres.Where(x => x.Date == dateDebut && x.CodeCentre != 0 && x.CodeCentre != 9).Select(x => x.Date).ToList();

                //var dateStock = _context.StockCentres.Where(x => x.Date == dateDebut && x.CodeCentre == 9).Select(x => x.Date).SingleOrDefault();

                list.Add(model);
                return View(list);
            }

           
        }




        [HttpGet]
        public ActionResult SituationCentreNKTT()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiqueCentreNKTT> list = new List<ModelStatistiqueCentreNKTT>();

                ModelStatistiqueCentreNKTT model = new ModelStatistiqueCentreNKTT();



                return View(list);

            }


        }



        [HttpPost]
        public ActionResult SituationCentreNKTT(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiqueCentreNKTT> list = new List<ModelStatistiqueCentreNKTT>();

                ModelStatistiqueCentreNKTT model = new ModelStatistiqueCentreNKTT();






                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;





                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model = BusinessGestionStatistiques.Instance.SituationCentreNktt(dateDebut, dateFin);


                //var dateStockInterieur = _context.StockCentres.Where(x => x.Date == dateDebut && x.CodeCentre != 0 && x.CodeCentre != 9).Select(x => x.Date).ToList();

                //var dateStock = _context.StockCentres.Where(x => x.Date == dateDebut && x.CodeCentre == 9).Select(x => x.Date).SingleOrDefault();

                list.Add(model);
                return View(list);
            }


        }







        public ActionResult Imprimer(DateTime? dateDebut, DateTime? dateFin)
        {

            ReportClass rpt = null;
            try
            {




                //rpt = new crpSituationGlobale();
                rpt = crpSituationGlobale.Instance;
                var dto = BusinessGestionStatistiques.Instance.GetStatistiqueGlobale(dateDebut,dateFin);







                List<ModelStatistiqueGlobale> list = new List<ModelStatistiqueGlobale>();
                list.Add(dto);
                rpt.SetDataSource(list);
                Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);


                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception) { }
            return null;
        }

        public ActionResult ImprimerCentreNKTT(DateTime? dateDebut, DateTime? dateFin)
        {

            ReportClass rpt = null;
            try
            {




                //rpt = new crpSituationCentreNktt();
                rpt = crpSituationCentreNktt.Instance;
                var dto = BusinessGestionStatistiques.Instance.SituationCentreNktt(dateDebut, dateFin);







                List<ModelStatistiqueCentreNKTT> list = new List<ModelStatistiqueCentreNKTT>();
                list.Add(dto);
                rpt.SetDataSource(list);
                Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);


                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception) { }
            return null;
        }



        public ActionResult ImprimerCentre(int? CodeCentre, DateTime? dateDebut, DateTime? dateFin)
        {

            ReportClass rpt = null;
            try
            {




                //rpt = new crpSituationParCentreInterieur();
                rpt = crpSituationParCentreInterieur.Instance;
                var dto = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(CodeCentre,dateDebut, dateFin);







                List<ModelStatistiqueInterieur> list = new List<ModelStatistiqueInterieur>();
                list.Add(dto);
                rpt.SetDataSource(list);
                Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);


                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception) { }
            return null;
        }



    }
}