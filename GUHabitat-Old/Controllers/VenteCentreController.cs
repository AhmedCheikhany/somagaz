﻿using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class VenteCentreController : Controller
    {

        [HttpPost]
        public ActionResult ValiderTypeVenteCentre(int CodeCentre)
        {
            return RedirectToAction("Ajouter", new { CodeCentre = CodeCentre });
        }



        // GET: VenteCentre
        public ActionResult Index(string filtreMotCle, int? page)
        {

            ModelListeVenteCentre model = new ModelListeVenteCentre();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeVenteCentre = BusinessGestionVenteCentre.Instance.ChercherListeVenteCentre(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);
        }



        public ActionResult EnInstance(int id)
        {


            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionVenteCentre.Instance.EnInstance(idEntite, id);

            return RedirectToAction("Index");
        }



        public ActionResult Valider(int id)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionVenteCentre.Instance.Valider(idEntite, id);


            return RedirectToAction("Index");
        }








        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelAjouterVenteCentre  model = new ModelAjouterVenteCentre();


            VenteCentreDto dto = BusinessGestionVenteCentre.Instance.GetVenteCentreParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.CodeCentre = dto.CodeCentre;
                model.LibelleCentre = dto.LibelleCentre;
               
               model.B12Revendeur = dto.B12Revendeur;
               model.B9Revendeur = dto.B9Revendeur;
               model.B6RRevendeur = dto.B6RRevendeur;
               model.B6VRevendeur = dto.B6VRevendeur;
               model.B3Revendeur = dto.B3Revendeur;
               model.B35Revendeur = dto.B35Revendeur;
              
               model.B12PrixRevendeur = dto.B12PrixRevendeur;
               model.B9PrixRevendeur = dto.B9PrixRevendeur;
               model.B6RPrixRevendeur = dto.B6RPrixRevendeur;
               model.B6VPrixRevendeur = dto.B6VPrixRevendeur;
               model.B3PrixRevendeur = dto.B3PrixRevendeur;
               model.B35PrixRevendeur = dto.B35PrixRevendeur;
              
               model.B12Consommateur = dto.B12Consommateur;
               model.B9Consommateur = dto.B9Consommateur;
               model.B6RConsommateur = dto.B6RConsommateur;
               model.B6VConsommateur = dto.B6VConsommateur;
               model.B3Consommateur = dto.B3Consommateur;
               model.B35Consommateur = dto.B35Consommateur;
            
               model.B12PrixConsommateur = dto.B12PrixConsommateur;
               model.B9PrixConsommateur = dto.B9PrixConsommateur;
               model.B6RPrixConsommateur = dto.B6RPrixConsommateur;
               model.B6VPrixConsommateur = dto.B6VPrixConsommateur;
               model.B3PrixConsommateur = dto.B3PrixConsommateur;
               model.B35PrixConsommateur = dto.B35PrixConsommateur;
              
               model.TotalPrixB12Consommateur = dto.TotalPrixB12Consommateur;
               model.TotalPrixB9Consommateur = dto.TotalPrixB9Consommateur;
               model.TotalPrixB6RConsommateur = dto.TotalPrixB6RConsommateur;
               model.TotalPrixB6VConsommateur = dto.TotalPrixB6VConsommateur;
               model.TotalPrixB3Consommateur = dto.TotalPrixB3Consommateur;
               model.TotalPrixB35Consommateur = dto.TotalPrixB35Consommateur;
              
               model.TotalPrixB12Revendeur = dto.TotalPrixB12Revendeur;
               model.TotalPrixB9Revendeur = dto.TotalPrixB9Revendeur;
               model.TotalPrixB6RRevendeur = dto.TotalPrixB6RRevendeur;
               model.TotalPrixB6VRevendeur = dto.TotalPrixB6VRevendeur;
               model.TotalPrixB3Revendeur = dto.TotalPrixB3Revendeur;
               model.TotalPrixB35Revendeur = dto.TotalPrixB35Revendeur;
             
               model.TotalMontantConsommateur = dto.TotalMontantConsommateur;
               model.TotalMontantRevendeur = dto.TotalMontantRevendeur;
            
               model.Commentaire = dto.Commentaire;
              
               model.etat = dto.etat;



            };
            return View(model);

        }



        public ActionResult Ajouter(int CodeCentre)
        {
            ModelAjouterVenteCentre model = new ModelAjouterVenteCentre();
            model.Date = DateTime.Now;

            string reference = GetNewCode();

            model.CodeCentre = BusinessGestionVenteCentre.Instance.GetCentreParCode(CodeCentre).Code;

            model.Reference = (reference == "1") ? "000001" : reference;


            switch (CodeCentre)
            {
                case 1:
                    model.B12PrixRevendeur = 2878;
                    model.B6RPrixRevendeur = 1383;
                    model.B6VPrixRevendeur = 1383;
                    model.B3PrixRevendeur = 635;
                    model.B35PrixRevendeur = 8070;

                    model.B12PrixConsommateur = 3195;
                    model.B6RPrixConsommateur = 1535;
                    model.B6VPrixConsommateur = 1535;
                    model.B3PrixConsommateur = 705;
                    model.B35PrixConsommateur = 8950;

                    break;

                case 2:

                    model.B12PrixRevendeur = 2980;
                    model.B6RPrixRevendeur = 1430;
                    model.B6VPrixRevendeur = 1430;
                    model.B3PrixRevendeur = 650;
                    model.B35PrixRevendeur = 8910;

                    model.B12PrixConsommateur = 3180;
                    model.B6RPrixConsommateur = 1530;
                    model.B6VPrixConsommateur = 1530;
                    model.B3PrixConsommateur = 700;
                    model.B35PrixConsommateur = 8910;

                    break;


                case 3:

                    model.B12PrixRevendeur = 3125;
                    model.B6RPrixRevendeur = 1495;
                    model.B6VPrixRevendeur = 1495;
                    model.B3PrixRevendeur = 685;
                    model.B35PrixRevendeur = 9310;

                    model.B12PrixConsommateur = 3325;
                    model.B6RPrixConsommateur = 1595;
                    model.B6VPrixConsommateur = 1595;
                    model.B3PrixConsommateur = 735;
                    model.B35PrixConsommateur = 9310;

                    break;

                case 4:

                    model.B12PrixRevendeur = 2905;
                    model.B6RPrixRevendeur = 1390;
                    model.B6VPrixRevendeur = 1390;
                    model.B3PrixRevendeur = 630;
                    model.B35PrixRevendeur = 8700;

                    model.B12PrixConsommateur = 3105;
                    model.B6RPrixConsommateur = 1490;
                    model.B6VPrixConsommateur = 1490;
                    model.B3PrixConsommateur = 680;
                    model.B35PrixConsommateur = 8700;

                    break;


                case 5:

                    model.B12PrixRevendeur = 3080;
                    model.B6RPrixRevendeur = 1480;
                    model.B6VPrixRevendeur = 1480;
                    model.B3PrixRevendeur = 675;
                    model.B35PrixRevendeur = 9220;

                    model.B12PrixConsommateur = 3280;
                    model.B6RPrixConsommateur = 1580;
                    model.B6VPrixConsommateur = 1580;
                    model.B3PrixConsommateur = 725;
                    model.B35PrixConsommateur = 9220;

                    break;


                case 6:

                    model.B12PrixRevendeur = 3040;
                    model.B6RPrixRevendeur = 1460;
                    model.B6VPrixRevendeur = 1460;
                    model.B3PrixRevendeur = 665;
                    model.B35PrixRevendeur = 9080;

                    model.B12PrixConsommateur = 3240;
                    model.B6RPrixConsommateur = 1560;
                    model.B6VPrixConsommateur = 1560;
                    model.B3PrixConsommateur = 715;
                    model.B35PrixConsommateur = 9080;

                    break;

                case 7:

                    model.B12PrixRevendeur = 3130;
                    model.B6RPrixRevendeur = 1505;
                    model.B6VPrixRevendeur = 1505;
                    model.B3PrixRevendeur = 685;
                    model.B35PrixRevendeur = 9362;

                    model.B12PrixConsommateur = 3330;
                    model.B6RPrixConsommateur = 1605;
                    model.B6VPrixConsommateur = 1605;
                    model.B3PrixConsommateur = 735;
                    model.B35PrixConsommateur = 9362;

                    break;

                case 8:

                    model.B12PrixRevendeur = 3240;
                    model.B6RPrixRevendeur = 1560;
                    model.B6VPrixRevendeur = 1560;
                    model.B3PrixRevendeur = 710;
                    model.B35PrixRevendeur = 9640;

                    model.B12PrixConsommateur = 3440;
                    model.B6RPrixConsommateur = 1660;
                    model.B6VPrixConsommateur = 1660;
                    model.B3PrixConsommateur = 760;
                    model.B35PrixConsommateur = 9640;

                    break;

            }


            return View(model);
        }




        [HttpPost]
        public ActionResult Ajouter(ModelAjouterVenteCentre model)
        {



            if (ModelState.IsValid)
            {

                VenteCentreDto dto = new VenteCentreDto
                {
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),

                    Reference = model.Reference,
                    Date = model.Date,
                    CodeCentre = model.CodeCentre,            
                    B12Revendeur = model.B12Revendeur,
                    B9Revendeur = model.B9Revendeur,
                    B6RRevendeur = model.B6RRevendeur,
                    B6VRevendeur = model.B6VRevendeur,
                    B3Revendeur = model.B3Revendeur,
                    B35Revendeur = model.B35Revendeur,

                    B12PrixRevendeur = model.B12PrixRevendeur,
                    B9PrixRevendeur = model.B9PrixRevendeur,
                    B6RPrixRevendeur = model.B6RPrixRevendeur,
                    B6VPrixRevendeur = model.B6VPrixRevendeur,
                    B3PrixRevendeur = model.B3PrixRevendeur,
                    B35PrixRevendeur = model.B35PrixRevendeur,


                    B12Consommateur = model.B12Consommateur,
                    B9Consommateur =  model.B9Consommateur,
                    B6RConsommateur = model.B6RConsommateur,
                    B6VConsommateur = model.B6VConsommateur,
                    B3Consommateur =  model.B3Consommateur,
                    B35Consommateur = model.B35Consommateur,


                    B12PrixConsommateur = model.B12PrixConsommateur,
                    B9PrixConsommateur =  model.B9PrixConsommateur,
                    B6RPrixConsommateur = model.B6RPrixConsommateur,
                    B6VPrixConsommateur = model.B6VPrixConsommateur,
                    B3PrixConsommateur =  model.B3PrixConsommateur,
                    B35PrixConsommateur = model.B35PrixConsommateur,


                    TotalPrixB12Consommateur = (model.B12Consommateur * model.B12PrixConsommateur),
                    TotalPrixB9Consommateur = (model.B9Consommateur * model.B9PrixConsommateur),
                    TotalPrixB6RConsommateur = (model.B6RConsommateur * model.B6RPrixConsommateur),
                    TotalPrixB6VConsommateur = (model.B6VConsommateur * model.B6VPrixConsommateur),
                    TotalPrixB3Consommateur = (model.B3Consommateur * model.B3PrixConsommateur),
                    TotalPrixB35Consommateur = (model.B35Consommateur * model.B35PrixConsommateur),

                    TotalPrixB12Revendeur = (model.B12Revendeur * model.B12PrixRevendeur),
                    TotalPrixB9Revendeur = (model.B9Revendeur * model.B9PrixRevendeur),
                    TotalPrixB6RRevendeur = (model.B6RRevendeur * model.B6RPrixRevendeur),
                    TotalPrixB6VRevendeur = (model.B6VRevendeur * model.B6VPrixRevendeur),
                    TotalPrixB3Revendeur = (model.B3Revendeur * model.B3PrixRevendeur),
                    TotalPrixB35Revendeur = (model.B35Revendeur * model.B35PrixRevendeur),


                    TotalMontantConsommateur = (model.TotalPrixB12Consommateur + model.TotalPrixB9Consommateur + model.TotalPrixB6RConsommateur + model.TotalPrixB6VConsommateur + model.TotalPrixB3Consommateur + model.TotalPrixB35Consommateur),
                    TotalMontantRevendeur = (model.TotalPrixB12Revendeur + model.TotalPrixB9Revendeur + model.TotalPrixB6RRevendeur + model.TotalPrixB6VRevendeur + model.TotalPrixB3Revendeur + model.TotalPrixB35Revendeur),



                    Commentaire = model.Commentaire,

                    etat = model.etat

                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);


                //decimal Tonnage = (decimal)((dto.B12Consommateur + dto.B12Revendeur) * 12.5 + (dto.B9Consommateur + dto.B9Revendeur) * 9 +(dto.B6RConsommateur + dto.B6RRevendeur) * 6 + (dto.B6VConsommateur + dto.B6VRevendeur) * 6 + (dto.B3Consommateur + dto.B3Revendeur) * 3 + (dto.B35Consommateur + dto.B35Revendeur) * 35) / 1000;

                //var StockActuel = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).StockActuel;
                //if (Tonnage > StockActuel)
                //{
                //    ViewBag.MessageStockAjout = "Le Tonnage non disponible";
                //    model.Date = DateTime.Now;
                //    return View(model);
                //}



                BusinessGestionVenteCentre.Instance.Ajouter(dto);





                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }





        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModifierVenteCentre model = new ModelModifierVenteCentre();
            model.ListeCentre = BusinessGestionVenteCentre.Instance.GetListeCentre();


            VenteCentreDto dto = BusinessGestionVenteCentre.Instance.GetVenteCentreParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.CodeCentre = dto.CodeCentre;
                model.LibelleCentre = dto.LibelleCentre;

                model.B12Revendeur = dto.B12Revendeur;
                model.B9Revendeur = dto.B9Revendeur;
                model.B6RRevendeur = dto.B6RRevendeur;
                model.B6VRevendeur = dto.B6VRevendeur;
                model.B3Revendeur = dto.B3Revendeur;
                model.B35Revendeur = dto.B35Revendeur;

                model.B12PrixRevendeur = dto.B12PrixRevendeur;
                model.B9PrixRevendeur = dto.B9PrixRevendeur;
                model.B6RPrixRevendeur = dto.B6RPrixRevendeur;
                model.B6VPrixRevendeur = dto.B6VPrixRevendeur;
                model.B3PrixRevendeur = dto.B3PrixRevendeur;
                model.B35PrixRevendeur = dto.B35PrixRevendeur;

                model.B12Consommateur = dto.B12Consommateur;
                model.B9Consommateur = dto.B9Consommateur;
                model.B6RConsommateur = dto.B6RConsommateur;
                model.B6VConsommateur = dto.B6VConsommateur;
                model.B3Consommateur = dto.B3Consommateur;
                model.B35Consommateur = dto.B35Consommateur;

                model.B12PrixConsommateur = dto.B12PrixConsommateur;
                model.B9PrixConsommateur = dto.B9PrixConsommateur;
                model.B6RPrixConsommateur = dto.B6RPrixConsommateur;
                model.B6VPrixConsommateur = dto.B6VPrixConsommateur;
                model.B3PrixConsommateur = dto.B3PrixConsommateur;
                model.B35PrixConsommateur = dto.B35PrixConsommateur;

                model.TotalPrixB12Consommateur = dto.TotalPrixB12Consommateur;
                model.TotalPrixB9Consommateur = dto.TotalPrixB9Consommateur;
                model.TotalPrixB6RConsommateur = dto.TotalPrixB6RConsommateur;
                model.TotalPrixB6VConsommateur = dto.TotalPrixB6VConsommateur;
                model.TotalPrixB3Consommateur = dto.TotalPrixB3Consommateur;
                model.TotalPrixB35Consommateur = dto.TotalPrixB35Consommateur;

                model.TotalPrixB12Revendeur = dto.TotalPrixB12Revendeur;
                model.TotalPrixB9Revendeur = dto.TotalPrixB9Revendeur;
                model.TotalPrixB6RRevendeur = dto.TotalPrixB6RRevendeur;
                model.TotalPrixB6VRevendeur = dto.TotalPrixB6VRevendeur;
                model.TotalPrixB3Revendeur = dto.TotalPrixB3Revendeur;
                model.TotalPrixB35Revendeur = dto.TotalPrixB35Revendeur;

                model.TotalMontantConsommateur = dto.TotalMontantConsommateur;
                model.TotalMontantRevendeur = dto.TotalMontantRevendeur;

                model.Commentaire = dto.Commentaire;

                model.etat = dto.etat;



            };
            return View(model);


        }




        [HttpPost]
        public ActionResult Modifier(ModelModifierVenteCentre model)
        {


            if (ModelState.IsValid)
            {

                VenteCentreDto dto = new VenteCentreDto
                {

                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    id = model.id,
                    Reference = model.Reference,
                    Date = model.Date,
                    CodeCentre = model.CodeCentre,
                    LibelleCentre = model.LibelleCentre,

                    B12Revendeur = model.B12Revendeur,
                    B9Revendeur = model.B9Revendeur,
                    B6RRevendeur = model.B6RRevendeur,
                    B6VRevendeur = model.B6VRevendeur,
                    B3Revendeur = model.B3Revendeur,
                    B35Revendeur = model.B35Revendeur,

                    B12PrixRevendeur = model.B12PrixRevendeur,
                    B9PrixRevendeur = model.B9PrixRevendeur,
                    B6RPrixRevendeur = model.B6RPrixRevendeur,
                    B6VPrixRevendeur = model.B6VPrixRevendeur,
                    B3PrixRevendeur = model.B3PrixRevendeur,
                    B35PrixRevendeur = model.B35PrixRevendeur,


                    B12Consommateur = model.B12Consommateur,
                    B9Consommateur = model.B9Consommateur,
                    B6RConsommateur = model.B6RConsommateur,
                    B6VConsommateur = model.B6VConsommateur,
                    B3Consommateur = model.B3Consommateur,
                    B35Consommateur = model.B35Consommateur,


                    B12PrixConsommateur = model.B12PrixConsommateur,
                    B9PrixConsommateur = model.B9PrixConsommateur,
                    B6RPrixConsommateur = model.B6RPrixConsommateur,
                    B6VPrixConsommateur = model.B6VPrixConsommateur,
                    B3PrixConsommateur = model.B3PrixConsommateur,
                    B35PrixConsommateur = model.B35PrixConsommateur,


                    TotalPrixB12Consommateur = (model.B12Consommateur * model.B12PrixConsommateur),
                    TotalPrixB9Consommateur =  (model.B9Consommateur * model.B9PrixConsommateur),
                    TotalPrixB6RConsommateur = (model.B6RConsommateur * model.B6RPrixConsommateur),
                    TotalPrixB6VConsommateur = (model.B6VConsommateur * model.B6VPrixConsommateur),
                    TotalPrixB3Consommateur =  (model.B3Consommateur * model.B3PrixConsommateur),
                    TotalPrixB35Consommateur = (model.B35Consommateur * model.B35PrixConsommateur),

                    TotalPrixB12Revendeur = (model.B12Revendeur * model.B12PrixRevendeur),
                    TotalPrixB9Revendeur =  (model.B9Revendeur * model.B9PrixRevendeur),
                    TotalPrixB6RRevendeur = (model.B6RRevendeur * model.B6RPrixRevendeur),
                    TotalPrixB6VRevendeur = (model.B6VRevendeur * model.B6VPrixRevendeur),
                    TotalPrixB3Revendeur =  (model.B3Revendeur * model.B3PrixRevendeur),
                    TotalPrixB35Revendeur = (model.B35Revendeur * model.B35PrixRevendeur),


                    TotalMontantConsommateur = (model.TotalPrixB12Consommateur + model.TotalPrixB9Consommateur + model.TotalPrixB6RConsommateur + model.TotalPrixB6VConsommateur + model.TotalPrixB3Consommateur + model.TotalPrixB35Consommateur),
                    TotalMontantRevendeur = (model.TotalPrixB12Revendeur + model.TotalPrixB9Revendeur + model.TotalPrixB6RRevendeur + model.TotalPrixB6VRevendeur + model.TotalPrixB3Revendeur + model.TotalPrixB35Revendeur),



                    Commentaire = model.Commentaire,

                    etat = model.etat

                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);


                //decimal Tonnage = (decimal)((dto.B12Consommateur + dto.B12Revendeur) * 12.5 + (dto.B9Consommateur + dto.B9Revendeur) * 9 + (dto.B6RConsommateur + dto.B6RRevendeur) * 6 + (dto.B6VConsommateur + dto.B6VRevendeur) * 6 + (dto.B3Consommateur + dto.B3Revendeur) * 3 + (dto.B35Consommateur + dto.B35Revendeur) * 35) / 1000;

                //var StockActuel = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).StockActuel;
                //if (Tonnage > StockActuel)
                //{
                //    ViewBag.MessageStockModifier = "Le Tonnage non disponible";

                //    model.ListeCentre = BusinessGestionVenteCentre.Instance.GetListeCentre();
                //    model.CodeCentre = BusinessGestionVenteCentre.Instance.GetCentreParCode(model.CodeCentre).Code;
                //    model.LibelleCentre = BusinessGestionVenteCentre.Instance.GetCentreParCode(model.CodeCentre).libelle;

                //    model.Date = DateTime.Now;
                //    return View(model);
                //}



                BusinessGestionVenteCentre.Instance.Modifier(dto);





                return RedirectToAction("Index");
            }
            else
            {
                model.ListeCentre = BusinessGestionVenteCentre.Instance.GetListeCentre();
                model.CodeCentre =  BusinessGestionVenteCentre.Instance.GetCentreParCode(model.CodeCentre).Code;
                model.LibelleCentre = BusinessGestionVenteCentre.Instance.GetCentreParCode(model.CodeCentre).libelle;


                model.Date = DateTime.Now;
                return View(model);
            }

        }







        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {

                maxCode = db.VenteCentres.OrderByDescending(s => s.id).Select(x => x.Reference).FirstOrDefault();


            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }
    }
}