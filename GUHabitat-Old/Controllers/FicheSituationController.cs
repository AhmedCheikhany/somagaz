﻿using CrystalDecisions.CrystalReports.Engine;
using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using GUHabitat.Reports;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class FicheSituationController : Controller
    {
        
        // GET: FicheSituation
        [HttpGet]
        public ActionResult Index(string filtreMotCle, int? page)
        {
            ModelListeFicheSituation model = new ModelListeFicheSituation();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheSituation = BusinessGestionFicheSituation.Instance.ChercherListeFicheSituation(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);


        }



        [HttpGet]
        public ActionResult Acloturer(string filtreMotCle, int? page)
        {
            ModelListeFicheSituation model = new ModelListeFicheSituation();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheSituation = BusinessGestionFicheSituation.Instance.ChercherListeFicheSituationCloturer(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            ViewBag.nbInstance = BusinessGestionFicheSituation.Instance.TotalCloturer();
            return View(model);

        }

        public void Selected(int[] ids)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            BusinessGestionFicheSituation.Instance.Cloturer(ids);
        }


        public void AjoutGainPerte(int id)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.FicheSituations.Find(id);
                entity.GainPerte =(decimal)BusinessGestionFicheSituation.Instance.CalculerGainPerte((int)entity.reference,entity.id);
                _context.SaveChanges();

            }
            
        }


        public ActionResult EnInstance(int id)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            BusinessGestionFicheSituation.Instance.EnInstance(id);

            return RedirectToAction("Index");
        }




        public ActionResult Valider(int id)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            BusinessGestionFicheSituation.Instance.Valider(id);

            return RedirectToAction("Index");
        }



        [HttpGet]
        public ActionResult Details(int id)
        {
            AjoutGainPerte(id);
           
            ModelAjouterFicheSituation model = new ModelAjouterFicheSituation();

            FicheSituationDto dto = BusinessGestionFicheSituation.Instance.GetFicheSituationParId(id);
          

            if (dto != null)
            {

                model.id = dto.id;
                model.Date = dto.Date;
                model.Reference = dto.Reference;
                model.StockNKTT = dto.StockNKTT;
                model.Depotage = dto.Depotage;
                model.StockInterieur = dto.StockInterieur;
                model.TotalStock = dto.TotalStock;
                model.VenteConditionneeNKTT = dto.VenteConditionneeNKTT;
                model.VenteCentresInterieur = dto.VenteCentresInterieur;
                model.VenteVrac = dto.VenteVrac;
                model.TotalVente = dto.TotalVente;
                model.TransfertCentres = dto.TransfertCentres;
                model.TransfertPartenaires = dto.TransfertPartenaires;
                model.TotalTransfert = dto.TotalTransfert;
                model.RecetteNKTT = dto.RecetteNKTT;
                model.RecetteInterieur = dto.RecetteInterieur;
                model.TotalRecette = dto.TotalRecette;
                model.Versement = dto.Versement;
                model.VersementInterieur = dto.VersementInterieur;
                model.TotalVersement = dto.TotalVersement;

                model.AutreReglement = dto.AutreReglement;
                model.PretsAceJour = dto.PretsAceJour;
                model.GainPerte = dto.GainPerte;

                model.Commentaire = dto.Commentaire;
                model.etat = dto.etat;
                model.etatString = dto.etatString;

            };
            return View(model);

        }


       





        [HttpGet]
        public ActionResult Ajouter()
        {
            ModelAjouterFicheSituation model = new ModelAjouterFicheSituation();
            model.Date = DateTime.Now;
            model.Reference = Convert.ToInt32( GetNewCode());
            //var reference = BusinessGestionFicheSituation.Instance.GetReference();

            //model.Reference = ++ reference;
            
            


            return View(model);
        }


        [HttpPost]
        public ActionResult Ajouter(ModelAjouterFicheSituation model)
        {


            if (ModelState.IsValid)
            {

                FicheSituationDto dto = new FicheSituationDto
                {
                    Date = model.Date,
                    Reference = model.Reference,
                    StockNKTT = model.StockNKTT,
                    Depotage = model.Depotage,
                    StockInterieur = model.StockInterieur,
                    TotalStock = (model.StockNKTT + model.StockInterieur),
                    VenteConditionneeNKTT = model.VenteConditionneeNKTT,
                    VenteCentresInterieur = model.VenteCentresInterieur,
                    VenteVrac = model.VenteVrac,
                    TotalVente = (model.VenteConditionneeNKTT + model.VenteCentresInterieur + model.VenteVrac),
                    TransfertCentres = model.TransfertCentres,
                    TransfertPartenaires = model.TransfertPartenaires,
                    TotalTransfert = (model.TransfertCentres + model.TransfertPartenaires),
                    GainPerte = 0,
                    RecetteNKTT = model.RecetteNKTT,
                    RecetteInterieur = model.RecetteInterieur,
                    TotalRecette = (model.RecetteNKTT + model.RecetteInterieur),
                    Versement = model.Versement,
                    VersementInterieur = model.VersementInterieur,
                    TotalVersement = (model.Versement + model.VersementInterieur),
                    AutreReglement = model.AutreReglement,
                    //PretsAceJour = model.PretsAceJour,

                    Commentaire = model.Commentaire




                };



                var nbrefiche = BusinessGestionFicheSituation.Instance.GetFicheSituationParDate(model.Date);
                if (nbrefiche >= 1)
                {
                    ViewBag.MessageAjouter = "Cette fiche est deja saisie";

                    return View(model);
                }

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);


                BusinessGestionFicheSituation.Instance.Ajouter(dto);

                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }




        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModifierFicheSituation model = new ModelModifierFicheSituation();

            FicheSituationDto dto = BusinessGestionFicheSituation.Instance.GetFicheSituationParId(id);

            if (dto != null)
            {
                    model.id = dto.id;
                    model.Date = dto.Date;
                    model.Reference = dto.Reference;
                    model.StockNKTT = dto.StockNKTT;
                    model.Depotage = dto.Depotage;
                    model.StockInterieur = dto.StockInterieur;
                    model.TotalStock = dto.TotalStock;
                    model.VenteConditionneeNKTT = dto.VenteConditionneeNKTT;
                    model.VenteCentresInterieur = dto.VenteCentresInterieur;
                    model.VenteVrac = dto.VenteVrac;
                    model.TotalVente = dto.TotalVente;
                    model.TransfertCentres = dto.TransfertCentres;
                    model.TransfertPartenaires = dto.TransfertPartenaires;
                    model.TotalTransfert = dto.TotalTransfert;
                    model.GainPerte = dto.GainPerte;
                    model.RecetteNKTT = dto.RecetteNKTT;
                    model.RecetteInterieur = dto.RecetteInterieur;
                    model.TotalRecette = dto.TotalRecette;
                    model.Versement = dto.Versement;
                    model.VersementInterieur = dto.VersementInterieur;
                    model.TotalVersement = dto.TotalVersement;

                    model.AutreReglement = dto.AutreReglement;
                    model.PretsAceJour = dto.PretsAceJour;

                    model.Commentaire = dto.Commentaire;
                    model.etat = dto.etat;


            };


            return View(model);
        }



        [HttpPost]
        public ActionResult Modifier(ModelModifierFicheSituation model)
        {



            if (ModelState.IsValid)
            {

                FicheSituationDto dto = new FicheSituationDto
                {

                    id = model.id,
                    Date = model.Date,
                    Reference = model.Reference,
                    StockNKTT = model.StockNKTT,
                    Depotage = model.Depotage,
                    StockInterieur = model.StockInterieur,
                    TotalStock = (model.StockNKTT + model.StockInterieur),
                    VenteConditionneeNKTT = model.VenteConditionneeNKTT,
                    VenteCentresInterieur = model.VenteCentresInterieur,
                    VenteVrac = model.VenteVrac,
                    TotalVente = (model.VenteConditionneeNKTT + model.VenteCentresInterieur + model.VenteVrac),
                    TransfertCentres = model.TransfertCentres,
                    TransfertPartenaires = model.TransfertPartenaires,
                    TotalTransfert = (model.TransfertCentres + model.TransfertPartenaires),
                    GainPerte = BusinessGestionFicheSituation.Instance.CalculerGainPerte(model.Reference, model.id),
                    RecetteNKTT = model.RecetteNKTT,
                    RecetteInterieur = model.RecetteInterieur,
                    TotalRecette = (model.RecetteNKTT + model.RecetteInterieur),
                    Versement = model.Versement,
                    VersementInterieur = model.VersementInterieur,
                    TotalVersement = (model.Versement + model.VersementInterieur),
                    AutreReglement = model.AutreReglement,
                    //PretsAceJour = model.PretsAceJour,

                    Commentaire = model.Commentaire


                };

               
                var nbrefiche = BusinessGestionFicheSituation.Instance.GetFicheSituationParDate(model.Date);
                if (nbrefiche > 1)
                {
                    ViewBag.MessageModifier = "Cette fiche est deja saisie";

                    return View(model);
                }

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

                BusinessGestionFicheSituation.Instance.Modifier(dto);


                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }



        }




        string GetNewCode()
        {

            
            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {
                //int reference;
                maxCode = Convert.ToString( db.FicheSituations.OrderByDescending(s => s.id).Select(x => x.reference).FirstOrDefault());
                //reference = db.FicheSituations.Select(x => x.reference).Max();
                //maxCode = Convert.ToString(reference);
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }





        public ActionResult Imprimer(int id)
        {
          
            ReportClass rpt = null;
            try
            {




                //rpt = new crpFicheSituation();
                rpt =  crpFicheSituation.Instance;
                var dto = BusinessGestionFicheSituation.Instance.GetFicheSituationImprimerParId(id);

              





                List<FicheSituationDto> list = new List<FicheSituationDto>();
                list.Add(dto);
                rpt.SetDataSource(list);
                Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);


                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception) { }
            return null;
        }



    }
}