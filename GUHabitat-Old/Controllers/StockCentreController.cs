﻿using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class StockCentreController : Controller
    {
       
        [HttpPost]
        public ActionResult ValiderStockCentre(int CodeCentreStock)
        {
            return RedirectToAction("Ajouter", new { CodeCentreStock = CodeCentreStock });
        }



        // GET: StockCentre
        public ActionResult Index(string filtreMotCle, int? page)
        {

            ModelListeStockCentre model = new ModelListeStockCentre();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeStockCentre = BusinessGestionStockCentre.Instance.ChercherListeStockCentre(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);
        }



        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelAjouterStockCentre model = new ModelAjouterStockCentre();


            StockCentreDto dto = BusinessGestionStockCentre.Instance.GetStockCentreParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.reference = dto.reference;
                model.Date = dto.Date;
                model.CodeCentre = dto.CodeCentre;
                model.LibelleCentre = dto.LibelleCentre;

                model.StockTheorie = dto.StockTheorie;
                model.Stock = dto.Stock;
                model.StockFin = dto.StockFin;
                model.Depotage = dto.Depotage;
                model.GainPerte = dto.GainPerte;
                model.Commentaire = dto.Commentaire;

                model.etat = dto.etat;



            };
            return View(model);

        }



        public ActionResult EnInstance(int id)
        {


            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionStockCentre.Instance.EnInstance(idEntite, id);

            return RedirectToAction("Index");
        }



        public ActionResult Valider(int id)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionStockCentre.Instance.Valider(idEntite, id);


            return RedirectToAction("Index");
        }




        [HttpGet]
        public ActionResult Ajouter(int CodeCentreStock)
        {

            ModelAjouterStockCentre model = new ModelAjouterStockCentre();
            model.Date = DateTime.Now;

            model.reference = Convert.ToInt32(GetNewCode());

            model.CodeCentre = BusinessGestionStockCentre.Instance.GetCentreParCode(CodeCentreStock).Code;

            model.Stock = Convert.ToDecimal(BusinessGestionStockCentre.Instance.StockDeut(model.CodeCentre).ToString("0.000"));
        


            return View(model);
        }




        [HttpPost]
        public ActionResult Ajouter(ModelAjouterStockCentre model)
        {


            if (ModelState.IsValid)
            {

                if (model.CodeCentre == 9)
                {
                    model.StockTheorie = Convert.ToDecimal(BusinessGestionStockCentre.Instance.StockAujourdhui(model.CodeCentre,model.Date).ToString("0.000"));
                }
                if (model.CodeCentre != 9 && model.CodeCentre != 0)
                {
                    model.StockTheorie = Convert.ToDecimal(BusinessGestionStockCentre.Instance.StockAujourdhuiInterieur(model.CodeCentre,model.Date).ToString("0.000"));
                }



                StockCentreDto dto = new StockCentreDto
                {
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),

                    reference = model.reference,
                    Date = model.Date,
                    CodeCentre = model.CodeCentre,
                   
                    Stock = model.Stock,
                    StockFin = model.StockFin,
                    StockTheorie = model.StockTheorie,
                    Depotage = model.Depotage,
                    GainPerte = model.StockFin >= 0 ? (model.StockFin - model.StockTheorie) : 0,
                    Commentaire = model.Commentaire,

                    etat = model.etat

                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

               


               
                var nbre = BusinessGestionStockCentre.Instance.GetStockParDate(model.CodeCentre,model.Date);
                if (nbre > 0)
                {
                    ViewBag.MessageAjouter = "Cette date  deja saisie";

                    return View(model);
                }

               

                BusinessGestionStockCentre.Instance.Ajouter(dto);





                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }




        public void AjoutGainPerte(int CodeCentre , DateTime date,decimal StockTheorie,decimal Stock)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
               
                var DateStockHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre).OrderByDescending(r => r.Date).Select(x => x.Date).FirstOrDefault();
                var entity = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date == DateStockHier).SingleOrDefault();

                entity.GainPerte = StockTheorie > 0 ? (Stock - StockTheorie) : 0;

                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }






        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModiferStockCentre model = new ModelModiferStockCentre();
            model.ListeCentre = BusinessGestionStockCentre.Instance.GetListeCentre();


             StockCentreDto dto = BusinessGestionStockCentre.Instance.GetStockCentreParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.reference = dto.reference;
                model.Date = dto.Date;
                model.CodeCentre = dto.CodeCentre;
                model.LibelleCentre = dto.LibelleCentre;
                //model.StockTheorie = Convert.ToDecimal(dto.StockTheorie.ToString("0.000"));
                model.Stock = Convert.ToDecimal(BusinessGestionStockCentre.Instance.StockDeut(model.CodeCentre,model.Date).ToString("0.000")) > 0 ? Convert.ToDecimal(BusinessGestionStockCentre.Instance.StockDeut(model.CodeCentre, model.Date).ToString("0.000")) : dto.Stock;
                model.StockFin = dto.StockFin;
                model.Depotage = dto.Depotage;
               

                model.Commentaire = dto.Commentaire;

                model.etat = dto.etat;


            };

             //model.Stock = Convert.ToDecimal(BusinessGestionStockCentre.Instance.StockDeut(model.CodeCentre,model.Date).ToString("0.000"));

            if (model.CodeCentre == 9)
            {
                model.StockTheorie = Convert.ToDecimal(BusinessGestionStockCentre.Instance.StockAujourdhui(model.CodeCentre, model.Date).ToString("0.000"));
            }
            if (model.CodeCentre != 9 && model.CodeCentre != 0)
            {
                model.StockTheorie = Convert.ToDecimal(BusinessGestionStockCentre.Instance.StockAujourdhuiInterieur(model.CodeCentre,model.Date).ToString("0.000"));
            }

            return View(model);


        }




        [HttpPost]
        public ActionResult Modifier(ModelModiferStockCentre model)
        {


            if (ModelState.IsValid)
            {

               


                StockCentreDto dto = new StockCentreDto
                {

                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    id = model.id,
                    reference = model.reference,
                    Date = model.Date,
                    CodeCentre = model.CodeCentre,
                    LibelleCentre = model.LibelleCentre,
                    
                    Stock = model.Stock,
                    StockFin = model.StockFin,
                    StockTheorie = model.StockTheorie,
                    Depotage = model.Depotage,
                    GainPerte = model.StockFin >= 0 ? (model.StockFin - model.StockTheorie) : 0,
                    Commentaire = model.Commentaire,

                    etat = model.etat

                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);




              

                //ModifierGainPerte(model.CodeCentre, model.Date, model.StockTheorie, model.Stock);
                BusinessGestionStockCentre.Instance.Modifier(dto);




                

                return RedirectToAction("Index");
            }
            else
            {
                model.ListeCentre = BusinessGestionStockCentre.Instance.GetListeCentre();
                model.CodeCentre = BusinessGestionStockCentre.Instance.GetCentreParCode(model.CodeCentre).Code;
                model.LibelleCentre = BusinessGestionStockCentre.Instance.GetCentreParCode(model.CodeCentre).libelle;


                model.Date = DateTime.Now;
                return View(model);
            }

        }




        public void ModifierGainPerte(int CodeCentre, DateTime date, decimal StockTheorie, decimal Stock)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var DateStockHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date < date).OrderByDescending(r => r.Date).Select(x => x.Date).FirstOrDefault();
                var entity = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date == DateStockHier).SingleOrDefault();

                entity.GainPerte = StockTheorie > 0 ? (Stock - StockTheorie) : 0;

                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }




        string GetNewCode()
        {


            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {
                //int reference;
                maxCode = Convert.ToString(db.StockCentres.OrderByDescending(s => s.id).Select(x => x.reference).FirstOrDefault());
                //reference = db.FicheSituations.Select(x => x.reference).Max();
                //maxCode = Convert.ToString(reference);
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }



    }
}