﻿using BLL;
using CrystalDecisions.CrystalReports.Engine;
using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using GUHabitat.Reports;
using PagedList;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class ARBController : Controller
    {
        [HttpGet]
        public ActionResult Index(string filtreMotCle, int? page)
        {
            ListeARBModel model = new ListeARBModel();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeARB = BusinessGestionARB.Instance.ChercherListeARB(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);


        }



        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelAjouterARB model = new ModelAjouterARB();


            ARBDto ArbDto = BusinessGestionARB.Instance.GetARBParId(id);

            if (ArbDto != null)
            {
                model.id = ArbDto.id;
                model.Serie = ArbDto.Serie;
                model.Date = ArbDto.Date;
                model.B12 = ArbDto.B12;
                model.B9 = ArbDto.B9;
                model.B6R = ArbDto.B6R;
                model.B6V = ArbDto.B6V;
                model.B3 = ArbDto.B3;
                model.B35 = ArbDto.B35;
                model.CodeClient = ArbDto.CodeClient;
                model.NomClient = ArbDto.NomClient;
                model.Commentaire = ArbDto.Commentaire;
                model.etat = ArbDto.etat;


            };
            return View(model);

        }




        [HttpGet]
        public ActionResult Ajouter()
        {
            ModelAjouterARB model = new ModelAjouterARB();
            model.Date = DateTime.Now;
            model.Serie = GetNewCode();


            return View(model);
        }


        [HttpPost]
        public ActionResult Ajouter(ModelAjouterARB model)
        {


            if (ModelState.IsValid)
            {

                ARBDto dto = new ARBDto
                {
                    Date = model.Date,
                    Serie = GetNewCode(),
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,
                    
                    Commentaire = model.Commentaire




                };
                BusinessGestionARB.Instance.Ajouter(dto);


                return RedirectToAction("Ajouter");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }


        [HttpGet]
        public ActionResult ModifierARB(int id)
        {

            ModelModifierARB model = new ModelModifierARB();

            ARBDto ARBDto = BusinessGestionARB.Instance.GetARBParId(id);

            if (ARBDto != null)
            {
                model.id = ARBDto.id;
                model.Serie = ARBDto.Serie;
                model.Date = ARBDto.Date;
                model.B12 = ARBDto.B12;
                model.B9 = ARBDto.B9;
                model.B6R = ARBDto.B6R;
                model.B6V = ARBDto.B6V;
                model.B3 = ARBDto.B3;
                model.B35 = ARBDto.B35;
                model.CodeClient = ARBDto.CodeClient;
                model.NomClient = ARBDto.NomClient;
                model.Commentaire = ARBDto.Commentaire;


            };


            return View(model);
        }



        [HttpPost]
        public ActionResult ModifierARB(ModelModifierARB model)
        {



            if (ModelState.IsValid)
            {

                ARBDto dto = new ARBDto
                {
                    id = model.id,
                    Date = model.Date,
                    Serie = GetNewCode(),
                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    Commentaire = model.Commentaire




                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);
                BusinessGestionARB.Instance.Modifier(dto);

               
                return RedirectToAction("Ajouter");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }



        }








        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {

                maxCode = db.ARBs.Select(x => x.Serie).Max();
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }



        public ActionResult Imprimer(int id)
        {
            ReportClass rpt = null;
            try
            {




                rpt = new crpARB();
                var dto = BusinessGestionARB.Instance.GetARBParId(id);

                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.mr" +"\n"+ id, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);

                using (MemoryStream ms = new MemoryStream())
                {
                    qrCodeImage.Save(ms, ImageFormat.Bmp);
                    dto.qrCodeImage = ms.ToArray();
                }






                List<ARBDto> list = new List<ARBDto>();
                list.Add(dto);
                rpt.SetDataSource(list);
                Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception) { }
            return null;
        }




    }
}