﻿using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class ConsignationCentreController : Controller
    {
        // GET: ConsignationCentre
        [HttpPost]
        public ActionResult ValiderConsignationCentre(int CodeCentreConsignation)
        {
            return RedirectToAction("Ajouter", new { CodeCentreConsignation = CodeCentreConsignation });
        }



        // GET: VenteCentre
        public ActionResult Index(string filtreMotCle, int? page)
        {

            ModelListeConsignationCentre model = new ModelListeConsignationCentre();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeConsignationCentre = BusinessGestionConsignationCentre.Instance.ChercherListeConsignationCentre(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);
        }


        public ActionResult EnInstance(int id)
        {


            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionConsignationCentre.Instance.EnInstance(idEntite, id);

            return RedirectToAction("Index");
        }



        public ActionResult Valider(int id)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionConsignationCentre.Instance.Valider(idEntite, id);


            return RedirectToAction("Index");
        }










        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelAjouterConsignationCentre model = new ModelAjouterConsignationCentre();


            ConsignationCentreDto dto = BusinessGestionConsignationCentre.Instance.GetConsignationCentreParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.CodeCentre = dto.CodeCentre;
                model.LibelleCentre = dto.LibelleCentre;

                model.B12 = dto.B12;
                model.B9 = dto.B9;
                model.B6R = dto.B6R;
                model.B6V = dto.B6V;
                model.B3 = dto.B3;
                model.B35 = dto.B35;

                model.B12Prix = dto.B12Prix;
                model.B9Prix = dto.B9Prix;
                model.B6RPrix = dto.B6RPrix;
                model.B6VPrix = dto.B6VPrix;
                model.B3Prix = dto.B3Prix;
                model.B35Prix = dto.B35Prix;
         
                model.TotalPrixB12 = dto.TotalPrixB12;
                model.TotalPrixB9= dto.TotalPrixB9;
                model.TotalPrixB6R = dto.TotalPrixB6R;
                model.TotalPrixB6V = dto.TotalPrixB6V;
                model.TotalPrixB3 = dto.TotalPrixB3;
                model.TotalPrixB35 = dto.TotalPrixB35;

               
                model.Commentaire = dto.Commentaire;

                model.etat = dto.etat;



            };
            return View(model);

        }



        public ActionResult Ajouter(int CodeCentreConsignation)
        {
            ModelAjouterConsignationCentre model = new ModelAjouterConsignationCentre();
            model.Date = DateTime.Now;

            string reference = GetNewCode();

            model.CodeCentre = BusinessGestionConsignationCentre.Instance.GetCentreParCode(CodeCentreConsignation).Code;

            model.Reference = (reference == "1") ? "000001" : reference;

            model.B12Prix = 600;
            model.B6RPrix = 800;
            model.B6VPrix = 800;
            model.B3Prix = 400;
            model.B35Prix = 2500;

            return View(model);
        }






        [HttpPost]
        public ActionResult Ajouter(ModelAjouterConsignationCentre model)
        {



            if (ModelState.IsValid)
            {

                ConsignationCentreDto dto = new ConsignationCentreDto
                {
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),

                    Reference = model.Reference,
                    Date = model.Date,
                    CodeCentre = model.CodeCentre,
                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,

                    B12Prix = model.B12Prix,
                    B9Prix = model.B9Prix,
                    B6RPrix = model.B6RPrix,
                    B6VPrix = model.B6VPrix,
                    B3Prix = model.B3Prix,
                    B35Prix = model.B35Prix,

                    TotalPrixB12 = (model.B12 * model.B12Prix),
                    TotalPrixB9 = (model.B9 * model.B9Prix),
                    TotalPrixB6R = (model.B6R * model.B6RPrix),
                    TotalPrixB6V = (model.B6V * model.B6VPrix),
                    TotalPrixB3 = (model.B3 * model.B3Prix),
                    TotalPrixB35 = (model.B35 * model.B35Prix),

                  

                    Commentaire = model.Commentaire,

                    etat = model.etat

                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);


               

                //decimal Tonnage = (decimal)(dto.B12 * 12.5 + dto.B9 * 9 + dto.B6R * 6 + dto.B6V * 6 + dto.B3 * 3 + dto.B35 * 35) / 1000;
                //var StockActuel =   BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).StockActuel;
                //if (Tonnage > StockActuel)    
                //{
                //    ViewBag.MessageStockAjout = "Le Tonnage non disponible";
                //    model.Date = DateTime.Now;
                //    return View(model);
                //}

                BusinessGestionConsignationCentre.Instance.Ajouter(dto);

                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }



        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModifierConsignationCentre model = new ModelModifierConsignationCentre();
            model.ListeCentre = BusinessGestionConsignationCentre.Instance.GetListeCentre();


            ConsignationCentreDto dto = BusinessGestionConsignationCentre.Instance.GetConsignationCentreParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.CodeCentre = dto.CodeCentre;
                model.LibelleCentre = dto.LibelleCentre;

                model.B12 = dto.B12;
                model.B9 = dto.B9;
                model.B6R = dto.B6R;
                model.B6V = dto.B6V;
                model.B3 = dto.B3;
                model.B35 = dto.B35;

                model.B12Prix = dto.B12Prix;
                model.B9Prix = dto.B9Prix;
                model.B6RPrix = dto.B6RPrix;
                model.B6VPrix = dto.B6VPrix;
                model.B3Prix = dto.B3Prix;
                model.B35Prix = dto.B35Prix;

              
                model.TotalPrixB12= dto.TotalPrixB12;
                model.TotalPrixB9 = dto.TotalPrixB9;
                model.TotalPrixB6R= dto.TotalPrixB6R;
                model.TotalPrixB6V = dto.TotalPrixB6V;
                model.TotalPrixB3= dto.TotalPrixB3;
                model.TotalPrixB35 = dto.TotalPrixB35;

                model.Commentaire = dto.Commentaire;

                model.etat = dto.etat;



            };
            return View(model);


        }





        [HttpPost]
        public ActionResult Modifier(ModelModifierConsignationCentre model)
        {


            if (ModelState.IsValid)
            {

               ConsignationCentreDto dto = new ConsignationCentreDto
               {

                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    id = model.id,
                    Reference = model.Reference,
                    Date = model.Date,
                    CodeCentre = model.CodeCentre,
                    LibelleCentre = model.LibelleCentre,

                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,

                    B12Prix = model.B12Prix,
                    B9Prix = model.B9Prix,
                    B6RPrix = model.B6RPrix,
                    B6VPrix = model.B6VPrix,
                    B3Prix = model.B3Prix,
                    B35Prix = model.B35Prix,

                  
                    TotalPrixB12 = (model.B12 * model.B12Prix),
                    TotalPrixB9 = (model.B9 * model.B9),
                    TotalPrixB6R = (model.B6R * model.B6RPrix),
                    TotalPrixB6V = (model.B6V * model.B6VPrix),
                    TotalPrixB3 = (model.B3 * model.B3Prix),
                    TotalPrixB35 = (model.B35 * model.B35Prix),

                    Commentaire = model.Commentaire,

                    etat = model.etat

                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);


                //decimal Tonnage = (decimal)(dto.B12 * 12.5 + dto.B9 * 9 + dto.B6R * 6 + dto.B6V * 6 + dto.B3 * 3 + dto.B35 * 35) / 1000;

                //var StockActuel = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(dto.CodeCentre, dto.Date, dto.Date).StockActuel;
                //if (Tonnage > StockActuel)
                //{
                //    ViewBag.MessageStockModifier = "Le Tonnage non disponible";

                //    model.ListeCentre = BusinessGestionConsignationCentre.Instance.GetListeCentre();
                //    model.CodeCentre = BusinessGestionConsignationCentre.Instance.GetCentreParCode(model.CodeCentre).Code;
                //    model.LibelleCentre = BusinessGestionConsignationCentre.Instance.GetCentreParCode(model.CodeCentre).libelle;

                //    model.Date = DateTime.Now;
                //    return View(model);
                //}

                BusinessGestionConsignationCentre.Instance.Modifier(dto);





                return RedirectToAction("Index");
            }
            else
            {
                model.ListeCentre = BusinessGestionConsignationCentre.Instance.GetListeCentre();
                model.CodeCentre = BusinessGestionConsignationCentre.Instance.GetCentreParCode(model.CodeCentre).Code;
                model.LibelleCentre = BusinessGestionConsignationCentre.Instance.GetCentreParCode(model.CodeCentre).libelle;


                model.Date = DateTime.Now;
                return View(model);
            }

        }












        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {

                maxCode = db.ConsignationCentres.OrderByDescending(s => s.id).Select(x => x.Reference).FirstOrDefault();


            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }



    }
}