﻿using BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class EntiteController : Controller
    {
        BusinessGestionEntites biz = new BusinessGestionEntites();

        [HttpGet]
        public ActionResult Index(ModelListEntite model /*binding*/)
        {
            model.ListeEntites = biz.ChercherListeEntites(model.filtreMotCle);

            return View(model);
        }

        [HttpGet]

        public PartialViewResult AjaxListeEntites(ModelListEntite model /*binding*/)
        {
            model.ListeEntites = biz.ChercherListeEntites(model.filtreMotCle);

            return PartialView("_PartialListeEntites", model);
        }


        public ActionResult Ajouter()
        {
            ModelAjouterEntite model = new ModelAjouterEntite();
            return View(model);
        }
        [HttpPost]
        public ActionResult Ajouter(ModelAjouterEntite model /*binding*/)
        {

            //capter la saisie du client: Request.Form, paramètres typé, model

            //valider la saisie
            if (ModelState.IsValid)
            {
                //stocker dans la base
                EntiteDto dto = new EntiteDto
                {
                   libelle = model.libelle
                };
                biz.EnregistrerNouvelleEntite(dto);

                //répondre le client
                return RedirectToAction("Index");

            }
            else
            {
                return View("Ajouter", model);
            }


        }
        [HttpGet]
        public ActionResult Supprimer(int id /*binding*/)
        {
            //Essayer de supprimer
            if (biz.SupprimerEntite(id))
            {
                //si ca marche => un message de succès

                var msg = "L'opération a réussi";
                TempData["SuccessMessage"] = msg;
            }
            else
            {
                //sinon =>un message d'erreur
                var msg = "L'opération a échoué";
                TempData["ErrorMessage"] = msg;
            }
            //revenir à la page index
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Modifier(int id /*binding*/)
        {
            ModelModifierEntite model = new ModelModifierEntite();
  
            var dto = biz.GetEntiteParId(id);
            model.libelle = dto.libelle;
            return View(model);
        }


        [HttpPost]
        public ActionResult ModifierEntiteExistante(ModelModifierEntite model /*binding*/)
        {
            //capter la saisie du client: Request.Form, paramètres typé, model

            //valider la saisie
            if (ModelState.IsValid)
            {
                //stoquer les modification dans la base

                EntiteDto dto = new EntiteDto
                {
                    id = model.id,
                    libelle = model.libelle
                };
                biz.ModifierEntite(dto);

                //répondre le client
                return RedirectToAction("Index");

            }
            else
            {
                 return View("Modifier", model);
            }
        }
    }
}