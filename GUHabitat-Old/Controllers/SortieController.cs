﻿using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class SortieController : Controller
    {

        [HttpPost]
        public ActionResult ValiderTypeSortie(int idTypeSortie)
        {
            return RedirectToAction("Ajouter", new { idTypeSortie = idTypeSortie });
        }

        // GET: Sortie
        public ActionResult Index(string filtreMotCle, int? page)
        {
            ModelListeSortie model = new ModelListeSortie();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeSortie = BusinessGestionSortie.Instance.ChercherListeSortie(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);


        }



        [HttpGet]
        public ActionResult Acloturer(string filtreMotCle, int? page)
        {
            ModelListeSortie model = new ModelListeSortie();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeSortie = BusinessGestionSortie.Instance.ChercherListeSortieCloturer(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            ViewBag.nbInstance = BusinessGestionSortie.Instance.TotalCloturer();

            return View(model);


        }


        [HttpGet]
        public ActionResult Valider(string filtreMotCle, int? page)
        {
            ModelListeSortie model = new ModelListeSortie();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeSortie = BusinessGestionSortie.Instance.ChercherListeSortieValider(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            ViewBag.nbInstance = BusinessGestionSortie.Instance.TotalValider();

            return View(model);


        }


        public void SelectedValider(int[] ids)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);


            BusinessGestionSortie.Instance.Valider(idEntite,ids);
        }


        public void Selected(int[] ids)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            BusinessGestionSortie.Instance.Cloturer(ids);
        }




        public ActionResult EnInstance(int id)
        {


            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionSortie.Instance.EnInstance(idEntite, id);

            return RedirectToAction("Index");
        }

        public ActionResult ValiderParDate(string date)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionSortie.Instance.Valider(idEntite,Convert.ToDateTime(date));

            return RedirectToAction("Index");
        }



        public ActionResult CloturerParDate(string date)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionSortie.Instance.CloturerParDate(idEntite, Convert.ToDateTime(date));

            return RedirectToAction("Index");
        }



        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelModifierSortie model = new ModelModifierSortie();


            SortieDto dto = BusinessGestionSortie.Instance.GetSortieParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.ReferenceTypeSortie = dto.ReferenceTypeSortie;
                model.NumeroFacture = dto.NumeroFacture;
                model.Client = dto.Client;
                model.Employe = dto.Employe;
                model.Date = dto.Date;
                model.idTypeSortie = dto.idTypeSortie;
                model.TypeSortie = dto.TypeSortie;

                model.B12 = dto.B12;
                model.B9 = dto.B9;
                model.B6R = dto.B6R;
                model.B6V = dto.B6V;
                model.B3 = dto.B3;
                model.B35 = dto.B35;
                model.Presentoir = dto.Presentoir;



                model.Commentaire = dto.Commentaires;
                model.etat = dto.etat;


            };
            return View(model);

        }






        [HttpGet]
        public ActionResult Ajouter(int idTypeSortie)
        {
            ModelAjouterSortie model = new ModelAjouterSortie();
            model.Date = DateTime.Now;

            string reference = GetNewCode();
            model.Reference = (reference == "1") ? "000001" : reference;
           
            model.idTypeSortie = BusinessGestionSortie.Instance.GetTypeSortieParId(idTypeSortie).id;
            //if (model.idTypeSortie == 2 )
            //{

            //    model.Employe = 0;
               
            //}

            //if (model.idTypeSortie != 2)
            //{
            //    model.Client = 0;
               
            //}
            return View(model);
        }


        [HttpPost]
        public ActionResult Ajouter(ModelAjouterSortie model)
        {


            if (ModelState.IsValid)
            {

                SortieDto dto = new SortieDto
                {
                   
                    Reference = model.Reference,
                    ReferenceTypeSortie = model.ReferenceTypeSortie,
                    NumeroFacture = model.NumeroFacture,
                    Client = model.Client,
                    Employe = model.Employe,
                    Date = model.Date,
                    idTypeSortie = model.idTypeSortie,
                   
                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,
                    Presentoir = model.Presentoir,

                    Commentaires = model.Commentaire,



                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);


                //var nbrefiche = BusinessGestionSortie.Instance.VerifierExistFiche(model.Reference);
                //if (nbrefiche >= 1)
                //{

                //    return View(model);
                //}

                BusinessGestionSortie.Instance.Ajouter(dto);


                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }



        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModifierSortie model = new ModelModifierSortie();

            model.ListeFiches = BusinessGestionSortie.Instance.GetListeTypeSortie();

            SortieDto dto = BusinessGestionSortie.Instance.GetSortieParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.ReferenceTypeSortie = dto.ReferenceTypeSortie;
                model.NumeroFacture = dto.NumeroFacture;
                model.Client = dto.Client;
                model.Employe = dto.Employe;
                model.Date = dto.Date;
                model.idTypeSortie = dto.idTypeSortie;
                model.TypeSortie = dto.TypeSortie;
                model.B12 = dto.B12;
                model.B9 = dto.B9;
                model.B6R = dto.B6R;
                model.B6V = dto.B6V;
                model.B3 = dto.B3;
                model.B35 = dto.B35;
                model.Presentoir = dto.Presentoir;

                

                model.Commentaire = dto.Commentaires;
                model.etat = dto.etat;


            };


            return View(model);
        }




        [HttpPost]
        public ActionResult Modifier(ModelModifierSortie model)
        {



            if (ModelState.IsValid)
            {

                SortieDto dto = new SortieDto
                {
                    id = model.id,
                    Reference = model.Reference,
                    ReferenceTypeSortie = model.ReferenceTypeSortie,
                    NumeroFacture = model.NumeroFacture,
                    Client = model.Client,
                    Employe = model.Employe,
                    Date = model.Date,
                    idTypeSortie = model.idTypeSortie,

                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,
                    Presentoir = model.Presentoir,

                    Commentaires = model.Commentaire,
                   



                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

               
                BusinessGestionSortie.Instance.Modifier(dto);


                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }



        }







        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {
                maxCode = db.Sorties.OrderByDescending(s => s.id).Select(x => x.Reference).FirstOrDefault();
                //maxCode = db.FicheConsignations.Select(x => x.Reference).Max();
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }
    }
}