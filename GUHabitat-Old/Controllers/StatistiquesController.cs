﻿using GUHabitat.BLL;
using GUHabitat.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using GUHabitat.DTO;

namespace GUHabitat.Controllers
{
    public class StatistiquesController : Controller
    {
        //int codeClient; DateTime? dateDebut; DateTime? dateFin; int? page;
        // GET: Statistiques
        public ActionResult Index()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiques> list = new List<ModelStatistiques>();

                ModelStatistiques model = new ModelStatistiques();



                return View(list);

            }



        }

        [HttpPost]
        public ActionResult Index(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiques> list = new List<ModelStatistiques>();

                ModelStatistiques model = new ModelStatistiques();


                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;


                //Nombre d’emballage a facturer

                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                var DateCollaborateurString = "2023-01-01";
                var DateCollaborateur = Convert.ToDateTime(DateCollaborateurString).Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;


                //Nombre d'emballage vide présenté par le client

                model.B12Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Client.Value).DefaultIfEmpty(0).Sum();
                model.B9Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Client.Value).DefaultIfEmpty(0).Sum();
                model.B6RClient = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RClient.Value).DefaultIfEmpty(0).Sum();
                model.B6VClient = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VClient.Value).DefaultIfEmpty(0).Sum();
                model.B3Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Client.Value).DefaultIfEmpty(0).Sum();
                model.B35Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Client.Value).DefaultIfEmpty(0).Sum();





                //Nombre d’emballage Vente

                model.B12Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
                model.B9Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
                model.B6RVente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VVente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
                model.B3Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
                model.B35Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();



                //Nombre d’emballage Vente Directe

                model.B12VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
                model.B9VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
                model.B6RVenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VVenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
                model.B3VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();





                //Nombre d’emballage dotation

                model.B12Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
                model.B9Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
                model.B3Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
                model.B35Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();




                //Nombre d’emballage a consigner

                model.B12Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
                model.B9Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                model.B3Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                model.B35Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();
                model.PresentoirConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.PresentoirDC.Value).DefaultIfEmpty(0).Sum();


                //Nombre d’emballage corps et charges a consigner

                model.B12CorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
                model.B9CorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                model.B6RCorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                model.B6VCorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                model.B3CorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                model.B35CorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();


                //Nombre d’emballage corps  a consigner

                model.B12Corps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
                model.B9Corps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                model.B6RCorps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                model.B6VCorps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                model.B3Corps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                model.B35Corps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();



                //Nombre d’emballage Avoir

                model.B12Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B9Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B6RAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum();
                model.B3Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B35Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum();





                //Nombre d’emballage Aredp

                model.B12Aredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum());
                model.B9Aredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum());
                model.B6RAredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum());
                model.B6VAredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum());
                model.B3Aredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum());
                model.B35Aredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum());




                //Nombre d’emballage a facturer

                model.B12Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum());
                model.B9Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum());
                model.B6RAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum());
                model.B6VAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum());
                model.B3Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum());
                model.B35Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum());




                //Nombre d’emballage produisez

                model.B12Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B9Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6RProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6VProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B3Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B35Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum());



                // Tonnage Manquant et Restant Avoir



                decimal pourcentageB12 = (Convert.ToDecimal("12,5") * 40) / 100; ;
                decimal pourcentageB9 = (9 * 40) / 100; ;
                decimal pourcentageB6R = (6 * 40) / 100;
                decimal pourcentageB6V = (6 * 40) / 100;
                decimal pourcentageB3 = (Convert.ToDecimal("2,75") * 40) / 100;
                decimal pourcentageB35 = (35 * 40) / 100;


                model.B12PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 1 && x.poidsRestant >= pourcentageB12).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
                model.B9PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 2 && x.poidsRestant >= pourcentageB9).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
                model.B6RPoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 3 && x.poidsRestant >= pourcentageB6R).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
                model.B6VPoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 4 && x.poidsRestant >= pourcentageB6V).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
                model.B3PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 5 && x.poidsRestant >= pourcentageB3).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
                model.B35PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 6 && x.poidsRestant >= pourcentageB35).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();


                model.B12PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 1 && x.poidsRestant >= pourcentageB12).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
                model.B9PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 2 && x.poidsRestant >= pourcentageB9).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
                model.B6RPoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 3 && x.poidsRestant >= pourcentageB6R).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
                model.B6VPoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 4 && x.poidsRestant >= pourcentageB6V).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
                model.B3PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 5 && x.poidsRestant >= pourcentageB3).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
                model.B35PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 6 && x.poidsRestant >= pourcentageB35).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();


                model.TotalPoidsManquantAvoir = (model.B12PoidManquantAvoir + model.B9PoidManquantAvoir + model.B6RPoidManquantAvoir + model.B6VPoidManquantAvoir + model.B3PoidManquantAvoir + model.B35PoidManquantAvoir) / 1000;
                model.TotalPoidsRestantAvoir = (model.B12PoidRestantAvoir + model.B9PoidRestantAvoir + model.B6RPoidRestantAvoir + model.B6VPoidRestantAvoir + model.B3PoidRestantAvoir + model.B35PoidRestantAvoir) / 1000;





                model.TotalPoidsManquantAredp = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum() / 1000;
                model.TotalPoidsRestantAredp = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum() / 1000;



                model.TonnageSomagaz = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == 0 && x.idTypeBonChargement == 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                model.TotalRetourLivrer = (decimal)_context.Livraisons.Where(x => x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient != 0).Select(x => x.PoidsRetour.Value).DefaultIfEmpty(0).Sum();


                // Vrac Facturation
                model.TonnagePrivee = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1 && x.idTypeBonChargement != 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - model.TotalRetourLivrer;

                // Tous les Vracs sauf somagaz



                //model.TonnagePartenaire = (float)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                model.AutresVrac = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                model.TonnageCollaborateurEnvoie = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();
                model.TonnageCollaborateurEnvoiePipes = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();

                model.TonnageCollaborateurReception = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idMouvementBonEnlvement == 1).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();

                model.TonnageCollaborateurEnvoieInventaire = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();
                model.TonnageCollaborateurReceptionInventaire = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idMouvementBonEnlvement == 1).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();



                model.TonnageCollaborateurActuel = model.TonnageCollaborateurEnvoieInventaire - model.TonnageCollaborateurReceptionInventaire;


                model.TotalProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000 + model.TonnageSomagaz + model.AutresVrac + model.TonnageCollaborateurEnvoiePipes;

                model.TotalCommerciale = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000 + model.TonnagePrivee;

                model.TotalConditionneeProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000;
                model.TotalConditionneeCommerciale = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000;


                model.B12Afact = model.B12Afact + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
                model.B9Afact = model.B9Afact   + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                model.B6RAfact = model.B6RAfact + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                model.B6VAfact = model.B6VAfact + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                model.B3Afact = model.B3Afact   + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                model.B35Afact = model.B35Afact + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();


                model.StockNKTT = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum();

                model.StockTheorieNKTT = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date == dateDebut).Select(x => x.StockTheorie.Value).DefaultIfEmpty(0).Sum();

                model.StockNKTTFin = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date == dateDebut).Select(x => x.StockFin.Value).DefaultIfEmpty(0).Sum();


                model.Depotage = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.Depotage.Value).DefaultIfEmpty(0).Sum();

                model.TotalRetour = (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();


                model.GainPerteNktt = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.GainPerte.Value).DefaultIfEmpty(0).Sum();


                model.StockActuelNktt = model.StockNKTT > 0 ? _context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() - model.TotalProduction + model.Depotage + model.TotalRetour  + model.TonnageCollaborateurReception: 0;




                list.Add(model);
                return View(list);

            }



        }


        public ModelStatistiques GetStatistiqueGlobale(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                ModelStatistiques model = new ModelStatistiques();


                //ViewBag.dateDebut = dateDebut;
                //ViewBag.dateFin = dateFin;


                //Nombre d’emballage a facturer

                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;




                //Nombre d’emballage dotation

                model.B12Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
                model.B9Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
                model.B3Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
                model.B35Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();




                //Nombre d’emballage a consigner

                model.B12Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
                model.B9Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                model.B3Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                model.B35Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();
                model.PresentoirConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.PresentoirDC.Value).DefaultIfEmpty(0).Sum();




                //Nombre d’emballage Avoir

                model.B12Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B9Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B6RAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum();
                model.B3Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B35Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum();





                //Nombre d’emballage produisez

                model.B12Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B9Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6RProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6VProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B3Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B35Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum());





                model.TonnageSomagaz = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == 0 && x.idTypeBonChargement == 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                model.TotalRetourLivrer = (decimal)_context.Livraisons.Where(x => x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient != 0).Select(x => x.PoidsRetour.Value).DefaultIfEmpty(0).Sum();


                // Vrac Facturation
                model.TonnagePrivee = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1 && x.idTypeBonChargement != 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - model.TotalRetourLivrer;

                // Tous les Vracs sauf somagaz



                //model.TonnagePartenaire = (float)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                model.AutresVrac = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                model.TotalProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000 + model.TonnageSomagaz + model.AutresVrac;

                model.TotalCommerciale = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000 + model.TonnagePrivee;

                model.TotalConditionneeProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000;
                model.TotalConditionneeCommerciale = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000;




                return model;

            }



        }




        public ActionResult Entree()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelEntrees> list = new List<ModelEntrees>();

                ModelEntrees model = new ModelEntrees();



                return View(list);

            }



        }




        [HttpPost]
        public ActionResult Entree(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelEntrees> list = new List<ModelEntrees>();

                ModelEntrees model = new ModelEntrees();


                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;


                //Nombre d’emballage a facturer

                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;




                //Total des entrees Vente + Vente Directe + Dotation + Aredp

                model.B12TotalEntree = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12VideDef.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B9TotalEntree = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9VideDef.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6RTotalEntree = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RVideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RVideDef.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6VTotalEntree = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VVideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VVideDef.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B3TotalEntree = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3VideDef.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B35TotalEntree = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35VideDef.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum());




                //Total Entree Vente

                model.B12TotalEntreeVente = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B12VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B12VideDef.Value).DefaultIfEmpty(0).Sum());
                model.B9TotalEntreeVente = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B9VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B9VideDef.Value).DefaultIfEmpty(0).Sum());
                model.B6RTotalEntreeVente = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B6RVideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B6RVideDef.Value).DefaultIfEmpty(0).Sum());
                model.B6VTotalEntreeVente = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B6VVideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B6VVideDef.Value).DefaultIfEmpty(0).Sum());
                model.B3TotalEntreeVente = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B3VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B3VideDef.Value).DefaultIfEmpty(0).Sum());
                model.B35TotalEntreeVente = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B35VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 1 && x.CodeClient != 0).Select(x => x.B35VideDef.Value).DefaultIfEmpty(0).Sum());



                //Total Entree Vente Directe

                model.B12TotalEntreeVenteDirecte = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B12VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B12VideDef.Value).DefaultIfEmpty(0).Sum());
                model.B9TotalEntreeVenteDirecte = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B9VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B9VideDef.Value).DefaultIfEmpty(0).Sum());
                model.B6RTotalEntreeVenteDirecte = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B6RVideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B6RVideDef.Value).DefaultIfEmpty(0).Sum());
                model.B6VTotalEntreeVenteDirecte = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B6VVideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B6VVideDef.Value).DefaultIfEmpty(0).Sum());
                model.B3TotalEntreeVenteDirecte = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B3VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B3VideDef.Value).DefaultIfEmpty(0).Sum());
                model.B35TotalEntreeVenteDirecte = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B35VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 3 && x.CodeClient != 0).Select(x => x.B35VideDef.Value).DefaultIfEmpty(0).Sum());







                //Total Entree Dotation

                model.B12TotalEntreeDotation = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B12VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B12VideDef.Value).DefaultIfEmpty(0).Sum());
                model.B9TotalEntreeDotation = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B9VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B9VideDef.Value).DefaultIfEmpty(0).Sum());
                model.B6RTotalEntreeDotation = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B6RVideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B6RVideDef.Value).DefaultIfEmpty(0).Sum());
                model.B6VTotalEntreeDotation = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B6VVideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B6VVideDef.Value).DefaultIfEmpty(0).Sum());
                model.B3TotalEntreeDotation = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B3VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B3VideDef.Value).DefaultIfEmpty(0).Sum());
                model.B35TotalEntreeDotation = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B35VideRec.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFiche == 2 && x.matriculeEmploye != 0).Select(x => x.B35VideDef.Value).DefaultIfEmpty(0).Sum());





                //Total des entrees Aredp

                model.B12TotalEntreeAredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B9TotalEntreeAredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6RTotalEntreeAredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6VTotalEntreeAredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B3TotalEntreeAredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B35TotalEntreeAredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum());






                //Nombre d’emballage Aredp A facturer

                model.B12AredpAfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
                model.B9AredpAfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
                model.B6RAredpAfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VAredpAfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
                model.B3AredpAfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
                model.B35AredpAfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();



                //Nombre d’emballage Aredp Non facturer

                model.B12AredpNonfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B9AredpNonfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B6RAredpNonfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VAredpNonfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum();
                model.B3AredpNonfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B35AredpNonfact = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum();






                //Nombre d'emballage vide présenté par le client

                model.B12Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Client.Value).DefaultIfEmpty(0).Sum();
                model.B9Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Client.Value).DefaultIfEmpty(0).Sum();
                model.B6RClient = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RClient.Value).DefaultIfEmpty(0).Sum();
                model.B6VClient = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VClient.Value).DefaultIfEmpty(0).Sum();
                model.B3Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Client.Value).DefaultIfEmpty(0).Sum();
                model.B35Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Client.Value).DefaultIfEmpty(0).Sum();




                //Nombre d’emballage vide recevable 

                model.B12VideRec = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12VideRec.Value).DefaultIfEmpty(0).Sum();
                model.B9VideRec = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9VideRec.Value).DefaultIfEmpty(0).Sum();
                model.B6RVideRec = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RVideRec.Value).DefaultIfEmpty(0).Sum();
                model.B6VVideRec = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VVideRec.Value).DefaultIfEmpty(0).Sum();
                model.B3VideRec = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3VideRec.Value).DefaultIfEmpty(0).Sum();
                model.B35VideRec = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35VideRec.Value).DefaultIfEmpty(0).Sum();



                //Nombre d’emballage vide défectueux

                model.B12VideDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12VideDef.Value).DefaultIfEmpty(0).Sum();
                model.B9VideDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9VideDef.Value).DefaultIfEmpty(0).Sum();
                model.B6RVideDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RVideDef.Value).DefaultIfEmpty(0).Sum();
                model.B6VVideDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VVideDef.Value).DefaultIfEmpty(0).Sum();
                model.B3VideDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3VideDef.Value).DefaultIfEmpty(0).Sum();
                model.B35VideDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35VideDef.Value).DefaultIfEmpty(0).Sum();




                //Nombre d’emballage rempli 

                model.B12PleinRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12PleinRemp.Value).DefaultIfEmpty(0).Sum();
                model.B9PleinRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9PleinRemp.Value).DefaultIfEmpty(0).Sum();
                model.B6RPleinRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RPleinRemp.Value).DefaultIfEmpty(0).Sum();
                model.B6VPleinRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VPleinRemp.Value).DefaultIfEmpty(0).Sum();
                model.B3PleinRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3PleinRemp.Value).DefaultIfEmpty(0).Sum();
                model.B35PleinRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35PleinRemp.Value).DefaultIfEmpty(0).Sum();



                //Nombre d’emballage rempli défectueux

                model.B12PleinDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12PleinDef.Value).DefaultIfEmpty(0).Sum();
                model.B9PleinDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9PleinDef.Value).DefaultIfEmpty(0).Sum();
                model.B6RPleinDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RPleinDef.Value).DefaultIfEmpty(0).Sum();
                model.B6VPleinDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VPleinDef.Value).DefaultIfEmpty(0).Sum();
                model.B3PleinDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3PleinDef.Value).DefaultIfEmpty(0).Sum();
                model.B35PleinDef = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35PleinDef.Value).DefaultIfEmpty(0).Sum();



                //Nombre d’emballage défectueux à remplacer 

                model.B12DefAremp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DefAremp.Value).DefaultIfEmpty(0).Sum();
                model.B9DefAremp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DefAremp.Value).DefaultIfEmpty(0).Sum();
                model.B6RDefAremp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDefAremp.Value).DefaultIfEmpty(0).Sum();
                model.B6VDefAremp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDefAremp.Value).DefaultIfEmpty(0).Sum();
                model.B3DefAremp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DefAremp.Value).DefaultIfEmpty(0).Sum();
                model.B35DefAremp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DefAremp.Value).DefaultIfEmpty(0).Sum();



                //Nombre d’emballage défectueux remplacés 

                model.B12DefRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DefRemp.Value).DefaultIfEmpty(0).Sum();
                model.B9DefRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DefRemp.Value).DefaultIfEmpty(0).Sum();
                model.B6RDefRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDefRemp.Value).DefaultIfEmpty(0).Sum();
                model.B6VDefRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDefRemp.Value).DefaultIfEmpty(0).Sum();
                model.B3DefRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DefRemp.Value).DefaultIfEmpty(0).Sum();
                model.B35DefRemp = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DefRemp.Value).DefaultIfEmpty(0).Sum();







                list.Add(model);
                return View(list);

            }



        }







        public ActionResult Sortie()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelSorties> list = new List<ModelSorties>();

                ModelSorties model = new ModelSorties();



                return View(list);

            }



        }



        [HttpPost]
        public ActionResult Sortie(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelSorties> list = new List<ModelSorties>();

                ModelSorties model = new ModelSorties();


                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;




                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;




                //Total des Sorties Vente + Dotation + Aredp

                model.B12TotalSortie = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9TotalSortie = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RTotalSortie = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VTotalSortie = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3TotalSortie = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35TotalSortie = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();




                //Total Sortie Vente

                model.B12TotalSortieVente = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 1).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9TotalSortieVente = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 1).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RTotalSortieVente = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 1).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VTotalSortieVente = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 1).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3TotalSortieVente = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 1).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35TotalSortieVente = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 1).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();




                //Total Sortie Vente Directe

                model.B12TotalSortieVenteDirecte = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 5).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9TotalSortieVenteDirecte = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 5).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RTotalSortieVenteDirecte = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 5).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VTotalSortieVenteDirecte = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 5).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3TotalSortieVenteDirecte = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 5).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35TotalSortieVenteDirecte = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 5).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();






                //Total Sortie Consignation

                model.B12TotalSortieConsignation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 4).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9TotalSortieConsignation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 4).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RTotalSortieConsignation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 4).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VTotalSortieConsignation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 4).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3TotalSortieConsignation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 4).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35TotalSortieConsignation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 4).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();
                model.PresentoirTotalSortieConsignation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 4).Select(x => x.Presentoir.Value).DefaultIfEmpty(0).Sum();







                //Total Sortie Dotation

                model.B12TotalSortieDotation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 2).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9TotalSortieDotation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 2).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RTotalSortieDotation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 2).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VTotalSortieDotation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 2).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3TotalSortieDotation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 2).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35TotalSortieDotation = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 2).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();





                //Total des Sortie Aredp

                model.B12TotalSortieAredp = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 3).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9TotalSortieAredp = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 3).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RTotalSortieAredp = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 3).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VTotalSortieAredp = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 3).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3TotalSortieAredp = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 3).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35TotalSortieAredp = _context.Sorties.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeSortie == 3).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();



                list.Add(model);
                return View(list);

            }



        }




        public ActionResult AvoirParClient()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelAvoirParClient> list = new List<ModelAvoirParClient>();

                ModelAvoirParClient model = new ModelAvoirParClient();



                return View(list);

            }

        }




        [HttpPost]
        public ActionResult AvoirParClient(int? codeClient, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelAvoirParClient> list = new List<ModelAvoirParClient>();

                ModelAvoirParClient model = new ModelAvoirParClient();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.codeClient = codeClient;

                //Nombre d’emballage a facturer

                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;




                // Tonnage Manquant et Restant Avoir



                decimal pourcentageB12 = (Convert.ToDecimal("12,5") * 40) / 100; ;
                decimal pourcentageB9 = (9 * 40) / 100; ;
                decimal pourcentageB6R = (6 * 40) / 100;
                decimal pourcentageB6V = (6 * 40) / 100;
                decimal pourcentageB3 = (Convert.ToDecimal("2,75") * 40) / 100;
                decimal pourcentageB35 = (35 * 40) / 100;




                //Nombre d’emballage Avoir

                model.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();

                model.B12Avoir = _context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B9Avoir = _context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B6RAvoir = _context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VAvoir = _context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum();
                model.B3Avoir = _context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B35Avoir = _context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum();




                model.B12PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 1 && x.poidsRestant >= pourcentageB12).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
                model.B9PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 2 && x.poidsRestant >= pourcentageB9).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
                model.B6RPoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 3 && x.poidsRestant >= pourcentageB6R).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
                model.B6VPoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 4 && x.poidsRestant >= pourcentageB6V).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
                model.B3PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 5 && x.poidsRestant >= pourcentageB3).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
                model.B35PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 6 && x.poidsRestant >= pourcentageB35).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();


                model.B12PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 1 && x.poidsRestant >= pourcentageB12).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
                model.B9PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 2 && x.poidsRestant >= pourcentageB9).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
                model.B6RPoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 3 && x.poidsRestant >= pourcentageB6R).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
                model.B6VPoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 4 && x.poidsRestant >= pourcentageB6V).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
                model.B3PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 5 && x.poidsRestant >= pourcentageB3).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
                model.B35PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.CodeClient != 0 && x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.AREDP.CodeClient == codeClient && x.idProduit == 6 && x.poidsRestant >= pourcentageB35).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();


                model.TotalPoidsManquantAvoir = (model.B12PoidManquantAvoir + model.B9PoidManquantAvoir + model.B6RPoidManquantAvoir + model.B6VPoidManquantAvoir + model.B3PoidManquantAvoir + model.B35PoidManquantAvoir) / 1000;
                model.TotalPoidsRestantAvoir = (model.B12PoidRestantAvoir + model.B9PoidRestantAvoir + model.B6RPoidRestantAvoir + model.B6VPoidRestantAvoir + model.B3PoidRestantAvoir + model.B35PoidRestantAvoir) / 1000;


                model.TotalTonnageAvoir = (decimal)_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.TonnageNonFacturer).DefaultIfEmpty(0).Sum();

                model.TotalTonnageAvoir = (decimal)(model.B12Avoir * 12.5 + model.B9Avoir * 9 + model.B6RAvoir * 6 + model.B6VAvoir * 6 + model.B3Avoir * 2.75 + model.B35Avoir * 35) / 1000;






                list.Add(model);
                return View(list);

            }



        }







        public ActionResult VenteParClient()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelVenteParClient> list = new List<ModelVenteParClient>();

                ModelVenteParClient model = new ModelVenteParClient();



                return View(list);

            }

        }



        [HttpPost]
        public ActionResult VenteParClient(int? codeClient, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelVenteParClient> list = new List<ModelVenteParClient>();

                ModelVenteParClient model = new ModelVenteParClient();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.codeClient = codeClient;



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();
                model.B12Afact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum());
                model.B9Afact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum());
                model.B6RAfact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum());
                model.B6VAfact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum());
                model.B3Afact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum());
                model.B35Afact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum());
                //model.Tonnage = (float)( _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum()) + (float)(_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.TonnageFacturer.Value).DefaultIfEmpty(0).Sum());

                model.Tonnage = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000;







                list.Add(model);
                return View(list);

            }



        }


        public ActionResult ConsignationParClient()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelConsignationParClient> list = new List<ModelConsignationParClient>();

                ModelConsignationParClient model = new ModelConsignationParClient();



                return View(list);

            }

        }



        [HttpPost]
        public ActionResult ConsignationParClient(int? codeClient, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelConsignationParClient> list = new List<ModelConsignationParClient>();

                ModelConsignationParClient model = new ModelConsignationParClient();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.codeClient = codeClient;

                //Nombre d’emballage a facturer

                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();
                model.B12Afact = _context.FicheConsignations.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12DE.Value).DefaultIfEmpty(0).Sum();
                model.B9Afact = _context.FicheConsignations.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9DE.Value).DefaultIfEmpty(0).Sum();
                model.B6RAfact = _context.FicheConsignations.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RDE.Value).DefaultIfEmpty(0).Sum();
                model.B6VAfact = _context.FicheConsignations.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VDE.Value).DefaultIfEmpty(0).Sum();
                model.B3Afact = _context.FicheConsignations.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3DE.Value).DefaultIfEmpty(0).Sum();
                model.B35Afact = _context.FicheConsignations.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35DE.Value).DefaultIfEmpty(0).Sum();
                model.Presentoir = _context.FicheConsignations.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.PresentoirDC.Value).DefaultIfEmpty(0).Sum();

                model.Tonnage = (decimal)_context.FicheConsignations.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();








                list.Add(model);
                return View(list);

            }



        }


        [HttpGet]
        public ActionResult VracPrivee()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelVracPrivee> list = new List<ModelVracPrivee>();

                ModelVracPrivee model = new ModelVracPrivee();



                return View(list);

            }


        }



        [HttpPost]
        public ActionResult VracPrivee(int? codeClient, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelVracPrivee> list = new List<ModelVracPrivee>();

                ModelVracPrivee model = new ModelVracPrivee();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.codeClient = codeClient;

                //Nombre d’emballage a facturer

                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();
                model.Tonnage = (decimal)(_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeBonChargement != 1 && x.CodeClient == codeClient).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum()) - (decimal)_context.Livraisons.Where(x => x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient == codeClient && x.BonRetourCiterne.idTypeBonRetour == 2).Select(x => x.PoidsRetour.Value).DefaultIfEmpty(0).Sum(); 


                list.Add(model);
                return View(list);

            }

        }









        public ActionResult BonusParClient()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelBonusParClient> list = new List<ModelBonusParClient>();

                ModelBonusParClient model = new ModelBonusParClient();



                return View(list);

            }

        }


        [HttpPost]
        public ActionResult BonusParClient(int? codeClient, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelBonusParClient> list = new List<ModelBonusParClient>();

                ModelBonusParClient model = new ModelBonusParClient();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.codeClient = codeClient;



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();
                model.B12Afact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B9Afact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6RAfact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6VAfact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B3Afact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B35Afact = (_context.FicheVenteBouteilles.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.CodeClient != 0 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum());

                model.Tonnage = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000;

                model.MontantMru = (decimal)model.Tonnage * 1900;
                model.MontantMro = (decimal)model.Tonnage * 19000;





                list.Add(model);
                return View(list);

            }



        }







        public ActionResult BonusVracPartenaire()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelBonusParVracPartenaire> list = new List<ModelBonusParVracPartenaire>();

                ModelBonusParVracPartenaire model = new ModelBonusParVracPartenaire();



                return View(list);

            }

        }



        [HttpPost]
        public ActionResult BonusVracPartenaire(int? codeClient, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelBonusParVracPartenaire> list = new List<ModelBonusParVracPartenaire>();

                ModelBonusParVracPartenaire model = new ModelBonusParVracPartenaire();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.codeClient = codeClient;



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();

                model.Tonnage = (decimal)(_context.BonChargementCiternes.Where(x => x.CodeClient != 0 && x.idTypeBonChargement == 5 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum()) - (_context.Livraisons.Where(x => x.BonRetourCiterne.CodeClient != 0 && x.BonRetourCiterne.idTypeBonRetour == 2 && x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient == codeClient).Select(x => x.PoidsRetour.Value).DefaultIfEmpty(0).Sum());

                model.MontantMru = (decimal)model.Tonnage * 1200;
                model.MontantMro = (decimal)model.Tonnage * 12000;





                list.Add(model);
                return View(list);

            }



        }





        public ActionResult BonusVracPrivee()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelBonusParVracPrivee> list = new List<ModelBonusParVracPrivee>();

                ModelBonusParVracPrivee model = new ModelBonusParVracPrivee();



                return View(list);

            }

        }



        [HttpPost]
        public ActionResult BonusVracPrivee(int? codeClient, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelBonusParVracPrivee> list = new List<ModelBonusParVracPrivee>();

                ModelBonusParVracPrivee model = new ModelBonusParVracPrivee();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.codeClient = codeClient;



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();

                model.Tonnage = (decimal)(_context.BonChargementCiternes.Where(x => x.CodeClient != 0 && x.idTypeBonChargement == 3 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum());

                model.MontantMru = (decimal)model.Tonnage * 800;
                model.MontantMro = (decimal)model.Tonnage * 8000;





                list.Add(model);
                return View(list);

            }



        }





        [HttpGet]
        public ActionResult VracSomagaz()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelVracSomagaz> list = new List<ModelVracSomagaz>();

                ModelVracSomagaz model = new ModelVracSomagaz();



                return View(list);

            }


        }


        [HttpPost]
        public ActionResult VracSomagaz(int? codeDestination, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelVracSomagaz> list = new List<ModelVracSomagaz>();

                ModelVracSomagaz model = new ModelVracSomagaz();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.codeClient = codeDestination;



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model.NomClient = _context.Destinations.Where(x => x.Code == codeDestination).Select(x => x.libelle).SingleOrDefault();
                model.Tonnage = (decimal)(_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeBonChargement == 1 && x.CodeClient == 0 && x.codeDestination == codeDestination).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum()) - (_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin && x.idTypeBonRetour == 1 && x.CodeCentre == codeDestination).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum());


                list.Add(model);
                return View(list);

            }

        }





        [HttpGet]
        public ActionResult RetourClient()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelRetourClient> list = new List<ModelRetourClient>();

                ModelRetourClient model = new ModelRetourClient();


                return View(list);

            }


        }


        [HttpPost]
        public ActionResult RetourClient(int? codeClient, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelRetourClient> list = new List<ModelRetourClient>();

                ModelRetourClient model = new ModelRetourClient();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.codeClient = codeClient;



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();
                model.Tonnage = (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin && x.idTypeBonRetour == 2 && x.CodeClient == codeClient).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();


                list.Add(model);
                return View(list);

            }

        }




        [HttpGet]
        public ActionResult RetourCentre()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelRetourCentre> list = new List<ModelRetourCentre>();

                ModelRetourCentre model = new ModelRetourCentre();


                return View(list);

            }


        }


        [HttpPost]
        public ActionResult RetourCentre(int? codeCentre, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelRetourCentre> list = new List<ModelRetourCentre>();

                ModelRetourCentre model = new ModelRetourCentre();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;
                ViewBag.codeClient = codeCentre;



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                model.LibelleCentre = _context.Centres.Where(x => x.Code == codeCentre).Select(x => x.libelle).SingleOrDefault();
                model.Tonnage = (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin && x.idTypeBonRetour == 1 && x.CodeCentre == codeCentre).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();


                list.Add(model);
                return View(list);

            }

        }








        [HttpGet]
        public ActionResult CentreSomagaz()
        {

            return View();

        }


        [HttpGet]
        public ActionResult Situation()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelSituationStatistique> list = new List<ModelSituationStatistique>();

                ModelSituationStatistique model = new ModelSituationStatistique();



                return View(list);

            }


        }


        [HttpPost]
        public ActionResult Situation(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelSituationStatistique> list = new List<ModelSituationStatistique>();

                ModelSituationStatistique model = new ModelSituationStatistique();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;




                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;


                model.StockNKTT = (_context.FicheSituations.Where(x => x.Date == dateDebut).Select(x => x.StockNKTT.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheSituations.Where(x => x.Date > dateDebut && x.Date <= dateFin).Select(x => x.Depotage.Value).DefaultIfEmpty(0).Sum());


                model.Depotage = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.Depotage.Value).DefaultIfEmpty(0).Sum();

                model.StockInterieur = (_context.FicheSituations.Where(x => x.Date == dateDebut).Select(x => x.StockInterieur.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheSituations.Where(x => x.Date > dateDebut && x.Date <= dateFin).Select(x => x.TransfertCentres.Value).DefaultIfEmpty(0).Sum());

                model.TotalStock = model.StockNKTT + model.StockInterieur;

                model.VenteConditionneeNKTT = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.VenteConditionneeNKTT.Value).DefaultIfEmpty(0).Sum();
                model.VenteCentresInterieur = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.VenteCentresInterieur.Value).DefaultIfEmpty(0).Sum();
                model.VenteVrac = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.VenteVrac.Value).DefaultIfEmpty(0).Sum();
                model.TotalVente = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TotalVente.Value).DefaultIfEmpty(0).Sum();

                model.TransfertCentres = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TransfertCentres.Value).DefaultIfEmpty(0).Sum();
                model.TransfertPartenaires = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TransfertPartenaires.Value).DefaultIfEmpty(0).Sum();
                model.TotalTransfert = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TotalTransfert.Value).DefaultIfEmpty(0).Sum();

                model.GainPerte = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.GainPerte.Value).DefaultIfEmpty(0).Sum();

                model.RecetteNKTT = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.RecetteNKTT.Value).DefaultIfEmpty(0).Sum();
                model.RecetteInterieur = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.RecetteInterieur.Value).DefaultIfEmpty(0).Sum();
                model.TotalRecette = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TotalRecette.Value).DefaultIfEmpty(0).Sum();

                model.Versement = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.Versement.Value).DefaultIfEmpty(0).Sum();
                model.VersementInterieur = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.VersementInterieur.Value).DefaultIfEmpty(0).Sum();
                model.TotalVersement = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TotalVersement.Value).DefaultIfEmpty(0).Sum();


                model.AutreReglement = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.AutreReglement.Value).DefaultIfEmpty(0).Sum();

                model.Pret = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.PretsAceJour.Value).DefaultIfEmpty(0).Sum();


                model.PretsAceJour = _context.FicheSituations.Select(x => x.PretsAceJour.Value).DefaultIfEmpty(0).Sum();


                model.VenteAceJour = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TotalVente.Value).DefaultIfEmpty(0).Sum();
                model.MoyenneJournaliere = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TotalVente.Value).DefaultIfEmpty(0).Average();
                model.MinJournaliere = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TotalVente.Value).DefaultIfEmpty(0).Min();
                model.MaxJournaliere = _context.FicheSituations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TotalVente.Value).DefaultIfEmpty(0).Max();



                list.Add(model);
                return View(list);

            }

        }





        [HttpGet]
        public ActionResult StatistiquesVrac()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiquesVrac> list = new List<ModelStatistiquesVrac>();

                ModelStatistiquesVrac model = new ModelStatistiquesVrac();



                return View(list);

            }


        }


        [HttpPost]
        public ActionResult StatistiquesVrac(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiquesVrac> list = new List<ModelStatistiquesVrac>();

                ModelStatistiquesVrac model = new ModelStatistiquesVrac();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;




                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;


                model.TonnageSomagaz = (decimal)(_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeBonChargement == 1 && x.CodeClient == 0).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum()) - (_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin && x.idTypeBonRetour == 1 && x.CodeCentre != 0).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum());
                model.TonnageCollaborateurs = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeBonChargement == 2 && x.CodeClient != 0).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();
                model.TonnagePrivee = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeBonChargement == 3 && x.CodeClient != 0).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();
                model.TonnageStar = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeBonChargement == 4 && x.CodeClient != 0).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();
                model.TonnagePartenaires = (decimal)(_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeBonChargement == 5 && x.CodeClient != 0).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum()) - (decimal)_context.Livraisons.Where(x => x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient != 0  && x.BonRetourCiterne.idTypeBonRetour == 2).Select(x => x.PoidsRetour.Value).DefaultIfEmpty(0).Sum();

                /*- (_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin && x.idTypeBonRetour == 2 && x.CodeClient != 0).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum());*/



                list.Add(model);
                return View(list);

            }

        }






        [HttpGet]
        public ActionResult StatistiquesRetourVrac()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiqueRetourVrac> list = new List<ModelStatistiqueRetourVrac>();

                ModelStatistiqueRetourVrac model = new ModelStatistiqueRetourVrac();



                return View(list);

            }


        }


        [HttpPost]
        public ActionResult StatistiquesRetourVrac(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiqueRetourVrac> list = new List<ModelStatistiqueRetourVrac>();

                ModelStatistiqueRetourVrac model = new ModelStatistiqueRetourVrac();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;




                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;


                model.TonnageSomagaz = (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin && x.idTypeBonRetour == 1 && x.CodeCentre != 0).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();
                model.TonnagePartenaires = (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin && x.idTypeBonRetour == 2 && x.CodeClient != 0).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();


                list.Add(model);
                return View(list);

            }

        }






        [HttpGet]
        public ActionResult BonusClients(DateTime? dateDebut, DateTime? dateFin, int? page)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                ListeBonusClients model = new ListeBonusClients();


                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                int pageSize = 10;
                int pageIndex = page.HasValue ? page.Value : 1;
                model.ListeBonusClient = BusinessGestionStatistiques.Instance.ChercherListeBonusClients(model.dateDebut, model.dateFin).ToPagedList(pageIndex, pageSize);




                return View(model);

            }



        }


        [HttpGet]
        public ActionResult BonusVracPartenaires(DateTime? dateDebut, DateTime? dateFin, int? page)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                ListeBonusVracPartenaires model = new ListeBonusVracPartenaires();


                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                int pageSize = 10;
                int pageIndex = page.HasValue ? page.Value : 1;
                model.ListeBonusPartenaires = BusinessGestionStatistiques.Instance.ChercherListeBonusVracPartenaires(model.dateDebut, model.dateFin).ToPagedList(pageIndex, pageSize);




                return View(model);

            }



        }



        [HttpGet]
        public ActionResult BonusVracPrivees(DateTime? dateDebut, DateTime? dateFin, int? page)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                ListeBonusVracPrivee model = new ListeBonusVracPrivee();


                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                int pageSize = 10;
                int pageIndex = page.HasValue ? page.Value : 1;
                model.ListeBonusPrivee = BusinessGestionStatistiques.Instance.ChercherListeBonusVracPrivee(model.dateDebut, model.dateFin).ToPagedList(pageIndex, pageSize);

                return View(model);

            }



        }



        [HttpGet]
        public ActionResult VenteClients(DateTime? dateDebut, DateTime? dateFin, int? page)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                ListeVenteClients model = new ListeVenteClients();


                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                int pageSize = 10;
                int pageIndex = page.HasValue ? page.Value : 1;
                model.ListeVenteClient = BusinessGestionStatistiques.Instance.ChercherListeVentesClients(model.dateDebut, model.dateFin).ToPagedList(pageIndex, pageSize);




                return View(model);

            }



        }






        [HttpGet]
        public ActionResult SituationCentres()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelSituationCentreStatistique> list = new List<ModelSituationCentreStatistique>();

                ModelSituationCentreStatistique model = new ModelSituationCentreStatistique();



                return View(list);

            }


        }


        [HttpPost]
        public ActionResult SituationCentres(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelSituationCentreStatistique> list = new List<ModelSituationCentreStatistique>();

                ModelSituationCentreStatistique model = new ModelSituationCentreStatistique();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;




                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;




                // Stock Centres


                model.StockNktt = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 10 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.StockCentres.Where(x => x.CodeCentre == 10 && x.Date > dateDebut && x.Date <= dateFin).Select(x => x.Depotage.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour > dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();

                model.DepotageNktt = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.Depotage.Value).DefaultIfEmpty(0).Sum();




                model.StockNOUADHIBOU = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 1 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - (decimal)_context.BonRetourCiternes.Where(x => x.CodeCentre == 1 && x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();
                model.StockATAR = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 2 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - (decimal)_context.BonRetourCiternes.Where(x => x.CodeCentre == 2 && x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();
                model.StockZOUERATE = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 3 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - (decimal)_context.BonRetourCiternes.Where(x => x.CodeCentre == 3 && x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();
                model.StockALEG = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 4 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - (decimal)_context.BonRetourCiternes.Where(x => x.CodeCentre == 4 && x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();
                model.StockSEILIBABY = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 5 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - (decimal)_context.BonRetourCiternes.Where(x => x.CodeCentre == 5 && x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();
                model.StockKIFFA = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 6 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - (decimal)_context.BonRetourCiternes.Where(x => x.CodeCentre == 6 && x.DateRetour > dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();
                model.StockAIOUN = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 7 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - (decimal)_context.BonRetourCiternes.Where(x => x.CodeCentre == 7 && x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();
                model.StockNEMA = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 8 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - (decimal)_context.BonRetourCiternes.Where(x => x.CodeCentre == 8 && x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();











                //Nombre d’emballage dotation

                model.B12Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
                model.B9Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
                model.B3Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
                model.B35Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();




                //Nombre d’emballage a consigner

                model.B12Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
                model.B9Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                model.B3Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                model.B35Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();




                //Nombre d’emballage Avoir

                model.B12Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B9Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B6RAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum();
                model.B6VAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum();
                model.B3Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum();
                model.B35Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum();





                //Nombre d’emballage produisez

                model.B12Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B9Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6RProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B6VProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum());
                model.B3Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum());
                model.B35Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum());





                model.TonnageSomagaz = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == 0 && x.idTypeBonChargement == 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                model.TotalRetourLivrer = (decimal)_context.Livraisons.Where(x => x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient != 0).Select(x => x.PoidsRetour.Value).DefaultIfEmpty(0).Sum();


                // Vrac Facturation
                model.TonnagePrivee = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1 && x.idTypeBonChargement != 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - model.TotalRetourLivrer;

                // Tous les Vracs sauf somagaz



                //model.TonnagePartenaire = (float)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                model.AutresVrac = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                model.TotalProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000 + model.TonnageSomagaz + model.AutresVrac;

                model.TotalCommerciale = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000 + model.TonnagePrivee;

                model.TotalConditionneeProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000;
                model.TotalConditionneeCommerciale = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000;






                // Vente Nktt


                model.B12VenteNouakchott = model.B12Afact;
                model.B9VenteNouakchott = model.B9Afact;
                model.B6RVenteNouakchott = model.B6RAfact;
                model.B6RVenteNouakchott = model.B6VAfact;
                model.B3VenteNouakchott = model.B3Afact;
                model.B35VenteNouakchott = model.B35Afact;


                // Consignation Nktt

                model.B12ConsignationNouakchott = model.B12Consignation;
                model.B9ConsignationNouakchott = model.B9Consignation;
                model.B6RConsignationNouakchott = model.B6RConsignation;
                model.B6VConsignationNouakchott = model.B6VConsignation;
                model.B3ConsignationNouakchott = model.B3Consignation;
                model.B35ConsignationNouakchott = model.B35Consignation;


                // Dotation Nktt

                model.B12DotationNouakchott = model.B12Dotation;
                model.B9DotationNouakchott = model.B9Dotation;
                model.B6RDotationNouakchott = model.B6RDotation;
                model.B6VDotationNouakchott = model.B6VDotation;
                model.B3DotationNouakchott = model.B3Dotation;
                model.B35DotationNouakchott = model.B35Dotation;




                // Emballage Nktt

                model.B12EmballageNouakchott = _context.EmballageCentres.Where(x => x.CodeCentre == 10 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9EmballageNouakchott = _context.EmballageCentres.Where(x => x.CodeCentre == 10 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6REmballageNouakchott = _context.EmballageCentres.Where(x => x.CodeCentre == 10 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6VEmballageNouakchott = _context.EmballageCentres.Where(x => x.CodeCentre == 10 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B3EmballageNouakchott = _context.EmballageCentres.Where(x => x.CodeCentre == 10 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B35EmballageNouakchott = _context.EmballageCentres.Where(x => x.CodeCentre == 10 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 10 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();







                // Vente NDB

                model.B12VenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B9VenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6RVenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6VVenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B3VenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();


                // Consignation NDB

                model.B12ConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9ConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3ConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35ConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                // Dotation NDB

                model.B12DotationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9DotationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35DotationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();

                // Emballage NDB

                model.B12EmballageNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9EmballageNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6REmballageNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VEmballageNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();




                // Vente ATAR

                model.B12VenteATAR = _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B9VenteATAR = _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6RVenteATAR = _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6VVenteATAR = _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B3VenteATAR = _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteATAR = _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();

                // Consignation ATAR

                model.B12ConsignationATAR = _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9ConsignationATAR = _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignationATAR = _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationATAR = _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3ConsignationATAR = _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35ConsignationATAR = _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                // Dotation ATAR

                model.B12DotationATAR = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9DotationATAR = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotationATAR = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationATAR = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationATAR = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35DotationATAR = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();

                // Emballage ATAR

                model.B12EmballageATAR = _context.EmballageCentres.Where(x => x.CodeCentre == 2 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9EmballageATAR = _context.EmballageCentres.Where(x => x.CodeCentre == 2 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6REmballageATAR = _context.EmballageCentres.Where(x => x.CodeCentre == 2 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VEmballageATAR = _context.EmballageCentres.Where(x => x.CodeCentre == 2 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationATAR = _context.EmballageCentres.Where(x => x.CodeCentre == 2 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteATAR = _context.EmballageCentres.Where(x => x.CodeCentre == 2 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();




                // Vente ZOUERATE

                model.B12VenteZOUERATE = _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B9VenteZOUERATE = _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6RVenteZOUERATE = _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6VVenteZOUERATE = _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B3VenteZOUERATE = _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteZOUERATE = _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();



                // Consignation ZOUERATE

                model.B12ConsignationZOUERATE = _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9ConsignationZOUERATE = _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignationZOUERATE = _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationZOUERATE = _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3ConsignationZOUERATE = _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35ConsignationZOUERATE = _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                // Dotation ZOUERATE

                model.B12DotationZOUERATE = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9DotationZOUERATE = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotationZOUERATE = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationZOUERATE = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationZOUERATE = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35DotationZOUERATE = _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();

                // Emballage ZOUERATE

                model.B12EmballageZOUERATE = _context.EmballageCentres.Where(x => x.CodeCentre == 3 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9EmballageZOUERATE = _context.EmballageCentres.Where(x => x.CodeCentre == 3 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6REmballageZOUERATE = _context.EmballageCentres.Where(x => x.CodeCentre == 3 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VEmballageZOUERATE = _context.EmballageCentres.Where(x => x.CodeCentre == 3 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationZOUERATE = _context.EmballageCentres.Where(x => x.CodeCentre == 3 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteZOUERATE = _context.EmballageCentres.Where(x => x.CodeCentre == 3 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 3 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();





                // Vente ALEG

                model.B12VenteALEG = _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B9VenteALEG = _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6RVenteALEG = _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6VVenteALEG = _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B3VenteALEG = _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteALEG = _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();



                // Consignation ALEG

                model.B12ConsignationALEG = _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9ConsignationALEG = _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignationALEG = _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationALEG = _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3ConsignationALEG = _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35ConsignationALEG = _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                // Dotation ALEG

                model.B12DotationALEG = _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9DotationALEG = _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotationALEG = _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationALEG = _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationALEG = _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35DotationALEG = _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();

                // Emballage ALEG

                model.B12EmballageALEG = _context.EmballageCentres.Where(x => x.CodeCentre == 4 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9EmballageALEG = _context.EmballageCentres.Where(x => x.CodeCentre == 4 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VEmballageALEG = _context.EmballageCentres.Where(x => x.CodeCentre == 4 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationALEG = _context.EmballageCentres.Where(x => x.CodeCentre == 4 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteALEG = _context.EmballageCentres.Where(x => x.CodeCentre == 4 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 4 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();







                // Vente SEILIBABY

                model.B12VenteSEILIBABY = _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B9VenteSEILIBABY = _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6RVenteSEILIBABY = _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6VVenteSEILIBABY = _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B3VenteSEILIBABY = _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteSEILIBABY = _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();


                // Consignation SEILIBABY

                model.B12ConsignationSEILIBABY = _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9ConsignationSEILIBABY = _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignationSEILIBABY = _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationSEILIBABY = _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3ConsignationSEILIBABY = _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35ConsignationSEILIBABY = _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                // Dotation SEILIBABY

                model.B12DotationSEILIBABY = _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9DotationSEILIBABY = _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotationSEILIBABY = _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationSEILIBABY = _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationSEILIBABY = _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35DotationSEILIBABY = _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();

                // Emballage SEILIBABY

                model.B9EmballageSEILIBABY = _context.EmballageCentres.Where(x => x.CodeCentre == 5 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6REmballageSEILIBABY = _context.EmballageCentres.Where(x => x.CodeCentre == 5 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VEmballageSEILIBABY = _context.EmballageCentres.Where(x => x.CodeCentre == 5 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationSEILIBABY = _context.EmballageCentres.Where(x => x.CodeCentre == 5 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteSEILIBABY = _context.EmballageCentres.Where(x => x.CodeCentre == 5 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 5 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();





                // Vente KIFFA

                model.B12VenteKIFFA = _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B9VenteKIFFA = _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6RVenteKIFFA = _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6VVenteKIFFA = _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B3VenteKIFFA = _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteKIFFA = _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();



                // Consignation KIFFA

                model.B12ConsignationKIFFA = _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9ConsignationKIFFA = _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignationKIFFA = _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationKIFFA = _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3ConsignationKIFFA = _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35ConsignationKIFFA = _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                // Dotation KIFFA

                model.B12DotationKIFFA = _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9DotationKIFFA = _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotationKIFFA = _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationKIFFA = _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationKIFFA = _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35DotationKIFFA = _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();

                // Emballage KIFFA

                model.B12EmballageKIFFA = _context.EmballageCentres.Where(x => x.CodeCentre == 6 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9EmballageKIFFA = _context.EmballageCentres.Where(x => x.CodeCentre == 6 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6REmballageKIFFA = _context.EmballageCentres.Where(x => x.CodeCentre == 6 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VEmballageKIFFA = _context.EmballageCentres.Where(x => x.CodeCentre == 6 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationKIFFA = _context.EmballageCentres.Where(x => x.CodeCentre == 6 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteKIFFA = _context.EmballageCentres.Where(x => x.CodeCentre == 6 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 6 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();





                // Vente AIOUN

                model.B12VenteAIOUN = _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B9VenteAIOUN = _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6RVenteAIOUN = _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6VVenteAIOUN = _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B3VenteAIOUN = _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteAIOUN = _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();


                // Consignation AIOUN

                model.B12ConsignationAIOUN = _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9ConsignationAIOUN = _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignationAIOUN = _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationAIOUN = _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3ConsignationAIOUN = _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35ConsignationAIOUN = _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                // Dotation AIOUN

                model.B12DotationAIOUN = _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9DotationAIOUN = _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotationAIOUN = _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationAIOUN = _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationAIOUN = _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35DotationAIOUN = _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();

                // Emballage AIOUN

                model.B12EmballageAIOUN = _context.EmballageCentres.Where(x => x.CodeCentre == 7 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9EmballageAIOUN = _context.EmballageCentres.Where(x => x.CodeCentre == 7 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6REmballageAIOUN = _context.EmballageCentres.Where(x => x.CodeCentre == 7 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VEmballageAIOUN = _context.EmballageCentres.Where(x => x.CodeCentre == 7 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationAIOUN = _context.EmballageCentres.Where(x => x.CodeCentre == 7 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteAIOUN = _context.EmballageCentres.Where(x => x.CodeCentre == 7 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 7 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();







                // Vente NEMA

                model.B12VenteNEMA = _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B9VenteNEMA = _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6RVenteNEMA = _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6VVenteNEMA = _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B3VenteNEMA = _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteNEMA = _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();


                // Consignation NEMA

                model.B12ConsignationNEMA = _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9ConsignationNEMA = _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignationNEMA = _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationNEMA = _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3ConsignationNEMA = _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35ConsignationNEMA = _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                // Dotation NEMA

                model.B12DotationNEMA = _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9DotationNEMA = _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotationNEMA = _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationNEMA = _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationNEMA = _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35DotationNEMA = _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();

                // Emballage NEMA

                model.B12EmballageNEMA = _context.EmballageCentres.Where(x => x.CodeCentre == 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9EmballageNEMA = _context.EmballageCentres.Where(x => x.CodeCentre == 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6REmballageNEMA = _context.EmballageCentres.Where(x => x.CodeCentre == 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VEmballageNEMA = _context.EmballageCentres.Where(x => x.CodeCentre == 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationNEMA = _context.EmballageCentres.Where(x => x.CodeCentre == 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteNEMA = _context.EmballageCentres.Where(x => x.CodeCentre == 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();




                // Production  Centres par Tonne Metrique

                model.TotalProductionNktt = model.TotalProduction;
                model.TotalProductionNOUADHIBOU = (decimal)((model.B12VenteNOUADHIBOU + model.B12ConsignationNOUADHIBOU + model.B12DotationNOUADHIBOU) * 12.5 + (model.B9VenteNOUADHIBOU + model.B9ConsignationNOUADHIBOU + model.B9DotationNOUADHIBOU) * 9 + (model.B6RVenteNOUADHIBOU + model.B6RConsignationNOUADHIBOU + model.B6RDotationNouakchott) * 6 + (model.B6VVenteNOUADHIBOU + model.B6VConsignationNOUADHIBOU + model.B6VDotationNouakchott) * 6 + (model.B3VenteNOUADHIBOU + model.B3ConsignationNOUADHIBOU + model.B3DotationNouakchott) * 2.75 + (model.B35VenteNOUADHIBOU + model.B35ConsignationNOUADHIBOU + model.B35DotationNouakchott) * 35) / 1000;
                model.TotalProductionATAR = (decimal)((model.B12VenteATAR + model.B12ConsignationATAR + model.B12DotationATAR) * 12.5 + (model.B9VenteATAR + model.B9ConsignationATAR + model.B9DotationATAR) * 9 + (model.B6RVenteATAR + model.B6RConsignationATAR + model.B6RDotationATAR) * 6 + (model.B6VVenteATAR + model.B6VConsignationATAR + model.B6VDotationATAR) * 6 + (model.B3VenteATAR + model.B3ConsignationATAR + model.B3DotationATAR) * 2.75 + (model.B35VenteATAR + model.B35ConsignationATAR + model.B35DotationATAR) * 35) / 1000;
                model.TotalProductionZOUERATE = (decimal)((model.B12VenteZOUERATE + model.B12ConsignationZOUERATE + model.B12DotationATAR) * 12.5 + (model.B9VenteZOUERATE + model.B9ConsignationZOUERATE + model.B9DotationZOUERATE) * 9 + (model.B6RVenteZOUERATE + model.B6RConsignationZOUERATE + model.B6RDotationZOUERATE) * 6 + (model.B6VVenteZOUERATE + model.B6VConsignationZOUERATE + model.B6VDotationZOUERATE) * 6 + (model.B3VenteZOUERATE + model.B3ConsignationZOUERATE + model.B3DotationZOUERATE) * 2.75 + (model.B35VenteZOUERATE + model.B35ConsignationZOUERATE + model.B35DotationZOUERATE) * 35) / 1000;
                model.TotalProductionALEG = (decimal)((model.B12VenteALEG + model.B12ConsignationALEG + model.B12DotationALEG) * 12.5 + (model.B9VenteALEG + model.B9ConsignationALEG + model.B9DotationALEG) * 9 + (model.B6RVenteALEG + model.B6RConsignationATAR + model.B6RDotationALEG) * 6 + (model.B6VVenteALEG + model.B6VConsignationALEG + model.B6VDotationALEG) * 6 + (model.B3VenteALEG + model.B3ConsignationALEG + model.B3DotationALEG) * 2.75 + (model.B35VenteALEG + model.B35ConsignationALEG + model.B35DotationALEG) * 35) / 1000;
                model.TotalProductionSEILIBABY = (decimal)((model.B12VenteSEILIBABY + model.B12ConsignationSEILIBABY + model.B12DotationSEILIBABY) * 12.5 + (model.B9VenteSEILIBABY + model.B9ConsignationSEILIBABY + model.B9DotationSEILIBABY) * 9 + (model.B6RVenteSEILIBABY + model.B6RConsignationSEILIBABY + model.B6RDotationSEILIBABY) * 6 + (model.B6VVenteSEILIBABY + model.B6VConsignationSEILIBABY + model.B6VDotationSEILIBABY) * 6 + (model.B3VenteSEILIBABY + model.B3ConsignationSEILIBABY + model.B3DotationSEILIBABY) * 2.75 + (model.B35VenteSEILIBABY + model.B35ConsignationSEILIBABY + model.B35DotationSEILIBABY) * 35) / 1000;
                model.TotalProductionKIFFA = (decimal)((model.B12VenteKIFFA + model.B12ConsignationKIFFA + model.B12DotationKIFFA) * 12.5 + (model.B9VenteKIFFA + model.B9ConsignationKIFFA + model.B9DotationKIFFA) * 9 + (model.B6RVenteKIFFA + model.B6RConsignationKIFFA + model.B6RDotationKIFFA) * 6 + (model.B6VVenteKIFFA + model.B6VConsignationKIFFA + model.B6VDotationKIFFA) * 6 + (model.B3VenteKIFFA + model.B3ConsignationKIFFA + model.B3DotationKIFFA) * 2.75 + (model.B35VenteKIFFA + model.B35ConsignationKIFFA + model.B35DotationKIFFA) * 35) / 1000;
                model.TotalProductionAIOUN = (decimal)((model.B12VenteAIOUN + model.B12ConsignationAIOUN + model.B12DotationAIOUN) * 12.5 + (model.B9VenteAIOUN + model.B9ConsignationAIOUN + model.B9DotationAIOUN) * 9 + (model.B6RVenteAIOUN + model.B6RConsignationAIOUN + model.B6RDotationAIOUN) * 6 + (model.B6VVenteAIOUN + model.B6VConsignationAIOUN + model.B6VDotationAIOUN) * 6 + (model.B3VenteAIOUN + model.B3ConsignationAIOUN + model.B3DotationAIOUN) * 2.75 + (model.B35VenteAIOUN + model.B35ConsignationAIOUN + model.B35DotationAIOUN) * 35) / 1000;
                model.TotalProductionNEMA = (decimal)((model.B12VenteNEMA + model.B12ConsignationNEMA + model.B12DotationNEMA) * 12.5 + (model.B9VenteNEMA + model.B9ConsignationNEMA + model.B9DotationNEMA) * 9 + (model.B6RVenteNEMA + model.B6RConsignationNEMA + model.B6RDotationNEMA) * 6 + (model.B6VVenteNEMA + model.B6VConsignationNEMA + model.B6VDotationNEMA) * 6 + (model.B3VenteNEMA + model.B3ConsignationNEMA + model.B3DotationNEMA) * 2.75 + (model.B35VenteNEMA + model.B35ConsignationNEMA + model.B35DotationNEMA) * 35) / 1000;


                // Facturations  Centres par Bouteilles



                model.B12TotalFacturationNouakchott = (model.B12Afact);
                model.B9TotalFacturationNouakchott = (model.B12Afact);
                model.B6RTotalFacturationNouakchott = (model.B12Afact);
                model.B6VTotalFacturationNouakchott = (model.B12Afact);
                model.B3TotalFacturationNouakchott = (model.B12Afact);
                model.B35TotalFacturationNouakchott = (model.B12Afact);



                model.B12TotalFacturationNOUADHIBOU = (model.B12VenteNOUADHIBOU + model.B12ConsignationNOUADHIBOU);
                model.B9TotalFacturationNOUADHIBOU = (model.B9VenteNOUADHIBOU + model.B9ConsignationNOUADHIBOU);
                model.B6RTotalFacturationNOUADHIBOU = (model.B6RVenteNOUADHIBOU + model.B6RConsignationNOUADHIBOU);
                model.B6VTotalFacturationNOUADHIBOU = (model.B6VVenteNOUADHIBOU + model.B6VConsignationNOUADHIBOU);
                model.B3TotalFacturationNOUADHIBOU = (model.B3VenteNOUADHIBOU + model.B3ConsignationNOUADHIBOU);
                model.B35TotalFacturationNOUADHIBOU = (model.B35VenteNOUADHIBOU + model.B35ConsignationNOUADHIBOU);


                model.B12TotalFacturationATAR = (model.B12VenteATAR + model.B12ConsignationATAR);
                model.B9TotalFacturationATAR = (model.B9VenteATAR + model.B9ConsignationATAR);
                model.B6RTotalFacturationATAR = (model.B6RVenteATAR + model.B6RConsignationATAR);
                model.B6VTotalFacturationATAR = (model.B6VVenteATAR + model.B6VConsignationATAR);
                model.B3TotalFacturationATAR = (model.B3VenteATAR + model.B3ConsignationATAR);
                model.B35TotalFacturationATAR = (model.B35VenteATAR + model.B35ConsignationATAR);



                model.B12TotalFacturationZOUERATE = (model.B12VenteZOUERATE + model.B12ConsignationZOUERATE);
                model.B9TotalFacturationZOUERATE = (model.B9VenteZOUERATE + model.B9ConsignationZOUERATE);
                model.B6RTotalFacturationZOUERATE = (model.B6RVenteZOUERATE + model.B6RConsignationZOUERATE);
                model.B6VTotalFacturationZOUERATE = (model.B6VVenteZOUERATE + model.B6VConsignationZOUERATE);
                model.B3TotalFacturationZOUERATE = (model.B3VenteZOUERATE + model.B3ConsignationZOUERATE);
                model.B35TotalFacturationZOUERATE = (model.B35VenteATAR + model.B35ConsignationZOUERATE);



                model.B12TotalFacturationALEG = (model.B12VenteALEG + model.B12ConsignationALEG);
                model.B9TotalFacturationALEG = (model.B9VenteALEG + model.B9ConsignationALEG);
                model.B6RTotalFacturationALEG = (model.B6RVenteALEG + model.B6RConsignationALEG);
                model.B6VTotalFacturationALEG = (model.B6VVenteALEG + model.B6VConsignationALEG);
                model.B3TotalFacturationALEG = (model.B3VenteALEG + model.B3ConsignationALEG);
                model.B35TotalFacturationALEG = (model.B35VenteALEG + model.B35ConsignationALEG);



                model.B12TotalFacturationSEILIBABY = (model.B12VenteSEILIBABY + model.B12ConsignationSEILIBABY);
                model.B9TotalFacturationSEILIBABY = (model.B9VenteSEILIBABY + model.B9ConsignationSEILIBABY);
                model.B6RTotalFacturationSEILIBABY = (model.B6RVenteSEILIBABY + model.B6RConsignationSEILIBABY);
                model.B6VTotalFacturationSEILIBABY = (model.B6VVenteSEILIBABY + model.B6VConsignationSEILIBABY);
                model.B3TotalFacturationALEG = (model.B3VenteSEILIBABY + model.B3ConsignationSEILIBABY);
                model.B35TotalFacturationALEG = (model.B35VenteSEILIBABY + model.B35ConsignationSEILIBABY);


                model.B12TotalFacturationKIFFA = (model.B12VenteSEILIBABY + model.B12ConsignationKIFFA);
                model.B9TotalFacturationKIFFA = (model.B9VenteSEILIBABY + model.B9ConsignationKIFFA);
                model.B6RTotalFacturationKIFFA = (model.B6RVenteSEILIBABY + model.B6RConsignationKIFFA);
                model.B6VTotalFacturationKIFFA = (model.B6VVenteSEILIBABY + model.B6VConsignationKIFFA);
                model.B3TotalFacturationKIFFA = (model.B3VenteSEILIBABY + model.B3ConsignationKIFFA);
                model.B35TotalFacturationKIFFA = (model.B35VenteSEILIBABY + model.B35ConsignationKIFFA);


                model.B12TotalFacturationAIOUN = (model.B12VenteAIOUN + model.B12ConsignationAIOUN);
                model.B9TotalFacturationAIOUN = (model.B9VenteAIOUN + model.B9ConsignationAIOUN);
                model.B6RTotalFacturationAIOUN = (model.B6RVenteAIOUN + model.B6RConsignationAIOUN);
                model.B6VTotalFacturationAIOUN = (model.B6VVenteAIOUN + model.B6VConsignationAIOUN);
                model.B3TotalFacturationAIOUN = (model.B3VenteAIOUN + model.B3ConsignationAIOUN);
                model.B35TotalFacturationAIOUN = (model.B35VenteAIOUN + model.B35ConsignationAIOUN);


                model.B12TotalFacturationNEMA = (model.B12VenteNEMA + model.B12ConsignationNEMA);
                model.B9TotalFacturationNEMA = (model.B9VenteNEMA + model.B9ConsignationNEMA);
                model.B6RTotalFacturationNEMA = (model.B6RVenteNEMA + model.B6RConsignationNEMA);
                model.B6VTotalFacturationNEMA = (model.B6VVenteNEMA + model.B6VConsignationNEMA);
                model.B3TotalFacturationNEMA = (model.B3VenteNEMA + model.B3ConsignationNEMA);
                model.B35TotalFacturationNEMA = (model.B35VenteNEMA + model.B35ConsignationNEMA);





                // Productions  Centres par Bouteilles


                model.B12TotalProductionNouakchott = (model.B12Production);
                model.B9TotalProductionNouakchott = (model.B12Production);
                model.B6RTotalProductionNouakchott = (model.B12Production);
                model.B6VTotalProductionNouakchott = (model.B12Production);
                model.B3TotalProductionNouakchott = (model.B12Production);
                model.B35TotalProductionNouakchott = (model.B12Production);



                model.B12TotalProductionNOUADHIBOU = (model.B12VenteNOUADHIBOU + model.B12ConsignationNOUADHIBOU + model.B12DotationNOUADHIBOU);
                model.B9TotalProductionNOUADHIBOU = (model.B9VenteNOUADHIBOU + model.B9ConsignationNOUADHIBOU + model.B9DotationNOUADHIBOU);
                model.B6RTotalProductionNOUADHIBOU = (model.B6RVenteNOUADHIBOU + model.B6RConsignationNOUADHIBOU + model.B6RDotationNOUADHIBOU);
                model.B6VTotalProductionNOUADHIBOU = (model.B6VVenteNOUADHIBOU + model.B6VConsignationNOUADHIBOU + model.B6VDotationNOUADHIBOU);
                model.B3TotalProductionNOUADHIBOU = (model.B3VenteNOUADHIBOU + model.B3ConsignationNOUADHIBOU + model.B3DotationNOUADHIBOU);
                model.B35TotalProductionNOUADHIBOU = (model.B35VenteNOUADHIBOU + model.B35ConsignationNOUADHIBOU + model.B35DotationNOUADHIBOU);


                model.B12TotalProductionATAR = (model.B12VenteATAR + model.B12ConsignationATAR + model.B12DotationATAR);
                model.B9TotalProductionATAR = (model.B9VenteATAR + model.B9ConsignationATAR + model.B9DotationATAR);
                model.B6RTotalProductionATAR = (model.B6RVenteATAR + model.B6RConsignationATAR + model.B6RDotationATAR);
                model.B6VTotalProductionATAR = (model.B6VVenteATAR + model.B6VConsignationATAR + model.B6VDotationATAR);
                model.B3TotalProductionATAR = (model.B3VenteATAR + model.B3ConsignationATAR + model.B3DotationATAR);
                model.B35TotalProductionATAR = (model.B35VenteATAR + model.B35ConsignationATAR + model.B35DotationATAR);



                model.B12TotalProductionZOUERATE = (model.B12VenteZOUERATE + model.B12ConsignationZOUERATE + model.B12DotationZOUERATE);
                model.B9TotalProductionZOUERATE = (model.B9VenteZOUERATE + model.B9ConsignationZOUERATE + model.B9DotationZOUERATE);
                model.B6RTotalProductionZOUERATE = (model.B6RVenteZOUERATE + model.B6RConsignationZOUERATE + model.B6RDotationZOUERATE);
                model.B6VTotalProductionZOUERATE = (model.B6VVenteZOUERATE + model.B6VConsignationZOUERATE + model.B6VDotationZOUERATE);
                model.B3TotalProductionZOUERATE = (model.B3VenteZOUERATE + model.B3ConsignationZOUERATE + model.B3DotationZOUERATE);
                model.B35TotalProductionZOUERATE = (model.B35VenteATAR + model.B35ConsignationZOUERATE + model.B35DotationZOUERATE);



                model.B12TotalProductionALEG = (model.B12VenteALEG + model.B12ConsignationALEG + model.B12DotationALEG);
                model.B9TotalProductionALEG = (model.B9VenteALEG + model.B9ConsignationALEG + model.B9DotationALEG);
                model.B6RTotalProductionALEG = (model.B6RVenteALEG + model.B6RConsignationALEG + model.B6RDotationALEG);
                model.B6VTotalProductionALEG = (model.B6VVenteALEG + model.B6VConsignationALEG + model.B6VDotationALEG);
                model.B3TotalProductionALEG = (model.B3VenteALEG + model.B3ConsignationALEG + model.B3DotationALEG);
                model.B35TotalProductionALEG = (model.B35VenteALEG + model.B35ConsignationALEG + model.B35DotationALEG);



                model.B12TotalProductionSEILIBABY = (model.B12VenteSEILIBABY + model.B12ConsignationSEILIBABY + model.B12DotationSEILIBABY);
                model.B9TotalProductionSEILIBABY = (model.B9VenteSEILIBABY + model.B9ConsignationSEILIBABY + model.B9DotationSEILIBABY);
                model.B6RTotalProductionSEILIBABY = (model.B6RVenteSEILIBABY + model.B6RConsignationSEILIBABY + model.B6RDotationSEILIBABY);
                model.B6VTotalProductionSEILIBABY = (model.B6VVenteSEILIBABY + model.B6VConsignationSEILIBABY + model.B6VDotationSEILIBABY);
                model.B3TotalProductionALEG = (model.B3VenteSEILIBABY + model.B3ConsignationSEILIBABY + model.B3DotationSEILIBABY);
                model.B35TotalProductionALEG = (model.B35VenteSEILIBABY + model.B35ConsignationSEILIBABY + model.B35DotationSEILIBABY);


                model.B12TotalProductionKIFFA = (model.B12VenteSEILIBABY + model.B12ConsignationKIFFA + model.B12DotationKIFFA);
                model.B9TotalProductionKIFFA = (model.B9VenteSEILIBABY + model.B9ConsignationKIFFA + model.B9DotationKIFFA);
                model.B6RTotalProductionKIFFA = (model.B6RVenteSEILIBABY + model.B6RConsignationKIFFA + model.B6RDotationKIFFA);
                model.B6VTotalProductionKIFFA = (model.B6VVenteSEILIBABY + model.B6VConsignationKIFFA + model.B6VDotationKIFFA);
                model.B3TotalProductionKIFFA = (model.B3VenteSEILIBABY + model.B3ConsignationKIFFA + model.B3DotationKIFFA);
                model.B35TotalProductionKIFFA = (model.B35VenteSEILIBABY + model.B35ConsignationKIFFA + model.B35DotationKIFFA);


                model.B12TotalProductionAIOUN = (model.B12VenteAIOUN + model.B12ConsignationAIOUN + model.B12DotationAIOUN);
                model.B9TotalProductionAIOUN = (model.B9VenteAIOUN + model.B9ConsignationAIOUN + model.B9DotationAIOUN);
                model.B6RTotalProductionAIOUN = (model.B6RVenteAIOUN + model.B6RConsignationAIOUN + model.B6RDotationAIOUN);
                model.B6VTotalProductionAIOUN = (model.B6VVenteAIOUN + model.B6VConsignationAIOUN + model.B6VDotationAIOUN);
                model.B3TotalProductionAIOUN = (model.B3VenteAIOUN + model.B3ConsignationAIOUN + model.B3DotationAIOUN);
                model.B35TotalProductionAIOUN = (model.B35VenteAIOUN + model.B35ConsignationAIOUN + model.B35DotationAIOUN);


                model.B12TotalProductionNEMA = (model.B12VenteNEMA + model.B12ConsignationNEMA + model.B12DotationNEMA);
                model.B9TotalProductionNEMA = (model.B9VenteNEMA + model.B9ConsignationNEMA + model.B9DotationNEMA);
                model.B6RTotalProductionNEMA = (model.B6RVenteNEMA + model.B6RConsignationNEMA + model.B6RDotationNEMA);
                model.B6VTotalProductionNEMA = (model.B6VVenteNEMA + model.B6VConsignationNEMA + model.B6VDotationNEMA);
                model.B3TotalProductionNEMA = (model.B3VenteNEMA + model.B3ConsignationNEMA + model.B3DotationNEMA);
                model.B35TotalProductionNEMA = (model.B35VenteNEMA + model.B35ConsignationNEMA + model.B35DotationNEMA);







                // Facturations  Centres

                model.TotalFacturationNktt = model.TotalCommerciale;
                model.TotalFacturationNOUADHIBOU = (decimal)((model.B12VenteNOUADHIBOU + model.B12ConsignationNOUADHIBOU) * 12.5 + (model.B9VenteNOUADHIBOU + model.B9ConsignationNOUADHIBOU) * 9 + (model.B6RVenteNOUADHIBOU + model.B6RConsignationNOUADHIBOU) * 6 + (model.B6VVenteNOUADHIBOU + model.B6VConsignationNOUADHIBOU) * 6 + (model.B3VenteNOUADHIBOU + model.B3ConsignationNOUADHIBOU) * 2.75 + (model.B35VenteNOUADHIBOU + model.B35ConsignationNOUADHIBOU) * 35) / 1000;
                model.TotalFacturationATAR = (decimal)((model.B12VenteATAR + model.B12ConsignationATAR) * 12.5 + (model.B9VenteATAR + model.B9ConsignationATAR) * 9 + (model.B6RVenteATAR + model.B6RConsignationATAR) * 6 + (model.B6VVenteATAR + model.B6VConsignationATAR) * 6 + (model.B3VenteATAR + model.B3ConsignationATAR) * 2.75 + (model.B35VenteATAR + model.B35ConsignationATAR) * 35) / 1000;
                model.TotalFacturationZOUERATE = (decimal)((model.B12VenteZOUERATE + model.B12ConsignationZOUERATE) * 12.5 + (model.B9VenteZOUERATE + model.B9ConsignationZOUERATE) * 9 + (model.B6RVenteZOUERATE + model.B6RConsignationZOUERATE) * 6 + (model.B6VVenteZOUERATE + model.B6VConsignationZOUERATE) * 6 + (model.B3VenteZOUERATE + model.B3ConsignationZOUERATE) * 2.75 + (model.B35VenteZOUERATE + model.B35ConsignationZOUERATE) * 35) / 1000;
                model.TotalProductionALEG = (decimal)((model.B12VenteALEG + model.B12ConsignationALEG) * 12.5 + (model.B9VenteALEG + model.B9ConsignationALEG) * 9 + (model.B6RVenteALEG + model.B6RConsignationATAR) * 6 + (model.B6VVenteALEG + model.B6VConsignationALEG) * 6 + (model.B3VenteALEG + model.B3ConsignationALEG) * 2.75 + (model.B35VenteALEG + model.B35ConsignationALEG) * 35) / 1000;
                model.TotalFacturationSEILIBABY = (decimal)((model.B12VenteSEILIBABY + model.B12ConsignationSEILIBABY) * 12.5 + (model.B9VenteSEILIBABY + model.B9ConsignationSEILIBABY) * 9 + (model.B6RVenteSEILIBABY + model.B6RConsignationSEILIBABY) * 6 + (model.B6VVenteSEILIBABY + model.B6VConsignationSEILIBABY) * 6 + (model.B3VenteSEILIBABY + model.B3ConsignationSEILIBABY) * 2.75 + (model.B35VenteSEILIBABY + model.B35ConsignationSEILIBABY) * 35) / 1000;
                model.TotalFacturationKIFFA = (decimal)((model.B12VenteKIFFA + model.B12ConsignationKIFFA) * 12.5 + (model.B9VenteKIFFA + model.B9ConsignationKIFFA) * 9 + (model.B6RVenteKIFFA + model.B6RConsignationKIFFA) * 6 + (model.B6VVenteKIFFA + model.B6VConsignationKIFFA) * 6 + (model.B3VenteKIFFA + model.B3ConsignationKIFFA) * 2.75 + (model.B35VenteKIFFA + model.B35ConsignationKIFFA) * 35) / 1000;
                model.TotalFacturationAIOUN = (decimal)((model.B12VenteAIOUN + model.B12ConsignationAIOUN) * 12.5 + (model.B9VenteAIOUN + model.B9ConsignationAIOUN) * 9 + (model.B6RVenteAIOUN + model.B6RConsignationAIOUN) * 6 + (model.B6VVenteAIOUN + model.B6VConsignationAIOUN) * 6 + (model.B3VenteAIOUN + model.B3ConsignationAIOUN) * 2.75 + (model.B35VenteAIOUN + model.B35ConsignationAIOUN) * 35) / 1000;
                model.TotalFacturationNEMA = (decimal)((model.B12VenteNEMA + model.B12ConsignationNEMA) * 12.5 + (model.B9VenteNEMA + model.B9ConsignationNEMA) * 9 + (model.B6RVenteNEMA + model.B6RConsignationNEMA) * 6 + (model.B6VVenteNEMA + model.B6VConsignationNEMA) * 6 + (model.B3VenteNEMA + model.B3ConsignationNEMA) * 2.75 + (model.B35VenteNEMA + model.B35ConsignationNEMA) * 35) / 1000;









                model.TotalStock = (model.StockNktt + model.StockNOUADHIBOU + model.StockATAR + model.StockZOUERATE + model.StockALEG + model.StockSEILIBABY + model.StockKIFFA + model.StockAIOUN + model.StockNEMA);
                model.TotalProductions = (model.TotalProductionNktt + model.TotalProductionNOUADHIBOU + model.TotalProductionATAR + model.TotalProductionZOUERATE + model.TotalProductionALEG + model.TotalProductionSEILIBABY + model.TotalProductionKIFFA + model.TotalProductionAIOUN + model.TotalProductionNEMA);
                model.TotalFacturations = (model.TotalFacturationNktt + model.TotalFacturationNOUADHIBOU + model.TotalFacturationATAR + model.TotalFacturationZOUERATE + model.TotalFacturationALEG + model.TotalFacturationSEILIBABY + model.TotalFacturationKIFFA + model.TotalFacturationAIOUN + model.TotalFacturationNEMA);



                model.TotalConditionneeCommercialeNktt = model.TotalConditionneeCommerciale;
                model.TotalConditionneeProductionNktt = model.TotalConditionneeProduction;
                model.TonnageSomagaz = model.TonnageSomagaz;
                model.AutresVrac = model.AutresVrac;

                model.TonnagePrivee = model.AutresVrac;

                list.Add(model);
                return View(list);

            }

        }








        [HttpGet]
        public ActionResult SituationNDB()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiqueSituationNdb> list = new List<ModelStatistiqueSituationNdb>();

                ModelStatistiqueSituationNdb model = new ModelStatistiqueSituationNdb();



                return View(list);

            }


        }
        [HttpPost]
        public ActionResult SituationNDB(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                List<ModelStatistiqueSituationNdb> list = new List<ModelStatistiqueSituationNdb>();

                ModelStatistiqueSituationNdb model = new ModelStatistiqueSituationNdb();



                ViewBag.dateDebut = dateDebut;
                ViewBag.dateFin = dateFin;




                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;




                // Stock Centres



                model.StockNOUADHIBOU = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 1 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - (decimal)_context.BonRetourCiternes.Where(x => x.CodeCentre == 1 && x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();


                // Vente NDB

                model.B12VenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B9VenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6RVenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B6VVenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                model.B3VenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteNOUADHIBOU = _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();


                // Consignation NDB

                model.B12ConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9ConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3ConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35ConsignationNOUADHIBOU = _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.ConsignationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                // Dotation NDB

                model.B12DotationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9DotationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6RDotationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VConsignationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35DotationNOUADHIBOU = _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.DotationCentres.Where(x => x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();

                // Emballage NDB

                model.B12EmballageNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                model.B9EmballageNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                model.B6REmballageNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                model.B6VEmballageNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                model.B3DotationNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                model.B35VenteNOUADHIBOU = _context.EmballageCentres.Where(x => x.CodeCentre == 1 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();







                // Production  Centres par Tonne Metrique

                model.TotalProductionNOUADHIBOU = (decimal)((model.B12VenteNOUADHIBOU + model.B12ConsignationNOUADHIBOU + model.B12DotationNOUADHIBOU) * 12.5 + (model.B9VenteNOUADHIBOU + model.B9ConsignationNOUADHIBOU + model.B9DotationNOUADHIBOU) * 9 + (model.B6RVenteNOUADHIBOU + model.B6RConsignationNOUADHIBOU + model.B6RDotationNOUADHIBOU) * 6 + (model.B6VVenteNOUADHIBOU + model.B6VConsignationNOUADHIBOU + model.B6VDotationNOUADHIBOU) * 6 + (model.B3VenteNOUADHIBOU + model.B3ConsignationNOUADHIBOU + model.B3DotationNOUADHIBOU) * 2.75 + (model.B35VenteNOUADHIBOU + model.B35ConsignationNOUADHIBOU + model.B35DotationNOUADHIBOU) * 35) / 1000;

                // Facturations  Centres par Bouteilles





                model.B12TotalFacturationNOUADHIBOU = (model.B12VenteNOUADHIBOU + model.B12ConsignationNOUADHIBOU);
                model.B9TotalFacturationNOUADHIBOU = (model.B9VenteNOUADHIBOU + model.B9ConsignationNOUADHIBOU);
                model.B6RTotalFacturationNOUADHIBOU = (model.B6RVenteNOUADHIBOU + model.B6RConsignationNOUADHIBOU);
                model.B6VTotalFacturationNOUADHIBOU = (model.B6VVenteNOUADHIBOU + model.B6VConsignationNOUADHIBOU);
                model.B3TotalFacturationNOUADHIBOU = (model.B3VenteNOUADHIBOU + model.B3ConsignationNOUADHIBOU);
                model.B35TotalFacturationNOUADHIBOU = (model.B35VenteNOUADHIBOU + model.B35ConsignationNOUADHIBOU);




                // Productions  Centres par Bouteilles





                model.B12TotalProductionNOUADHIBOU = (model.B12VenteNOUADHIBOU + model.B12ConsignationNOUADHIBOU + model.B12DotationNOUADHIBOU);
                model.B9TotalProductionNOUADHIBOU = (model.B9VenteNOUADHIBOU + model.B9ConsignationNOUADHIBOU + model.B9DotationNOUADHIBOU);
                model.B6RTotalProductionNOUADHIBOU = (model.B6RVenteNOUADHIBOU + model.B6RConsignationNOUADHIBOU + model.B6RDotationNOUADHIBOU);
                model.B6VTotalProductionNOUADHIBOU = (model.B6VVenteNOUADHIBOU + model.B6VConsignationNOUADHIBOU + model.B6VDotationNOUADHIBOU);
                model.B3TotalProductionNOUADHIBOU = (model.B3VenteNOUADHIBOU + model.B3ConsignationNOUADHIBOU + model.B3DotationNOUADHIBOU);
                model.B35TotalProductionNOUADHIBOU = (model.B35VenteNOUADHIBOU + model.B35ConsignationNOUADHIBOU + model.B35DotationNOUADHIBOU);






                // Facturations  Centres


                model.TotalFacturationNOUADHIBOU = (decimal)((model.B12VenteNOUADHIBOU + model.B12ConsignationNOUADHIBOU) * 12.5 + (model.B9VenteNOUADHIBOU + model.B9ConsignationNOUADHIBOU) * 9 + (model.B6RVenteNOUADHIBOU + model.B6RConsignationNOUADHIBOU) * 6 + (model.B6VVenteNOUADHIBOU + model.B6VConsignationNOUADHIBOU) * 6 + (model.B3VenteNOUADHIBOU + model.B3ConsignationNOUADHIBOU) * 2.75 + (model.B35VenteNOUADHIBOU + model.B35ConsignationNOUADHIBOU) * 35) / 1000;










                list.Add(model);
                return View(list);

            }




        }








    }
}