﻿using BLL;
using GUHabitat.DTO;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
           
           return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
     

        public JsonResult GetClient(int Code)
        {
            ClientDto client = BLL.BusinessGestionBonChargementCiterne.Instance.GetClientParId(Code);

            if (client == null) client = new ClientDto();
           
            return Json(client.nomComplet, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmploye(int matricule)
        {
            EmployeDto employe = BLL.BusinessGestionFicheVenteBouteille.Instance.GetEmployeParId(matricule);

            if (employe == null) employe = new EmployeDto();

            return Json(employe.nomComplet, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetCentre(int Code)
        {
            CentreDto centre = BLL.BusinessGestionBonRetourCiterne.Instance.GetCentreParId(Code);

            if (centre == null) centre = new CentreDto();

            return Json(centre.libelle, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetBonChargement(string ReferenceBonChargement)
        {
            BonChargementCiterneDto bon = BLL.BusinessGestionBonRetourCiterne.Instance.GetBonChargementParId(ReferenceBonChargement);

            if (bon == null) bon = new BonChargementCiterneDto();

            return Json(bon, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetListeTypeFiche()
        {
            var ty = BLL.BusinessGestionFicheVenteBouteille.Instance.GetListeTypeFiche();
            var types = (from t in ty select new { id = t.id, libelle = t.libelle }).ToList();
            return Json(types, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetListeMouvementBonEnlevement()
        {
            var enlevement = BLL.BusinessGestionBonEnlevementGaz.Instance.GetListeMouvementBonEnlevement();
            var enlevements = (from c in enlevement select new { id = c.id, libelle = c.libelle }).ToList();
            return Json(enlevements, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetListeCentre()
        {
            var centre = BLL.BusinessGestionVenteCentre.Instance.GetListeCentre();
            var centres = (from c in centre select new { Code = c.Code, libelle = c.libelle }).ToList();
            return Json(centres, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetListeConsignationCentre()
        {
            var centre = BLL.BusinessGestionConsignationCentre.Instance.GetListeCentre();
            var centres = (from c in centre select new { Code = c.Code, libelle = c.libelle }).ToList();
            return Json(centres, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListeDotationCentre()
        {
            var centre = BLL.BusinessGestionDotationCentre.Instance.GetListeCentre();
            var centres = (from c in centre select new { Code = c.Code, libelle = c.libelle }).ToList();
            return Json(centres, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListeStockCentre()
        {
            var centre = BLL.BusinessGestionStockCentre.Instance.GetListeCentre();
            var centres = (from c in centre select new { Code = c.Code, libelle = c.libelle }).ToList();
            return Json(centres, JsonRequestBehavior.AllowGet);
        }




        public JsonResult GetListeTypeBonsRetour()
        {
            var ty = BLL.BusinessGestionBonRetourCiterne.Instance.GetListeTypeBon();
            var types = (from t in ty select new { id = t.id, libelle = t.libelle }).ToList();
            return Json(types, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetListeTypeFicheConsignation()
        {
            var ty = BLL.BusinessGestionFicheConsignation.Instance.GetListeTypeFiche();
            var types = (from t in ty select new { id = t.id, libelle = t.libelle }).ToList();
            return Json(types, JsonRequestBehavior.AllowGet);
        }




        public JsonResult GetListeTypeBon()
        {
            var ty = BLL.BusinessGestionBonChargementCiterne.Instance.GetListeTypeBon();
            var types = (from t in ty select new { id = t.id, libelle = t.libelle }).ToList();
            return Json(types, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListeTypeBonDE()
        {
            var ty = BLL.BusinessGestionBonChargementCiterne.Instance.GetListeTypeBonDE();
            var types = (from t in ty select new { id = t.id, libelle = t.libelle }).ToList();
            return Json(types, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetListeTypeSortie()
        {
            var ty = BLL.BusinessGestionSortie.Instance.GetListeTypeSortie();
            var types = (from t in ty select new { id = t.id, libelle = t.libelle }).ToList();
            return Json(types, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetDestination(int Code)
        {
            DestinationDto destination = BLL.BusinessGestionBonChargementCiterne.Instance.GetDestinationParId(Code);

            if (destination == null) destination = new DestinationDto();

            return Json(destination.libelle, JsonRequestBehavior.AllowGet);
        }


        public ActionResult LoadPrduits()
        {
            var ty = BLL.BusinessGestionAredpDetails.Instance.GetListeProduit();
            var types = (from t in ty select new { id = t.id, libelle = t.libelle }).ToList();
            return Json(types, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetPoidsProduit(int idProduit)
        {
            ProduitDto tare = BLL.BusinessGestionAredp.Instance.GetProduitParId(idProduit);

            if (tare == null) tare = new ProduitDto();

            return Json(Convert.ToString(tare.tare), JsonRequestBehavior.AllowGet);
        }

      
    }
}