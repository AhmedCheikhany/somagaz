﻿using CrystalDecisions.CrystalReports.Engine;
using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using GUHabitat.Reports;
using PagedList;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class FicheVenteBouteilleController : Controller
    {

        [HttpPost]
        public ActionResult ValiderTypeFiche(int idTypeFiche)
        {
            return RedirectToAction("Ajouter", new { idTypeFiche = idTypeFiche });
        }




        public ActionResult Index(string filtreMotCle, int? page)
        {

            ModelListeFicheVenteBouteille model = new ModelListeFicheVenteBouteille();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheVente = BusinessGestionFicheVenteBouteille.Instance.ChercherListeFicheVenteBouteille(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);
        }


        public ActionResult IndexParDate(string filtreMotCle, DateTime? dateDebut, DateTime? dateFin, int? page)
        {

            ModelListFicheVenteBouteilleParClient model = new ModelListFicheVenteBouteilleParClient();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            if (dateDebut == null) dateDebut = DateTime.Now.Date;
            if (dateFin == null) dateFin = DateTime.Now.Date;

            model.filtreMotCle = filtreMotCle;
            model.dateDebut = (DateTime)dateDebut;
            model.dateFin = (DateTime)dateFin;
           

            int pageSize = 20;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheVenteParClient = BusinessGestionFicheVenteBouteille.Instance.ChercherListeFicheVenteBouteilleParClient(model.filtreMotCle,model.dateDebut,model.dateFin, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);
        }



        public ActionResult Instance(string filtreMotCle, int? page)
        {

            ModelListeFicheVenteBouteille model = new ModelListeFicheVenteBouteille();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheVente = BusinessGestionFicheVenteBouteille.Instance.ChercherListeFicheVenteBouteilleInstance(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);


            
            ViewBag.nbInstance = BusinessGestionFicheVenteBouteille.Instance.TotalInstance();
            return View(model);
        }



        public ActionResult Acloturer(string filtreMotCle, int? page)
        {

            ModelListeFicheVenteBouteille model = new ModelListeFicheVenteBouteille();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheVente = BusinessGestionFicheVenteBouteille.Instance.ChercherListeFicheVenteBouteilleAcloturer(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);


            ViewBag.nbInstance = BusinessGestionFicheVenteBouteille.Instance.TotalCloturer();

            return View(model);
        }




        public ActionResult NbrePresonnes(string filtreMotCle, int? page)
        {
            
            ModelListeFicheVenteBouteille model = new ModelListeFicheVenteBouteille();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheVente = BusinessGestionFicheVenteBouteille.Instance.ChercherListeNbrePersonnes(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            ViewBag.totalPersonnes = BusinessGestionFicheVenteBouteille.Instance.TotalNbrePersonnes(model.filtreMotCle);
            return View(model);
        }




        public void Selected(int[] ids)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);


            BusinessGestionFicheVenteBouteille.Instance.Cloturer(ids);
         
        }



        public ActionResult EnInstance(int id)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionFicheVenteBouteille.Instance.EnInstance(idEntite,id);

            return RedirectToAction("Index");
        }


        public ActionResult Valider(int id)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionFicheVenteBouteille.Instance.Valider(idEntite, id);

            return RedirectToAction("Index");
        }



        public ActionResult CloturerParDate(string date)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionFicheVenteBouteille.Instance.CloturerParDate(idEntite, Convert.ToDateTime(date));

            return RedirectToAction("Index");
        }




        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelAjouterFicheVenteBouteille model = new ModelAjouterFicheVenteBouteille();


            FicheVenteBouteilleDto dto = BusinessGestionFicheVenteBouteille.Instance.GetFichVenteParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.idTypeFiche = dto.idTypeFiche;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.CodeClient = dto.CodeClient;
                model.NomClient = dto.NomClient;
                model.VehiculeImmatricule = dto.VehiculeImmatricule;
                model.NbrePersonnes = dto.NbrePersonnes;
                model.matriculeEmploye = dto.matriculeEmploye;
                model.NomEmploye = dto.NomEmploye;
                model.TypeFiche = dto.TypeFiche;


                model.B12Client = dto.B12Client;
                model.B9Client = dto.B9Client;
                model.B6RClient = dto.B6RClient;
                model.B6VClient = dto.B6VClient;
                model.B3Client = dto.B3Client;
                model.B35Client = dto.B35Client;

                model.B12VideRec = dto.B12VideRec;
                model.B9VideRec = dto.B9VideRec;
                model.B6RVideRec = dto.B6RVideRec;
                model.B6VVideRec = dto.B6VVideRec;
                model.B3VideRec = dto.B3VideRec;
                model.B35VideRec = dto.B35VideRec;

                model.B12VideDef = dto.B12VideDef;
                model.B9VideDef = dto.B9VideDef;
                model.B6RVideDef = dto.B6RVideDef;
                model.B6VVideDef = dto.B6VVideDef;
                model.B3VideDef = dto.B3VideDef;
                model.B35VideDef = dto.B35VideDef;

                model.B12PleinRemp = dto.B12PleinRemp;
                model.B9PleinRemp = dto.B9PleinRemp;
                model.B6RPleinRemp = dto.B6RPleinRemp;
                model.B6VPleinRemp = dto.B6VPleinRemp;
                model.B3PleinRemp = dto.B3PleinRemp;
                model.B35PleinRemp = dto.B35PleinRemp;

                model.B12PleinDef = dto.B12PleinDef;
                model.B9PleinDef = dto.B9PleinDef;
                model.B6RPleinDef = dto.B6RPleinDef;
                model.B6VPleinDef = dto.B6VPleinDef;
                model.B3PleinDef = dto.B3PleinDef;
                model.B35PleinDef = dto.B35PleinDef;

                model.B12DefAremp = dto.B12DefAremp;
                model.B9DefAremp = dto.B9DefAremp;
                model.B6RDefAremp = dto.B6RDefAremp;
                model.B6VDefAremp = dto.B6VDefAremp;
                model.B3DefAremp = dto.B3DefAremp;
                model.B35DefAremp = dto.B35DefAremp;

                model.B12DefRemp = dto.B12DefRemp;
                model.B9DefRemp = dto.B9DefRemp;
                model.B6RDefRemp = dto.B6RDefRemp;
                model.B6VDefRemp = dto.B6VDefRemp;
                model.B3DefRemp = dto.B3DefRemp;
                model.B35DefRemp = dto.B35DefRemp;

                model.B12Afact = dto.B12Afact;
                model.B9Afact = dto.B9Afact;
                model.B6RAfact = dto.B6RAfact;
                model.B6VAfact = dto.B6VAfact;
                model.B3Afact = dto.B3Afact;
                model.B35Afact = dto.B35Afact;

                model.Commentaire = dto.Commentaire;
                model.etat = dto.etat;


            };
            return View(model);

        }




        [HttpGet]
        public ActionResult Ajouter(int idTypeFiche)
        {
            ModelAjouterFicheVenteBouteille model = new ModelAjouterFicheVenteBouteille();
            model.Date = DateTime.Now;

            string reference = GetNewCode();
            model.Reference = (reference == "1") ? "000001" : reference;


            model.idTypeFiche = BusinessGestionFicheVenteBouteille.Instance.GetTypeFicheParId(idTypeFiche).id;

            if (model.idTypeFiche == 1 || model.idTypeFiche == 3)
            {

                model.matriculeEmploye = 0;
                model.NomEmploye = "Somagaz";
            }

            if (model.idTypeFiche == 2)
            {
                model.CodeClient = 0;
                model.NomClient = "Somagaz";
            }

            return View(model);
        }


        [HttpPost]
        public ActionResult Ajouter(ModelAjouterFicheVenteBouteille model)
        {

          

            if (ModelState.IsValid)
            {

                FicheVenteBouteilleDto dto = new FicheVenteBouteilleDto
                {
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    idTypeFiche = model.idTypeFiche,
                    Date = model.Date,
                    Reference = model.Reference,
                    
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    VehiculeImmatricule = model.VehiculeImmatricule,
                    NbrePersonnes = model.NbrePersonnes,
                    matriculeEmploye = model.matriculeEmploye,
                    NomEmploye = model.NomEmploye,


                    B12Client = model.B12Client,
                    B9Client = model.B9Client,
                    B6RClient = model.B6RClient,
                    B6VClient = model.B6VClient,
                    B3Client = model.B3Client,
                    B35Client = model.B35Client,

                    B12VideRec = model.B12VideRec,
                    B9VideRec =  model.B9VideRec,
                    B6RVideRec = model.B6RVideRec,
                    B6VVideRec = model.B6VVideRec,
                    B3VideRec = model.B3VideRec,
                    B35VideRec = model.B35VideRec,

                    B12VideDef = model.B12VideDef,
                    B9VideDef =  model.B9VideDef,
                    B6RVideDef = model.B6RVideDef,
                    B6VVideDef = model.B6VVideDef,
                    B3VideDef =  model.B3VideDef,
                    B35VideDef = model.B35VideDef,

                    B12PleinRemp = model.B12PleinRemp,
                    B9PleinRemp =  model.B9PleinRemp,
                    B6RPleinRemp = model.B6RPleinRemp,
                    B6VPleinRemp = model.B6VPleinRemp,
                    B3PleinRemp =  model.B3PleinRemp,
                    B35PleinRemp = model.B35PleinRemp,

                    B12PleinDef = model.B12PleinDef,
                    B9PleinDef = model.B9PleinDef,
                    B6RPleinDef = model.B6RPleinDef,
                    B6VPleinDef = model.B6VPleinDef,
                    B3PleinDef = model.B3PleinDef,
                    B35PleinDef = model.B35PleinDef,

                    B12DefAremp = (model.B12VideDef + model.B12PleinDef),
                    B9DefAremp = (model.B9VideDef + model.B9PleinDef),
                    B6RDefAremp = (model.B6RVideDef + model.B6RPleinDef),
                    B6VDefAremp = (model.B6VVideDef + model.B6VPleinDef),
                    B3DefAremp = (model.B3VideDef + model.B3PleinDef),
                    B35DefAremp = (model.B35VideDef + model.B35PleinDef),

                    B12DefRemp = model.B12DefRemp,
                    B9DefRemp = model.B9DefRemp,
                    B6RDefRemp = model.B6RDefRemp,
                    B6VDefRemp = model.B6VDefRemp,
                    B3DefRemp = model.B3DefRemp,
                    B35DefRemp = model.B35DefRemp,

                    B12Afact = (model.B12PleinRemp + model.B12DefRemp),
                    B9Afact = (model.B9PleinRemp + model.B9DefRemp),
                    B6RAfact = (model.B6RPleinRemp + model.B6RDefRemp),
                    B6VAfact = (model.B6VPleinRemp + model.B6VDefRemp),
                    B3Afact = (model.B3PleinRemp + model.B3DefRemp),
                    B35Afact = (model.B35PleinRemp + model.B35DefRemp),

                    Commentaire = model.Commentaire,

                    //Tonnage = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000



                };


                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

                if (model.B12PleinRemp > model.B12VideRec || model.B9PleinRemp > model.B9VideRec || model.B6RPleinRemp > model.B6RVideRec || model.B6VPleinRemp > model.B6VVideRec || model.B3PleinRemp > model.B3VideRec || model.B35PleinRemp > model.B35VideRec)
                {
                    ViewBag.MessageAjout1 = "Le nombre d'emballage rempli ne peut pas être plus grand que le nombre d'emballage recevable";
                    return View(model);
                    
                } 

                if (model.B12PleinDef > ( model.B12VideRec - model.B12PleinRemp) || model.B9PleinDef > (model.B9VideRec - model.B9PleinRemp) || model.B6RPleinDef > (model.B6RVideRec - model.B6RPleinRemp) || model.B6VPleinDef > (model.B6VVideRec - model.B6VPleinRemp) || model.B3PleinDef > (model.B3VideRec - model.B3PleinRemp) || model.B35PleinDef > (model.B35VideRec - model.B35PleinRemp))
                {
                    ViewBag.MessageAjout2 = "Le nombre d’emballage rempli défectueux  ne peut pas être plus grand que (nombre d'emballage recevable -  nombre d'emballage rempli)";
                    return View(model);
                }
                    

                if (model.B12DefRemp > model.B12DefAremp || model.B9DefRemp > model.B9DefAremp || model.B6RDefRemp > model.B6RDefAremp || model.B6VDefRemp > model.B6VDefAremp || model.B3DefRemp > model.B3DefAremp || model.B35DefRemp > model.B35DefAremp)
                {
                    ViewBag.MessageAjout3 = "Le nombre d’emballage défectueux remplacés ne peut pas être plus grand que le nombre d’emballage défectueux à remplacer ";
                    return View(model);
                }

                var nbrefiche = BusinessGestionFicheVenteBouteille.Instance.VerifierExistFiche(model.Reference);
                if (nbrefiche >= 1)
                {
                   
                    return View(model);
                }


                BusinessGestionFicheVenteBouteille.Instance.Ajouter(dto);
                
                
              

            
                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }


        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModifierFicheVenteBouteille model = new ModelModifierFicheVenteBouteille();
            model.ListeFiches = BusinessGestionFicheVenteBouteille.Instance.GetListeTypeFiche();

            FicheVenteBouteilleDto dto = BusinessGestionFicheVenteBouteille.Instance.GetFichVenteParId(id);
           

            if (dto != null)
            {
                model.id = dto.id;
                model.idTypeFiche = dto.idTypeFiche;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.CodeClient = dto.CodeClient;
                model.NomClient = dto.NomClient;
                model.VehiculeImmatricule = dto.VehiculeImmatricule;
                model.NbrePersonnes = dto.NbrePersonnes;
                model.matriculeEmploye = dto.matriculeEmploye;
                model.NomEmploye = dto.NomEmploye;


                model.B12Client = dto.B12Client;
                model.B9Client = dto.B9Client;
                model.B6RClient = dto.B6RClient;
                model.B6VClient = dto.B6VClient;
                model.B3Client = dto.B3Client;
                model.B35Client = dto.B35Client;


                model.B12VideRec = dto.B12VideRec;
                model.B9VideRec = dto.B9VideRec;
                model.B6RVideRec = dto.B6RVideRec;
                model.B6VVideRec = dto.B6VVideRec;
                model.B3VideRec = dto.B3VideRec;
                model.B35VideRec = dto.B35VideRec;

                model.B12VideDef = dto.B12VideDef;
                model.B9VideDef = dto.B9VideDef;
                model.B6RVideDef = dto.B6RVideDef;
                model.B6VVideDef = dto.B6VVideDef;
                model.B3VideDef = dto.B3VideDef;
                model.B35VideDef = dto.B35VideDef;

                model.B12PleinRemp = dto.B12PleinRemp;
                model.B9PleinRemp = dto.B9PleinRemp;
                model.B6RPleinRemp = dto.B6RPleinRemp;
                model.B6VPleinRemp = dto.B6VPleinRemp;
                model.B3PleinRemp = dto.B3PleinRemp;
                model.B35PleinRemp = dto.B35PleinRemp;

                model.B12PleinDef = dto.B12PleinDef;
                model.B9PleinDef = dto.B9PleinDef;
                model.B6RPleinDef = dto.B6RPleinDef;
                model.B6VPleinDef = dto.B6VPleinDef;
                model.B3PleinDef = dto.B3PleinDef;
                model.B35PleinDef = dto.B35PleinDef;

                model.B12DefAremp = dto.B12DefAremp;
                model.B9DefAremp = dto.B9DefAremp;
                model.B6RDefAremp = dto.B6RDefAremp;
                model.B6VDefAremp = dto.B6VDefAremp;
                model.B3DefAremp = dto.B3DefAremp;
                model.B35DefAremp = dto.B35DefAremp;

                model.B12DefRemp = dto.B12DefRemp;
                model.B9DefRemp = dto.B9DefRemp;
                model.B6RDefRemp = dto.B6RDefRemp;
                model.B6VDefRemp = dto.B6VDefRemp;
                model.B3DefRemp = dto.B3DefRemp;
                model.B35DefRemp = dto.B35DefRemp;

                model.B12Afact = dto.B12Afact;
                model.B9Afact = dto.B9Afact;
                model.B6RAfact = dto.B6RAfact;
                model.B6VAfact = dto.B6VAfact;
                model.B3Afact = dto.B3Afact;
                model.B35Afact = dto.B35Afact;

                model.Commentaire = dto.Commentaire;
                model.etat = dto.etat;





            };

            ViewBag.NbreAredp = BusinessGestionFicheVenteBouteille.Instance.AredpParClient(model.CodeClient, model.Date);

            return View(model);
        }




        [HttpPost]
        public ActionResult Modifier(ModelModifierFicheVenteBouteille model)
        {


            if (ModelState.IsValid)
            {

                FicheVenteBouteilleDto dto = new FicheVenteBouteilleDto
                {

                    id = model.id,
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    idTypeFiche = model.idTypeFiche,
                    Reference = model.Reference,
                    Date = model.Date,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    VehiculeImmatricule = model.VehiculeImmatricule,
                    NbrePersonnes = model.NbrePersonnes,
                    matriculeEmploye = model.matriculeEmploye,
                    NomEmploye = model.NomEmploye,

                    B12Client = model.B12Client,
                    B9Client = model.B9Client,
                    B6RClient = model.B6RClient,
                    B6VClient = model.B6VClient,
                    B3Client = model.B3Client,
                    B35Client = model.B35Client,


                    B12VideRec = model.B12VideRec,
                    B9VideRec = model.B9VideRec,
                    B6RVideRec = model.B6RVideRec,
                    B6VVideRec = model.B6VVideRec,
                    B3VideRec = model.B3VideRec,
                    B35VideRec = model.B35VideRec,

                    B12VideDef = model.B12VideDef,
                    B9VideDef = model.B9VideDef,
                    B6RVideDef = model.B6RVideDef,
                    B6VVideDef = model.B6VVideDef,
                    B3VideDef = model.B3VideDef,
                    B35VideDef = model.B35VideDef,

                    B12PleinRemp = model.B12PleinRemp,
                    B9PleinRemp = model.B9PleinRemp,
                    B6RPleinRemp = model.B6RPleinRemp,
                    B6VPleinRemp = model.B6VPleinRemp,
                    B3PleinRemp = model.B3PleinRemp,
                    B35PleinRemp = model.B35PleinRemp,

                    B12PleinDef = model.B12PleinDef,
                    B9PleinDef = model.B9PleinDef,
                    B6RPleinDef = model.B6RPleinDef,
                    B6VPleinDef = model.B6VPleinDef,
                    B3PleinDef = model.B3PleinDef,
                    B35PleinDef = model.B35PleinDef,

                    B12DefAremp = (model.B12VideDef + model.B12PleinDef),
                    B9DefAremp =  (model.B9VideDef + model.B9PleinDef),
                    B6RDefAremp = (model.B6RVideDef + model.B6RPleinDef),
                    B6VDefAremp = (model.B6VVideDef + model.B6VPleinDef),
                    B3DefAremp =  (model.B3VideDef + model.B3PleinDef),
                    B35DefAremp = (model.B35VideDef + model.B35PleinDef),

                    B12DefRemp = model.B12DefRemp,
                    B9DefRemp = model.B9DefRemp,
                    B6RDefRemp = model.B6RDefRemp,
                    B6VDefRemp = model.B6VDefRemp,
                    B3DefRemp = model.B3DefRemp,
                    B35DefRemp = model.B35DefRemp,

                    B12Afact = (model.B12PleinRemp + model.B12DefRemp),
                    B9Afact =  (model.B9PleinRemp + model.B9DefRemp),
                    B6RAfact = (model.B6RPleinRemp + model.B6RDefRemp),
                    B6VAfact = (model.B6VPleinRemp + model.B6VDefRemp),
                    B3Afact =  (model.B3PleinRemp + model.B3DefRemp),
                    B35Afact = (model.B35PleinRemp + model.B35DefRemp),

                    Commentaire = model.Commentaire,

                    //Tonnage = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000


                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

                if (model.B12PleinRemp > model.B12VideRec || model.B9PleinRemp > model.B9VideRec || model.B6RPleinRemp > model.B6RVideRec || model.B6VPleinRemp > model.B6VVideRec || model.B3PleinRemp > model.B3VideRec || model.B35PleinRemp > model.B35VideRec)
                {
                    ViewBag.MessageModifier1 = "Le nombre d'emballage rempli ne peut pas être plus grand que le nombre d'emballage recevable";
                    return View(model);

                }

                if (model.B12PleinDef > (model.B12VideRec - model.B12PleinRemp) || model.B9PleinDef > (model.B9VideRec - model.B9PleinRemp) || model.B6RPleinDef > (model.B6RVideRec - model.B6RPleinRemp) || model.B6VPleinDef > (model.B6VVideRec - model.B6VPleinRemp) || model.B3PleinDef > (model.B3VideRec - model.B3PleinRemp) || model.B35PleinDef > (model.B35VideRec - model.B35PleinRemp))
                {
                    ViewBag.MessageModifier2 = "Le nombre d’emballage rempli défectueux  ne peut pas être plus grand que (nombre d'emballage recevable -  nombre d'emballage rempli)";
                    return View(model);
                }


                if (model.B12DefRemp > model.B12DefAremp || model.B9DefRemp > model.B9DefAremp || model.B6RDefRemp > model.B6RDefAremp || model.B6VDefRemp > model.B6VDefAremp || model.B3DefRemp > model.B3DefAremp || model.B35DefRemp > model.B35DefAremp)
                {
                    ViewBag.MessageModifier3 = "Le nombre d’emballage défectueux remplacés ne peut pas être plus grand que le nombre d’emballage défectueux à remplacer ";
                    return View(model);
                }

               
                


                BusinessGestionFicheVenteBouteille.Instance.Modifier(dto);
                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }



        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {

                maxCode = db.FicheVenteBouteilles.OrderByDescending(s => s.id).Select(x => x.Reference).FirstOrDefault();


            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }
        public ActionResult Imprimer(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {


                rpt = crpFicheVenteBouteille.Instance;

                //rpt = new crpFicheVenteBouteille();

                var dto = BusinessGestionFicheVenteBouteille.Instance.GetFichVenteParId(id);

                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    //QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.com" + "\n" + dto.Reference, QRCodeGenerator.ECCLevel.Q);
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode("==> FICHE DE VENTE DE BOUTEILLE " + "\n\n" + "Référence :" + "\n\n" + dto.Reference + "\n\n" + "Date :" + "\n\n" + dto.Date.Date.ToShortDateString() + "\n\n" + "Code Client :" + "\n\n" + dto.CodeClient + "\n\n" + "Nom Client :" + "\n\n" + dto.NomClient + "\n\n" + "B12 :" + "\n\n" + dto.B12Afact + "\n\n" + "B9 :" + "\n\n" + dto.B9Afact + "\n\n" + "B6R :" + "\n\n" + dto.B6RAfact + "\n\n" + "B6V :" + "\n\n" + dto.B6VAfact + "\n\n" + "B3 :" + "\n\n" + dto.B3Afact + "\n\n" + "B35 :" + "\n\n" + dto.B35Afact, QRCodeGenerator.ECCLevel.Q);

                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeImage.Save(ms, ImageFormat.Bmp);
                        dto.qrCodeImage = ms.ToArray();
                    }



                    List<FicheVenteBouteilleDto> list = new List<FicheVenteBouteilleDto>();
                    list.Add(dto);
                    rpt.SetDataSource(list);
                    Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

                    if (idEntite == 5 && (dto.B12Afact != 0 || dto.B9Afact != 0 || dto.B6RAfact != 0 || dto.B6VAfact != 0 || dto.B3Afact != 0 || dto.B35Afact != 0))
                    {
                        BusinessGestionFicheVenteBouteille.Instance.Valider(idEntite, id);
                    }

                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            catch (Exception) { }
            return null;
        }



        public ActionResult ImprimerA5(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {


                //"B12 :" + "\n\n" + dto.B12Afact + "\n\n" + "B9 :" + "\n\n" + dto.B9Afact + "\n\n" + "B6R :" + "\n\n" + dto.B6RAfact + "\n\n" + "B6V :" + "\n\n" + dto.B6VAfact + "\n\n" + "B3 :" + "\n\n" + dto.B3Afact + "\n\n" + "B35 :" + "\n\n" + dto.B35Afact

                //rpt = new crpFicheVenteBouteilleA5();

                rpt = crpFicheVenteBouteilleA5.Instance;
                var dto = BusinessGestionFicheVenteBouteille.Instance.GetFichVenteParId(id);

                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    // QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.com" + "\n" + dto.Reference, QRCodeGenerator.ECCLevel.Q);
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode("==> FICHE DE VENTE DE BOUTEILLE " + "\n\n" + "Référence :" + "\n\n" + dto.Reference + "\n\n" + "Date :" + "\n\n" + dto.Date.Date.ToShortDateString() + "\n\n" + "Code Client :" + "\n\n" + dto.CodeClient + "\n\n" + "Nom Client :" + "\n\n" + dto.NomClient + "\n\n" + "B12 :" + "\n\n" + dto.B12Afact + "\n\n" + "B9 :" + "\n\n" + dto.B9Afact + "\n\n" + "B6R :" + "\n\n" + dto.B6RAfact + "\n\n" + "B6V :" + "\n\n" + dto.B6VAfact + "\n\n" + "B3 :" + "\n\n" + dto.B3Afact + "\n\n" + "B35 :" + "\n\n" + dto.B35Afact, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeImage.Save(ms, ImageFormat.Bmp);
                        dto.qrCodeImage = ms.ToArray();
                    }






                    List<FicheVenteBouteilleDto> list = new List<FicheVenteBouteilleDto>();
                    list.Add(dto);
                    rpt.SetDataSource(list);
                    Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

                    if (idEntite == 5 && (dto.B12Afact != 0 || dto.B9Afact != 0 || dto.B6RAfact != 0 || dto.B6VAfact != 0 || dto.B3Afact != 0 || dto.B35Afact != 0))
                    {
                        BusinessGestionFicheVenteBouteille.Instance.Valider(idEntite, id);
                    }

                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            catch (Exception) { }
            return null;
        }




        public ActionResult ImprimerDotation(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {




                //rpt = new crpFicheDotation();
                rpt = crpFicheDotation.Instance;
                var dto = BusinessGestionFicheVenteBouteille.Instance.GetFichVenteParId(id);

                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    //QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.com" + "\n" + dto.Reference, QRCodeGenerator.ECCLevel.Q);
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode("==> FICHE DOTATION DE BOUTEILLE " + "\n\n" + "Référence :" + "\n\n" + dto.Reference + "\n\n" + "Date :" + "\n\n" + dto.Date.Date.ToShortDateString() + "\n\n" + "Matricule :" + "\n\n" + dto.matriculeEmploye + "\n\n" + "Nom Employé :" + "\n\n" + dto.NomEmploye + "\n\n" + "B12 :" + "\n\n" + dto.B12Afact + "\n\n" + "B9 :" + "\n\n" + dto.B9Afact + "\n\n" + "B6R :" + "\n\n" + dto.B6RAfact + "\n\n" + "B6V :" + "\n\n" + dto.B6VAfact + "\n\n" + "B3 :" + "\n\n" + dto.B3Afact + "\n\n" + "B35 :" + "\n\n" + dto.B35Afact, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeImage.Save(ms, ImageFormat.Bmp);
                        dto.qrCodeImage = ms.ToArray();
                    }






                    List<FicheVenteBouteilleDto> list = new List<FicheVenteBouteilleDto>();
                    list.Add(dto);
                    rpt.SetDataSource(list);
                    Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

                    if (idEntite == 5 && (dto.B12Afact != 0 || dto.B9Afact != 0 || dto.B6RAfact != 0 || dto.B6VAfact != 0 || dto.B3Afact != 0 || dto.B35Afact != 0))
                    {
                        BusinessGestionFicheVenteBouteille.Instance.Valider(idEntite, id);
                    }

                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            catch (Exception) { }
            return null;
        }


        public ActionResult ImprimerDotationA5(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {




                //rpt = new crpFicheDotationA5();
                rpt = crpFicheDotationA5.Instance;

                var dto = BusinessGestionFicheVenteBouteille.Instance.GetFichVenteParId(id);

                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    //QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.com" + "\n" + dto.Reference, QRCodeGenerator.ECCLevel.Q);
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode("==> FICHE DOTATION DE BOUTEILLE " + "\n\n" + "Référence :" + "\n\n" + dto.Reference + "\n\n" + "Date :" + "\n\n" + dto.Date.Date.ToShortDateString() + "\n\n" + "Matricule :" + "\n\n" + dto.matriculeEmploye + "\n\n" + "Nom Employé :" + "\n\n" + dto.NomEmploye + "\n\n" + "B12 :" + "\n\n" + dto.B12Afact + "\n\n" + "B9 :" + "\n\n" + dto.B9Afact + "\n\n" + "B6R :" + "\n\n" + dto.B6RAfact + "\n\n" + "B6V :" + "\n\n" + dto.B6VAfact + "\n\n" + "B3 :" + "\n\n" + dto.B3Afact + "\n\n" + "B35 :" + "\n\n" + dto.B35Afact, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeImage.Save(ms, ImageFormat.Bmp);
                        dto.qrCodeImage = ms.ToArray();
                    }






                    List<FicheVenteBouteilleDto> list = new List<FicheVenteBouteilleDto>();
                    list.Add(dto);
                    rpt.SetDataSource(list);
                    Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

                    if (idEntite == 5 && (dto.B12Afact != 0 || dto.B9Afact != 0 || dto.B6RAfact != 0 || dto.B6VAfact != 0 || dto.B3Afact != 0 || dto.B35Afact != 0))
                    {
                        BusinessGestionFicheVenteBouteille.Instance.Valider(idEntite, id);
                    }

                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            catch (Exception) { }
            return null;
        }

    }
}