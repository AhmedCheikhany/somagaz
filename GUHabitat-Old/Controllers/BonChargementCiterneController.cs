﻿using BLL;
using CrystalDecisions.CrystalReports.Engine;
using GUHabitat.BLL;
using GUHabitat.DAL;
using GUHabitat.DTO;
using GUHabitat.Models;
using GUHabitat.Reports;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class BonChargementCiterneController : Controller
    {

        [HttpPost]
        public ActionResult ValiderTypeBon(int idTypeBon)
        {
            return RedirectToAction("Ajouter", new { idTypeBon = idTypeBon });
        }

        //[HttpPost]
        //public ActionResult ValiderTypeBonDE(int idTypeBonDE)
        //{
        //    return RedirectToAction("Ajouter", new { idTypeBon = idTypeBonDE });
        //}


        [HttpGet]
        public ActionResult Index(string filtreMotCle, int? page)
        {
            ModelListeBonChargementCiterne model = new ModelListeBonChargementCiterne();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeBonChargementCiterne = BusinessGestionBonChargementCiterne.Instance.ChercherListeBon(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);


        }

        public ActionResult Instance(string filtreMotCle, int? page)
        {
            ModelListeBonChargementCiterne model = new ModelListeBonChargementCiterne();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeBonChargementCiterne = BusinessGestionBonChargementCiterne.Instance.BonEnInstance(model.filtreMotCle,idEntite, login).ToPagedList(pageIndex, pageSize);

            ViewBag.nbInstance = BusinessGestionBonChargementCiterne.Instance.TotalInstance();

            return View(model);


        }


        public ActionResult Acloturer(string filtreMotCle, int? page)
        {
            ModelListeBonChargementCiterne model = new ModelListeBonChargementCiterne();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeBonChargementCiterne = BusinessGestionBonChargementCiterne.Instance.ChercherListeBonsAcloturer(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            ViewBag.nbInstance = BusinessGestionBonChargementCiterne.Instance.TotalCloturer();

            return View(model);


        }


        public ActionResult NbrePersonnes(string filtreMotCle, int? page)
        {
            ModelListeBonChargementCiterne model = new ModelListeBonChargementCiterne();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeBonChargementCiterne = BusinessGestionBonChargementCiterne.Instance.ChercherListeNbrePersonnes(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            ViewBag.totalPersonnes = BusinessGestionBonChargementCiterne.Instance.TotalNbrePersonnes(model.filtreMotCle);
            return View(model);


        }



        public void Selected(int[] ids)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            BusinessGestionBonChargementCiterne.Instance.Cloturer(ids);
        }



        public ActionResult EnInstance(int id)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionBonChargementCiterne.Instance.EnInstance(idEntite,id);

            return RedirectToAction("Index");
        }


        public ActionResult Valider(int id)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionBonChargementCiterne.Instance.Valider(idEntite, id);

            return RedirectToAction("Index");
        }



        public ActionResult CloturerParDate(string date)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionBonChargementCiterne.Instance.CloturerParDate(idEntite, Convert.ToDateTime(date));

            return RedirectToAction("Index");
        }



        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelModifierBonChargement model = new ModelModifierBonChargement();


            BonChargementCiterneDto BonDto = BusinessGestionBonChargementCiterne.Instance.GetBonParId(id);

            if (BonDto != null)
            {
                model.id = BonDto.id;
                model.Reference = BonDto.Reference;
                model.Date = BonDto.Date;
                model.idTypeBon = BonDto.idTypeBon;
                model.TypeBon = BonDto.TypeBon;
                model.ImmtriculeCamion = BonDto.ImmtriculeCamion;
                model.NbrePersonnes = BonDto.NbrePersonnes;
                model.ResponsableCamion = BonDto.ResponsableCamion;
                model.ChauffeurCamion = BonDto.ChauffeurCamion;
                model.CodeClient = BonDto.CodeClient;
                model.NomClient = BonDto.NomClient;
                model.Tonnage = (float)BonDto.Tonnage;
                model.CodeDestination = BonDto.CodeDestination;
                model.libelleDestination = BonDto.libelleDestination;
                model.Demmarage = BonDto.Demmarage;
                model.EtatPneus = BonDto.EtatPneus;
                model.Freinage = BonDto.Freinage;
                model.Extincteur = BonDto.Extincteur;
                model.Observation = BonDto.Observation;
                model.Numeropermis = BonDto.Numeropermis;
                model.NpoliceRC = BonDto.NpoliceRC;
                model.NpoliceRCGaz = BonDto.NpoliceRCGaz;
                model.AutoriserEntree = BonDto.AutoriserEntree;
                model.Facturer = BonDto.Facturer;
                model.QuantiteFactQuantiteChargee = BonDto.QuantiteFactQuantiteChargee;
                model.AutoriserSortie = BonDto.AutoriserSortie;
                model.PoidDebut = BonDto.PoidDebut;
                model.NumeroPontBascule = BonDto.NumeroPontBascule;
                model.ClapetVanne = BonDto.ClapetVanne;
                model.CalageVehicule = BonDto.CalageVehicule;
                model.MiseTerre = BonDto.MiseTerre;
                model.ObservationExploitation = BonDto.ObservationExploitation;
                model.AutoriserCharger = BonDto.AutoriserCharger;
                model.heureChargement = BonDto.heureChargement;
                model.heureFinChargement = BonDto.heureFinChargement;
                model.PoidsApresChargement = BonDto.PoidsApresChargement;
                model.TonnageCharger = (float)BonDto.TonnageCharger;
                model.NumeroFacture = BonDto.NumeroFacture;
                model.TempsEstime = BonDto.TempsEstime;
                model.etat = BonDto.etat;


            };
            return View(model);

        }





        [HttpGet]
        public ActionResult Ajouter(int idTypeBon)
        {
            ModelAjouterBonChargementCiterne model = new ModelAjouterBonChargementCiterne();
            model.Date = DateTime.Now;
            model.heureChargement = DateTime.Now;
            model.heureFinChargement = DateTime.Now;
           
            string reference = GetNewCode();
            model.Reference = (reference == "1") ? "000001" : reference;
            model.idTypeBon = BusinessGestionBonChargementCiterne.Instance.GetTypeBonParId(idTypeBon).id;

           

            return View(model);
        }

  


        [HttpPost]
        public ActionResult Ajouter(ModelAjouterBonChargementCiterne model)
        {


            if (ModelState.IsValid)
            {

                BonChargementCiterneDto dto = new BonChargementCiterneDto
                {
                   
                    Reference = model.Reference,
                    Date = model.Date,
                    idTypeBon = model.idTypeBon,
                    ImmtriculeCamion = model.ImmtriculeCamion,
                    NbrePersonnes  = model.NbrePersonnes,
                    ResponsableCamion = model.ResponsableCamion,
                    ChauffeurCamion = model.ChauffeurCamion,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    Tonnage = (float)model.Tonnage,
                    CodeDestination = model.CodeDestination,
                    libelleDestination = model.libelleDestination,
                    Demmarage = model.Demmarage,
                    EtatPneus = model.EtatPneus,
                    Freinage = model.Freinage,
                    Extincteur = model.Extincteur,
                    Observation = model.Observation,
                    Numeropermis = model.Numeropermis,
                    NpoliceRC = model.NpoliceRC,
                    NpoliceRCGaz = model.NpoliceRCGaz,
                    AutoriserEntree = model.AutoriserEntree,
                    Facturer = model.Facturer,
                    QuantiteFactQuantiteChargee = model.QuantiteFactQuantiteChargee,
                    AutoriserSortie = model.AutoriserSortie,
                    PoidDebut = model.PoidDebut,
                    NumeroPontBascule = model.NumeroPontBascule,
                    ClapetVanne = model.ClapetVanne,
                    CalageVehicule = model.CalageVehicule,
                    MiseTerre = model.MiseTerre,
                    ObservationExploitation = model.ObservationExploitation,
                    AutoriserCharger = model.AutoriserCharger,
                    heureChargement = model.heureChargement,
                    heureFinChargement = model.heureFinChargement,
                    PoidsApresChargement = model.PoidsApresChargement,
                    TonnageCharger = (float)(model.PoidsApresChargement - model.PoidDebut),
                    NumeroFacture = model.NumeroFacture,
                    TempsEstime = model.TempsEstime,
                    
                   




                };

               

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);



                BusinessGestionBonChargementCiterne.Instance.Ajouter(dto);
                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);

            }

        }




        [HttpGet]
        public ActionResult ModifierBonChargement(int id)
        {
            ModelModifierBonChargement model = new ModelModifierBonChargement();
            model.ListeTypeBons = BusinessGestionBonChargementCiterne.Instance.GetListeTypeBon();



            BonChargementCiterneDto BonDto = BusinessGestionBonChargementCiterne.Instance.GetBonParId(id);

            //if (BonDto.idTypeBon != 1) model.ListeTypeBons = BusinessGestionBonChargementCiterne.Instance.GetListeTypeBon();
            //if (BonDto.idTypeBon == 1) model.ListeTypeBons = BusinessGestionBonChargementCiterne.Instance.GetListeTypeBonDE();

            if (BonDto != null)
            {
                model.id = BonDto.id;
                model.Reference = BonDto.Reference;
                model.Date = BonDto.Date;
                model.idTypeBon = BonDto.idTypeBon;
                model.TypeBon = BonDto.TypeBon;
                model.ImmtriculeCamion = BonDto.ImmtriculeCamion;
                model.NbrePersonnes = BonDto.NbrePersonnes;
                model.ResponsableCamion = BonDto.ResponsableCamion;
                model.ChauffeurCamion = BonDto.ChauffeurCamion;
                model.CodeClient = BonDto.CodeClient;
                model.NomClient = BonDto.NomClient;
                model.Tonnage = (float)BonDto.Tonnage;
                model.CodeDestination = BonDto.CodeDestination;
                model.libelleDestination = BonDto.libelleDestination;
                model.Demmarage = BonDto.Demmarage;
                model.EtatPneus = BonDto.EtatPneus;
                model.Freinage = BonDto.Freinage;
                model.Extincteur = BonDto.Extincteur;
                model.Observation = BonDto.Observation;
                model.Numeropermis = BonDto.Numeropermis;
                model.NpoliceRC = BonDto.NpoliceRC;
                model.NpoliceRCGaz = BonDto.NpoliceRCGaz;
                model.AutoriserEntree = BonDto.AutoriserEntree;
                model.Facturer = BonDto.Facturer;
                model.QuantiteFactQuantiteChargee = BonDto.QuantiteFactQuantiteChargee;
                model.AutoriserSortie = BonDto.AutoriserSortie;
                model.PoidDebut = BonDto.PoidDebut;
                model.NumeroPontBascule = BonDto.NumeroPontBascule;
                model.ClapetVanne = BonDto.ClapetVanne;
                model.CalageVehicule = BonDto.CalageVehicule;
                model.MiseTerre = BonDto.MiseTerre;
                model.ObservationExploitation = BonDto.ObservationExploitation;
                model.AutoriserCharger = BonDto.AutoriserCharger;
                model.heureChargement = BonDto.heureChargement;
                model.heureFinChargement = BonDto.heureFinChargement;
                model.PoidsApresChargement = BonDto.PoidsApresChargement;
                model.TonnageCharger = (float)BonDto.TonnageCharger;
                model.NumeroFacture = BonDto.NumeroFacture;
                model.TempsEstime = BonDto.TempsEstime;
                model.etat = BonDto.etat;


            };
            return View(model);


        }



        [HttpPost]
        public ActionResult ModifierBonChargement(ModelModifierBonChargement model)
        {


            if (ModelState.IsValid)
            {

                BonChargementCiterneDto dto = new BonChargementCiterneDto
                {
                    id = model.id,
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    Reference = model.Reference,
                    Date = model.Date,
                    idTypeBon = model.idTypeBon,
                    ImmtriculeCamion = model.ImmtriculeCamion,
                    NbrePersonnes = model.NbrePersonnes,
                    ResponsableCamion = model.ResponsableCamion,
                    ChauffeurCamion = model.ChauffeurCamion,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    Tonnage = (float)model.Tonnage,
                    CodeDestination = model.CodeDestination,
                    libelleDestination = model.libelleDestination,
                    Demmarage = model.Demmarage,
                    EtatPneus = model.EtatPneus,
                    Freinage = model.Freinage,
                    Extincteur = model.Extincteur,
                    Observation = model.Observation,
                    Numeropermis = model.Numeropermis,
                    NpoliceRC = model.NpoliceRC,
                    NpoliceRCGaz = model.NpoliceRCGaz,
                    AutoriserEntree = model.AutoriserEntree,
                    Facturer = model.Facturer,
                    QuantiteFactQuantiteChargee = model.QuantiteFactQuantiteChargee,
                    AutoriserSortie = model.AutoriserSortie,
                    PoidDebut = model.PoidDebut,
                    NumeroPontBascule = model.NumeroPontBascule,
                    ClapetVanne = model.ClapetVanne,
                    CalageVehicule = model.CalageVehicule,
                    MiseTerre = model.MiseTerre,
                    ObservationExploitation = model.ObservationExploitation,
                    AutoriserCharger = model.AutoriserCharger,
                    heureChargement = model.heureChargement,
                    heureFinChargement = model.heureFinChargement,
                    PoidsApresChargement = model.PoidsApresChargement,
                    TonnageCharger = (float)(model.PoidsApresChargement - model.PoidDebut),
                    NumeroFacture = model.NumeroFacture,
                    TempsEstime = model.TempsEstime



                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);


                if(dto.idEntite == 5 && (model.PoidsApresChargement <= model.PoidDebut))
                {
                    ViewBag.MessageModifier = "Le poids après chargement  ne peut pas etre inferieur ou egale au poids debut";

                    model.ListeTypeBons = BusinessGestionBonChargementCiterne.Instance.GetListeTypeBon();
                    model.idTypeBon = BusinessGestionBonChargementCiterne.Instance.GetTypeBonParId(model.idTypeBon).id;
                    model.TypeBon = BusinessGestionBonChargementCiterne.Instance.GetTypeBonParId(model.idTypeBon).libelle;
                    return View(model);

                }


                //var modelStatistiques = BusinessGestionStatistiques.Instance.GetStatistiqueGlobale(model.Date, model.Date);
                //if ((decimal)model.TonnageCharger > modelStatistiques.StockActuelNktt)
                //{
                //    model.ListeTypeBons = BusinessGestionBonChargementCiterne.Instance.GetListeTypeBon();
                //    model.idTypeBon = BusinessGestionBonChargementCiterne.Instance.GetTypeBonParId(model.idTypeBon).id;
                //    model.TypeBon = BusinessGestionBonChargementCiterne.Instance.GetTypeBonParId(model.idTypeBon).libelle;
                //    model.Date = DateTime.Now;
                //    return View(model);
                //}


                BusinessGestionBonChargementCiterne.Instance.ModifierBonChargement(dto);
                return RedirectToAction("Index");
            }
            else
            {
                model.ListeTypeBons = BusinessGestionBonChargementCiterne.Instance.GetListeTypeBon();
                model.idTypeBon = BusinessGestionBonChargementCiterne.Instance.GetTypeBonParId(model.idTypeBon).id;
                model.TypeBon = BusinessGestionBonChargementCiterne.Instance.GetTypeBonParId(model.idTypeBon).libelle;
                model.Date = DateTime.Now;
                return View(model);
            }

        }









        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {
                maxCode = db.BonChargementCiternes.OrderByDescending(s => s.id).Select(x => x.Reference).FirstOrDefault();

                //maxCode = db.BonChargementCiternes.Select(x => x.Reference).Max();
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }



        public ActionResult Imprimer(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {


                //rpt = new crpBonChargement();
                rpt =  crpBonChargement.Instance;

                var dto = BusinessGestionBonChargementCiterne.Instance.GetBonParId(id);

                List<BonChargementCiterneDto> list = new List<BonChargementCiterneDto>();
                list.Add(dto);
                rpt.SetDataSource(list);
                Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

                if (idEntite == 5 && dto.TonnageCharger > 0)
                {
                    BusinessGestionBonChargementCiterne.Instance.Valider(idEntite,id);
                }
                

                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception) { }
            return null;


        }


    }
}