﻿using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class AredpDetailsController : Controller
    {
        // GET: AredpDetails
        [HttpGet]
        public ActionResult Index(int? page ,int id)
        {
            ModelListeAredpDetails model = new ModelListeAredpDetails();
            
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeAredpDetails = BusinessGestionAredpDetails.Instance.ChercherListeAredpDetails( idEntite, login,id).ToPagedList(pageIndex, pageSize);

            return View(model);


        }


        public ActionResult Ajouter(int id)
        {

            ModelAjouterAredpDetails model = new ModelAjouterAredpDetails();
            model.ListeProduit = BusinessGestionAredpDetails.Instance.GetListeProduit();

            model.idAredp = id;
              
            return View(model);

        }


        [HttpPost]
        public ActionResult Ajouter(ModelAjouterAredpDetails model)
        {



            if (ModelState.IsValid)
            {

                AredpDetailsDto dto = new AredpDetailsDto
                {
                    id = model.id,
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    idAredp = model.idAredp,
                    idProduit = model.idProduit,
                    poidsLu = model.poidsLu,
                    Tare = model.Tare,
                    poidsManquant = model.poidsManquant,
                    poidsRestant = model.poidsRestant




                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

               

                BusinessGestionAredpDetails.Instance.Ajouter(dto);


                return RedirectToAction("Index", "Aredp");
            }
            else
            {
                return View(model);
            }



        }







        public ActionResult Modifier(int id)
        {

            ModelModifierAredpDetails model = new ModelModifierAredpDetails();
            model.ListeProduit = BusinessGestionAredpDetails.Instance.GetListeProduit();

            AredpDetailsDto dto = BusinessGestionAredpDetails.Instance.GetAredpDetailsParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.idAredp = dto.idAredp;
                model.idProduit = dto.idProduit;
                model.poidsLu = dto.poidsLu;
                model.Tare = dto.Tare;
                model.poidsManquant = dto.poidsManquant;
                model.poidsRestant = dto.poidsRestant;



            };
            return View(model);

        }



        [HttpPost]
        public ActionResult Modifier(ModelModifierAredpDetails model)
        {



            if (ModelState.IsValid)
            {

                AredpDetailsDto dto = new AredpDetailsDto
                {
                    id = model.id,
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    idAredp = model.idAredp,
                    idProduit = model.idProduit,
                    poidsLu = model.poidsLu,
                    Tare = model.Tare,
                    poidsManquant = model.poidsManquant,
                    poidsRestant = model.poidsRestant




                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

             

                BusinessGestionAredpDetails.Instance.Modifier(dto);


                return RedirectToAction("Index","Aredp");
            }
            else
            {
                return View(model);
            }



        }


    }
}