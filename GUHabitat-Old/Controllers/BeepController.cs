﻿using BLL;
using CrystalDecisions.CrystalReports.Engine;
using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using GUHabitat.Reports;
using PagedList;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class BeepController : Controller
    {
        [HttpGet]
        public ActionResult Index(string filtreMotCle, int? page)
        {
            ListeBeepModel model = new ListeBeepModel();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeBeep = BusinessGestionBeep.Instance.ChercherListeBeep(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);


        }



        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelAjouterBeep model = new ModelAjouterBeep();


            BeepDto BeepDto = BusinessGestionBeep.Instance.GetBeepParId(id);

            if (BeepDto != null)
            {
                model.id = BeepDto.id;
                model.Serie = BeepDto.Serie;
                model.Date = BeepDto.Date;
                model.B12 = BeepDto.B12;
                model.B9 = BeepDto.B9;
                model.B6R = BeepDto.B6R;
                model.B6V = BeepDto.B6V;
                model.B3 = BeepDto.B3;
                model.B35 = BeepDto.B35;
                model.CodeClient = BeepDto.CodeClient;
                model.NomClient = BeepDto.NomClient;
                model.Commentaire = BeepDto.Commentaire;
                model.NumeroFacture = BeepDto.NumeroFacture;
                model.VehiculeImmatricule = BeepDto.VehiculeImmatricule;
                model.DotationImmatricule = BeepDto.DotationImmatricule;
                model.etat = BeepDto.etat;

            };
            return View(model);

        }






        [HttpGet]
        public ActionResult Ajouter()
        {
            ModelAjouterBeep model = new ModelAjouterBeep();
            model.Date = DateTime.Now;
            model.Serie = GetNewCode();


            return View(model);
        }


        [HttpPost]
        public ActionResult Ajouter(ModelAjouterBeep model)
        {


            if (ModelState.IsValid)
            {

                BeepDto dto = new BeepDto
                {
                    Date = model.Date,
                    Serie = GetNewCode(),
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    NumeroFacture = model.NumeroFacture,
                    VehiculeImmatricule = model.VehiculeImmatricule,
                    DotationImmatricule = model.DotationImmatricule,
                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,
                    Commentaire = model.Commentaire




                };
                BusinessGestionBeep.Instance.Ajouter(dto);
                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }



        [HttpGet]
        public ActionResult ModifierBeep(int id)
        {

            ModelModifierBeep model = new ModelModifierBeep();

            BeepDto BeepDto = BusinessGestionBeep.Instance.GetBeepParId(id);

            if (BeepDto != null)
            {
                model.id = BeepDto.id;
                model.Serie = BeepDto.Serie;
                model.Date = BeepDto.Date;
                model.B12 = BeepDto.B12;
                model.B9 = BeepDto.B9;
                model.B6R = BeepDto.B6R;
                model.B6V = BeepDto.B6V;
                model.B3 = BeepDto.B3;
                model.B35 = BeepDto.B35;
                model.CodeClient = BeepDto.CodeClient;
                model.NomClient = BeepDto.NomClient;
                model.Commentaire = BeepDto.Commentaire;
                model.NumeroFacture = BeepDto.NumeroFacture;
                model.VehiculeImmatricule = BeepDto.VehiculeImmatricule;
                model.DotationImmatricule = BeepDto.DotationImmatricule;

            };


            return View(model);
        }




        [HttpPost]
        public ActionResult ModifierBeep(ModelModifierBeep model)
        {



            if (ModelState.IsValid)
            {

                BeepDto dto = new BeepDto
                {
                    id = model.id,
                    Date = model.Date,
                    Serie = GetNewCode(),
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    NumeroFacture = model.NumeroFacture,
                    VehiculeImmatricule = model.VehiculeImmatricule,
                    DotationImmatricule = model.DotationImmatricule,
                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,
                    Commentaire = model.Commentaire



                };
                BusinessGestionBeep.Instance.Modifier(dto);
                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }



        }







        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {

                maxCode = db.BEEPs.Select(x => x.Serie).Max();
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }


        public ActionResult Imprimer(int id)
        {
            ReportClass rpt = null;
            try
            {




                rpt = new crpBeep();
                var dto = BusinessGestionBeep.Instance.GetBeepParId(id);

                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.mr" +"\n"+ id, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);

                using (MemoryStream ms = new MemoryStream())
                {
                    qrCodeImage.Save(ms, ImageFormat.Bmp);
                    dto.qrCodeImage = ms.ToArray();
                }






                List<BeepDto> list = new List<BeepDto>();
                list.Add(dto);
                rpt.SetDataSource(list);
                Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception) { }
            return null;
        }
    }
}