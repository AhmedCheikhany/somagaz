﻿using BLL;
using CrystalDecisions.CrystalReports.Engine;
using GUHabitat.DAL;
using GUHabitat.DTO;
using GUHabitat.Models;
using GUHabitat.Reports;
using PagedList;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class BeevController : Controller
    {

        [HttpGet]
        public ActionResult Index(string filtreMotCle, int? page)
        {
            ListeBeevModel model = new ListeBeevModel();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeBeev = BusnessGestionBeev.Instance.ChercherListeBeev(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);


        }



        [HttpGet]
        public ActionResult Details(int id)
        {

            AjouterBeevModel model = new AjouterBeevModel();


            BeevDto BeevDto = BusnessGestionBeev.Instance.GetBeevParId(id);

            if (BeevDto != null)
            {
                model.id = BeevDto.id;
                model.Serie = BeevDto.Serie;
                model.Date = BeevDto.Date;
                model.B12 = BeevDto.B12;
                model.B9 = BeevDto.B9;
                model.B6R = BeevDto.B6R;
                model.B6V = BeevDto.B6V;
                model.B3 = BeevDto.B3;
                model.B35 = BeevDto.B35;
                model.CodeClient = BeevDto.CodeClient;
                model.NomClient = BeevDto.NomClient;
                model.Commentaire = BeevDto.Commentaire;
                model.etat = BeevDto.etat;


            };
            return View(model);

        }






        [HttpGet]
        public ActionResult Ajouter()
        {
            AjouterBeevModel model = new AjouterBeevModel();
            model.Date = DateTime.Now;
            model.Serie = GetNewCode();


            return View(model);
        }


        [HttpPost]
        public ActionResult Ajouter(AjouterBeevModel model )
        {

            
            if (ModelState.IsValid)
            {
             
                BeevDto dto = new BeevDto
                {
                    Date = model.Date,
                    Serie = GetNewCode(),
                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    Commentaire  = model.Commentaire
                



                };
                BusnessGestionBeev.Instance.Ajouter(dto);
                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }
          
        }


        [HttpGet]
        public ActionResult ModifierBeev(int id)
        {

            ModelModifierBeev model = new ModelModifierBeev();

            BeevDto BeevDto = BusnessGestionBeev.Instance.GetBeevParId(id);

            if (BeevDto != null)
            {
                model.id = BeevDto.id;
                model.Serie = BeevDto.Serie;
                model.Date = BeevDto.Date;
                model.B12 = BeevDto.B12;
                model.B9 = BeevDto.B9;
                model.B6R = BeevDto.B6R;
                model.B6V = BeevDto.B6V;
                model.B3 = BeevDto.B3;
                model.B35 = BeevDto.B35;
                model.CodeClient = BeevDto.CodeClient;
                model.NomClient = BeevDto.NomClient;
                model.Commentaire = BeevDto.Commentaire;


            };


            return View(model);
        }



        [HttpPost]
        public ActionResult ModifierBeev(ModelModifierBeev model)
        {



            if (ModelState.IsValid)
            {

                BeevDto dto = new BeevDto
                {
                    id = model.id,
                    Date = model.Date,
                    Serie = GetNewCode(),
                    B12 = model.B12,
                    B9 = model.B9,
                    B6R = model.B6R,
                    B6V = model.B6V,
                    B3 = model.B3,
                    B35 = model.B35,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    Commentaire = model.Commentaire




                };
                BusnessGestionBeev.Instance.Modifier(dto);
                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }


           
        }




        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {
               
                maxCode = db.BEEVs.Select(x => x.Serie).Max();
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }


        public ActionResult Imprimer(int id)
        {
            ReportClass rpt = null;
            try
            {


                

                rpt = new crpBeev();
                var dto = BusnessGestionBeev.Instance.GetBeevParId(id);

                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.mr" + "\n" + id, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);

                using (MemoryStream ms = new MemoryStream())
                {
                    qrCodeImage.Save(ms, ImageFormat.Bmp);
                    dto.qrCodeImage = ms.ToArray();
                }


            


              
                List<BeevDto> list = new List<BeevDto>();
                list.Add(dto);
                rpt.SetDataSource(list);
                Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception) { }
            return null;
        }

    }
}