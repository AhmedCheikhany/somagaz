﻿using CrystalDecisions.CrystalReports.Engine;
using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using GUHabitat.Reports;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class BonRetourCiterneController : Controller
    {

        [HttpPost]
        public ActionResult ValiderTypeBon(int idTypeBonRetour)
        {
            return RedirectToAction("Ajouter", new { idTypeBonRetour = idTypeBonRetour });
        }


        // GET: BonRetourCiterne

        [HttpGet]
        public ActionResult Index(string filtreMotCle, int? page)
        {
            ModelListeBonRetourCiterne model = new ModelListeBonRetourCiterne();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeBonRetourCiterne = BusinessGestionBonRetourCiterne.Instance.ChercherListeBon(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);


        }



        public ActionResult Acloturer(string filtreMotCle, int? page)
        {
            ModelListeBonRetourCiterne model = new ModelListeBonRetourCiterne();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeBonRetourCiterne = BusinessGestionBonRetourCiterne.Instance.ChercherListeBonAcloturer(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            ViewBag.nbInstance = BusinessGestionBonRetourCiterne.Instance.TotalCloturer();
            return View(model);

        }


        public ActionResult CloturerParDate(string date)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionBonRetourCiterne.Instance.CloturerParDate(idEntite, Convert.ToDateTime(date));

            return RedirectToAction("Index");
        }




        public ActionResult EnInstance(int id)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionBonRetourCiterne.Instance.EnInstance(idEntite, id);

            return RedirectToAction("Index");
        }


        public ActionResult Valider(int id)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionBonRetourCiterne.Instance.Valider(idEntite, id);

            return RedirectToAction("Index");
        }



        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelModifierBonRetourCiterne model = new ModelModifierBonRetourCiterne();


            BonRetourCiterneDto BonDto = BusinessGestionBonRetourCiterne.Instance.GetBonParId(id);

            if (BonDto != null)
            {
                model.id = BonDto.id;
                model.Reference = BonDto.Reference;
                model.idTypeBon = BonDto.idTypeBon;
                model.TypeBon = BonDto.TypeBon;
                model.CodeClient = BonDto.CodeClient;
                model.NomClient = BonDto.NomClient;
                model.CodeCentre = BonDto.CodeCentre;
                model.libelleCentre = BonDto.LibelleCentre;
                model.ImmtriculeCamion = BonDto.ImmtriculeCamion;
                model.DateEnvoie = BonDto.DateEnvoie;
                model.ReferenceBonChargement = BonDto.ReferenceBonChargement;
                model.PoidsVide = BonDto.PoidsVide;
                model.PoidsPlein = BonDto.PoidsPlein;
                model.PoidsGaz = BonDto.PoidsGaz;
                model.ReferencePVPesee = BonDto.ReferencePVPesee;
                model.DateRetour = BonDto.DateRetour;
                model.PoidsRetour = BonDto.PoidsRetour;
                model.PoidsVideRetour = BonDto.PoidsVideRetour;
                model.PoidsPleinRetour = BonDto.PoidsPleinRetour;

                model.PoidsGazRestant = BonDto.PoidsGazRestant;
                model.PoidsReceptionnerCentre = BonDto.PoidsReceptionnerCentre;
                model.Commentaire = BonDto.Commentaire;
                model.etat = BonDto.etat;
                model.DateLivraison = BusinessGestionBonRetourCiterne.Instance.GetDateLivraison(BonDto.id);
                model.DateLivraisonString = model.DateLivraison.Date.ToString("dd/MM/yyyy");

            };
            return View(model);

        }




        [HttpGet]
        public ActionResult Ajouter(int idTypeBonRetour)
        {
            ModelAjouterBonRetourCiterne model = new ModelAjouterBonRetourCiterne();
            model.DateEnvoie = DateTime.Now;
            model.DateRetour = DateTime.Now;
           
            string reference = GetNewCode();
            model.Reference = (reference == "1") ? "000001" : reference;
            model.idTypeBon = BusinessGestionBonRetourCiterne.Instance.GetTypeBonParId(idTypeBonRetour).id;


            if (model.idTypeBon == 1)
            {
                model.CodeClient = 0;
                model.NomClient = "Somagaz";
            }
            if (model.idTypeBon == 2)
            {

                model.CodeCentre = 0;
                model.libelleCentre  = "Somagaz";
            }
           


            return View(model);
        }



        [HttpPost]
        public ActionResult Ajouter(ModelAjouterBonRetourCiterne model)
        {


            if (ModelState.IsValid)
            {

                BonRetourCiterneDto dto = new BonRetourCiterneDto
                {


                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    Reference = model.Reference,
                    idTypeBon = model.idTypeBon,
                    TypeBon = model.TypeBon,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    CodeCentre = model.CodeCentre,
                    LibelleCentre = model.libelleCentre,
                    ImmtriculeCamion = model.ImmtriculeCamion,
                    DateEnvoie = model.DateEnvoie,
                    ReferenceBonChargement = model.ReferenceBonChargement,
                    PoidsVide = model.PoidsVide,
                    PoidsPlein = model.PoidsPlein,
                    PoidsGaz = model.PoidsGaz,
                    ReferencePVPesee = model.ReferencePVPesee,
                    DateRetour = model.DateRetour,
                    PoidsRetour = model.PoidsRetour,
                    PoidsVideRetour = model.PoidsVideRetour,
                    PoidsPleinRetour = model.PoidsPleinRetour,

                    PoidsGazRestant = model.PoidsGazRestant,

                    //PoidsGazRestant = model.PoidsRetour > 0 ? (model.PoidsRetour - model.PoidsVide) : (model.PoidsVideRetour - model.PoidsPleinRetour),
                    //PoidsReceptionnerCentre = (model.PoidsGaz - model.PoidsGazRestant),

                    Commentaire = model.Commentaire,
                    etat = model.etat,




               };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

                var refPV = BusinessGestionBonRetourCiterne.Instance.GetBonRetourByRefPV(dto.ReferencePVPesee, dto.Reference);

                //if (model.PoidsGazRestant <= model.PoidsVide)
                //{
                //    ViewBag.MessageAjout = "Le poids de retour ne peut pas etre inferieur ou egale au poids de l'allee";
                //    return View(model);

                ////}

                if (refPV != null)
                {
                    ViewBag.MessageAjoutPV = "La reference du Bon Pesee est deja existe ";
                    return View(model);
                }

                BusinessGestionBonRetourCiterne.Instance.Ajouter(dto);
                return RedirectToAction("Index");
            }
            else
            {
                model.DateEnvoie = DateTime.Now;
                model.DateRetour = DateTime.Now;
                return View(model);
            }

        }







        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModifierBonRetourCiterne model = new ModelModifierBonRetourCiterne();
            model.ListeTypeBons = BusinessGestionBonRetourCiterne.Instance.GetListeTypeBon();

            BonRetourCiterneDto dto = BusinessGestionBonRetourCiterne.Instance.GetBonParId(id);
           
            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.idTypeBon = dto.idTypeBon;
                model.TypeBon = dto.TypeBon;
                model.CodeClient = dto.CodeClient;
                model.NomClient = dto.NomClient;
                model.CodeCentre = dto.CodeCentre;
                model.libelleCentre = dto.LibelleCentre;
                model.ImmtriculeCamion = dto.ImmtriculeCamion;
                model.DateEnvoie = dto.DateEnvoie;
                model.ReferenceBonChargement = dto.ReferenceBonChargement;
                model.PoidsVide = dto.PoidsVide;
                model.PoidsPlein = dto.PoidsPlein;
                model.PoidsGaz = dto.PoidsGaz;
                model.ReferencePVPesee = dto.ReferencePVPesee;
                model.DateRetour = dto.DateRetour;
                model.PoidsRetour = dto.PoidsRetour;
                model.PoidsVideRetour = dto.PoidsVideRetour;
                model.PoidsPleinRetour = dto.PoidsPleinRetour;

                model.PoidsGazRestant = dto.PoidsGazRestant;
                model.PoidsReceptionnerCentre = dto.PoidsReceptionnerCentre;
                model.Commentaire = dto.Commentaire;
                model.etat = dto.etat;

                


            };

            return View(model);
        }




        [HttpPost]
        public ActionResult Modifier(ModelModifierBonRetourCiterne model)
        {


            if (ModelState.IsValid)
            {

                BonRetourCiterneDto dto = new BonRetourCiterneDto
                {

                    id = model.id,
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    Reference = model.Reference,
                    idTypeBon = model.idTypeBon,
                    TypeBon = model.TypeBon,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    CodeCentre = model.CodeCentre,
                    LibelleCentre = model.libelleCentre,
                    ImmtriculeCamion = model.ImmtriculeCamion,
                    DateEnvoie = model.DateEnvoie,
                    ReferenceBonChargement = model.ReferenceBonChargement,
                    PoidsVide = model.PoidsVide,
                    PoidsPlein = model.PoidsPlein,
                    PoidsGaz = model.PoidsGaz,
                    ReferencePVPesee = model.ReferencePVPesee,
                    DateRetour = model.DateRetour,
                    PoidsRetour = model.PoidsRetour,
                    PoidsVideRetour = model.PoidsVideRetour,
                    PoidsPleinRetour = model.PoidsPleinRetour,

                    //PoidsGazRestant = model.PoidsRetour > 0 ? (model.PoidsRetour - model.PoidsVide) : (model.PoidsVideRetour - model.PoidsPleinRetour),

                    PoidsGazRestant = model.PoidsRetour > 0 ? (model.PoidsRetour - model.PoidsVide) : model.PoidsGazRestant,

                   
                    Commentaire = model.Commentaire,
                    etat = model.etat,


                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

                //if (model.PoidsGazRestant <= model.PoidsVide)
                //{
                //    ViewBag.MessageModifier = "Le poids de retour ne peut pas etre inferieur ou egale au poids de l'allee";
                //    model.ListeTypeBons = BusinessGestionBonRetourCiterne.Instance.GetListeTypeBon();
                //    model.idTypeBon = BusinessGestionBonRetourCiterne.Instance.GetTypeBonParId(model.idTypeBon).id;
                //    model.TypeBon = BusinessGestionBonRetourCiterne.Instance.GetTypeBonParId(model.idTypeBon).libelle;
                //    return View(model);

                //}

                var refPV = BusinessGestionBonRetourCiterne.Instance.GetBonRetourByRefPV(dto.ReferencePVPesee,dto.Reference);

                if (refPV != null)
                {
                    ViewBag.MessageModifierPV = "La reference du Bon Pesee est deja existe ";
                    model.ListeTypeBons = BusinessGestionBonRetourCiterne.Instance.GetListeTypeBon();
                    model.idTypeBon = BusinessGestionBonRetourCiterne.Instance.GetTypeBonParId(model.idTypeBon).id;
                    model.TypeBon = BusinessGestionBonRetourCiterne.Instance.GetTypeBonParId(model.idTypeBon).libelle;
                    return View(model);
                }

                BusinessGestionBonRetourCiterne.Instance.Modifier(dto);
                return RedirectToAction("Index");
            }
            else
            {
                model.ListeTypeBons = BusinessGestionBonRetourCiterne.Instance.GetListeTypeBon();
                model.idTypeBon = BusinessGestionBonRetourCiterne.Instance.GetTypeBonParId(model.idTypeBon).id;
                model.TypeBon = BusinessGestionBonRetourCiterne.Instance.GetTypeBonParId(model.idTypeBon).libelle;
                model.DateEnvoie = DateTime.Now;
                model.DateRetour = DateTime.Now;
                return View(model);
            }

        }


       
    
        [HttpPost]
        public JsonResult AjouterLivraisonRetourne(ModelAjouterLivraison model)
        {

                    LivraisonDto dto = new LivraisonDto
                    {
                        idBonRetour = model.idBonRetour,
                        DateLivraison = model.DateLivraison,
                        PoidsRetour = model.PoidsRetour,
                        commentaire = model.commentaire
                    };


                    string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                    GetUser.GetUserr(login);

                    BusinessGestionBonRetourCiterne.Instance.EnregistrerLivraisonRetour(dto);
                    return Json(dto, JsonRequestBehavior.AllowGet);
        }





        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {
                maxCode = db.BonRetourCiternes.OrderByDescending(s => s.id).Select(x => x.Reference).FirstOrDefault();

                //maxCode = db.BonChargementCiternes.Select(x => x.Reference).Max();
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }



        public ActionResult Imprimer(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {


                rpt = new crpBonRetour();
                //rpt = crpBonRetour.Instance;

                var dto = BusinessGestionBonRetourCiterne.Instance.GetBonImprimerParId(id);

                List<BonRetourCiterneDto> list = new List<BonRetourCiterneDto>();
                list.Add(dto);
                rpt.SetDataSource(list);
                Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

                //if (idEntite == 5 && dto.TonnageCharger > 0)
                //{
                //    BusinessGestionBonChargementCiterne.Instance.Valider(idEntite, id);
                //}

                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception) { }
            return null;


        }






    }


}