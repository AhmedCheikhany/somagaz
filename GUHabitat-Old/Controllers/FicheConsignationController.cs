﻿using CrystalDecisions.CrystalReports.Engine;
using GUHabitat.BLL;
using GUHabitat.DTO;
using GUHabitat.Models;
using GUHabitat.Reports;
using PagedList;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class FicheConsignationController : Controller
    {

        [HttpPost]
        public ActionResult ValiderTypeFiche(int idTypeFicheConsignation)
        {
            return RedirectToAction("Ajouter", new { idTypeFicheConsignation = idTypeFicheConsignation });
        }



        // GET: FicheConsignation
        [HttpGet]
        public ActionResult Index(string filtreMotCle, int? page)
        {
            ModelListeFicheConsignation model = new ModelListeFicheConsignation();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheConsignation = BusinessGestionFicheConsignation.Instance.ChercherListeConsignation(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);


        }


        public ActionResult IndexParDate(string filtreMotCle, DateTime? dateDebut, DateTime? dateFin, int? page)
        {

            ModelListeFicheConsignationParClient model = new ModelListeFicheConsignationParClient();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            if (dateDebut == null) dateDebut = DateTime.Now.Date;
            if (dateFin == null) dateFin = DateTime.Now.Date;

            model.filtreMotCle = filtreMotCle;
            model.dateDebut = (DateTime)dateDebut;
            model.dateFin = (DateTime)dateFin;


            int pageSize = 20;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheConsignationParClient = BusinessGestionFicheConsignation.Instance.ChercherListeConsignationParClient(model.filtreMotCle, model.dateDebut, model.dateFin, idEntite, login).ToPagedList(pageIndex, pageSize);

            return View(model);
        }




        [HttpGet]
        public ActionResult Instance(string filtreMotCle, int? page)
        {
            ModelListeFicheConsignation model = new ModelListeFicheConsignation();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheConsignation = BusinessGestionFicheConsignation.Instance.ChercherListeConsignationInstance(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            ViewBag.nbInstance = BusinessGestionFicheConsignation.Instance.TotalInstance();

            return View(model);


        }

        [HttpGet]
        public ActionResult Acloturer(string filtreMotCle, int? page)
        {
            ModelListeFicheConsignation model = new ModelListeFicheConsignation();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeFicheConsignation = BusinessGestionFicheConsignation.Instance.ChercherListeConsignationCloturer(model.filtreMotCle, idEntite, login).ToPagedList(pageIndex, pageSize);

            ViewBag.nbInstance = BusinessGestionFicheConsignation.Instance.TotalCloturer();

            return View(model);


        }



        public void Selected(int[] ids)
        {
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            BusinessGestionFicheConsignation.Instance.Cloturer(ids);  
        }



        public ActionResult EnInstance(int id)
        {


            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionFicheConsignation.Instance.EnInstance(idEntite,id);

            return RedirectToAction("Index");
        }



        public ActionResult Valider(int id)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionFicheConsignation.Instance.Valider(idEntite, id);


            return RedirectToAction("Index");
        }


        public ActionResult CloturerParDate(string date)
        {

            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            GetUser.GetUserr(login);

            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            BusinessGestionFicheConsignation.Instance.CloturerParDate(idEntite, Convert.ToDateTime(date));

            return RedirectToAction("Index");
        }



        [HttpGet]
        public ActionResult Details(int id)
        {

            ModelAjouterFicheConsignation model = new ModelAjouterFicheConsignation();


            FicheConsignationDto dto = BusinessGestionFicheConsignation.Instance.GetFicheConsignationParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.idTypeFicheConsignation = dto.idTypeFicheConsignation;
                model.CodeClient = dto.CodeClient;
                model.NomClient = dto.NomClient;
                model.TypeFicheConsignation = dto.TypeFicheConsignation;
                model.NumeroSerie = dto.NumeroSerie;
                model.VehiculeImmatricule = dto.VehiculeImmatricule;

                model.B12DC = dto.B12DC;
                model.B9DC = dto.B9DC;
                model.B6RDC = dto.B6RDC;
                model.B6VDC = dto.B6VDC;
                model.B3DC = dto.B3DC;
                model.B35DC = dto.B35DC;
                model.PresentoirDC = dto.PresentoirDC;

                model.B12Prix = dto.B12Prix;
                model.B9Prix = dto.B9Prix;
                model.B6RPrix = dto.B6RPrix;
                model.B6VPrix = dto.B6VPrix;
                model.B3Prix = dto.B3Prix;
                model.B35Prix = dto.B35Prix;
                model.PresentoirPrix = dto.PresentoirPrix;


                model.TotalPrixB12 = dto.TotalPrixB12;
                model.TotalPrixB9 = dto.TotalPrixB9;
                model.TotalPrixB6R = dto.TotalPrixB6R;
                model.TotalPrixB6V = dto.TotalPrixB6V;
                model.TotalPrixB3 = dto.TotalPrixB3;
                model.TotalPrixB35 = dto.TotalPrixB35;
                model.TotalPrixPresentoir = dto.TotalPrixPresentoir;

                model.B12DE = dto.B12DE;
                model.B9DE = dto.B9DE;
                model.B6RDE = dto.B6RDE;
                model.B6VDE = dto.B6VDE;
                model.B3DE = dto.B3DE;
                model.B35DE = dto.B35DE;
                model.PresentoirDE = dto.PresentoirDE;

                model.Commentaire = dto.Commentaire;
                model.etat = dto.etat;


            };
            return View(model);

        }




        [HttpGet]
        public ActionResult Ajouter(int idTypeFicheConsignation)
        {
            ModelAjouterFicheConsignation model = new ModelAjouterFicheConsignation();
            model.Date = DateTime.Now;
            string reference = GetNewCode();
            model.Reference = (reference == "1") ? "000001" : reference;

            model.idTypeFicheConsignation = BusinessGestionFicheConsignation.Instance.GetTypeFicheParId(idTypeFicheConsignation).id;

            model.B12Prix = 600;
            model.B6RPrix = 800;
            model.B6VPrix = 800;
            model.B3Prix = 400;
            model.B35Prix = 2500;
            model.PresentoirPrix = 0;


            return View(model);
        }


        [HttpPost]
        public ActionResult Ajouter(ModelAjouterFicheConsignation model)
        {


            if (ModelState.IsValid)
            {

                FicheConsignationDto dto = new FicheConsignationDto
                {
                    Date = model.Date,
                    Reference = model.Reference,
                    idTypeFicheConsignation = model.idTypeFicheConsignation,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,
                    NumeroSerie = model.NumeroSerie,
                    VehiculeImmatricule = model.VehiculeImmatricule,

                    B12DC = model.B12DC,
                    B9DC = model.B9DC,
                    B6RDC = model.B6RDC,
                    B6VDC = model.B6VDC,
                    B3DC = model.B3DC,
                    B35DC = model.B35DC,
                    PresentoirDC = model.PresentoirDC,

                    B12Prix = model.B12Prix,
                    B9Prix = model.B9Prix,
                    B6RPrix = model.B6RPrix,
                    B6VPrix = model.B6VPrix,
                    B3Prix = model.B3Prix,
                    B35Prix = model.B35Prix,
                    PresentoirPrix = 0,

                    TotalPrixB12 = model.B12DC * model.B12Prix,
                    TotalPrixB9 = model.B9DC * model.B9Prix,
                    TotalPrixB6R = model.B6RDC * model.B6RPrix,
                    TotalPrixB6V = model.B6VDC * model.B6VPrix,
                    TotalPrixB3 = model.B3DC * model.B3Prix,
                    TotalPrixB35 = model.B35DC * model.B35Prix,
                    TotalPrixPresentoir = model.PresentoirDC * model.PresentoirPrix,



                    B12DE = model.B12DE,
                    B9DE = model.B9DE,
                    B6RDE = model.B6RDE,
                    B6VDE = model.B6VDE,
                    B3DE = model.B3DE,
                    B35DE = model.B35DE,
                    PresentoirDE = model.PresentoirDE,


                    Commentaire = model.Commentaire,

                    

                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);


                //var nbrefiche = BusinessGestionFicheConsignation.Instance.VerifierExistFiche(model.Reference);
                //if (nbrefiche == 0)
                //{

                //    BusinessGestionFicheConsignation.Instance.Ajouter(dto);
                //    return RedirectToAction("Index");
                //}



                if (model.B12DE > model.B12DC || model.B9DE > model.B9DC || model.B6RDE > model.B6RDC || model.B6VDE > model.B6VDC || model.B3DE > model.B3DC || model.B35DE > model.B35DC || model.PresentoirDE > model.PresentoirDC)
                {

                    ViewBag.MessageAjout1 = "Le nombre consigné  ne peut pas être plus grand que le nombre à consigner ";

                    return View(model);
                }


                BusinessGestionFicheConsignation.Instance.Ajouter(dto);
                return RedirectToAction("Index");

            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }

        }





        [HttpGet]
        public ActionResult Modifier(int id)
        {

            ModelModifierFicheConsignation model = new ModelModifierFicheConsignation();
            model.ListeFiches = BusinessGestionFicheConsignation.Instance.GetListeTypeFiche();

            FicheConsignationDto dto = BusinessGestionFicheConsignation.Instance.GetFicheConsignationParId(id);

            if (dto != null)
            {
                model.id = dto.id;
                model.Reference = dto.Reference;
                model.Date = dto.Date;
                model.idTypeFicheConsignation = dto.idTypeFicheConsignation;
                model.CodeClient = dto.CodeClient;
                model.NomClient = dto.NomClient;

                model.NumeroSerie = dto.NumeroSerie;
                model.VehiculeImmatricule = dto.VehiculeImmatricule;

                model.B12DC = dto.B12DC;
                model.B9DC = dto.B9DC;
                model.B6RDC = dto.B6RDC;
                model.B6VDC = dto.B6VDC;
                model.B3DC = dto.B3DC;
                model.B35DC = dto.B35DC;
                model.PresentoirDC = dto.PresentoirDC;

                model.B12Prix = dto.B12Prix;
                model.B9Prix = dto.B9Prix;
                model.B6RPrix = dto.B6RPrix;
                model.B6VPrix = dto.B6VPrix;
                model.B3Prix = dto.B3Prix;
                model.B35Prix = dto.B35Prix;
                model.PresentoirPrix = dto.PresentoirPrix;


                model.TotalPrixB12 = dto.TotalPrixB12;
                model.TotalPrixB9 = dto.TotalPrixB9;
                model.TotalPrixB6R = dto.TotalPrixB6R;
                model.TotalPrixB6V = dto.TotalPrixB6V;
                model.TotalPrixB3 = dto.TotalPrixB3;
                model.TotalPrixB35 = dto.TotalPrixB35;
                model.TotalPrixPresentoir = dto.TotalPrixPresentoir;


                model.B12DE = dto.B12DE;
                model.B9DE = dto.B9DE;
                model.B6RDE = dto.B6RDE;
                model.B6VDE = dto.B6VDE;
                model.B3DE = dto.B3DE;
                model.B35DE = dto.B35DE;
                model.PresentoirDE = dto.PresentoirDE;

               
                model.Commentaire = dto.Commentaire;
                model.etat = dto.etat;


            };


            return View(model);
        }



        [HttpPost]
        public ActionResult Modifier(ModelModifierFicheConsignation model)
        {



            if (ModelState.IsValid)
            {

                FicheConsignationDto dto = new FicheConsignationDto
                {
                    id = model.id,
                    idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value),
                    Date = model.Date,
                    Reference = model.Reference,
                    idTypeFicheConsignation = model.idTypeFicheConsignation,
                    CodeClient = model.CodeClient,
                    NomClient = model.NomClient,

                    NumeroSerie = model.NumeroSerie,
                    VehiculeImmatricule = model.VehiculeImmatricule,

                    B12DC = model.B12DC,
                    B9DC = model.B9DC,
                    B6RDC = model.B6RDC,
                    B6VDC = model.B6VDC,
                    B3DC = model.B3DC,
                    B35DC = model.B35DC,
                    PresentoirDC = model.PresentoirDC,

                    B12Prix = model.B12Prix,
                    B9Prix = model.B9Prix,
                    B6RPrix = model.B6RPrix,
                    B6VPrix = model.B6VPrix,
                    B3Prix = model.B3Prix,
                    B35Prix = model.B35Prix,
                    PresentoirPrix = 0,



                    TotalPrixB12 = model.B12DC * model.B12Prix,
                    TotalPrixB9 = model.B9DC * model.B9Prix,
                    TotalPrixB6R = model.B6RDC * model.B6RPrix,
                    TotalPrixB6V = model.B6VDC * model.B6VPrix,
                    TotalPrixB3 = model.B3DC * model.B3Prix,
                    TotalPrixB35 = model.B35DC * model.B35Prix,
                    TotalPrixPresentoir = model.PresentoirDC * model.PresentoirPrix,

                    B12DE = model.B12DE,
                    B9DE = model.B9DE,
                    B6RDE = model.B6RDE,
                    B6VDE = model.B6VDE,
                    B3DE  = model.B3DE,
                    B35DE = model.B35DE,
                    PresentoirDE = model.PresentoirDE,


                    Commentaire = model.Commentaire

                    

                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

                if (model.B12DE > model.B12DC || model.B9DE > model.B9DC || model.B6RDE > model.B6RDC || model.B6VDE > model.B6VDC || model.B3DE > model.B3DC || model.B35DE > model.B35DC || model.PresentoirDE > model.PresentoirDC)
                {

                    ViewBag.MessageModifier1 = "Le nombre consigné  ne peut pas être plus grand que le nombre  à consigner ";

                    return View(model);
                }

                BusinessGestionFicheConsignation.Instance.Modifier(dto);


                return RedirectToAction("Index");
            }
            else
            {
                model.Date = DateTime.Now;
                return View(model);
            }



        }





        string GetNewCode()
        {

            string maxCode;
            using (var db = new DAL.GUHabitatDBEntities())
            {
                maxCode = db.FicheConsignations.OrderByDescending(s => s.id).Select(x => x.Reference).FirstOrDefault();
                //maxCode = db.FicheConsignations.Select(x => x.Reference).Max();
            }

            return GetNextNumberInString(maxCode);
        }

        string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";

            string str1 = "";
            foreach (Char c in Number)

                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt32(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");

            int index = Number.LastIndexOf(str1);
            Number = Number.Remove(index);
            Number = Number.Insert(index, str3);
            return Number;

        }


        public ActionResult Imprimer(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {




                //rpt = new crpFicheConsignation();
                rpt =  crpFicheConsignation.Instance;
                var dto = BusinessGestionFicheConsignation.Instance.GetFicheConsignationParId(id);

                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    //QRCodeGenerator qrGenerator = new QRCodeGenerator();
                    //QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.com" + "\n" + dto.Reference, QRCodeGenerator.ECCLevel.Q);
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode("==> FICHE DE CONSIGNATION " + "\n\n" + "Référence :" + "\n\n" + dto.Reference + "\n\n" + "Date :" + "\n\n" + dto.Date.Date.ToShortDateString() + "\n\n" + "Code Client :" + "\n\n" + dto.CodeClient + "\n\n" + "Nom Client :" + "\n\n" + dto.NomClient + "\n\n" + "B12 :" + "\n\n" + dto.B12DC + "\n\n" + "B9 :" + "\n\n" + dto.B9DC + "\n\n" + "B6R :" + "\n\n" + dto.B6RDC + "\n\n" + "B6V :" + "\n\n" + dto.B6VDC + "\n\n" + "B3 :" + "\n\n" + dto.B3DC + "\n\n" + "B35 :" + "\n\n" + dto.B35DC, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeImage.Save(ms, ImageFormat.Bmp);
                        dto.qrCodeImage = ms.ToArray();
                    }





                    List<FicheConsignationDto> list = new List<FicheConsignationDto>();
                    list.Add(dto);
                    rpt.SetDataSource(list);
                    Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

                    if (idEntite == 5 && (dto.B12DE != 0 || dto.B9DE != 0 || dto.B6RDE != 0 || dto.B6VDE != 0 || dto.B3DE != 0 || dto.B35DE != 0))
                    {
                        BusinessGestionFicheConsignation.Instance.Valider(idEntite, id);
                    }

                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            catch (Exception) { }
            return null;
        }



        public ActionResult ImprimerA5(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {




                //rpt = new crpFicheConsignationA5();
                rpt = crpFicheConsignationA5.Instance;
                var dto = BusinessGestionFicheConsignation.Instance.GetFicheConsignationParId(id);


                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    //QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.com" + "\n" + dto.Reference, QRCodeGenerator.ECCLevel.Q);
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode("==> FICHE DE CONSIGNATION DE BOUTEILLES" + "\n\n" + "Référence :" + "\n\n" + dto.Reference + "\n\n" + "Date :" + "\n\n" + dto.Date.Date.ToShortDateString() + "\n\n" + "Type :" + "\n\n" + dto.TypeFicheConsignation + "\n\n" + "Code Client :" + "\n\n" + dto.CodeClient + "\n\n" + "Nom Client :" + "\n\n" + dto.NomClient + "\n\n" + "B12 :" + "\n\n" + dto.B12DC + "\n\n" + "B9 :" + "\n\n" + dto.B9DC + "\n\n" + "B6R :" + "\n\n" + dto.B6RDC + "\n\n" + "B6V :" + "\n\n" + dto.B6VDC + "\n\n" + "B3 :" + "\n\n" + dto.B3DC + "\n\n" + "B35 :" + "\n\n" + dto.B35DC, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);


                

                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeImage.Save(ms, ImageFormat.Bmp);
                        dto.qrCodeImage = ms.ToArray();
                    }





                    List<FicheConsignationDto> list = new List<FicheConsignationDto>();
                    list.Add(dto);
                    rpt.SetDataSource(list);
                    Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

                    if (idEntite == 5 && (dto.B12DE != 0 || dto.B9DE != 0 || dto.B6RDE != 0 || dto.B6VDE != 0 || dto.B3DE != 0 || dto.B35DE != 0))
                    {
                        BusinessGestionFicheConsignation.Instance.Valider(idEntite, id);
                    }

                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            catch (Exception) { }
            return null;
        }




        public ActionResult ImprimerPresentoir(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {




                //rpt = new crpFichePresentoir();
                rpt = crpFichePresentoir.Instance;
                var dto = BusinessGestionFicheConsignation.Instance.GetFicheConsignationParId(id);


                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    //QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.com" + "\n" + dto.Reference, QRCodeGenerator.ECCLevel.Q);
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode("==> FICHE DE CONSIGNATION DE PRESENTOIRES" + "\n\n" + "Référence :" + "\n\n" + dto.Reference + "\n\n" + "Date :" + "\n\n" + dto.Date.Date.ToShortDateString() + "\n\n" + "Code Client :" + "\n\n" + dto.CodeClient + "\n\n" + "Nom Client :" + "\n\n" + dto.NomClient +  "\n\n" + "Nbre a consigné :" + "\n\n" + dto.PresentoirDC + "\n\n" + "Nbre consigné" + "\n\n" + dto.PresentoirDE, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);




                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeImage.Save(ms, ImageFormat.Bmp);
                        dto.qrCodeImage = ms.ToArray();
                    }





                    List<FicheConsignationDto> list = new List<FicheConsignationDto>();
                    list.Add(dto);
                    rpt.SetDataSource(list);
                    Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

                    if (idEntite == 5 && (dto.PresentoirDE != 0))
                    {
                        BusinessGestionFicheConsignation.Instance.Valider(idEntite, id);
                    }

                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            catch (Exception) { }
            return null;
        }



        public ActionResult ImprimerBulletin(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {




                //rpt = new crpBulletinConsignation();
                rpt = crpBulletinConsignation.Instance;
                var dto = BusinessGestionFicheConsignation.Instance.GetFicheConsignationParId(id);

                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.mr" + "\n" + dto.Reference, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeImage.Save(ms, ImageFormat.Bmp);
                        dto.qrCodeImage = ms.ToArray();
                    }

                
               






                    List<FicheConsignationDto> list = new List<FicheConsignationDto>();
                    list.Add(dto);
                    rpt.SetDataSource(list);
                    Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);


                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            catch (Exception) { }
            return null;
        }



        public ActionResult ImprimerBulletinPresentoir(int id)
        {
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);

            ReportClass rpt = null;
            try
            {




                //rpt = new crpBulletinPresentoir();
                rpt = crpBulletinPresentoir.Instance;
                var dto = BusinessGestionFicheConsignation.Instance.GetFicheConsignationParId(id);

                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode("www.somagaz.mr" + "\n" + dto.Reference, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeImage.Save(ms, ImageFormat.Bmp);
                        dto.qrCodeImage = ms.ToArray();
                    }



                   


                    List<FicheConsignationDto> list = new List<FicheConsignationDto>();
                    list.Add(dto);
                    rpt.SetDataSource(list);
                    Stream stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);


                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            catch (Exception) { }
            return null;
        }



    }
}