﻿using BLL;
using GUHabitat.DTO;
using GUHabitat.Helpers;
using GUHabitat.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class UtilisateurController : Controller
    {
        [HttpGet]
        public ActionResult Index(string filtreMotCle, int? page)
        {
            
            ModelListUtilisateur model = new ModelListUtilisateur();
            string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
            int idEntite = int.Parse((User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Locality).Value);
            model.filtreMotCle = filtreMotCle;
            int pageSize = 10;
            int pageIndex = page.HasValue ? page.Value : 1;
            model.ListeUtilisateurs = BusinessGestionUtilisateurs.Instance.ChercherListeUtilisateurs(model.filtreMotCle, model.filtreEntite).ToPagedList(pageIndex, pageSize);

            return View(model);

        }



        [HttpGet]
        public ActionResult Ajouter()
        {
            ModelAjouterUtilisateur model = new ModelAjouterUtilisateur();
            model.ListeEntites = BusinessGestionUtilisateurs.Instance.GetListeEntites();
            model.ListeRoles = BusinessGestionUtilisateurs.Instance.GetListeRoles();
            return View(model);
        }


        [HttpPost]
        public ActionResult Ajouter(ModelAjouterUtilisateur model /*binding*/)
        {

                 //valider la saisie
            if (ModelState.IsValid)
            {
                byte[] uploadedFile = null;
                if (model.signature != null)
                {
                    uploadedFile = new byte[model.signature.InputStream.Length];
                    model.signature.InputStream.Read(uploadedFile, 0, uploadedFile.Length);
                }

                //stocker dans la base
                UtilisateurDto dto = new UtilisateurDto
                {
                    

                    login = model.login,
                    password = CryptorEngine.Encrypt(model.password, true),
                    //password = model.password,
                    Nom = model.Nom,
                    idEntite = model.idEntite,
                    idRole = model.idRole,
                    signature = uploadedFile

                };

                var user = BusinessGestionUtilisateurs.Instance.GetPasswordParlogin(dto.login);

                if (user != null)
                {
                    model.ListeEntites = BusinessGestionUtilisateurs.Instance.GetListeEntites();
                    model.ListeRoles = BusinessGestionUtilisateurs.Instance.GetListeRoles();

                    ViewBag.MessageAjout = "Cet utilisateur est déjà existe ";

                    return View("Ajouter", model);
                }
                else
                {
                    string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                    GetUser.GetUserr(login);

                    BusinessGestionUtilisateurs.Instance.EnregistrerNouvelleUtilisateur(dto);
                    return RedirectToAction("Index");
                }

                

            }
            else
            {
                model.ListeEntites = BusinessGestionUtilisateurs.Instance.GetListeEntites();
                model.ListeRoles = BusinessGestionUtilisateurs.Instance.GetListeRoles();
                return View("Ajouter", model);
            }


        }


   
        [HttpGet]
        public ActionResult Modifier(int id /*binding*/)
        {
            ModelModifierUtilisateur model = new ModelModifierUtilisateur();
            model.ListeEntites = BusinessGestionUtilisateurs.Instance.GetListeEntites();
            model.ListeRoles = BusinessGestionUtilisateurs.Instance.GetListeRoles();

            UtilisateurDto Userdto = BusinessGestionUtilisateurs.Instance.GetUtilisateurParId(id);

            if (Userdto != null)
            {
                model.id = Userdto.id;
                model.login = Userdto.login;
                //model.password = CryptorEngine.Decrypt(Userdto.password, true);
                model.password = Userdto.password;
                model.Nom = Userdto.Nom;
                model.idEntite = Userdto.idEntite;
                model.idRole = Userdto.idRole;

            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Modifier(ModelModifierUtilisateur model /*binding*/)
        {
            //valider la saisie
            if (ModelState.IsValid)
            {
                byte[] uploadedFile = null;
                if (model.signature != null)
                {
                    uploadedFile = new byte[model.signature.InputStream.Length];
                    model.signature.InputStream.Read(uploadedFile, 0, uploadedFile.Length);
                }
                UtilisateurDto Userdto = BusinessGestionUtilisateurs.Instance.GetUtilisateurParId(model.id);

                UtilisateurDto dto = new UtilisateurDto
                {
                    
                    id = model.id,
                    login = model.login,
                    password = (model.password == Userdto.password)? Userdto.password : CryptorEngine.Encrypt(model.password, true) ,
                    Nom = model.Nom,
                    idEntite = model.idEntite,
                    idRole = model.idRole,
                    signature = uploadedFile

                };

                string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                GetUser.GetUserr(login);

                BusinessGestionUtilisateurs.Instance.ModifierUtilisateur(dto);

                //répondre le client
                return RedirectToAction("Index");

            }
            else
            {
                model.ListeEntites = BusinessGestionUtilisateurs.Instance.GetListeEntites();
                model.ListeRoles = BusinessGestionUtilisateurs.Instance.GetListeRoles();
                return View("Modifier", model);
            }
        }






        [HttpGet]
        public ActionResult ModifierPassword(string login /*binding*/)
        {
            ModelModifierPasswordUser model = new ModelModifierPasswordUser();
            model.ListeEntites = BusinessGestionUtilisateurs.Instance.GetListeEntites();
            model.ListeRoles = BusinessGestionUtilisateurs.Instance.GetListeRoles();

            UtilisateurDto Userdto = BusinessGestionUtilisateurs.Instance.GetUtilisateurLogin(login);

            if (Userdto != null)
            {
                model.id = Userdto.id;
                model.login = Userdto.login;
                model.password = CryptorEngine.Decrypt(Userdto.password, true);
                //model.password = Userdto.password;
                model.Nom = Userdto.Nom;
                model.idEntite = Userdto.idEntite;
                model.idRole = Userdto.idRole;

            }

            return View(model);
        }



        [HttpPost]
        public ActionResult ModifierPassword(ModelModifierPasswordUser model)
        {
            //valider la saisie
            if (ModelState.IsValid)
            {
                byte[] uploadedFile = null;
                if (model.signature != null)
                {
                    uploadedFile = new byte[model.signature.InputStream.Length];
                    model.signature.InputStream.Read(uploadedFile, 0, uploadedFile.Length);
                }

                string OldPassword = BusinessGestionUtilisateurs.Instance.GetPasswordParlogin(model.login);

                if (model.Oldpassword == CryptorEngine.Decrypt(OldPassword, true))
                {
                    UtilisateurDto Userdto = BusinessGestionUtilisateurs.Instance.GetUtilisateurLogin(model.login);

                    UtilisateurDto dto = new UtilisateurDto
                    {

                        id = Userdto.id,
                        login = model.login,
                        password = model.password == null ? Userdto.password : CryptorEngine.Encrypt(model.password, true),
                        Nom = Userdto.Nom,
                        idEntite = Userdto.idEntite,
                        idRole = Userdto.idRole,
                        signature = uploadedFile

                    };

                    string login = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.NameIdentifier).Value;
                    GetUser.GetUserr(login);

                    BusinessGestionUtilisateurs.Instance.ModifierPasswordUtilisateur(dto);

                    //répondre le client
                    return RedirectToAction("Index","Home");
                }
                else
                {
                    ViewBag.Message = "L'ancien mot de passe est incorrecte";
                    return View("ModifierPassword", model);
                }
              

            }
            else
            {
                model.ListeEntites = BusinessGestionUtilisateurs.Instance.GetListeEntites();
                model.ListeRoles = BusinessGestionUtilisateurs.Instance.GetListeRoles();
                return View("ModifierPassword", model);
            }
        }






        [HttpGet]
        public ActionResult Supprimer(string id /*binding*/)
        {
            //Essayer de supprimer
            if (BusinessGestionUtilisateurs.Instance.SupprimerUtilisateur(id))
            {
                //si ca marche => un message de succès

                var msg = "L'opération a réussi";
                TempData["SuccessMessage"] = msg;
            }
            else
            {
                //sinon =>un message d'erreur
                var msg = "L'opération a échoué";
                TempData["ErrorMessage"] = msg;
            }
            //revenir à la page index
            return RedirectToAction("Index");
        }

       

       
    }
}