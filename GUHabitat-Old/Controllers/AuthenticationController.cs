﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using GUHabitat.Helpers;
using GUHabitat.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GUHabitat.Controllers
{
    public class AuthenticationController : Controller
    {
        GUHabitatDBEntities db = new GUHabitatDBEntities();
        Utilisateur userConnected;

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            string pwd = CryptorEngine.Encrypt(model.Password, true);
            if (!ValidateUser(model.Login, pwd))
            if (!ValidateUser(model.Login, model.Password))
            {
                //ModelState.AddModelError(string.Empty /*"Le nom d'utilisateur ou le mot de passe est incorrect."*/);
                return View(model);
            }

            // L'authentification est réussie, 
            // injecter l'identifiant utilisateur dans le cookie d'authentification :
            var loginClaim = new Claim(ClaimTypes.NameIdentifier, model.Login);
            var profilClaim = new Claim(ClaimTypes.Role, userConnected.Role.libelle);
            var entiteClaim = new Claim(ClaimTypes.Locality, userConnected.idEntite == null ? "0" : userConnected.idEntite.Value.ToString());
            var claimsIdentity = new ClaimsIdentity(new[] { loginClaim,profilClaim,entiteClaim }, DefaultAuthenticationTypes.ApplicationCookie);
            var ctx = Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignIn(claimsIdentity);

            // Rediriger vers l'URL d'origine :
            if (Url.IsLocalUrl(ViewBag.ReturnUrl))
                return Redirect(ViewBag.ReturnUrl);
            // Par défaut, rediriger vers la page d'accueil :

            // Get login user pour le trasser et enregistrer ces operation 
            GetUser.GetUserr(userConnected.login);

            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public ActionResult Logout()
        {
            var ctx = Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignOut();

            // Rediriger vers la page d'accueil :
            return RedirectToAction("Index", "Home");
        }
        private bool ValidateUser(string login, string password)
        {
            userConnected = db.Utilisateurs.Where(x => x.login == login && x.password == password).SingleOrDefault();
            if (userConnected != null) return true;
            return false;
        }
	}
}