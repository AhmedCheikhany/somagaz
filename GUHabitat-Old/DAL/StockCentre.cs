//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GUHabitat.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class StockCentre
    {
        public int id { get; set; }
        public Nullable<int> reference { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> CodeCentre { get; set; }
        public Nullable<decimal> StockTheorie { get; set; }
        public Nullable<decimal> Stock { get; set; }
        public Nullable<decimal> StockFin { get; set; }
        public Nullable<decimal> Depotage { get; set; }
        public Nullable<decimal> GainPerte { get; set; }
        public string Commentaire { get; set; }
        public Nullable<int> etat { get; set; }
    
        public virtual Centre Centre { get; set; }
    }
}
