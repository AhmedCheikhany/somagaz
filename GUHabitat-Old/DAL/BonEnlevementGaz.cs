//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GUHabitat.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class BonEnlevementGaz
    {
        public int id { get; set; }
        public string Reference { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> idMouvementBonEnlvement { get; set; }
        public Nullable<int> idTypeBonEnlevement { get; set; }
        public string ImmatriculeCamion { get; set; }
        public string ChauffeurCamion { get; set; }
        public string ReferenceDepotage { get; set; }
        public string ReferenceBonPesee { get; set; }
        public Nullable<int> CodeClient { get; set; }
        public Nullable<decimal> Tonnage { get; set; }
        public string Commentaires { get; set; }
        public Nullable<int> etat { get; set; }
    
        public virtual MouvementBonEnlevement MouvementBonEnlevement { get; set; }
        public virtual TypeBonEnlevement TypeBonEnlevement { get; set; }
        public virtual Client Client { get; set; }
    }
}
