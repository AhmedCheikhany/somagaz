//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GUHabitat.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class VenteCentre
    {
        public int id { get; set; }
        public string Reference { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> CodeCentre { get; set; }
        public Nullable<int> B12Revendeur { get; set; }
        public Nullable<int> B9Revendeur { get; set; }
        public Nullable<int> B6RRevendeur { get; set; }
        public Nullable<int> B6VRevendeur { get; set; }
        public Nullable<int> B3Revendeur { get; set; }
        public Nullable<int> B35Revendeur { get; set; }
        public Nullable<decimal> B12PrixRevendeur { get; set; }
        public Nullable<decimal> B9PrixRevendeur { get; set; }
        public Nullable<decimal> B6RPrixRevendeur { get; set; }
        public Nullable<decimal> B6VPrixRevendeur { get; set; }
        public Nullable<decimal> B3PrixRevendeur { get; set; }
        public Nullable<decimal> B35PrixRevendeur { get; set; }
        public Nullable<int> B12Consommateur { get; set; }
        public Nullable<int> B9Consommateur { get; set; }
        public Nullable<int> B6RConsommateur { get; set; }
        public Nullable<int> B6VConsommateur { get; set; }
        public Nullable<int> B3Consommateur { get; set; }
        public Nullable<int> B35Consommateur { get; set; }
        public Nullable<decimal> B12PrixConsommateur { get; set; }
        public Nullable<decimal> B9PrixConsommateur { get; set; }
        public Nullable<decimal> B6RPrixConsommateur { get; set; }
        public Nullable<decimal> B6VPrixConsommateur { get; set; }
        public Nullable<decimal> B3PrixConsommateur { get; set; }
        public Nullable<decimal> B35PrixConsommateur { get; set; }
        public Nullable<decimal> TotalPrixB12Consommateur { get; set; }
        public Nullable<decimal> TotalPrixB9Consommateur { get; set; }
        public Nullable<decimal> TotalPrixB6RConsommateur { get; set; }
        public Nullable<decimal> TotalPrixB6VConsommateur { get; set; }
        public Nullable<decimal> TotalPrixB3Consommateur { get; set; }
        public Nullable<decimal> TotalPrixB35Consommateur { get; set; }
        public Nullable<decimal> TotalPrixB12Revendeur { get; set; }
        public Nullable<decimal> TotalPrixB9Revendeur { get; set; }
        public Nullable<decimal> TotalPrixB6RRevendeur { get; set; }
        public Nullable<decimal> TotalPrixB6VRevendeur { get; set; }
        public Nullable<decimal> TotalPrixB3Revendeur { get; set; }
        public Nullable<decimal> TotalPrixB35Revendeur { get; set; }
        public Nullable<decimal> TotalMontantConsommateur { get; set; }
        public Nullable<decimal> TotalMontantRevendeur { get; set; }
        public string Commentaire { get; set; }
        public Nullable<int> etat { get; set; }
    
        public virtual Centre Centre { get; set; }
    }
}
