﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionFicheSituation
    {

        private static BusinessGestionFicheSituation instance;
        private BusinessGestionFicheSituation() { }

        public static BusinessGestionFicheSituation Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionFicheSituation();
                }
                return instance;
            }
        }



        public List<FicheSituationDto> ChercherListeFicheSituation(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                //Utilisateur user = _context.Utilisateurs.Find(login);
                //if (string.IsNullOrEmpty(filtreMotCle)) 
                //_context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    var baseQuery = from o in _context.FicheSituations.AsEnumerable()
                                    where o.Date == Convert.ToDateTime(filtreMotCle)

                                    select o;

                    var query = from o in baseQuery
                                select new FicheSituationDto
                                {
                                    id = o.id,
                                    Reference = o.reference.Value,
                                    Date = o.Date.Value,
                                    StockNKTT = (decimal)o.StockNKTT,
                                    Depotage = (decimal)o.Depotage,
                                    StockInterieur = (decimal)o.StockInterieur,
                                    TotalStock = (decimal)o.TotalStock,
                                    VenteConditionneeNKTT = (decimal)o.VenteConditionneeNKTT,
                                    VenteCentresInterieur = (decimal)o.VenteCentresInterieur,
                                    VenteVrac = (decimal)o.VenteVrac,
                                    TotalVente = (decimal)o.TotalVente,
                                    TransfertCentres = (decimal)o.TransfertCentres,
                                    TransfertPartenaires = (decimal)o.TransfertPartenaires,
                                    GainPerte = (decimal)o.GainPerte,
                                    RecetteNKTT = (decimal)o.RecetteNKTT,
                                    RecetteInterieur = (decimal)o.RecetteInterieur,
                                    TotalRecette = (decimal)o.TotalRecette,
                                    TotalTransfert = (decimal)o.TotalTransfert,
                                    Versement = (decimal)o.Versement,
                                    VersementInterieur = (decimal)o.VersementInterieur,
                                    TotalVersement = (decimal)o.TotalVersement,
                                    AutreReglement = (decimal)o.AutreReglement,
                                    PretsAceJour = (decimal)o.PretsAceJour,

                                    Commentaire = o.commentaire,
                                    etat = o.etat.Value

                                };

                    return query.OrderByDescending(x => x.id).ToList();
                }
                else
                {

                    var baseQuery = from o in _context.FicheSituations.AsEnumerable()
                                    

                                    select o;

                    var query = from o in baseQuery
                                select new FicheSituationDto
                                {
                                    id = o.id,
                                    Reference = o.reference.Value,
                                    Date = o.Date.Value,
                                    StockNKTT = (decimal)o.StockNKTT,
                                    Depotage = (decimal)o.Depotage,
                                    StockInterieur = (decimal)o.StockInterieur,
                                    TotalStock = (decimal)o.TotalStock,
                                    VenteConditionneeNKTT = (decimal)o.VenteConditionneeNKTT,
                                    VenteCentresInterieur = (decimal)o.VenteCentresInterieur,
                                    VenteVrac = (decimal)o.VenteVrac,
                                    TotalVente = (decimal)o.TotalVente,
                                    TransfertCentres = (decimal)o.TransfertCentres,
                                    TransfertPartenaires = (decimal)o.TransfertPartenaires,
                                    GainPerte = (decimal)o.GainPerte,
                                    RecetteNKTT = (decimal)o.RecetteNKTT,
                                    RecetteInterieur = (decimal)o.RecetteInterieur,
                                    TotalRecette = (decimal)o.TotalRecette,
                                    TotalTransfert = (decimal)o.TotalTransfert,
                                    Versement = (decimal)o.Versement,
                                    VersementInterieur = (decimal)o.VersementInterieur,
                                    TotalVersement = (decimal)o.TotalVersement,
                                    AutreReglement = (decimal)o.AutreReglement,
                                    PretsAceJour = (decimal)o.PretsAceJour,

                                    Commentaire = o.commentaire,
                                    etat = o.etat.Value

                                };

                    return query.OrderByDescending(x => x.id).ToList();
                }


                


            }

        }





        public List<FicheSituationDto> ChercherListeFicheSituationCloturer(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

               
                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    var baseQuery = from o in _context.FicheSituations.AsEnumerable()
                                    where (o.etat == 2 && o.Date == Convert.ToDateTime(filtreMotCle))

                                    select o;

                    var query = from o in baseQuery
                                select new FicheSituationDto
                                {
                                    id = o.id,
                                    Reference = o.reference.Value,
                                    Date = o.Date.Value,
                                    StockNKTT = (decimal)o.StockNKTT,
                                    Depotage = (decimal)o.Depotage,
                                    StockInterieur = (decimal)o.StockInterieur,
                                    TotalStock = (decimal)o.TotalStock,
                                    VenteConditionneeNKTT = (decimal)o.VenteConditionneeNKTT,
                                    VenteCentresInterieur = (decimal)o.VenteCentresInterieur,
                                    VenteVrac = (decimal)o.VenteVrac,
                                    TotalVente = (decimal)o.TotalVente,
                                    TransfertCentres = (decimal)o.TransfertCentres,
                                    TransfertPartenaires = (decimal)o.TransfertPartenaires,
                                    GainPerte = (decimal)o.GainPerte,
                                    RecetteNKTT = (decimal)o.RecetteNKTT,
                                    RecetteInterieur = (decimal)o.RecetteInterieur,
                                    TotalRecette = (decimal)o.TotalRecette,
                                    TotalTransfert = (decimal)o.TotalTransfert,
                                    Versement = (decimal)o.Versement,
                                    VersementInterieur = (decimal)o.VersementInterieur,
                                    TotalVersement = (decimal)o.TotalVersement,
                                    AutreReglement = (decimal)o.AutreReglement,
                                    PretsAceJour = (decimal)o.PretsAceJour,

                                    Commentaire = o.commentaire,
                                    etat = o.etat.Value

                                };

                    return query.OrderByDescending(x => x.id).ToList();
                }
                else
                {

                    var baseQuery = from o in _context.FicheSituations.AsEnumerable()
                                    where o.etat == 2

                                    select o;

                    var query = from o in baseQuery
                                select new FicheSituationDto
                                {
                                    id = o.id,
                                    Reference = o.reference.Value,
                                    Date = o.Date.Value,
                                    StockNKTT = (decimal)o.StockNKTT,
                                    Depotage = (decimal)o.Depotage,
                                    StockInterieur = (decimal)o.StockInterieur,
                                    TotalStock = (decimal)o.TotalStock,
                                    VenteConditionneeNKTT = (decimal)o.VenteConditionneeNKTT,
                                    VenteCentresInterieur = (decimal)o.VenteCentresInterieur,
                                    VenteVrac = (decimal)o.VenteVrac,
                                    TotalVente = (decimal)o.TotalVente,
                                    TransfertCentres = (decimal)o.TransfertCentres,
                                    TransfertPartenaires = (decimal)o.TransfertPartenaires,
                                    GainPerte = (decimal)o.GainPerte,
                                    RecetteNKTT = (decimal)o.RecetteNKTT,
                                    RecetteInterieur = (decimal)o.RecetteInterieur,
                                    TotalRecette = (decimal)o.TotalRecette,
                                    TotalTransfert = (decimal)o.TotalTransfert,
                                    Versement = (decimal)o.Versement,
                                    VersementInterieur = (decimal)o.VersementInterieur,
                                    TotalVersement = (decimal)o.TotalVersement,
                                    AutreReglement = (decimal)o.AutreReglement,
                                    PretsAceJour = (decimal)o.PretsAceJour,

                                    Commentaire = o.commentaire,
                                    etat = o.etat.Value

                                };

                    return query.OrderByDescending(x => x.id).ToList();
                }





            }

        }



        public void Ajouter(FicheSituationDto dto)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                int referenceExist = GetFicheSituationParReference(dto.Reference);

                var entity = new FicheSituation
                {

                    reference = dto.Reference,
                    Date = dto.Date,
                    StockNKTT = (decimal)dto.StockNKTT,
                    Depotage = (decimal)dto.Depotage,
                    StockInterieur = (decimal)dto.StockInterieur,
                    TotalStock = (decimal)dto.TotalStock,
                    VenteConditionneeNKTT = (decimal)dto.VenteConditionneeNKTT,
                    VenteCentresInterieur = (decimal)dto.VenteCentresInterieur,
                    VenteVrac = (decimal)dto.VenteVrac,
                    TotalVente = (decimal)dto.TotalVente,
                    TransfertCentres = (decimal)dto.TransfertCentres,
                    TransfertPartenaires = (decimal)dto.TransfertPartenaires,
                    TotalTransfert = (decimal)dto.TotalTransfert,
                    GainPerte = (decimal)dto.GainPerte,
                    RecetteNKTT = (decimal)dto.RecetteNKTT,
                    RecetteInterieur = (decimal)dto.RecetteInterieur,
                    TotalRecette = (decimal)dto.TotalRecette,
                    Versement = (decimal)dto.Versement,
                    VersementInterieur = (decimal)dto.VersementInterieur,
                    TotalVersement = (decimal)dto.TotalVersement,
                    AutreReglement = (decimal)dto.AutreReglement,
                    PretsAceJour = (decimal)(dto.TotalRecette - dto.TotalVersement) - (decimal)dto.AutreReglement,

                    commentaire = dto.Commentaire,
                    etat = 1,
                   



                };
                _context.FicheSituations.Add(entity);
                try
                {
                    
                    if(referenceExist == 0)
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }




        public void Modifier(FicheSituationDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                

                var entity = _context.FicheSituations.Find(dto.id);
                if (entity != null)

                {
                    entity.reference = dto.Reference;
                    entity.Date = dto.Date;
                    entity.StockNKTT = (decimal)dto.StockNKTT;
                    entity.Depotage = (decimal)dto.Depotage;
                    entity.StockInterieur = (decimal)dto.StockInterieur;
                    entity.TotalStock = (decimal)dto.TotalStock;
                    entity.VenteConditionneeNKTT = (decimal)dto.VenteConditionneeNKTT;
                    entity.VenteCentresInterieur = (decimal)dto.VenteCentresInterieur;
                    entity.VenteVrac = (decimal)dto.VenteVrac;
                    entity.TotalVente = (decimal)dto.TotalVente;
                    entity.TransfertCentres = (decimal)dto.TransfertCentres;
                    entity.TransfertPartenaires = (decimal)dto.TransfertPartenaires;
                    entity.TotalTransfert = (decimal)dto.TotalTransfert;
                    entity.GainPerte = (decimal)dto.GainPerte;
                    entity.RecetteNKTT = (decimal)dto.RecetteNKTT;
                    entity.RecetteInterieur = (decimal)dto.RecetteInterieur;
                    entity.TotalRecette = (decimal)dto.TotalRecette;
                    entity.Versement = (decimal)dto.Versement;
                    entity.VersementInterieur = (decimal)dto.VersementInterieur;
                    entity.TotalVersement = (decimal)dto.TotalVersement;
                    entity.AutreReglement = (decimal)dto.AutreReglement;
                    entity.PretsAceJour = (decimal)(dto.TotalRecette - dto.TotalVersement) - (decimal)dto.AutreReglement;

                    entity.commentaire = dto.Commentaire;
                    entity.etat = entity.etat;
                   


                   



                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }



        public FicheSituationDto GetFicheSituationParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.FicheSituations.Find(id);

                var dto = new FicheSituationDto
                {
                  
                    id = o.id,
                    Reference = o.reference.Value,
                    Date = o.Date.Value,
                    StockNKTT = (decimal)o.StockNKTT,
                    Depotage = (decimal)o.Depotage,
                    StockInterieur = (decimal)o.StockInterieur,
                    TotalStock = (decimal)o.TotalStock,
                    VenteConditionneeNKTT = (decimal)o.VenteConditionneeNKTT,
                    VenteCentresInterieur = (decimal)o.VenteCentresInterieur,
                    VenteVrac = (decimal)o.VenteVrac,
                    TotalVente = (decimal)o.TotalVente,
                    TransfertCentres = (decimal)o.TransfertCentres,
                    TransfertPartenaires = (decimal)o.TransfertPartenaires,
                    GainPerte = (decimal)o.GainPerte,
                    RecetteNKTT = (decimal)o.RecetteNKTT,
                    RecetteInterieur = (decimal)o.RecetteInterieur,
                    TotalRecette = (decimal)o.TotalRecette,
                    TotalTransfert = (decimal)o.TotalTransfert,
                    Versement = (decimal)o.Versement,
                    VersementInterieur = (decimal)o.VersementInterieur,
                    TotalVersement = (decimal)o.TotalVersement,
                    AutreReglement = (decimal)o.AutreReglement,
                    PretsAceJour = (decimal)o.PretsAceJour,


                    Commentaire = o.commentaire,
                    etat = o.etat.Value

                };


                return dto;

            }

        }


        public int GetFicheSituationParDate(DateTime date)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var nbreFiche = _context.FicheSituations.Where(x=>x.Date == date.Date).Count();

                return nbreFiche;
            }     

        }


        public int GetFicheSituationParReference(int reference)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var references = _context.FicheSituations.Where(x => x.reference == reference).Count();

                return references;
            }

        }



        public FicheSituationDto GetFicheSituationImprimerParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.FicheSituations.Find(id);

                var dto = new FicheSituationDto
                {

                    id = o.id,
                    Reference = o.reference.Value,
                    Date = o.Date.Value,
                    StockNKTT = (decimal)o.StockNKTT,
                    StockInterieur = (decimal)o.StockInterieur,
                    TotalStock = (decimal)o.TotalStock,
                    VenteConditionneeNKTT = (decimal)o.VenteConditionneeNKTT,
                    VenteCentresInterieur = (decimal)o.VenteCentresInterieur,
                    VenteVrac = (decimal)o.VenteVrac,
                    TotalVente = (decimal)o.TotalVente,
                    TransfertCentres = (decimal)o.TransfertCentres,
                    TransfertPartenaires = (decimal)o.TransfertPartenaires,
                    GainPerte = (decimal)o.GainPerte,
                    RecetteNKTT = (decimal)o.RecetteNKTT,
                    RecetteInterieur = (decimal)o.RecetteInterieur,
                    TotalRecette = (decimal)o.TotalRecette,
                    TotalTransfert = (decimal)o.TotalTransfert,
                    Versement = (decimal)o.Versement,
                    VersementInterieur = (decimal)o.VersementInterieur,
                    TotalVersement = (decimal)o.TotalVersement,
                    AutreReglement = (decimal)o.AutreReglement,
                    PretsAceJour = (decimal)_context.FicheSituations.Where(x => x.Date <= o.Date).Select(x => x.PretsAceJour).DefaultIfEmpty(0).Sum(),
                    VenteAceJour = (decimal)_context.FicheSituations.Where(x => x.Date.Value.Month == o.Date.Value.Month).Select(x => x.TotalVente).DefaultIfEmpty(0).Sum(),
                    MoyenneJournaliere = (decimal)_context.FicheSituations.Where(x => x.Date <= o.Date).Select(x => x.TotalVente).DefaultIfEmpty(0).Average(),


                    Commentaire = o.commentaire,
                    etat = o.etat.Value

                };


                return dto;

            }

        }

        public void Valider(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                    var entity = _context.FicheSituations.Find(id);
                    entity.etat = 2;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
                
            
            }

        }



        public void EnInstance(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.FicheSituations.Find(id);
                entity.etat = 1;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }



        public void Cloturer(int[] details)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                if (details == null) return;

                foreach (int id in details)
                {


                    FicheSituation fiche = _context.FicheSituations.Find(id);
                    fiche.etat = 3;
                    _context.Entry(fiche).State = EntityState.Modified;
                    _context.SaveChanges();

                }
            }
        }


        public int TotalCloturer()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.FicheSituations.Where(x => x.etat == 2).Count();

                return Total;
            }


        }



        public decimal CalculerGainPerte(int reference , int id)
        {
            var refe = reference - 1;

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                //var VenteCeJour = _context.FicheSituations.Where(x => x.Date.Value.Month == (date.Month)).Select(x => x.TotalStock).Sum();
                var StockNkttHier = _context.FicheSituations.Where(x =>x.reference == refe).Select(x => x.StockNKTT).DefaultIfEmpty(0).SingleOrDefault();
                var StockNkttNow = _context.FicheSituations.Where(x => x.id == id).Select(x => x.StockNKTT).DefaultIfEmpty(0).SingleOrDefault();
                var VenteBouteillenow = _context.FicheSituations.Where(x => x.id == id).Select(x => x.VenteConditionneeNKTT).DefaultIfEmpty(0).SingleOrDefault();
                var VenteVracenow = _context.FicheSituations.Where(x => x.id == id).Select(x => x.VenteVrac).DefaultIfEmpty(0).SingleOrDefault();
                var TotalTransfert = _context.FicheSituations.Where(x => x.id == id).Select(x => x.TotalTransfert).DefaultIfEmpty(0).SingleOrDefault();
                var Depotage = _context.FicheSituations.Where(x => x.id == id).Select(x => x.Depotage).DefaultIfEmpty(0).SingleOrDefault();


                var StockNKTTNowDepotage = StockNkttNow - Depotage;
                var VenteNkttNow = VenteBouteillenow + VenteVracenow + TotalTransfert;
                var difference = StockNkttHier - VenteNkttNow;
              
                var GainPerte = StockNKTTNowDepotage - difference;
                if (StockNkttHier == 0)
                    GainPerte = 0;

                return (decimal)GainPerte;
            }


        }


        public decimal AjouterPretCeJour(int id, decimal TotalRecette, decimal Versement)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.FicheSituations.Find(id);
                 var PretCeJour = entity.PretsAceJour = (TotalRecette - Versement);

                return (decimal)PretCeJour;
            }

        }



    }
}