﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionAredp
    {

        private static BusinessGestionAredp instance;
        private BusinessGestionAredp() { }

        public static BusinessGestionAredp Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionAredp();
                }
                return instance;
            }
        }



        public List<AredpDto> ChercherListeAredp(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<AREDP> baseQuery = Enumerable.Empty<AREDP>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

              
               
                    if (string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.AREDPs.AsEnumerable()

                                    select o;
                    }




                    if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                    {
                        baseQuery = from o in _context.AREDPs.AsEnumerable()
                                    where o.Reference.Contains(filtreMotCle)
                                    select o;

                    }
                    else if (!string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.AREDPs.AsEnumerable()
                                    where o.Client.Code == Convert.ToInt32(filtreMotCle)
                                    select o;

                    }

                    var query = from o in baseQuery
                                select new AredpDto
                                {
                                    id = o.id,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,

                                    B12Afact = o.B12Afact.Value,
                                    B9Afact = o.B9Afact.Value,
                                    B6RAfact = o.B6RAfact.Value,
                                    B6VAfact = o.B6VAfact.Value,
                                    B3Afact = o.B3Afact.Value,
                                    B35Afact = o.B35Afact.Value,

                                    B12Nonfact = o.B12Nonfact.Value,
                                    B9Nonfact = o.B9Nonfact.Value,
                                    B6RNonfact = o.B6RNonfact.Value,
                                    B6VNonfact = o.B6VNonfact.Value,
                                    B3Nonfact = o.B3Nonfact.Value,
                                    B35Nonfact = o.B6RNonfact.Value,

                                    Commentaire = o.Commentaire,
                                    etat = o.etat.Value,
                                    

                                };


                    return query.OrderByDescending(x => x.id).ToList();


         
            }

        }


        public List<AredpDto> ChercherListeAredp(string filtreMotCle, DateTime? dateDebut, DateTime? dateFin, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<AREDP> baseQuery = Enumerable.Empty<AREDP>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);




                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.AREDPs.AsEnumerable()
                                where o.Client.Code == Convert.ToInt32(filtreMotCle) && o.Date >= dateDebut && o.Date <= dateFin

                                select o;

                }
              

                var query = from o in baseQuery
                            select new AredpDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,

                                B12Afact = o.B12Afact.Value + o.B12Nonfact.Value,
                                B9Afact = o.B9Afact.Value + o.B9Nonfact.Value,
                                B6RAfact = o.B6RAfact.Value + o.B6RNonfact.Value,
                                B6VAfact = o.B6VAfact.Value + o.B6VNonfact.Value,
                                B3Afact = o.B3Afact.Value + o.B3Nonfact.Value,
                                B35Afact = o.B35Afact.Value + o.B6RNonfact.Value,

                                Commentaire = o.Commentaire,
                                etat = o.etat.Value,


                            };


                return query.OrderByDescending(x => x.id).ToList();



            }

        }


        public List<AredpDto> ChercherListeAredpCloturer(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<AREDP> baseQuery = Enumerable.Empty<AREDP>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);



                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.AREDPs.AsEnumerable()
                               where  o.etat == 2
                                select o;
                }




                if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                {
                    baseQuery = from o in _context.AREDPs.AsEnumerable()
                                where o.etat == 2 && (o.Date == Convert.ToDateTime(filtreMotCle))
                                select o;

                }
                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.AREDPs.AsEnumerable()
                                where o.etat == 2 && ( o.Client.Code == Convert.ToInt32(filtreMotCle))
                                select o;

                }

                var query = from o in baseQuery
                            select new AredpDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,

                                B12Afact = o.B12Afact.Value,
                                B9Afact = o.B9Afact.Value,
                                B6RAfact = o.B6RAfact.Value,
                                B6VAfact = o.B6VAfact.Value,
                                B3Afact = o.B3Afact.Value,
                                B35Afact = o.B35Afact.Value,

                                B12Nonfact = o.B12Nonfact.Value,
                                B9Nonfact = o.B9Nonfact.Value,
                                B6RNonfact = o.B6RNonfact.Value,
                                B6VNonfact = o.B6VNonfact.Value,
                                B3Nonfact = o.B3Nonfact.Value,
                                B35Nonfact = o.B6RNonfact.Value,

                                Commentaire = o.Commentaire,
                                etat = o.etat.Value,


                            };


                return query.OrderByDescending(x => x.id).ToList();



            }

        }


        public AredpDto GetAredpParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.AREDPs.Find(id);

                var dto = new AredpDto
                {
                    id = o.id,
                    Reference = o.Reference,
                    Date = o.Date.Value,
                    CodeClient = o.CodeClient.Value,
                    NomClient = o.Client.nomComplet,
                  
                    B12Afact = o.B12Afact.Value,
                    B9Afact = o.B9Afact.Value,
                    B6RAfact = o.B6RAfact.Value,
                    B6VAfact = o.B6VAfact.Value,
                    B3Afact = o.B3Afact.Value,
                    B35Afact = o.B35Afact.Value,

                    B12Nonfact = o.B12Nonfact.Value,
                    B9Nonfact = o.B9Nonfact.Value,
                    B6RNonfact = o.B6RNonfact.Value,
                    B6VNonfact = o.B6VNonfact.Value,
                    B3Nonfact = o.B3Nonfact.Value,
                    B35Nonfact = o.B35Nonfact.Value,

                    Commentaire = o.Commentaire,
                    etat = o.etat.Value,
                };


                return dto;

            }

        }









        public void Ajouter(AredpDto dto,bool status)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }



                var entity = new AREDP
                {
                    Date = dto.Date,
                    Reference = dto.Reference,
                    Client = client,
                  

                    B12Afact = dto.B12Afact,
                    B9Afact = dto.B9Afact,
                    B6RAfact = dto.B6RAfact,
                    B6VAfact = dto.B6VAfact,
                    B3Afact = dto.B3Afact,
                    B35Afact = dto.B35Afact,

                    B12Nonfact = dto.B12Nonfact,
                    B9Nonfact = dto.B9Nonfact,
                    B6RNonfact = dto.B6RNonfact,
                    B6VNonfact = dto.B6VNonfact,
                    B3Nonfact = dto.B3Nonfact,
                    B35Nonfact = dto.B35Nonfact,


                    Commentaire = dto.Commentaire,
                    etat = 1,





                };
                _context.AREDPs.Add(entity);
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


               
                int idAredp = _context.AREDPs.Max(o => o.id);

                decimal poidsNormal = 0;
                decimal poidsRestant = 0;
                decimal pourcentageB12 = (Convert.ToDecimal("12,5") * 40) / 100;
                decimal pourcentageB9 = (9 * 40) / 100;
                decimal pourcentageB6R = (6 * 40) / 100;
                decimal pourcentageB6V = (6 * 40) / 100;
                decimal pourcentageB3 = (Convert.ToDecimal("2,75") * 40) / 100;
                decimal pourcentageB35 = (35 * 40) / 100;

              

                foreach (var item in dto.ListeAredpDetails)
                {

                    if (item.idProduit == 1)
                    {
                        poidsNormal =  Convert.ToDecimal("12,5");
                        poidsRestant = (item.poidsLu > 0 && item.Tare > 0 && item.Tare < item.poidsLu) ? (item.poidsLu - item.Tare) : 0;


                    }
                    if (item.idProduit == 2)
                    {
                        poidsNormal =  9;
                        poidsRestant = (item.poidsLu > 0 && item.Tare > 0 && item.Tare < item.poidsLu) ? (item.poidsLu - item.Tare) : 0;

                    }
                    if (item.idProduit == 3)
                    {
                        poidsNormal =  6;
                        poidsRestant = (item.poidsLu > 0 && item.Tare > 0 && item.Tare < item.poidsLu) ? (item.poidsLu - item.Tare) : 0;

                    }
                    if (item.idProduit == 4)
                    {
                        poidsNormal = 6;
                        poidsRestant = (item.poidsLu > 0 && item.Tare > 0 && item.Tare < item.poidsLu) ? (item.poidsLu - item.Tare) : 0;


                    }
                    if (item.idProduit == 5)
                    {
                        poidsNormal = Convert.ToDecimal("2,75");
                        poidsRestant = (item.poidsLu > 0 && item.Tare > 0 && item.Tare < item.poidsLu) ? (item.poidsLu - item.Tare) : 0;
                       
                    }
                    if (item.idProduit == 6)
                    {
                        poidsNormal = 35;
                        poidsRestant = (item.poidsLu > 0 && item.Tare > 0 && item.Tare < item.poidsLu) ? (item.poidsLu - item.Tare) : 0;

                    }

                    AredpDetail AredpDetails = new AredpDetail()
                    {
                        idAredp = idAredp,
                        idProduit = item.idProduit,
                        poidsLu = (item.poidsLu > 0) ? item.poidsLu : 0,
                        Tare = (item.Tare > 0) ? item.Tare : 0,
                        poidsRestant = (item.Tare < item.poidsLu && item.poidsLu > 0 && item.Tare > 0) ? (item.poidsLu - item.Tare) : 0,
                        poidsManquant = (poidsRestant < poidsNormal && poidsRestant > 0) ? (poidsNormal - poidsRestant) : 0
                        
                    };

                  
                        _context.AredpDetails.Add(AredpDetails);
                  
                }
    
                        try
                        {
                           
                            _context.SaveChanges();

                           
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }

                           
                            throw;
                      
                        }
                       
                var ListeAredpDetails = _context.AredpDetails.Where(x => x.idAredp == idAredp).ToList();
                foreach (var item in ListeAredpDetails)
                {

                  


                    // Nbre Nonfacturer par Type
                    
                     entity.B12Nonfact = (item.idProduit == 1) ? ListeAredpDetails.Where(x => x.idProduit == 1  && x.poidsLu > 0 &&  x.poidsRestant >= pourcentageB12).Count() : entity.B12Nonfact;
                     entity.B9Nonfact =  (item.idProduit == 2) ? ListeAredpDetails.Where(x => x.idProduit == 2  && x.poidsLu > 0 && x.poidsRestant >= pourcentageB9).Count()  : entity.B9Nonfact;
                     entity.B6RNonfact = (item.idProduit == 3) ?  ListeAredpDetails.Where(x => x.idProduit == 3 && x.poidsLu > 0 &&  x.poidsRestant >= pourcentageB6R).Count() : entity.B6RNonfact;
                     entity.B6VNonfact = (item.idProduit == 4) ? ListeAredpDetails.Where(x => x.idProduit == 4  && x.poidsLu > 0 &&  x.poidsRestant >= pourcentageB6V).Count() : entity.B6VNonfact;
                     entity.B3Nonfact =  (item.idProduit == 5) ?  ListeAredpDetails.Where(x => x.idProduit == 5 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB3).Count()  : entity.B3Nonfact;
                     entity.B35Nonfact = (item.idProduit == 6) ? ListeAredpDetails.Where(x => x.idProduit == 6  && x.poidsLu > 0 &&  x.poidsRestant >= pourcentageB35).Count() : entity.B35Nonfact;


                    // Nbre Afacturer par Type
                     entity.B12Afact = (item.idProduit == 1) ? ListeAredpDetails.Where(x => x.idProduit == 1 && x.poidsLu > 0 && x.poidsRestant < pourcentageB12).Count() : entity.B12Afact;
                     entity.B9Afact =  (item.idProduit == 2)  ? ListeAredpDetails.Where(x => x.idProduit == 2  && x.poidsLu > 0 &&  x.poidsRestant < pourcentageB9).Count() : entity.B9Afact;
                     entity.B6RAfact = (item.idProduit == 3) ? ListeAredpDetails.Where(x => x.idProduit == 3 && x.poidsLu > 0 && x.poidsRestant < pourcentageB6R).Count() : entity.B6RAfact;
                     entity.B6VAfact = (item.idProduit == 4) ? ListeAredpDetails.Where(x => x.idProduit == 4 && x.poidsLu > 0 &&  x.poidsRestant < pourcentageB6V).Count() : entity.B6VAfact;
                     entity.B3Afact =  (item.idProduit == 5)  ? ListeAredpDetails.Where(x => x.idProduit == 5  && x.poidsLu > 0 &&  x.poidsRestant < pourcentageB3).Count() : entity.B3Afact;
                     entity.B35Afact = (item.idProduit == 6) ? ListeAredpDetails.Where(x => x.idProduit == 6 && x.poidsLu > 0 && x.poidsRestant < pourcentageB35).Count() : entity.B35Afact;

  
                    entity.TonnageFacturer = (decimal)(entity.B12Afact * 12.5 + entity.B9Afact * 9 + entity.B6RAfact * 6 + entity.B6VAfact * 6 + entity.B3Afact * 2.75 + entity.B35Afact * 35) / 1000;

                    entity.TonnageNonFacturer = (decimal)(entity.B12Nonfact * 12.5 + entity.B9Nonfact * 9 + entity.B6RNonfact * 6 + entity.B6VNonfact * 6 + entity.B3Nonfact * 2.75 + entity.B35Nonfact * 35) / 1000;

                    

                }

                try
                {
                    _context.SaveChanges();

                    status = true;
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }

                    status = false;
                    throw;

                }
                dto.id = entity.id;

            }

        }


        public void Modifier(AredpDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }



                var entity = _context.AREDPs.Find(dto.id);
                if (entity != null)

                {

                    entity.Date = dto.Date;
                    entity.Reference = dto.Reference;
                    entity.Client = client;
                   
                    entity.B12Afact = entity.B12Afact;
                    entity.B9Afact = entity.B9Afact;
                    entity.B6RAfact = entity.B6RAfact;
                    entity.B6VAfact = entity.B6VAfact;
                    entity.B3Afact = entity.B3Afact;
                    entity.B35Afact = entity.B35Afact;

                    entity.B12Nonfact = entity.B12Nonfact;
                    entity.B9Nonfact =  entity.B9Nonfact;
                    entity.B6RNonfact = entity.B6RNonfact;
                    entity.B6VNonfact = entity.B6VNonfact;
                    entity.B3Nonfact =  entity.B3Nonfact;
                    entity.B35Nonfact = entity.B35Nonfact;

                    entity.Commentaire = dto.Commentaire;
                    entity.etat = entity.etat;



                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }



        public AredpDto GetAredpImprimerParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.AREDPs.Find(id);

                var dto = new AredpDto
                {
                    id = o.id,
                    Reference = o.Reference,
                    Date = o.Date.Value,
                    CodeClient = o.CodeClient.Value,
                    NomClient = o.Client.nomComplet,
                  
                    B12Afact = o.B12Afact.Value,
                    B9Afact = o.B9Afact.Value,
                    B6RAfact = o.B6RAfact.Value,
                    B6VAfact = o.B6VAfact.Value,
                    B3Afact = o.B3Afact.Value,
                    B35Afact = o.B35Afact.Value,

                    B12Nonfact = o.B12Nonfact.Value,
                    B9Nonfact = o.B9Nonfact.Value,
                    B6RNonfact = o.B6RNonfact.Value,
                    B6VNonfact = o.B6VNonfact.Value,
                    B3Nonfact = o.B3Nonfact.Value,
                    B35Nonfact = o.B35Nonfact.Value,

                    TotalB12 = (int)(o.B12Afact + o.B12Nonfact),
                    TotalB9 = (int)(o.B9Afact + o.B9Nonfact),
                    TotalB6R = (int)(o.B6RAfact + o.B6RNonfact),
                    TotalB6V = (int)(o.B6VAfact + o.B6VNonfact),
                    TotalB3 = (int)(o.B3Afact + o.B3Nonfact),
                    TotalB35 = (int)(o.B35Afact  + o.B35Nonfact),
                    Commentaire = o.Commentaire,
                    etat = o.etat.Value,
                };


                return dto;

            }

        }


        public List<AredpDetailsDto> GetAredpDetailsImprimerParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var list = _context.AredpDetails.Where(x => x.idAredp == id).ToList();
                List<AredpDetailsDto> listDto = new List<AredpDetailsDto>();
                foreach (AredpDetail item in list)
                {
                    AredpDetailsDto itemDto = new AredpDetailsDto
                    {
                        id = item.id,
                        idAredp = item.idAredp.Value,
                        idProduit = item.idProduit.Value,
                        NomProduit = item.Produit.libelle,
                        poidsLu = (decimal)item.poidsLu,
                        Tare = (decimal)item.Tare,
                        poidsManquant = (decimal)item.poidsManquant,
                        poidsRestant = (decimal)item.poidsRestant,






                    };
                    listDto.Add(itemDto);
                }

                return listDto;
            }
           
        }



        public ProduitDto GetProduitParId(int idProduit)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Produits.Find(idProduit);
                if (entity == null) return null;
                var dto = new ProduitDto
                {
                    id = entity.id,
                    libelle = entity.libelle,
                    tare = (decimal)entity.tare,

                };

                return dto;
            }



        }


        public void Cloturer(int[] details)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                if (details == null) return;

                foreach (int id in details)
                {


                    AREDP aredp = _context.AREDPs.Find(id);
                    aredp.etat = 3;
                    _context.Entry(aredp).State = EntityState.Modified;
                    _context.SaveChanges();

                }
            }
        }


        public void EnInstance(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                    var entity = _context.AREDPs.Find(id);
                    entity.etat = 1;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
               

            }

        }


        public void Valider(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.AREDPs.Find(id);
                entity.etat = 2;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }


        public void CloturerParDate(int idEntite, DateTime date)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var entity = _context.AREDPs.Where(x => x.Date == date.Date).Select(x => x.id).ToArray();

                foreach (int id in entity)
                {

                    AREDP aredp = _context.AREDPs.Find(id);
                    aredp.etat = 3;
                    //var sortie = _context.Sorties.Where(x => x.id == item.id).SingleOrDefault();
                    _context.Entry(aredp).State = EntityState.Modified;
                    _context.SaveChanges();

                }


            }
        }







        public int TotalCloturer()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.AREDPs.Where(x => x.etat == 2).Count();

                return Total;
            }


        }

    }
}