﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionEmballageCentre
    {

        private static BusinessGestionEmballageCentre instance;
        private BusinessGestionEmballageCentre() { }

        public static BusinessGestionEmballageCentre Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionEmballageCentre();
                }
                return instance;
            }
        }


        public List<EmballageCentreDto> ChercherListeEmballageCentre(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<EmballageCentre> baseQuery = Enumerable.Empty<EmballageCentre>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                //if (string.IsNullOrEmpty(filtreMotCle)) 
                //_context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.EmballageCentres.AsEnumerable()
                                select o;

                }




                if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                {
                    baseQuery = from o in _context.EmballageCentres.AsEnumerable()
                                where o.Reference.Contains(filtreMotCle)
                                select o;

                }


                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.EmballageCentres.AsEnumerable()
                                where o.CodeCentre == Convert.ToInt32(filtreMotCle)
                                select o;

                }

                var query = from o in baseQuery
                            select new EmballageCentreDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                CodeCentre = o.CodeCentre.Value,
                                LibelleCentre = o.Centre.libelle,

                                idTypeMouvement = o.idTypeMouvement.Value,
                                LibelleMouvement = o.TypeMouvementEmballage.libelle,

                                idDestinationSource = o.idDestinationSource.Value,
                                LibelleDestinationSource = o.SourceDestinationEmballage.libelle,

                                idEtatEmballage = o.idEtatEmballage.Value,
                                LibelleEtatEmballage = o.EtatEmballage.libelle,

                                B12 = o.B12.Value,
                                B9 = o.B9.Value,
                                B6R = o.B6R.Value,
                                B6V = o.B6V.Value,
                                B3 = o.B3.Value,
                                B35 = o.B35.Value,

                                Commentaire = o.Commentaire,

                                etat = o.etat.Value

                            };

                return query.OrderByDescending(x => x.id).ToList();


            }

        }


        public EmballageCentreDto GetEmballageCentreParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.EmballageCentres.Find(id);

                var dto = new EmballageCentreDto
                {
                    id = o.id,
                    Reference = o.Reference,
                    Date = o.Date.Value,
                    CodeCentre = o.CodeCentre.Value,
                    LibelleCentre = o.Centre.libelle,

                    idTypeMouvement = o.idTypeMouvement.Value,
                    LibelleMouvement = o.TypeMouvementEmballage.libelle,

                    idDestinationSource = o.idDestinationSource.Value,
                    LibelleDestinationSource = o.SourceDestinationEmballage.libelle,

                    idEtatEmballage = o.idEtatEmballage.Value,
                    LibelleEtatEmballage = o.EtatEmballage.libelle,

                    B12 = o.B12.Value,
                    B9 = o.B9.Value,
                    B6R = o.B6R.Value,
                    B6V = o.B6V.Value,
                    B3 = o.B3.Value,
                    B35 = o.B35.Value,

                    Commentaire = o.Commentaire,

                    etat = o.etat.Value



                };


                return dto;

            }

        }



        public void Ajouter(EmballageCentreDto dto)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var centre = _context.Centres.Find(dto.CodeCentre);

                if (centre == null)
                {

                    centre = new Centre
                    {
                        Code = dto.CodeCentre,
                        libelle = dto.LibelleCentre,

                    };
                }





                var entity = new EmballageCentre
                {

                    Reference = dto.Reference,
                    Date = dto.Date,
                    Centre = centre,

                    idTypeMouvement = dto.idTypeMouvement,
                    idDestinationSource = dto.idDestinationSource,
                    idEtatEmballage = dto.idEtatEmballage,

                    B12 = dto.B12,
                    B9 = dto.B9,
                    B6R = dto.B6R,
                    B6V = dto.B6V,
                    B3 = dto.B3,
                    B35 = dto.B35,


                    Commentaire = dto.Commentaire,

                    etat = 1



                };
                _context.EmballageCentres.Add(entity);
                try
                {
                    _context.SaveChanges();

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }


        public void Modifier(EmballageCentreDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var centre = _context.Centres.Find(dto.CodeCentre);

                if (centre == null)
                {

                    centre = new Centre
                    {
                        Code = dto.CodeCentre,
                        libelle = dto.LibelleCentre,

                    };
                }




                var entity = _context.EmballageCentres.Find(dto.id);
                if (entity != null)

                {


                    entity.Reference = dto.Reference;
                    entity.Date = dto.Date;
                    entity.Centre = centre;

                    entity.idTypeMouvement = dto.idTypeMouvement;
                    entity.idDestinationSource = dto.idDestinationSource;
                    entity.idEtatEmballage = dto.idEtatEmballage;



                    entity.B12 = dto.B12;
                    entity.B9 = dto.B9;
                    entity.B6R = dto.B6R;
                    entity.B6V = dto.B6V;
                    entity.B3 = dto.B3;
                    entity.B35 = dto.B35;



                    entity.Commentaire = dto.Commentaire;





                    if (dto.idEntite == 4)
                        entity.etat = entity.etat;
                    else if (dto.idEntite == 5)
                        entity.etat = 2;
                    else
                        entity.etat = entity.etat;




                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }




        public CentreDto GetCentreParCode(int code)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Centres.Find(code);

                var dto = new CentreDto
                {
                    Code = entity.Code,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public List<CentreDto> GetListeCentre()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.Centres.Where(x => x.Code != 0).ToList();
                var listeDto = new List<CentreDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new CentreDto
                    {
                        Code = item.Code,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }





        public TypeMouvementEmballageDto GetTypeMouvementParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.TypeMouvementEmballages.Find(id);

                var dto = new TypeMouvementEmballageDto
                {
                    id = entity.id,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public List<TypeMouvementEmballageDto> GetListeMouvements()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.TypeMouvementEmballages.ToList();
                var listeDto = new List<TypeMouvementEmballageDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new TypeMouvementEmballageDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }

        public TypeMouvementEmballageDto GetTypeMouvementInterieursParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.TypeMouvementEmballages.Where(x => x.id == 1).SingleOrDefault();

                var dto = new TypeMouvementEmballageDto
                {
                    id = entity.id,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public List<TypeMouvementEmballageDto> GetListeMouvementsInterieurs()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.TypeMouvementEmballages.Where(x=>x.id == 1).ToList();
                var listeDto = new List<TypeMouvementEmballageDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new TypeMouvementEmballageDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }



        public SourceDestinationEmballageDto GetSourceDestinationParId(int code)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.SourceDestinationEmballages.Find(code);

                var dto = new SourceDestinationEmballageDto
                {
                    id = entity.id,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public List<SourceDestinationEmballageDto> GetListeSourceDestination()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.SourceDestinationEmballages.ToList();
                var listeDto = new List<SourceDestinationEmballageDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new SourceDestinationEmballageDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }


        public List<SourceDestinationEmballageDto> GetListeSourceDestination(int codeCentre)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.SourceDestinationEmballages.Where(x=>x.id != codeCentre).ToList();
                var listeDto = new List<SourceDestinationEmballageDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new SourceDestinationEmballageDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }


        public EtatEmballageDto GetEtatEmballageParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.EtatEmballages.Find(id);

                var dto = new EtatEmballageDto
                {
                    id = entity.id,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public List<EtatEmballageDto> GetListeEtatEmballages()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.EtatEmballages.ToList();
                var listeDto = new List<EtatEmballageDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new EtatEmballageDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }



        public void EnInstance(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var entity = _context.EmballageCentres.Find(id);
                entity.etat = 1;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();



            }

        }


        public void Valider(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.EmballageCentres.Find(id);
                entity.etat = 2;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }






    }
}