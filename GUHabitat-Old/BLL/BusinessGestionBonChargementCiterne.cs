﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionBonChargementCiterne
    {


        private static BusinessGestionBonChargementCiterne instance;
        private BusinessGestionBonChargementCiterne() { }

        public static BusinessGestionBonChargementCiterne Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionBonChargementCiterne();
                }
                return instance;
            }
        }



        public List<BonChargementCiterneDto> ChercherListeBon(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                IEnumerable<BonChargementCiterne> baseQuery = Enumerable.Empty<BonChargementCiterne>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

                if (idEntite == 5 || idEntite == 9)
                {
                    if (string.IsNullOrEmpty(filtreMotCle))
                    { 
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where o.etat != 1 && o.etat != 2
                                    select o;

                    }



                    if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                    {
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where o.etat != 1 && o.etat != 2 && (o.Reference.Contains(filtreMotCle))
                                    select o;
                       

                    }

                    else if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() == 1)
                    {
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where o.etat != 1 && o.etat != 2 && (o.CodeClient == 0 && o.Destination.Code == Convert.ToInt32(filtreMotCle))
                                    select o;

                    }

                    else if (!string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where o.etat != 1 && o.etat != 2 && (o.Client.Code == Convert.ToInt32(filtreMotCle))
                                    
                                    select o;

                    }


                    if (filtreMotCle.Any(Char.IsLetter))
                    {
                        var expected = filtreMotCle.Substring(0, 4);

                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where o.ImmatriculeCamion.Contains(expected)
                                    select o;
                    }




                    var query = from o in baseQuery
                                select new BonChargementCiterneDto
                                {
                                    id = o.id,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    idTypeBon = o.idTypeBonChargement.Value,
                                    TypeBon = o.TypeBonChargement.libelle,
                                    ImmtriculeCamion = o.ImmatriculeCamion,
                                    NbrePersonnes = o.NbrePersonnes.Value,
                                    ResponsableCamion = o.ResponsableCamion,
                                    ChauffeurCamion = o.ChauffeurCamion,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,
                                    Tonnage = (float)o.Tonnage,
                                    CodeDestination = o.codeDestination.Value,
                                    libelleDestination = o.Destination.libelle,
                                    Demmarage = o.Demmarage,
                                    EtatPneus = o.EtatPneus,
                                    Freinage = o.Freinage,
                                    Extincteur = o.Extincteur,
                                    Observation = o.Observation,
                                    Numeropermis = o.Numeropermis,
                                    NpoliceRC = o.NpoliceRC,
                                    NpoliceRCGaz = o.NpoliceRCGaz,
                                    AutoriserEntree = o.AutoriserEntree,
                                    Facturer = o.Facturer,
                                    QuantiteFactQuantiteChargee = o.QuantiteFactQuantiteChargee,
                                    AutoriserSortie = o.AutoriserSortie,
                                    PoidDebut = (float)o.PoidDebut,
                                    NumeroPontBascule = o.NumeroPontBascule,
                                    ClapetVanne = o.ClapetVanne,
                                    CalageVehicule = o.CalageVehicule,
                                    MiseTerre = o.MiseTerre,
                                    ObservationExploitation = o.ObservationExploitation,
                                    AutoriserCharger = o.AutoriserCharger,
                                    heureChargement = o.heureChargement.Value,
                                    heureFinChargement = o.heureFinChargement.Value,
                                    PoidsApresChargement = (float)o.PoidsApresChargement,
                                    TonnageCharger = (float)o.TonnageCharger,
                                    NumeroFacture = o.NumeroFacture,
                                    TempsEstime = o.TemsEstime,
                                    etat = o.etat.Value


                                };


                    return query.OrderByDescending(x => x.id).ToList();

                }
                else
                {

                    if (string.IsNullOrEmpty(filtreMotCle))
                    { 
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()

                                    select o;
                    }
                    


                    if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                    {
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where o.Reference.Contains(filtreMotCle) || o.ImmatriculeCamion.Contains(filtreMotCle)
                                    select o; 

                    }


                    else if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() == 1)
                    {
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where (o.CodeClient == 0 && o.Destination.Code == Convert.ToInt32(filtreMotCle))
                                    select o;

                    }

                    else if (!string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where o.Client.Code == Convert.ToInt32(filtreMotCle)

                                    select o;

                    }


                    

                    if (filtreMotCle.Any(Char.IsLetter))
                    {
                        var expected = filtreMotCle.Substring(0, 4);

                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where  o.ImmatriculeCamion.Contains(expected)
                                    select o;
                    }


                        var query = from o in baseQuery
                                select new BonChargementCiterneDto
                                {
                                    id = o.id,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    idTypeBon = o.idTypeBonChargement.Value,
                                    TypeBon = o.TypeBonChargement.libelle,
                                    ImmtriculeCamion = o.ImmatriculeCamion,
                                    NbrePersonnes = o.NbrePersonnes.Value,
                                    ResponsableCamion = o.ResponsableCamion,
                                    ChauffeurCamion = o.ChauffeurCamion,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,
                                    Tonnage = (float)o.Tonnage,
                                    CodeDestination = o.codeDestination.Value,
                                    libelleDestination = o.Destination.libelle,
                                    Demmarage = o.Demmarage,
                                    EtatPneus = o.EtatPneus,
                                    Freinage = o.Freinage,
                                    Extincteur = o.Extincteur,
                                    Observation = o.Observation,
                                    Numeropermis = o.Numeropermis,
                                    NpoliceRC = o.NpoliceRC,
                                    NpoliceRCGaz = o.NpoliceRCGaz,
                                    AutoriserEntree = o.AutoriserEntree,
                                    Facturer = o.Facturer,
                                    QuantiteFactQuantiteChargee = o.QuantiteFactQuantiteChargee,
                                    AutoriserSortie = o.AutoriserSortie,
                                    PoidDebut = (float)o.PoidDebut,
                                    NumeroPontBascule = o.NumeroPontBascule,
                                    ClapetVanne = o.ClapetVanne,
                                    CalageVehicule = o.CalageVehicule,
                                    MiseTerre = o.MiseTerre,
                                    ObservationExploitation = o.ObservationExploitation,
                                    AutoriserCharger = o.AutoriserCharger,
                                    heureChargement = o.heureChargement.Value,
                                    heureFinChargement = o.heureFinChargement.Value,
                                    PoidsApresChargement = (float)o.PoidsApresChargement,
                                    TonnageCharger = (float)o.TonnageCharger,
                                    NumeroFacture = o.NumeroFacture,
                                    TempsEstime = o.TemsEstime,
                                    etat = o.etat.Value


                                };


                    return query.OrderByDescending(x => x.id).ToList();


                }

                   
            }

        }



        public List<BonChargementCiterneDto> BonEnInstance(string filtreMotCle, int idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                IEnumerable<BonChargementCiterne> baseQuery = Enumerable.Empty<BonChargementCiterne>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

               
                    if (string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where (o.etat == 1 || o.etat == 2)
                                    select o;

                    }



                    if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                    {
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where (o.etat == 1 || o.etat == 2) && (o.Reference.Contains(filtreMotCle))
                                    select o;

                    }

                    else if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() == 1)
                    {
                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where (o.etat == 1 || o.etat == 2) && (o.CodeClient == 0 && o.Destination.Code == Convert.ToInt32(filtreMotCle))
                                    select o;

                    }

                    else if (!string.IsNullOrEmpty(filtreMotCle))
                    {
                            baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                        where (o.etat == 1 || o.etat == 2) && (o.Client.Code == Convert.ToInt32(filtreMotCle))

                                        select o;

                    }

                    if (filtreMotCle.Any(Char.IsLetter))
                    {
                        var expected = filtreMotCle.Substring(0, 4);

                        baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                    where o.ImmatriculeCamion.Contains(expected)
                                    select o;
                    }

                var query = from o in baseQuery
                                select new BonChargementCiterneDto
                                {
                                    id = o.id,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    idTypeBon = o.idTypeBonChargement.Value,
                                    TypeBon = o.TypeBonChargement.libelle,
                                    ImmtriculeCamion = o.ImmatriculeCamion,
                                    NbrePersonnes = o.NbrePersonnes.Value,
                                    ResponsableCamion = o.ResponsableCamion,
                                    ChauffeurCamion = o.ChauffeurCamion,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,
                                    Tonnage = (float)o.Tonnage,
                                    CodeDestination = o.codeDestination.Value,
                                    libelleDestination = o.Destination.libelle,
                                    Demmarage = o.Demmarage,
                                    EtatPneus = o.EtatPneus,
                                    Freinage = o.Freinage,
                                    Extincteur = o.Extincteur,
                                    Observation = o.Observation,
                                    Numeropermis = o.Numeropermis,
                                    NpoliceRC = o.NpoliceRC,
                                    NpoliceRCGaz = o.NpoliceRCGaz,
                                    AutoriserEntree = o.AutoriserEntree,
                                    Facturer = o.Facturer,
                                    QuantiteFactQuantiteChargee = o.QuantiteFactQuantiteChargee,
                                    AutoriserSortie = o.AutoriserSortie,
                                    PoidDebut = (float)o.PoidDebut,
                                    NumeroPontBascule = o.NumeroPontBascule,
                                    ClapetVanne = o.ClapetVanne,
                                    CalageVehicule = o.CalageVehicule,
                                    MiseTerre = o.MiseTerre,
                                    ObservationExploitation = o.ObservationExploitation,
                                    AutoriserCharger = o.AutoriserCharger,
                                    heureChargement = o.heureChargement.Value,
                                    heureFinChargement = o.heureFinChargement.Value,
                                    PoidsApresChargement = (float)o.PoidsApresChargement,
                                    TonnageCharger = (float)o.TonnageCharger,
                                    NumeroFacture = o.NumeroFacture,
                                    TempsEstime = o.TemsEstime,
                                    etat = o.etat.Value


                                };


                    return query.OrderByDescending(x => x.id).ToList();

                

               

            }

        }



        public List<BonChargementCiterneDto> ChercherListeBonsAcloturer(string filtreMotCle, int idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<BonChargementCiterne> baseQuery = Enumerable.Empty<BonChargementCiterne>();

                //Utilisateur user = _context.Utilisateurs.Find(login);

                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                where o.etat == 3
                                select o;

                }


                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                where o.etat == 3 && (o.Date == Convert.ToDateTime(filtreMotCle))
                                select o;

                }
                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                where o.etat == 3 && (o.Client.Code == Convert.ToInt32(filtreMotCle))

                                select o;

                }

                if (filtreMotCle.Any(Char.IsLetter))
                {
                    var expected = filtreMotCle.Substring(0, 4);

                    baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                where o.ImmatriculeCamion.Contains(expected)
                                select o;
                }
                

                var query = from o in baseQuery

                            select new BonChargementCiterneDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                idTypeBon = o.idTypeBonChargement.Value,
                                TypeBon = o.TypeBonChargement.libelle,
                                ImmtriculeCamion = o.ImmatriculeCamion,
                                NbrePersonnes = o.NbrePersonnes.Value,
                                ResponsableCamion = o.ResponsableCamion,
                                ChauffeurCamion = o.ChauffeurCamion,
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,
                                Tonnage = (float)o.Tonnage,
                                CodeDestination = o.codeDestination.Value,
                                libelleDestination = o.Destination.libelle,
                                Demmarage = o.Demmarage,
                                EtatPneus = o.EtatPneus,
                                Freinage = o.Freinage,
                                Extincteur = o.Extincteur,
                                Observation = o.Observation,
                                Numeropermis = o.Numeropermis,
                                NpoliceRC = o.NpoliceRC,
                                NpoliceRCGaz = o.NpoliceRCGaz,
                                AutoriserEntree = o.AutoriserEntree,
                                Facturer = o.Facturer,
                                QuantiteFactQuantiteChargee = o.QuantiteFactQuantiteChargee,
                                AutoriserSortie = o.AutoriserSortie,
                                PoidDebut = (float)o.PoidDebut,
                                NumeroPontBascule = o.NumeroPontBascule,
                                ClapetVanne = o.ClapetVanne,
                                CalageVehicule = o.CalageVehicule,
                                MiseTerre = o.MiseTerre,
                                ObservationExploitation = o.ObservationExploitation,
                                AutoriserCharger = o.AutoriserCharger,
                                heureChargement = o.heureChargement.Value,
                                heureFinChargement = o.heureFinChargement.Value,
                                PoidsApresChargement = (float)o.PoidsApresChargement,
                                TonnageCharger = (float)o.TonnageCharger,
                                NumeroFacture = o.NumeroFacture,
                                TempsEstime = o.TemsEstime,
                                etat = o.etat.Value



                            };


                return query.OrderByDescending(x => x.id).ToList();
            }

        }

        public List<BonChargementCiterneDto> ChercherListeNbrePersonnes(string filtreMotCle, int idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<BonChargementCiterne> baseQuery = Enumerable.Empty<BonChargementCiterne>();

                //Utilisateur user = _context.Utilisateurs.Find(login);

                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                where (o.Date == DateTime.Now.Date)
                                select o;

                }

               
                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.BonChargementCiternes.AsEnumerable()
                                where (o.Date == Convert.ToDateTime(filtreMotCle))
                                select o;

                }

                var query = from o in baseQuery

                            select new BonChargementCiterneDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                idTypeBon = o.idTypeBonChargement.Value,
                                TypeBon = o.TypeBonChargement.libelle,
                                ImmtriculeCamion = o.ImmatriculeCamion,
                                NbrePersonnes = o.NbrePersonnes.Value, 
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,
                                libelleDestination = o.Destination.libelle,
                                etat = o.etat.Value



                            };


                return query.OrderByDescending(x => x.id).ToList();
            }

        }





        public void Ajouter(BonChargementCiterneDto dto)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                 var client = _context.Clients.Find(dto.CodeClient);
                 var destination = _context.Destinations.Find(dto.CodeDestination);
                

                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,
                       
                    };
                }


                if (destination == null)
                {

                    destination = new Destination
                    {
                        Code = dto.CodeDestination,
                        libelle = dto.libelleDestination,

                    };
                }






                var entity = new BonChargementCiterne
                {


                    Reference = dto.Reference,
                    Date = dto.Date,
                    idTypeBonChargement = dto.idTypeBon,
                    ImmatriculeCamion = dto.ImmtriculeCamion,
                    NbrePersonnes = dto.NbrePersonnes,
                    ResponsableCamion = dto.ResponsableCamion,
                    ChauffeurCamion = dto.ChauffeurCamion,
                    Client = client,
                    Destination = destination,
                    Tonnage = (float)dto.Tonnage,

                    Demmarage = dto.Demmarage,
                    EtatPneus = dto.EtatPneus,
                    Freinage = dto.Freinage,
                    Extincteur = dto.Extincteur,
                    Observation = dto.Observation,
                    Numeropermis = dto.Numeropermis,
                    NpoliceRC = dto.NpoliceRC,
                    NpoliceRCGaz = dto.NpoliceRCGaz,
                    AutoriserEntree = dto.AutoriserEntree,
                    Facturer = dto.Facturer,
                    QuantiteFactQuantiteChargee = dto.QuantiteFactQuantiteChargee,
                    AutoriserSortie = dto.AutoriserSortie,
                    PoidDebut = dto.PoidDebut,
                    NumeroPontBascule = dto.NumeroPontBascule,
                    ClapetVanne = dto.ClapetVanne,
                    CalageVehicule = dto.CalageVehicule,
                    MiseTerre = dto.MiseTerre,
                    ObservationExploitation = dto.ObservationExploitation,
                    AutoriserCharger = dto.AutoriserCharger,
                    heureChargement = dto.heureChargement,
                    heureFinChargement = dto.heureFinChargement,
                    PoidsApresChargement = dto.PoidsApresChargement,
                    TonnageCharger = (float)dto.TonnageCharger,
                    NumeroFacture = dto.NumeroFacture,
                    TemsEstime = dto.TempsEstime,
                    etat = 1,
                    //etat = dto.etat,



                };
                _context.BonChargementCiternes.Add(entity);
                try
                {
                    _context.SaveChanges();
                }
              
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
                dto.id = entity.id;

            }


        }



        public void ModifierBonChargement(BonChargementCiterneDto dto)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);
                var destination = _context.Destinations.Find(dto.CodeDestination);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }


                if (destination == null)
                {

                    destination = new Destination
                    {
                        Code = dto.CodeDestination,
                        libelle = dto.libelleDestination,

                    };
                }

                var entity = _context.BonChargementCiternes.Find(dto.id);
                if (entity != null)

                {


                    entity.Reference = dto.Reference;
                    entity.Date = dto.Date;
                    entity.idTypeBonChargement = dto.idTypeBon;
                    entity.ImmatriculeCamion = dto.ImmtriculeCamion;
                    entity.NbrePersonnes = dto.NbrePersonnes;
                    entity.ResponsableCamion = dto.ResponsableCamion;
                    entity.ChauffeurCamion = dto.ChauffeurCamion;
                    entity.Client = client;
                    entity.Client.Code = dto.CodeClient;
                    entity.Destination = destination; 
                    entity.Destination.Code = dto.CodeDestination;
                   
                    entity.Tonnage = (float)dto.Tonnage;

                    entity.Demmarage = dto.Demmarage;
                    entity.EtatPneus = dto.EtatPneus;
                    entity.Freinage = dto.Freinage;
                    entity.Extincteur = dto.Extincteur;
                    entity.Observation = dto.Observation;
                    entity.Numeropermis = dto.Numeropermis;
                    entity.NpoliceRC = dto.NpoliceRC;
                    entity.NpoliceRCGaz = dto.NpoliceRCGaz;
                    entity.AutoriserEntree = dto.AutoriserEntree;
                    entity.Facturer = dto.Facturer;
                    entity.QuantiteFactQuantiteChargee = dto.QuantiteFactQuantiteChargee;
                    entity.AutoriserSortie = dto.AutoriserSortie;
                    entity.PoidDebut = dto.PoidDebut;
                    entity.NumeroPontBascule = dto.NumeroPontBascule;
                    entity.ClapetVanne = dto.ClapetVanne;
                    entity.CalageVehicule = dto.CalageVehicule;
                    entity.MiseTerre = dto.MiseTerre;
                    entity.ObservationExploitation = dto.ObservationExploitation;
                    entity.AutoriserCharger = dto.AutoriserCharger;
                    entity.heureChargement = dto.heureChargement;
                    entity.heureFinChargement = dto.heureFinChargement;
                    entity.PoidsApresChargement = dto.PoidsApresChargement;
                    entity.TonnageCharger = (float)dto.TonnageCharger;
                    entity.NumeroFacture = dto.NumeroFacture;
                    entity.TemsEstime = dto.TempsEstime;

                    if (dto.idEntite == 4)
                        entity.etat = entity.etat;
                    else if (dto.idEntite == 5)
                        entity.etat = 2;
                    else
                        entity.etat = entity.etat;



                };
                _context.Entry(entity).State = EntityState.Modified;
               
                try
                {
                    _context.SaveChanges();
                }

                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
                //dto.id = entity.id;

            }


        }







        public ClientDto GetClientParId(int code)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Clients.Find(code);
                if (entity == null) return null;
                var dto = new ClientDto
                {
                    Code = entity.Code,
                    nomComplet = entity.nomComplet,
                    
                };

                return dto;
            }

          

        }


        public  DestinationDto GetDestinationParId(int code)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Destinations.Find(code);
                if (entity == null) return null;
                var dto = new DestinationDto
                {
                    Code = entity.Code,
                    libelle = entity.libelle,

                };

                return dto;
            }



        }


        public BonChargementCiterneDto GetBonParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.BonChargementCiternes.Find(id);

                var dto = new BonChargementCiterneDto
                {
                    id = o.id,
                    Reference = o.Reference,
                    Date = o.Date.Value,
                    idTypeBon = o.idTypeBonChargement.Value,
                    TypeBon = o.TypeBonChargement.libelle,
                    ImmtriculeCamion = o.ImmatriculeCamion,
                    NbrePersonnes = o.NbrePersonnes.Value,
                    ResponsableCamion = o.ResponsableCamion,
                    ChauffeurCamion = o.ChauffeurCamion,

                    Tonnage = (float)o.Tonnage,

                    Demmarage = o.Demmarage,
                    EtatPneus = o.EtatPneus,
                    Freinage = o.Freinage,
                    Extincteur = o.Extincteur,
                    Observation = o.Observation,
                    Numeropermis = o.Numeropermis,
                    NpoliceRC = o.NpoliceRC,
                    NpoliceRCGaz = o.NpoliceRCGaz,
                    AutoriserEntree = o.AutoriserEntree,
                    Facturer = o.Facturer,
                    QuantiteFactQuantiteChargee = o.QuantiteFactQuantiteChargee,
                    AutoriserSortie = o.AutoriserSortie,
                    PoidDebut = (float)o.PoidDebut,
                    NumeroPontBascule = o.NumeroPontBascule,
                    ClapetVanne = o.ClapetVanne,
                    CalageVehicule = o.CalageVehicule,
                    MiseTerre = o.MiseTerre,
                    ObservationExploitation = o.ObservationExploitation,
                    AutoriserCharger = o.AutoriserCharger,
                    heureChargement = o.heureChargement.Value,
                    heureFinChargement = o.heureFinChargement.Value,
                    PoidsApresChargement = (float)o.PoidsApresChargement,
                    TonnageCharger = (float)o.TonnageCharger,
                    NumeroFacture = o.NumeroFacture,
                    TempsEstime = o.TemsEstime,
                    etat = o.etat.Value

                };

                Destination destination = _context.Destinations.Find(o.codeDestination);
                if (destination != null)
                {
                    dto.CodeDestination = destination.Code;
                    dto.libelleDestination = destination.libelle;

                }

                Client client = _context.Clients.Find(o.CodeClient);
               
                if (client != null)
                {
                    dto.CodeClient = client.Code;
                    dto.NomClient = client.nomComplet;
                    
                }


                return dto;

            }
        }



        public void EnInstance(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                if (idEntite == 5)
                {
                    var entity = _context.BonChargementCiternes.Find(id);
                    entity.etat = 2;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                if (idEntite == 4)
                {
                    var entity = _context.BonChargementCiternes.Find(id);
                    entity.etat = 1;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
                }
             

            }

        }


        public void Valider(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.BonChargementCiternes.Find(id);
                entity.etat = 3;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }



        public void CloturerParDate(int idEntite, DateTime date)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var entity = _context.BonChargementCiternes.Where(x => x.Date == date.Date).Select(x => x.id).ToArray();

                foreach (int id in entity)
                {

                    BonChargementCiterne fiche = _context.BonChargementCiternes.Find(id);
                    fiche.etat = 4;
                    //var sortie = _context.Sorties.Where(x => x.id == item.id).SingleOrDefault();
                    _context.Entry(fiche).State = EntityState.Modified;
                    _context.SaveChanges();

                }


            }
        }




        public void Cloturer(int[] details)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                if (details == null) return;

                foreach (int id in details)
                {


                    BonChargementCiterne fiche = _context.BonChargementCiternes.Find(id);
                    fiche.etat = 4;
                    _context.Entry(fiche).State = EntityState.Modified;
                    _context.SaveChanges();

                }
            }
        }


        public int VerifierExistBon(string reference)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var nbreBon = _context.BonChargementCiternes.Where(x => x.Reference == reference).Count();

                return nbreBon;
            }

        }

        public int TotalInstance()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.BonChargementCiternes.Where(x => x.etat == 1 || x.etat == 2).Count();

                return Total;
            }


        }

        public int TotalCloturer()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.BonChargementCiternes.Where(x => x.etat == 3).Count();

                return Total;
            }


        }


        public TypeBonChargementDto GetTypeBonParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.TypeBonChargements.Find(id);

                var dto = new TypeBonChargementDto
                {
                    
                    id = entity.id,
                    libelle = entity.libelle
                 
                };
                return dto;

            }




        }


        public List<TypeBonChargementDto> GetListeTypeBon()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                //var list = _context.TypeBonChargements.Where(x=>x.id != 1).ToList();
                var list = _context.TypeBonChargements.ToList();
                var listeDto = new List<TypeBonChargementDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new TypeBonChargementDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }

        public List<TypeBonChargementDto> GetListeTypeBonDE()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.TypeBonChargements.Where(x=>x.id == 1).ToList();
                var listeDto = new List<TypeBonChargementDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new TypeBonChargementDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }


        public int TotalNbrePersonnes(string filtreMotCle)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                DateTime dateNull = DateTime.Now.Date;
                DateTime date = Convert.ToDateTime(filtreMotCle);
                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    var Total = _context.BonChargementCiternes.Where(x=> x.Date == dateNull).Select(x => x.NbrePersonnes.Value).DefaultIfEmpty(0).Sum();

                    return Total;

                }

                else
                {
                    var Total = _context.BonChargementCiternes.Where(x => x.Date == date.Date).Select(x => x.NbrePersonnes.Value).DefaultIfEmpty(0).Sum();

                    return Total;
                }

            }


        }



    }
}