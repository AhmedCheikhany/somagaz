﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionARB
    {

        private static BusinessGestionARB instance;
        private BusinessGestionARB() { }

        public static BusinessGestionARB Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionARB();
                }
                return instance;
            }
        }

        public List<ARBDto> ChercherListeARB(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

                var baseQuery = from o in _context.ARBs.AsEnumerable()
                                where o.Serie.Contains(filtreMotCle) || Convert.ToString(o.Client.Code).Contains(filtreMotCle)

                                select o;

                var query = from o in baseQuery
                            select new ARBDto
                            {
                                id = o.id,
                                Serie = o.Serie,
                                Date = o.Date.Value,
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,
                                B12 = o.B12.Value,
                                B9 = o.B9.Value,
                                B6R = o.B6R.Value,
                                B6V = o.B6V.Value,
                                B3 = o.B3.Value,
                                B35 = o.B35.Value,

                                Commentaire = o.Commentaire,
                                etat = o.etat.Value

                            };


                return query.OrderByDescending(x => x.id).ToList();
            }

        }



        public void Ajouter(ARBDto dto)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }



                var entity = new ARB
                {
                    Date = dto.Date,
                    Serie = dto.Serie,
                    B12 = dto.B12,
                    B9 = dto.B9,
                    B6R = dto.B6R,
                    B6V = dto.B6V,
                    B3 = dto.B3,
                    B35 = dto.B35,
                    Client = client,
                    Commentaire = dto.Commentaire,
                    etat = 1


                };
                _context.ARBs.Add(entity);
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }


        public ARBDto GetARBParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.ARBs.Find(id);

                var dto = new ARBDto
                {
                    id = entity.id,
                    Serie = entity.Serie,
                    Date = entity.Date.Value,
                    CodeClient = entity.CodeClient.Value,
                    NomClient = entity.Client.nomComplet,
                    B12 = entity.B12.Value,
                    B9 = entity.B9.Value,
                    B6R = entity.B6R.Value,
                    B6V = entity.B6V.Value,
                    B3 = entity.B3.Value,
                    B35 = entity.B35.Value,
                    Commentaire = entity.Commentaire,
                    etat = entity.etat.Value
                };


                return dto;

            }

        }


        public void Modifier(ARBDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }



                var entity = _context.ARBs.Find(dto.id);
                if (entity != null)

                {
                    entity.Date = dto.Date;
                    entity.Serie = dto.Serie;
                    entity.B12 = dto.B12;
                    entity.B9 = dto.B9;
                    entity.B6R = dto.B6R;
                    entity.B6V = dto.B6V;
                    entity.B3 = dto.B3;
                    entity.B35 = dto.B35;
                    entity.Client = client;
                    entity.Commentaire = dto.Commentaire;
                    entity.etat = 1;


                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }

    }
}