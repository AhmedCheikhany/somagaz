﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using GUHabitat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class BusinessGestionEntites
    {
        GUHabitatDBEntities _context = new GUHabitatDBEntities();
        public List<EntiteDto> GetListeEntites()
        {
            var list = _context.Entites.ToList();
            var listeDto = new List<EntiteDto>();
            foreach (var item in list)
            {
                listeDto.Add(new EntiteDto
                {
                    id = item.id,
                    libelle = item.libelle,
                });

            }
            return listeDto;
        }
        public void EnregistrerNouvelleEntite(EntiteDto dto)
        {
            var entity = new Entite
            {
                libelle = dto.libelle
            };
            _context.Entites.Add(entity);
            _context.SaveChanges();
        }

        public List<EntiteDto> ChercherListeEntites(string filtreMotCle)
        {

            _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

            //requête linq to entity : jointure + projection => max performance

            var baseQuery = from o in _context.Entites
                            select o;

            if (!string.IsNullOrWhiteSpace(filtreMotCle))
            {
                baseQuery = baseQuery.Where(x=>x.libelle.ToLower().Contains(filtreMotCle.ToLower()));
            }
            var query = from o in baseQuery
                        select new EntiteDto
                        {
                            id = o.id,
                            libelle = o.libelle
                        };

            //exécution différé (l'execution est forcé par la méthode ToList)
            return query.ToList();
        }

        public bool SupprimerEntite(int id)
        {
            try
            {
                var entity = _context.Entites.Find(id);
                _context.Entites.Remove(entity);
                _context.SaveChanges();
                return true;

            }
            catch (Exception)
            {

                return false;
            }
        }

        public EntiteDto GetEntiteParId(int id)
        {
            var entity = _context.Entites.Find(id);

            var dto = new EntiteDto
            {
                id = entity.id,
                libelle = entity.libelle
            };
            return dto;

        }

        public void ModifierEntite(EntiteDto dto)
        {
            var entity = _context.Entites.Find(dto.id);
            entity.libelle = dto.libelle;
            _context.SaveChanges();
        }
    }
}
