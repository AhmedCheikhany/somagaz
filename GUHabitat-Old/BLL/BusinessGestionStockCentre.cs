﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionStockCentre
    {

        private static BusinessGestionStockCentre instance;
        private BusinessGestionStockCentre() { }

        public static BusinessGestionStockCentre Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionStockCentre();
                }
                return instance;
            }
        }



        public List<StockCentreDto> ChercherListeStockCentre(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<StockCentre> baseQuery = Enumerable.Empty<StockCentre>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                //if (string.IsNullOrEmpty(filtreMotCle)) 
                //_context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


              

                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.StockCentres.AsEnumerable()
                                where  o.Date == Convert.ToDateTime(filtreMotCle)
                                select o;

                    var query = from o in baseQuery
                                select new StockCentreDto
                                {
                                    id = o.id,
                                    reference = o.reference.Value,
                                    Date = o.Date.Value,
                                    CodeCentre = o.CodeCentre.Value,
                                    LibelleCentre = o.Centre.libelle,
                                    StockTheorie = o.StockTheorie.Value,
                                    Stock = o.Stock.Value,
                                    StockFin = o.StockFin.Value,
                                    Depotage = o.Depotage.Value,
                                    GainPerte = o.GainPerte.Value,

                                    Commentaire = o.Commentaire,

                                    etat = o.etat.Value

                                };

                    return query.OrderByDescending(x => x.id).ToList();

                }




                else 
                {
                    baseQuery = from o in _context.StockCentres.AsEnumerable()
                                select o;
                    var query = from o in baseQuery
                                select new StockCentreDto
                                {
                                    id = o.id,
                                    reference = o.reference.Value,
                                    Date = o.Date.Value,
                                    CodeCentre = o.CodeCentre.Value,
                                    LibelleCentre = o.Centre.libelle,
                                    StockTheorie = o.StockTheorie.Value,
                                    Stock = o.Stock.Value,
                                    StockFin = o.StockFin.Value,
                                    Depotage = o.Depotage.Value,
                                    GainPerte = o.GainPerte.Value,

                                    Commentaire = o.Commentaire,

                                    etat = o.etat.Value

                                };

                    return query.OrderByDescending(x => x.id).ToList();
                }
            }

        }


        public StockCentreDto GetStockCentreParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.StockCentres.Find(id);

                var dto = new StockCentreDto
                {

                    id = o.id,
                    reference = o.reference.Value,
                    Date = o.Date.Value,
                    CodeCentre = o.CodeCentre.Value,
                    LibelleCentre = o.Centre.libelle,
                    StockTheorie = o.StockTheorie.Value,
                    Stock = o.Stock.Value,
                    StockFin = o.StockFin.Value,
                    Depotage = o.Depotage.Value,
                    GainPerte = o.GainPerte.Value,

                    Commentaire = o.Commentaire,

                    etat = o.etat.Value

                };


                return dto;

            }

        }


        public void Ajouter(StockCentreDto dto)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var centre = _context.Centres.Find(dto.CodeCentre);

                if (centre == null)
                {

                    centre = new Centre
                    {
                        Code = dto.CodeCentre,
                        libelle = dto.LibelleCentre,

                    };
                }



               
                var entity = new StockCentre
                {

                    reference = dto.reference,
                    Date = dto.Date,
                    Centre = centre,

                    StockTheorie = dto.StockTheorie,
                    Stock = dto.Stock,
                    StockFin = dto.StockFin,
                    Depotage = dto.Depotage,
                    GainPerte = dto.GainPerte,

                    Commentaire = dto.Commentaire,

                    etat = 1

                   

                };




                _context.StockCentres.Add(entity);
                try
                {
                    _context.SaveChanges();

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }

        public void Modifier(StockCentreDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var centre = _context.Centres.Find(dto.CodeCentre);

                if (centre == null)
                {

                    centre = new Centre
                    {
                        Code = dto.CodeCentre,
                        libelle = dto.LibelleCentre,

                    };
                }




                var entity = _context.StockCentres.Find(dto.id);
                if (entity != null)

                {


                    entity.reference = dto.reference;
                    entity.Date = dto.Date;
                    entity.Centre = centre;

                    entity.StockTheorie = dto.StockTheorie;
                    entity.Stock = dto.Stock;
                    entity.StockFin = dto.StockFin;
                    entity.Depotage = dto.Depotage;
                    entity.GainPerte = dto.GainPerte;
                   

                    entity.Commentaire = dto.Commentaire;





                    if (dto.idEntite == 4)
                        entity.etat = entity.etat;
                    else if (dto.idEntite == 5)
                        entity.etat = 2;
                    else
                        entity.etat = entity.etat;




                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }


        public CentreDto GetCentreParCode(int code)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Centres.Find(code);

                var dto = new CentreDto
                {
                    Code = entity.Code,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public List<CentreDto> GetListeCentre()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.Centres.Where(x=>x.Code != 0).ToList();
                var listeDto = new List<CentreDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new CentreDto
                    {
                        Code = item.Code,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }

        public decimal StockDeut(int CodeCentre)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var dateStock = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre).OrderByDescending(r => r.Date).Select(x => x.Date).FirstOrDefault();



                var StockFin = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date == dateStock).Select(x => x.StockFin).DefaultIfEmpty(0).SingleOrDefault();

              
                return (decimal)StockFin;
            }


        }

        public decimal StockDeut(int CodeCentre,DateTime date)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var dateStock = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date < date).OrderByDescending(r => r.Date).Select(x => x.Date).FirstOrDefault();
                


                var StockFin = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date == dateStock).Select(x => x.StockFin).DefaultIfEmpty(0).SingleOrDefault();


                return (decimal)StockFin;
            }


        }


        public decimal StockAujourdhui(int CodeCentre)
        {


           

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                //var referenceHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre).OrderByDescending(r => r.reference).Select(x => x.reference).FirstOrDefault();

                var dateStockHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre).OrderByDescending(r => r.Date).Select(x => x.Date).FirstOrDefault();



                //var dateStockHier = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Date).SingleOrDefault();

                var ModelSituationNktt = BusinessGestionStatistiques.Instance.SituationCentreNktt(dateStockHier, dateStockHier);
                var TotalProductionNktt = ModelSituationNktt.TotalProduction;
                var Depotage = _context.StockCentres.Where(x => x.Date == dateStockHier && x.CodeCentre == CodeCentre).Select(x => x.Depotage).DefaultIfEmpty(0).SingleOrDefault();
                var Retour = _context.BonRetourCiternes.Where(x => x.DateRetour == dateStockHier).Select(x => x.PoidsGazRestant).DefaultIfEmpty(0).Sum();
                var ReceptionCollaborateur = ModelSituationNktt.TonnageCollaborateurReception;
                var StockHier = _context.StockCentres.Where(x => x.Date == dateStockHier && x.CodeCentre == CodeCentre).Select(x => x.Stock).DefaultIfEmpty(0).SingleOrDefault();

                var StockAujourdhui = StockHier - TotalProductionNktt + Depotage + Retour + ReceptionCollaborateur;

                //if (StockHier < 0)
                //    StockAujourdhui = 0;

                return (decimal)StockAujourdhui;
            }


        }

        public decimal StockAujourdhui(int CodeCentre,DateTime date)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var dateStockHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre  && x.Date == date).Select(x => x.Date).SingleOrDefault();

               

                var ModelSituationNktt = BusinessGestionStatistiques.Instance.SituationCentreNktt(dateStockHier, dateStockHier);
                var TotalProductionNktt = ModelSituationNktt.TotalProduction;
                var Depotage = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date == dateStockHier).Select(x => x.Depotage).DefaultIfEmpty(0).SingleOrDefault();
                var Retour = _context.BonRetourCiternes.Where(x => x.DateRetour == dateStockHier).Select(x => x.PoidsGazRestant).DefaultIfEmpty(0).Sum();
                var ReceptionCollaborateur = ModelSituationNktt.TonnageCollaborateurReception;
                var StockHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date == dateStockHier).Select(x => x.Stock).DefaultIfEmpty(0).SingleOrDefault();

                var StockAujourdhui = StockHier - TotalProductionNktt + Depotage + Retour + ReceptionCollaborateur;

                //if (StockHier < 0)
                //    StockAujourdhui = 0;

                return (decimal)StockAujourdhui;
            }


        }


        public decimal StockAujourdhuiInterieur(int CodeCentre)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var dateStockHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre).OrderByDescending(r => r.Date).Select(x => x.Date).FirstOrDefault();

                var ModelStatistiqueCentre = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(CodeCentre,dateStockHier, dateStockHier);
                var TotalProduction = ModelStatistiqueCentre.TotalProduction;
                var Reception = (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == CodeCentre & x.Date == dateStockHier).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();
                var Retour = _context.BonRetourCiternes.Where(x => x.DateRetour == dateStockHier && x.CodeCentre == CodeCentre).Select(x => x.PoidsGazRestant).DefaultIfEmpty(0).Sum();

                var StockHier = _context.StockCentres.Where(x => x.Date == dateStockHier && x.CodeCentre == CodeCentre).Select(x => x.Stock).DefaultIfEmpty(0).SingleOrDefault();

                var StockAujourdhui = StockHier - TotalProduction + Reception - Retour;

                //if (StockHier < 0)
                //    StockAujourdhui = 0;

                return (decimal)StockAujourdhui;
            }


        }


        public decimal StockAujourdhuiInterieur(int CodeCentre, DateTime date)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var dateStockHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date == date).Select(x => x.Date).SingleOrDefault();

                var ModelStatistiqueCentre = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(CodeCentre, dateStockHier, dateStockHier);
                var TotalProduction = ModelStatistiqueCentre.TotalProduction;
                var Reception = (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == CodeCentre & x.Date == dateStockHier).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();
                var Retour = _context.BonRetourCiternes.Where(x => x.DateRetour == dateStockHier && x.CodeCentre == CodeCentre).Select(x => x.PoidsGazRestant).DefaultIfEmpty(0).Sum();

                var StockHier = _context.StockCentres.Where(x => x.Date == dateStockHier && x.CodeCentre == CodeCentre).Select(x => x.Stock).DefaultIfEmpty(0).SingleOrDefault();

                var DateRetard = "2023-04-01";

                var StockAujourdhui = dateStockHier < Convert.ToDateTime(DateRetard).Date  ?  StockHier - TotalProduction : StockHier - TotalProduction + Reception - Retour;

                //if (StockHier < 0)
                //    StockAujourdhui = 0;

                return (decimal)StockAujourdhui;
            }


        }


        public decimal CalculerGainPerteNktt(int CodeCentre, decimal stockAujourdhui, decimal depotageAujourdhui)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var referenceHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre).OrderByDescending(r => r.reference).Select(x => x.reference).FirstOrDefault();

                var dateStockHier = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Date).SingleOrDefault();

                var ModelStatistiqueCentre = BusinessGestionStatistiques.Instance.SituationCentreNktt(dateStockHier, dateStockHier);
                var TotalProduction = ModelStatistiqueCentre.TotalProduction;
               
                var Retour = _context.BonRetourCiternes.Where(x => x.DateRetour == dateStockHier && x.CodeCentre == CodeCentre).Select(x => x.PoidsGazRestant).DefaultIfEmpty(0).SingleOrDefault();
                var Depotage = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Depotage).DefaultIfEmpty(0).SingleOrDefault();
                var StockHier = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Stock).DefaultIfEmpty(0).SingleOrDefault();

                //var StockAujourdhui = StockHier - TotalProduction + Reception - Retour;

                var Stock = stockAujourdhui - depotageAujourdhui;
                var difference = StockHier - TotalProduction;


                var GainPerte = Stock - difference;

                if (StockHier == 0)
                    GainPerte = 0;

                return (decimal)GainPerte;
            }

        }




        public decimal CalculerGainPerteNktt(DateTime date,int CodeCentre, decimal stockAujourdhui, decimal depotageAujourdhui)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                //var stock = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date < date).FirstOrDefault();

                var referenceHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date < date).OrderByDescending(r => r.reference).Select(x => x.reference).FirstOrDefault();

                var dateStockHier = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Date).SingleOrDefault();

                var ModelStatistiqueCentre = BusinessGestionStatistiques.Instance.SituationCentreNktt(dateStockHier, dateStockHier);
                var TotalProduction = ModelStatistiqueCentre.TotalProduction;

                var Retour = _context.BonRetourCiternes.Where(x => x.DateRetour == dateStockHier && x.CodeCentre == CodeCentre).Select(x => x.PoidsGazRestant).DefaultIfEmpty(0).SingleOrDefault();
                var Depotage = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Depotage).DefaultIfEmpty(0).SingleOrDefault();
                var StockHier = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Stock).DefaultIfEmpty(0).SingleOrDefault();

                //var StockAujourdhui = StockHier - TotalProduction + Reception - Retour;

                var Stock = stockAujourdhui - depotageAujourdhui;
                var difference = StockHier - TotalProduction;


                var GainPerte = Stock - difference;

                if (StockHier == 0)
                    GainPerte = 0;

                return (decimal)GainPerte;
            }

        }




        public decimal CalculerGainPerteInterieur(int CodeCentre, decimal stockAujourdhui , decimal depotageAujourdhui)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var referenceHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre).OrderByDescending(r => r.reference).Select(x => x.reference).FirstOrDefault();

                var dateStockHier = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Date).SingleOrDefault();

                var ModelStatistiqueCentre = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(CodeCentre, dateStockHier, dateStockHier);
                var TotalProduction = ModelStatistiqueCentre.TotalProduction;
                var Reception = (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == CodeCentre && x.Date > dateStockHier && x.Date <= dateStockHier).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).SingleOrDefault();
                var Retour = _context.BonRetourCiternes.Where(x => x.DateRetour == dateStockHier && x.CodeCentre == CodeCentre).Select(x => x.PoidsGazRestant).DefaultIfEmpty(0).SingleOrDefault();

                var StockHier = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Stock).DefaultIfEmpty(0).SingleOrDefault();

                //var StockAujourdhui = StockHier - TotalProduction + Reception - Retour;

                var Stock = stockAujourdhui - Reception - Retour;
                var difference = StockHier - TotalProduction;


                var GainPerte = Stock - difference;

                if (StockHier == 0)
                    GainPerte = 0;

                return (decimal)GainPerte;
            }

        }

        public decimal CalculerGainPerteInterieur(DateTime date, int CodeCentre, decimal stockAujourdhui, decimal depotageAujourdhui)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var referenceHier = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date < date).OrderByDescending(r => r.reference).Select(x => x.reference).FirstOrDefault();

                var dateStockHier = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Date).SingleOrDefault();

                var ModelStatistiqueCentre = BusinessGestionStatistiques.Instance.SituationInterieurParCentre(CodeCentre, dateStockHier, dateStockHier);
                var TotalProduction = ModelStatistiqueCentre.TotalProduction;
                var Reception = (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == CodeCentre && x.Date > dateStockHier && x.Date <= dateStockHier).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).SingleOrDefault();
                var Retour = _context.BonRetourCiternes.Where(x => x.DateRetour == dateStockHier && x.CodeCentre == CodeCentre).Select(x => x.PoidsGazRestant).DefaultIfEmpty(0).SingleOrDefault();

                var StockHier = _context.StockCentres.Where(x => x.reference == referenceHier).Select(x => x.Stock).DefaultIfEmpty(0).SingleOrDefault();

                //var StockAujourdhui = StockHier - TotalProduction + Reception - Retour;

                var Stock = stockAujourdhui - Reception - Retour;
                var difference = StockHier - TotalProduction;


                var GainPerte = Stock - difference;

                if (StockHier == 0)
                    GainPerte = 0;

                return (decimal)GainPerte;
            }

        }


        public int GetStockParDate(int CodeCentre,DateTime date)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var nbre = _context.StockCentres.Where(x =>x.CodeCentre == CodeCentre && x.Date == date.Date).Count();

                return nbre;
            }

        }

        public int GetStockParDate(int CodeCentre ,DateTime date, int reference)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var nbre = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.reference == reference && x.Date == date).Count();

                return nbre;
            }

        }
        public bool GetStockDate(int CodeCentre ,DateTime date)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var etat = _context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date == date).Any();

                return etat;
            }

        }



        public void EnInstance(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var entity = _context.StockCentres.Find(id);
                entity.etat = 1;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();



            }

        }


        public void Valider(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.StockCentres.Find(id);
                entity.etat = 2;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }



    }
}