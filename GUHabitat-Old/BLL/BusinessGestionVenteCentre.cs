﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionVenteCentre
    {


        private static BusinessGestionVenteCentre instance;
        private BusinessGestionVenteCentre() { }

        public static BusinessGestionVenteCentre Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionVenteCentre();
                }
                return instance;
            }
        }


        public List<VenteCentreDto> ChercherListeVenteCentre(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<VenteCentre> baseQuery = Enumerable.Empty<VenteCentre>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                //if (string.IsNullOrEmpty(filtreMotCle)) 
                //_context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.VenteCentres.AsEnumerable()
                                select o;

                }




                if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                {
                    baseQuery = from o in _context.VenteCentres.AsEnumerable()
                                where o.Reference.Contains(filtreMotCle)
                                select o;

                }


                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.VenteCentres.AsEnumerable()
                                where o.CodeCentre == Convert.ToInt32(filtreMotCle)
                                select o;

                }

                var query = from o in baseQuery
                            select new VenteCentreDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                CodeCentre = o.CodeCentre.Value,
                                LibelleCentre = o.Centre.libelle,

                                B12Revendeur = o.B12Revendeur.Value,
                                B9Revendeur = o.B9Revendeur.Value,
                                B6RRevendeur = o.B6RRevendeur.Value,
                                B6VRevendeur = o.B6VRevendeur.Value,
                                B3Revendeur = o.B3Revendeur.Value,
                                B35Revendeur = o.B35Revendeur.Value,

                                B12PrixRevendeur = o.B12PrixRevendeur.Value,
                                B9PrixRevendeur = o.B9PrixRevendeur.Value,
                                B6RPrixRevendeur = o.B6RPrixRevendeur.Value,
                                B6VPrixRevendeur = o.B6VPrixRevendeur.Value,
                                B3PrixRevendeur = o.B3PrixRevendeur.Value,
                                B35PrixRevendeur = o.B35PrixRevendeur.Value,


                                B12Consommateur = o.B12Consommateur.Value,
                                B9Consommateur = o.B9Consommateur.Value,
                                B6RConsommateur = o.B6RConsommateur.Value,
                                B6VConsommateur = o.B6VConsommateur.Value,
                                B3Consommateur = o.B3Consommateur.Value,
                                B35Consommateur = o.B35Consommateur.Value,


                                B12PrixConsommateur = o.B12PrixConsommateur.Value,
                                B9PrixConsommateur = o.B9PrixConsommateur.Value,
                                B6RPrixConsommateur = o.B6RPrixConsommateur.Value,
                                B6VPrixConsommateur = o.B6VPrixConsommateur.Value,
                                B3PrixConsommateur = o.B3PrixConsommateur.Value,
                                B35PrixConsommateur = o.B35PrixConsommateur.Value,

                                TotalNbreB12 = (o.B12Consommateur.Value + o.B12Revendeur.Value),
                                TotalNbreB9 =  (o.B9Consommateur.Value + o.B9Revendeur.Value),
                                TotalNbreB6R = (o.B6RConsommateur.Value + o.B6RRevendeur.Value),
                                TotalNbreB6V = (o.B6VConsommateur.Value + o.B6VRevendeur.Value),
                                TotalNbreB3 =  (o.B3Consommateur.Value + o.B3Revendeur.Value),
                                TotalNbreB35 = (o.B35Consommateur.Value + o.B35Revendeur.Value),


                                TotalPrixB12Consommateur = o.TotalPrixB12Consommateur.Value,
                                TotalPrixB9Consommateur = o.TotalPrixB9Consommateur.Value,
                                TotalPrixB6RConsommateur = o.TotalPrixB6RConsommateur.Value,
                                TotalPrixB6VConsommateur = o.TotalPrixB6VConsommateur.Value,
                                TotalPrixB3Consommateur = o.TotalPrixB3Consommateur.Value,
                                TotalPrixB35Consommateur = o.TotalPrixB35Consommateur.Value,

                                TotalPrixB12Revendeur = o.TotalPrixB12Revendeur.Value,
                                TotalPrixB9Revendeur = o.TotalPrixB9Revendeur.Value,
                                TotalPrixB6RRevendeur = o.TotalPrixB6RRevendeur.Value,
                                TotalPrixB6VRevendeur = o.TotalPrixB6VRevendeur.Value,
                                TotalPrixB3Revendeur = o.TotalPrixB3Revendeur.Value,
                                TotalPrixB35Revendeur = o.TotalPrixB35Revendeur.Value,


                                TotalMontantConsommateur = o.TotalMontantConsommateur.Value,
                                TotalMontantRevendeur = o.TotalMontantRevendeur.Value,



                                Commentaire = o.Commentaire,

                                etat = o.etat.Value

                            };

                return query.OrderByDescending(x => x.id).ToList();


            }

        }




        public VenteCentreDto GetVenteCentreParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.VenteCentres.Find(id);

                var dto = new VenteCentreDto
                {

                    id = o.id,
                    Reference = o.Reference,
                    Date = o.Date.Value,
                    CodeCentre = o.CodeCentre.Value,
                    LibelleCentre = o.Centre.libelle,

                    B12Revendeur = o.B12Revendeur.Value,
                    B9Revendeur = o.B9Revendeur.Value,
                    B6RRevendeur = o.B6RRevendeur.Value,
                    B6VRevendeur = o.B6VRevendeur.Value,
                    B3Revendeur = o.B3Revendeur.Value,
                    B35Revendeur = o.B35Revendeur.Value,

                    B12PrixRevendeur = o.B12PrixRevendeur.Value,
                    B9PrixRevendeur = o.B9PrixRevendeur.Value,
                    B6RPrixRevendeur = o.B6RPrixRevendeur.Value,
                    B6VPrixRevendeur = o.B6VPrixRevendeur.Value,
                    B3PrixRevendeur = o.B3PrixRevendeur.Value,
                    B35PrixRevendeur = o.B35PrixRevendeur.Value,


                    B12Consommateur = o.B12Consommateur.Value,
                    B9Consommateur = o.B9Consommateur.Value,
                    B6RConsommateur = o.B6RConsommateur.Value,
                    B6VConsommateur = o.B6VConsommateur.Value,
                    B3Consommateur = o.B3Consommateur.Value,
                    B35Consommateur = o.B35Consommateur.Value,


                    B12PrixConsommateur = o.B12PrixConsommateur.Value,
                    B9PrixConsommateur = o.B9PrixConsommateur.Value,
                    B6RPrixConsommateur = o.B6RPrixConsommateur.Value,
                    B6VPrixConsommateur = o.B6VPrixConsommateur.Value,
                    B3PrixConsommateur = o.B3PrixConsommateur.Value,
                    B35PrixConsommateur = o.B35PrixConsommateur.Value,


                    TotalPrixB12Consommateur = o.TotalPrixB12Consommateur.Value,
                    TotalPrixB9Consommateur = o.TotalPrixB9Consommateur.Value,
                    TotalPrixB6RConsommateur = o.TotalPrixB6RConsommateur.Value,
                    TotalPrixB6VConsommateur = o.TotalPrixB6VConsommateur.Value,
                    TotalPrixB3Consommateur = o.TotalPrixB3Consommateur.Value,
                    TotalPrixB35Consommateur = o.TotalPrixB35Consommateur.Value,

                    TotalPrixB12Revendeur = o.TotalPrixB12Revendeur.Value,
                    TotalPrixB9Revendeur = o.TotalPrixB9Revendeur.Value,
                    TotalPrixB6RRevendeur = o.TotalPrixB6RRevendeur.Value,
                    TotalPrixB6VRevendeur = o.TotalPrixB6VRevendeur.Value,
                    TotalPrixB3Revendeur = o.TotalPrixB3Revendeur.Value,
                    TotalPrixB35Revendeur = o.TotalPrixB35Revendeur.Value,


                    TotalMontantConsommateur = o.TotalMontantConsommateur.Value,
                    TotalMontantRevendeur = o.TotalMontantRevendeur.Value,



                    Commentaire = o.Commentaire,

                    etat = o.etat.Value



                };


                return dto;

            }

        }




        public void Ajouter(VenteCentreDto dto)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var centre = _context.Centres.Find(dto.CodeCentre);
                
                if (centre == null)
                {

                    centre = new Centre
                    {
                        Code = dto.CodeCentre,
                        libelle = dto.LibelleCentre,

                    };
                }

               



                var entity = new VenteCentre
                {

                    Reference = dto.Reference,
                    Date = dto.Date,
                    Centre = centre,
                   

                    B12Revendeur = dto.B12Revendeur,
                    B9Revendeur =  dto.B9Revendeur,
                    B6RRevendeur = dto.B6RRevendeur,
                    B6VRevendeur = dto.B6VRevendeur,
                    B3Revendeur =  dto.B3Revendeur,
                    B35Revendeur = dto.B35Revendeur,

                    B12PrixRevendeur = dto.B12PrixRevendeur,
                    B9PrixRevendeur =  dto.B9PrixRevendeur,
                    B6RPrixRevendeur = dto.B6RPrixRevendeur,
                    B6VPrixRevendeur = dto.B6VPrixRevendeur,
                    B3PrixRevendeur =  dto.B3PrixRevendeur,
                    B35PrixRevendeur = dto.B35PrixRevendeur,


                    B12Consommateur = dto.B12Consommateur,
                    B9Consommateur =  dto.B9Consommateur,
                    B6RConsommateur = dto.B6RConsommateur,
                    B6VConsommateur = dto.B6VConsommateur,
                    B3Consommateur =  dto.B3Consommateur,
                    B35Consommateur = dto.B35Consommateur,


                    B12PrixConsommateur = dto.B12PrixConsommateur,
                    B9PrixConsommateur = dto.B9PrixConsommateur,
                    B6RPrixConsommateur = dto.B6RPrixConsommateur,
                    B6VPrixConsommateur = dto.B6VPrixConsommateur,
                    B3PrixConsommateur = dto.B3PrixConsommateur,
                    B35PrixConsommateur = dto.B35PrixConsommateur,


                    TotalPrixB12Consommateur = dto.TotalPrixB12Consommateur,
                    TotalPrixB9Consommateur = dto.TotalPrixB9Consommateur,
                    TotalPrixB6RConsommateur = dto.TotalPrixB6RConsommateur,
                    TotalPrixB6VConsommateur = dto.TotalPrixB6VConsommateur,
                    TotalPrixB3Consommateur = dto.TotalPrixB3Consommateur,
                    TotalPrixB35Consommateur = dto.TotalPrixB35Consommateur,

                    TotalPrixB12Revendeur = dto.TotalPrixB12Revendeur,
                    TotalPrixB9Revendeur = dto.TotalPrixB9Revendeur,
                    TotalPrixB6RRevendeur = dto.TotalPrixB6RRevendeur,
                    TotalPrixB6VRevendeur = dto.TotalPrixB6VRevendeur,
                    TotalPrixB3Revendeur = dto.TotalPrixB3Revendeur,
                    TotalPrixB35Revendeur = dto.TotalPrixB35Revendeur,


                    TotalMontantConsommateur = dto.TotalMontantConsommateur,
                    TotalMontantRevendeur = dto.TotalMontantRevendeur,



                    Commentaire = dto.Commentaire,

                    etat = 1



                };
                _context.VenteCentres.Add(entity);
                try
                {
                    _context.SaveChanges();

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }



        public void Modifier(VenteCentreDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var centre = _context.Centres.Find(dto.CodeCentre);

                if (centre == null)
                {

                    centre = new Centre
                    {
                        Code = dto.CodeCentre,
                        libelle = dto.LibelleCentre,

                    };
                }




                var entity = _context.VenteCentres.Find(dto.id);
                if (entity != null)

                {


                    entity.Reference = dto.Reference;
                    entity.Date = dto.Date;
                    entity.Centre = centre;

                    entity.B12Revendeur = dto.B12Revendeur;
                    entity.B9Revendeur = dto.B9Revendeur;
                    entity.B6RRevendeur = dto.B6RRevendeur;
                    entity.B6VRevendeur = dto.B6VRevendeur;
                    entity.B3Revendeur = dto.B3Revendeur;
                    entity.B35Revendeur = dto.B35Revendeur;

                    entity.B12PrixRevendeur = dto.B12PrixRevendeur;
                    entity.B9PrixRevendeur = dto.B9PrixRevendeur;
                    entity.B6RPrixRevendeur = dto.B6RPrixRevendeur;
                    entity.B6VPrixRevendeur = dto.B6VPrixRevendeur;
                    entity.B3PrixRevendeur = dto.B3PrixRevendeur;
                    entity.B35PrixRevendeur = dto.B35PrixRevendeur;

                    entity.B12Consommateur = dto.B12Consommateur;
                    entity.B9Consommateur = dto.B9Consommateur;
                    entity.B6RConsommateur = dto.B6RConsommateur;
                    entity.B6VConsommateur = dto.B6VConsommateur;
                    entity.B3Consommateur = dto.B3Consommateur;
                    entity.B35Consommateur = dto.B35Consommateur;

                    entity.B12PrixConsommateur = dto.B12PrixConsommateur;
                    entity.B9PrixConsommateur = dto.B9PrixConsommateur;
                    entity.B6RPrixConsommateur = dto.B6RPrixConsommateur;
                    entity.B6VPrixConsommateur = dto.B6VPrixConsommateur;
                    entity.B3PrixConsommateur = dto.B3PrixConsommateur;
                    entity.B35PrixConsommateur = dto.B35PrixConsommateur;

                    entity.TotalPrixB12Consommateur = dto.TotalPrixB12Consommateur;
                    entity.TotalPrixB9Consommateur = dto.TotalPrixB9Consommateur;
                    entity.TotalPrixB6RConsommateur = dto.TotalPrixB6RConsommateur;
                    entity.TotalPrixB6VConsommateur = dto.TotalPrixB6VConsommateur;
                    entity.TotalPrixB3Consommateur = dto.TotalPrixB3Consommateur;
                    entity.TotalPrixB35Consommateur = dto.TotalPrixB35Consommateur;

                    entity.TotalPrixB12Revendeur = dto.TotalPrixB12Revendeur;
                    entity.TotalPrixB9Revendeur = dto.TotalPrixB9Revendeur;
                    entity.TotalPrixB6RRevendeur = dto.TotalPrixB6RRevendeur;
                    entity.TotalPrixB6VRevendeur = dto.TotalPrixB6VRevendeur;
                    entity.TotalPrixB3Revendeur = dto.TotalPrixB3Revendeur;
                    entity.TotalPrixB35Revendeur = dto.TotalPrixB35Revendeur;

                    entity.TotalMontantConsommateur = dto.TotalMontantConsommateur;
                    entity.TotalMontantRevendeur = dto.TotalMontantRevendeur;
              
                    entity.Commentaire = dto.Commentaire;



                   

                    if (dto.idEntite == 4)
                        entity.etat = entity.etat;
                    else if (dto.idEntite == 5)
                        entity.etat = 2;
                    else
                        entity.etat = entity.etat;




                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }



        public CentreDto GetCentreParCode(int code)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Centres.Find(code);

                var dto = new CentreDto
                {
                    Code = entity.Code,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public List<CentreDto> GetListeCentre()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.Centres.Where(x => x.Code != 0 && x.Code != 9).ToList();
                var listeDto = new List<CentreDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new CentreDto
                    {
                        Code = item.Code,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }

        public void EnInstance(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var entity = _context.VenteCentres.Find(id);
                entity.etat = 1;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();



            }

        }


        public void Valider(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.VenteCentres.Find(id);
                entity.etat = 2;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }






    }
}