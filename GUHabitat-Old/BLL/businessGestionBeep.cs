﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionBeep
    {

        private static BusinessGestionBeep instance;
        private BusinessGestionBeep() { }

        public static BusinessGestionBeep Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionBeep();
                }
                return instance;
            }
        }



        public List<BeepDto> ChercherListeBeep(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

                var baseQuery = from o in _context.BEEPs.AsEnumerable()
                                where o.Serie.Contains(filtreMotCle) || Convert.ToString(o.Client.Code).Contains(filtreMotCle)
                                || o.NumeroFacture.Contains(filtreMotCle) || o.VehiculeImmatricule.Contains(filtreMotCle)
                                || o.DotationImmatricule.Contains(filtreMotCle)
                                select o;

                var query = from o in baseQuery
                            select new BeepDto
                            {
                                id = o.id,
                                Serie = o.Serie,
                                Date = o.Date.Value,
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,
                                NumeroFacture = o.NumeroFacture,
                                VehiculeImmatricule = o.VehiculeImmatricule,
                                DotationImmatricule = o.DotationImmatricule,
                                B12 = o.B12.Value,
                                B9 = o.B9.Value,
                                B6R = o.B6R.Value,
                                B6V = o.B6V.Value,
                                B3 = o.B3.Value,
                                B35 = o.B35.Value,
                                
                                Commentaire = o.Commentaire,
                                etat = o.etat.Value

                            };


                return query.OrderByDescending(x => x.id).ToList();
            }

        }


        public void Ajouter(BeepDto dto)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }



                var entity = new BEEP
                {
                    Date = dto.Date,
                    Serie = dto.Serie,
                    Client = client,
                    NumeroFacture = dto.NumeroFacture,
                    VehiculeImmatricule = dto.VehiculeImmatricule,
                    DotationImmatricule = dto.DotationImmatricule,
                    B12 = dto.B12,
                    B9 = dto.B9,
                    B6R = dto.B6R,
                    B6V = dto.B6V,
                    B3 = dto.B3,
                    B35 = dto.B35,
                    Commentaire = dto.Commentaire,
                    etat = 1


                };
                _context.BEEPs.Add(entity);
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }




        public void Modifier(BeepDto dto)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }



                var entity = _context.BEEPs.Find(dto.id);
                if (entity != null)

                {
                    entity.Date = dto.Date;
                    entity.Serie = dto.Serie;
                    entity.Client = client;
                    entity.NumeroFacture = dto.NumeroFacture;
                    entity.VehiculeImmatricule = dto.VehiculeImmatricule;
                    entity.DotationImmatricule = dto.DotationImmatricule;
                    entity.B12 = dto.B12;
                    entity.B9 = dto.B9;
                    entity.B6R = dto.B6R;
                    entity.B6V = dto.B6V;
                    entity.B3 = dto.B3;
                    entity.B35 = dto.B35;
                    
                    entity.Commentaire = dto.Commentaire;
                    entity.etat = 1;


                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }




        public BeepDto GetBeepParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.BEEPs.Find(id);

                var dto = new BeepDto
                {
                    id = entity.id,
                    Serie = entity.Serie,
                    Date = entity.Date.Value,
                    CodeClient = entity.CodeClient.Value,
                    NomClient = entity.Client.nomComplet,
                    NumeroFacture = entity.NumeroFacture,
                    VehiculeImmatricule = entity.VehiculeImmatricule,
                    DotationImmatricule = entity.DotationImmatricule,
                    B12 = entity.B12.Value,
                    B9 = entity.B9.Value,
                    B6R = entity.B6R.Value,
                    B6V = entity.B6V.Value,
                    B3 = entity.B3.Value,
                    B35 = entity.B35.Value,
                    etat = entity.etat.Value
                };


                return dto;

            }




        }



    }
}