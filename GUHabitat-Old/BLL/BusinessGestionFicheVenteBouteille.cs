﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionFicheVenteBouteille
    {

        private static BusinessGestionFicheVenteBouteille instance;
        private BusinessGestionFicheVenteBouteille() { }

        public static BusinessGestionFicheVenteBouteille Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionFicheVenteBouteille();
                }
                return instance;
            }
        }


        public List<FicheVenteBouteilleDto> ChercherListeFicheVenteBouteille(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                IEnumerable<FicheVenteBouteille> baseQuery = Enumerable.Empty<FicheVenteBouteille>();
                
                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

              


                if (idEntite == 5)
                {

                    if (string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                    where o.etat != 1
                                    select o;

                    }



                    if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                    {
                          baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                        where o.etat != 1 && (o.Reference.Contains(filtreMotCle))
                                        select o;

                    }
                    else if (!string.IsNullOrEmpty(filtreMotCle))
                    {
                         baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                        where o.etat != 1 && (o.Client.Code == Convert.ToInt32(filtreMotCle)
                                        || o.Employe.matricule == Convert.ToInt32(filtreMotCle))
                                        select o;

                    }






                    var query = from o in baseQuery
                                select new FicheVenteBouteilleDto
                                {
                                    id = o.id,
                                    idTypeFiche = o.idTypeFiche.Value,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,
                                    VehiculeImmatricule = o.VehiculeImmatricule,
                                    NbrePersonnes  = o.NbrePersonnes.Value,
                                    matriculeEmploye = o.matriculeEmploye.Value,
                                    NomEmploye = o.Employe.nomComplet,
                                    TypeFiche = o.TypeFicheVente.libelle,

                                    B12Client = o.B12Client.Value,
                                    B9Client = o.B9Client.Value,
                                    B6RClient = o.B6RClient.Value,
                                    B6VClient = o.B6VClient.Value,
                                    B3Client = o.B3Client.Value,
                                    B35Client = o.B35Client.Value,


                                    B12VideRec = o.B12VideRec.Value,
                                    B9VideRec = o.B9VideRec.Value,
                                    B6RVideRec = o.B6RVideRec.Value,
                                    B6VVideRec = o.B6VVideRec.Value,
                                    B3VideRec = o.B3VideRec.Value,
                                    B35VideRec = o.B35VideRec.Value,

                                    B12VideDef = o.B12VideDef.Value,
                                    B9VideDef = o.B9VideDef.Value,
                                    B6RVideDef = o.B6RVideDef.Value,
                                    B6VVideDef = o.B6VVideDef.Value,
                                    B3VideDef = o.B3VideDef.Value,
                                    B35VideDef = o.B35VideDef.Value,

                                    B12PleinRemp = o.B12PleinRemp.Value,
                                    B9PleinRemp = o.B9PleinRemp.Value,
                                    B6RPleinRemp = o.B6RPleinRemp.Value,
                                    B6VPleinRemp = o.B6VPleinRemp.Value,
                                    B3PleinRemp = o.B3PleinRemp.Value,
                                    B35PleinRemp = o.B35PleinRemp.Value,

                                    B12PleinDef = o.B12PleinDef.Value,
                                    B9PleinDef = o.B9PleinDef.Value,
                                    B6RPleinDef = o.B6RPleinDef.Value,
                                    B6VPleinDef = o.B6VPleinDef.Value,
                                    B3PleinDef = o.B3PleinDef.Value,
                                    B35PleinDef = o.B35PleinDef.Value,

                                    B12DefAremp = o.B12DefAremp.Value,
                                    B9DefAremp = o.B9DefAremp.Value,
                                    B6RDefAremp = o.B6RDefAremp.Value,
                                    B6VDefAremp = o.B6VDefAremp.Value,
                                    B3DefAremp = o.B3DefAremp.Value,
                                    B35DefAremp = o.B35DefAremp.Value,

                                    B12DefRemp = o.B12DefRemp.Value,
                                    B9DefRemp = o.B9DefRemp.Value,
                                    B6RDefRemp = o.B6RDefRemp.Value,
                                    B6VDefRemp = o.B6VDefRemp.Value,
                                    B3DefRemp = o.B3DefRemp.Value,
                                    B35DefRemp = o.B35DefRemp.Value,

                                    B12Afact = o.B12Afact.Value,
                                    B9Afact = o.B9Afact.Value,
                                    B6RAfact = o.B6RAfact.Value,
                                    B6VAfact = o.B6VAfact.Value,
                                    B3Afact = o.B3Afact.Value,
                                    B35Afact = o.B35Afact.Value,

                                    Commentaire = o.Commentaire,
                                    etat = o.etat.Value,
                                    Tonnage = (decimal)o.Tonnage

                                };


                    return query.OrderByDescending(x => x.id).ToList();
                }
                else
                {
                    if (string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                    select o;


                    }


                    if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                    {
                        baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                    where o.Reference.Contains(filtreMotCle)
                                    select o;

                    }
                    else if (!string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                    where  o.Client.Code == Convert.ToInt32(filtreMotCle)
                                    || o.Employe.matricule == Convert.ToInt32(filtreMotCle)
                                    select o;

                    }

                    var query = from o in baseQuery
                                select new FicheVenteBouteilleDto
                                {
                                    id = o.id,
                                    idTypeFiche = o.idTypeFiche.Value,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,
                                    VehiculeImmatricule = o.VehiculeImmatricule,
                                    NbrePersonnes = o.NbrePersonnes.Value,
                                    matriculeEmploye = o.matriculeEmploye.Value,
                                    NomEmploye = o.Employe.nomComplet,
                                    TypeFiche = o.TypeFicheVente.libelle,

                                    B12Client = o.B12Client.Value,
                                    B9Client = o.B9Client.Value,
                                    B6RClient = o.B6RClient.Value,
                                    B6VClient = o.B6VClient.Value,
                                    B3Client = o.B3Client.Value,
                                    B35Client = o.B35Client.Value,


                                    B12VideRec = o.B12VideRec.Value,
                                    B9VideRec = o.B9VideRec.Value,
                                    B6RVideRec = o.B6RVideRec.Value,
                                    B6VVideRec = o.B6VVideRec.Value,
                                    B3VideRec = o.B3VideRec.Value,
                                    B35VideRec = o.B35VideRec.Value,

                                    B12VideDef = o.B12VideDef.Value,
                                    B9VideDef = o.B9VideDef.Value,
                                    B6RVideDef = o.B6RVideDef.Value,
                                    B6VVideDef = o.B6VVideDef.Value,
                                    B3VideDef = o.B3VideDef.Value,
                                    B35VideDef = o.B35VideDef.Value,

                                    B12PleinRemp = o.B12PleinRemp.Value,
                                    B9PleinRemp = o.B9PleinRemp.Value,
                                    B6RPleinRemp = o.B6RPleinRemp.Value,
                                    B6VPleinRemp = o.B6VPleinRemp.Value,
                                    B3PleinRemp = o.B3PleinRemp.Value,
                                    B35PleinRemp = o.B35PleinRemp.Value,

                                    B12PleinDef = o.B12PleinDef.Value,
                                    B9PleinDef = o.B9PleinDef.Value,
                                    B6RPleinDef = o.B6RPleinDef.Value,
                                    B6VPleinDef = o.B6VPleinDef.Value,
                                    B3PleinDef = o.B3PleinDef.Value,
                                    B35PleinDef = o.B35PleinDef.Value,

                                    B12DefAremp = o.B12DefAremp.Value,
                                    B9DefAremp = o.B9DefAremp.Value,
                                    B6RDefAremp = o.B6RDefAremp.Value,
                                    B6VDefAremp = o.B6VDefAremp.Value,
                                    B3DefAremp = o.B3DefAremp.Value,
                                    B35DefAremp = o.B35DefAremp.Value,

                                    B12DefRemp = o.B12DefRemp.Value,
                                    B9DefRemp = o.B9DefRemp.Value,
                                    B6RDefRemp = o.B6RDefRemp.Value,
                                    B6VDefRemp = o.B6VDefRemp.Value,
                                    B3DefRemp = o.B3DefRemp.Value,
                                    B35DefRemp = o.B35DefRemp.Value,

                                    B12Afact = o.B12Afact.Value,
                                    B9Afact = o.B9Afact.Value,
                                    B6RAfact = o.B6RAfact.Value,
                                    B6VAfact = o.B6VAfact.Value,
                                    B3Afact = o.B3Afact.Value,
                                    B35Afact = o.B35Afact.Value,

                                    Commentaire = o.Commentaire,
                                    etat = o.etat.Value,
                                    Tonnage = (decimal)o.Tonnage


                                };


                    return query.OrderByDescending(x => x.id).ToList();
                }





            }

        }




        public List<FicheVenteBouteilleDto> ChercherListeFicheVenteBouteilleParClient(string filtreMotCle, DateTime? dateDebut, DateTime? dateFin, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                IEnumerable<FicheVenteBouteille> baseQuery = Enumerable.Empty<FicheVenteBouteille>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);




               

                    //if (string.IsNullOrEmpty(filtreMotCle))
                    //{
                    //    baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                    //                select o;

                    //}

                     if (!string.IsNullOrEmpty(filtreMotCle))
                     {
                        baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                    where o.Client.Code == Convert.ToInt32(filtreMotCle) && o.Date >= dateDebut && o.Date <= dateFin
                                   
                                    select o;

                     }






                    var query = from o in baseQuery
                                select new FicheVenteBouteilleDto
                                {
                                    id = o.id,
                                    idTypeFiche = o.idTypeFiche.Value,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,
                                    VehiculeImmatricule = o.VehiculeImmatricule,
                                    NbrePersonnes = o.NbrePersonnes.Value,
                                    matriculeEmploye = o.matriculeEmploye.Value,
                                    NomEmploye = o.Employe.nomComplet,
                                    TypeFiche = o.TypeFicheVente.libelle,

                                    B12Client = o.B12Client.Value,
                                    B9Client = o.B9Client.Value,
                                    B6RClient = o.B6RClient.Value,
                                    B6VClient = o.B6VClient.Value,
                                    B3Client = o.B3Client.Value,
                                    B35Client = o.B35Client.Value,


                                    B12VideRec = o.B12VideRec.Value,
                                    B9VideRec = o.B9VideRec.Value,
                                    B6RVideRec = o.B6RVideRec.Value,
                                    B6VVideRec = o.B6VVideRec.Value,
                                    B3VideRec = o.B3VideRec.Value,
                                    B35VideRec = o.B35VideRec.Value,

                                    B12VideDef = o.B12VideDef.Value,
                                    B9VideDef = o.B9VideDef.Value,
                                    B6RVideDef = o.B6RVideDef.Value,
                                    B6VVideDef = o.B6VVideDef.Value,
                                    B3VideDef = o.B3VideDef.Value,
                                    B35VideDef = o.B35VideDef.Value,

                                    B12PleinRemp = o.B12PleinRemp.Value,
                                    B9PleinRemp = o.B9PleinRemp.Value,
                                    B6RPleinRemp = o.B6RPleinRemp.Value,
                                    B6VPleinRemp = o.B6VPleinRemp.Value,
                                    B3PleinRemp = o.B3PleinRemp.Value,
                                    B35PleinRemp = o.B35PleinRemp.Value,

                                    B12PleinDef = o.B12PleinDef.Value,
                                    B9PleinDef = o.B9PleinDef.Value,
                                    B6RPleinDef = o.B6RPleinDef.Value,
                                    B6VPleinDef = o.B6VPleinDef.Value,
                                    B3PleinDef = o.B3PleinDef.Value,
                                    B35PleinDef = o.B35PleinDef.Value,

                                    B12DefAremp = o.B12DefAremp.Value,
                                    B9DefAremp = o.B9DefAremp.Value,
                                    B6RDefAremp = o.B6RDefAremp.Value,
                                    B6VDefAremp = o.B6VDefAremp.Value,
                                    B3DefAremp = o.B3DefAremp.Value,
                                    B35DefAremp = o.B35DefAremp.Value,

                                    B12DefRemp = o.B12DefRemp.Value,
                                    B9DefRemp = o.B9DefRemp.Value,
                                    B6RDefRemp = o.B6RDefRemp.Value,
                                    B6VDefRemp = o.B6VDefRemp.Value,
                                    B3DefRemp = o.B3DefRemp.Value,
                                    B35DefRemp = o.B35DefRemp.Value,

                                    B12Afact = o.B12Afact.Value,
                                    B9Afact = o.B9Afact.Value,
                                    B6RAfact = o.B6RAfact.Value,
                                    B6VAfact = o.B6VAfact.Value,
                                    B3Afact = o.B3Afact.Value,
                                    B35Afact = o.B35Afact.Value,

                                    Commentaire = o.Commentaire,
                                    etat = o.etat.Value,
                                    Tonnage = (decimal)o.Tonnage

                                };


                    return query.OrderByDescending(x => x.id).ToList();
             
            }

        }





        public List<FicheVenteBouteilleDto> ChercherListeFicheVenteBouteilleInstance(string filtreMotCle, int? idEntite, string login)
        {
            

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                IEnumerable<FicheVenteBouteille> baseQuery = Enumerable.Empty<FicheVenteBouteille>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);
               

                    if (string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                    where o.etat == 1
                                    select o;

                    }



                    if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                    {
                        baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                    where o.etat == 1 && (o.Reference.Contains(filtreMotCle))
                                    select o;

                    }
                    else if (!string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                    where o.etat == 1 && (o.Client.Code == Convert.ToInt32(filtreMotCle)
                                    || o.Employe.matricule == Convert.ToInt32(filtreMotCle))
                                    select o;

                    }






                    var query = from o in baseQuery
                                select new FicheVenteBouteilleDto
                                {
                                    id = o.id,
                                    idTypeFiche = o.idTypeFiche.Value,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,
                                    VehiculeImmatricule = o.VehiculeImmatricule,
                                    NbrePersonnes = o.NbrePersonnes.Value,
                                    matriculeEmploye = o.matriculeEmploye.Value,
                                    NomEmploye = o.Employe.nomComplet,
                                    TypeFiche = o.TypeFicheVente.libelle,

                                    B12Client = o.B12Client.Value,
                                    B9Client = o.B9Client.Value,
                                    B6RClient = o.B6RClient.Value,
                                    B6VClient = o.B6VClient.Value,
                                    B3Client = o.B3Client.Value,
                                    B35Client = o.B35Client.Value,


                                    B12VideRec = o.B12VideRec.Value,
                                    B9VideRec = o.B9VideRec.Value,
                                    B6RVideRec = o.B6RVideRec.Value,
                                    B6VVideRec = o.B6VVideRec.Value,
                                    B3VideRec = o.B3VideRec.Value,
                                    B35VideRec = o.B35VideRec.Value,

                                    B12VideDef = o.B12VideDef.Value,
                                    B9VideDef = o.B9VideDef.Value,
                                    B6RVideDef = o.B6RVideDef.Value,
                                    B6VVideDef = o.B6VVideDef.Value,
                                    B3VideDef = o.B3VideDef.Value,
                                    B35VideDef = o.B35VideDef.Value,

                                    B12PleinRemp = o.B12PleinRemp.Value,
                                    B9PleinRemp = o.B9PleinRemp.Value,
                                    B6RPleinRemp = o.B6RPleinRemp.Value,
                                    B6VPleinRemp = o.B6VPleinRemp.Value,
                                    B3PleinRemp = o.B3PleinRemp.Value,
                                    B35PleinRemp = o.B35PleinRemp.Value,

                                    B12PleinDef = o.B12PleinDef.Value,
                                    B9PleinDef = o.B9PleinDef.Value,
                                    B6RPleinDef = o.B6RPleinDef.Value,
                                    B6VPleinDef = o.B6VPleinDef.Value,
                                    B3PleinDef = o.B3PleinDef.Value,
                                    B35PleinDef = o.B35PleinDef.Value,

                                    B12DefAremp = o.B12DefAremp.Value,
                                    B9DefAremp = o.B9DefAremp.Value,
                                    B6RDefAremp = o.B6RDefAremp.Value,
                                    B6VDefAremp = o.B6VDefAremp.Value,
                                    B3DefAremp = o.B3DefAremp.Value,
                                    B35DefAremp = o.B35DefAremp.Value,

                                    B12DefRemp = o.B12DefRemp.Value,
                                    B9DefRemp = o.B9DefRemp.Value,
                                    B6RDefRemp = o.B6RDefRemp.Value,
                                    B6VDefRemp = o.B6VDefRemp.Value,
                                    B3DefRemp = o.B3DefRemp.Value,
                                    B35DefRemp = o.B35DefRemp.Value,

                                    B12Afact = o.B12Afact.Value,
                                    B9Afact = o.B9Afact.Value,
                                    B6RAfact = o.B6RAfact.Value,
                                    B6VAfact = o.B6VAfact.Value,
                                    B3Afact = o.B3Afact.Value,
                                    B35Afact = o.B35Afact.Value,

                                    Commentaire = o.Commentaire,
                                    etat = o.etat.Value,
                                    Tonnage = (decimal)o.Tonnage

                                };


                    return query.OrderByDescending(x => x.id).ToList();

            }
        }



        public List<FicheVenteBouteilleDto> ChercherListeFicheVenteBouteilleAcloturer(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                IEnumerable<FicheVenteBouteille> baseQuery = Enumerable.Empty<FicheVenteBouteille>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                where o.etat == 3
                                select o;

                }



                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                where o.etat == 3 && (o.Date == Convert.ToDateTime(filtreMotCle))
                                select o;

                }
                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                where o.etat == 3 && (o.Client.Code == Convert.ToInt32(filtreMotCle)
                                || o.Employe.matricule == Convert.ToInt32(filtreMotCle))
                                select o;

                }



                var query = from o in baseQuery
                            select new FicheVenteBouteilleDto
                            {
                                id = o.id,
                                idTypeFiche = o.idTypeFiche.Value,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,
                                VehiculeImmatricule = o.VehiculeImmatricule,
                                NbrePersonnes = o.NbrePersonnes.Value,
                                matriculeEmploye = o.matriculeEmploye.Value,
                                NomEmploye = o.Employe.nomComplet,
                                TypeFiche = o.TypeFicheVente.libelle,

                                B12Client = o.B12Client.Value,
                                B9Client = o.B9Client.Value,
                                B6RClient = o.B6RClient.Value,
                                B6VClient = o.B6VClient.Value,
                                B3Client = o.B3Client.Value,
                                B35Client = o.B35Client.Value,


                                B12VideRec = o.B12VideRec.Value,
                                B9VideRec = o.B9VideRec.Value,
                                B6RVideRec = o.B6RVideRec.Value,
                                B6VVideRec = o.B6VVideRec.Value,
                                B3VideRec = o.B3VideRec.Value,
                                B35VideRec = o.B35VideRec.Value,

                                B12VideDef = o.B12VideDef.Value,
                                B9VideDef = o.B9VideDef.Value,
                                B6RVideDef = o.B6RVideDef.Value,
                                B6VVideDef = o.B6VVideDef.Value,
                                B3VideDef = o.B3VideDef.Value,
                                B35VideDef = o.B35VideDef.Value,

                                B12PleinRemp = o.B12PleinRemp.Value,
                                B9PleinRemp = o.B9PleinRemp.Value,
                                B6RPleinRemp = o.B6RPleinRemp.Value,
                                B6VPleinRemp = o.B6VPleinRemp.Value,
                                B3PleinRemp = o.B3PleinRemp.Value,
                                B35PleinRemp = o.B35PleinRemp.Value,

                                B12PleinDef = o.B12PleinDef.Value,
                                B9PleinDef = o.B9PleinDef.Value,
                                B6RPleinDef = o.B6RPleinDef.Value,
                                B6VPleinDef = o.B6VPleinDef.Value,
                                B3PleinDef = o.B3PleinDef.Value,
                                B35PleinDef = o.B35PleinDef.Value,

                                B12DefAremp = o.B12DefAremp.Value,
                                B9DefAremp = o.B9DefAremp.Value,
                                B6RDefAremp = o.B6RDefAremp.Value,
                                B6VDefAremp = o.B6VDefAremp.Value,
                                B3DefAremp = o.B3DefAremp.Value,
                                B35DefAremp = o.B35DefAremp.Value,

                                B12DefRemp = o.B12DefRemp.Value,
                                B9DefRemp = o.B9DefRemp.Value,
                                B6RDefRemp = o.B6RDefRemp.Value,
                                B6VDefRemp = o.B6VDefRemp.Value,
                                B3DefRemp = o.B3DefRemp.Value,
                                B35DefRemp = o.B35DefRemp.Value,

                                B12Afact = o.B12Afact.Value,
                                B9Afact = o.B9Afact.Value,
                                B6RAfact = o.B6RAfact.Value,
                                B6VAfact = o.B6VAfact.Value,
                                B3Afact = o.B3Afact.Value,
                                B35Afact = o.B35Afact.Value,

                                Commentaire = o.Commentaire,
                                etat = o.etat.Value

                            };


                return query.OrderByDescending(x => x.id).ToList();
            }

        }



        public List<FicheVenteBouteilleDto> ChercherListeNbrePersonnes(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                IEnumerable<FicheVenteBouteille> baseQuery = Enumerable.Empty<FicheVenteBouteille>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                where o.idTypeFiche != 2 && (o.Date == DateTime.Now.Date)
                                select o;

                }



                if (!string.IsNullOrEmpty(filtreMotCle))
                { 
                    baseQuery = from o in _context.FicheVenteBouteilles.AsEnumerable()
                                where o.idTypeFiche != 2 && (o.Date == Convert.ToDateTime(filtreMotCle))
                                select o;

                }
                


                var query = from o in baseQuery
                            where o.idTypeFiche != 2

                            select new FicheVenteBouteilleDto
                            {
                                id = o.id,
                                idTypeFiche = o.idTypeFiche.Value,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,
                                VehiculeImmatricule = o.VehiculeImmatricule,
                                NbrePersonnes = o.NbrePersonnes.Value,
                                TypeFiche = o.TypeFicheVente.libelle,
                                etat = o.etat.Value

                            };


                return query.OrderByDescending(x => x.id).ToList();
            }

        }


        public void Ajouter(FicheVenteBouteilleDto dto)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);
                var employe = _context.Employes.Find(dto.matriculeEmploye);
               

                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }

                if (employe == null)
                {

                    employe = new Employe
                    {
                        matricule = dto.matriculeEmploye,
                        nomComplet = dto.NomEmploye,

                    };
                }



                var entity = new FicheVenteBouteille
                {

                    Reference = dto.Reference,
                    Date = dto.Date,
                    Client = client,
                    VehiculeImmatricule = dto.VehiculeImmatricule,
                    NbrePersonnes = dto.NbrePersonnes,
                    idTypeFiche = dto.idTypeFiche,
                    Employe = employe,


                    B12Client = dto.B12Client,
                    B9Client = dto.B9Client,
                    B6RClient = dto.B6RClient,
                    B6VClient = dto.B6VClient,
                    B3Client = dto.B3Client,
                    B35Client = dto.B35Client,


                    B12VideRec = dto.B12VideRec,
                    B9VideRec = dto.B9VideRec,
                    B6RVideRec = dto.B6RVideRec,
                    B6VVideRec = dto.B6VVideRec,
                    B3VideRec = dto.B3VideRec,
                    B35VideRec = dto.B35VideRec,

                    B12VideDef = dto.B12VideDef,
                    B9VideDef = dto.B9VideDef,
                    B6RVideDef = dto.B6RVideDef,
                    B6VVideDef = dto.B6VVideDef,
                    B3VideDef = dto.B3VideDef,
                    B35VideDef = dto.B35VideDef,

                    B12PleinRemp = dto.B12PleinRemp,
                    B9PleinRemp = dto.B9PleinRemp,
                    B6RPleinRemp = dto.B6RPleinRemp,
                    B6VPleinRemp = dto.B6VPleinRemp,
                    B3PleinRemp = dto.B3PleinRemp,
                    B35PleinRemp = dto.B35PleinRemp,

                    B12PleinDef = dto.B12PleinDef,
                    B9PleinDef = dto.B9PleinDef,
                    B6RPleinDef = dto.B6RPleinDef,
                    B6VPleinDef = dto.B6VPleinDef,
                    B3PleinDef = dto.B3PleinDef,
                    B35PleinDef = dto.B35PleinDef,

                    B12DefAremp = dto.B12DefAremp,
                    B9DefAremp = dto.B9DefAremp,
                    B6RDefAremp = dto.B6RDefAremp,
                    B6VDefAremp = dto.B6VDefAremp,
                    B3DefAremp = dto.B3DefAremp,
                    B35DefAremp = dto.B35DefAremp,

                    B12DefRemp = dto.B12DefRemp,
                    B9DefRemp = dto.B9DefRemp,
                    B6RDefRemp = dto.B6RDefRemp,
                    B6VDefRemp = dto.B6VDefRemp,
                    B3DefRemp = dto.B3DefRemp,
                    B35DefRemp = dto.B35DefRemp,

                    B12Afact = dto.B12Afact,
                    B9Afact = dto.B9Afact,
                    B6RAfact = dto.B6RAfact,
                    B6VAfact = dto.B6VAfact,
                    B3Afact = dto.B3Afact,
                    B35Afact = dto.B35Afact,

                    Commentaire = dto.Commentaire,
                    etat = 1,

                    Tonnage = (decimal)(dto.B12Afact * 12.5 + dto.B9Afact * 9 + dto.B6RAfact * 6 + dto.B6VAfact * 6 + dto.B3Afact * 2.75 + dto.B35Afact * 35) / 1000

                



                };
                _context.FicheVenteBouteilles.Add(entity);
                try
                {
                     _context.SaveChanges();

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }


        public FicheVenteBouteilleDto GetFichVenteParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.FicheVenteBouteilles.Find(id);

                var dto = new FicheVenteBouteilleDto
                {


                    id = o.id,
                    idTypeFiche = o.idTypeFiche.Value,
                    Reference = o.Reference,
                    Date = o.Date.Value,
                    CodeClient = o.CodeClient.Value,
                    NomClient = o.Client.nomComplet,
                    VehiculeImmatricule = o.VehiculeImmatricule,
                    NbrePersonnes = o.NbrePersonnes.Value,
                    matriculeEmploye = o.matriculeEmploye.Value,
                    NomEmploye = o.Employe.nomComplet,
                    TypeFiche = o.TypeFicheVente.libelle,

                    B12Client = o.B12Client.Value,
                    B9Client = o.B9Client.Value,
                    B6RClient = o.B6RClient.Value,
                    B6VClient = o.B6VClient.Value,
                    B3Client = o.B3Client.Value,
                    B35Client = o.B35Client.Value,


                    B12VideRec = o.B12VideRec.Value,
                    B9VideRec = o.B9VideRec.Value,
                    B6RVideRec = o.B6RVideRec.Value,
                    B6VVideRec = o.B6VVideRec.Value,
                    B3VideRec = o.B3VideRec.Value,
                    B35VideRec = o.B35VideRec.Value,

                    B12VideDef = o.B12VideDef.Value,
                    B9VideDef = o.B9VideDef.Value,
                    B6RVideDef = o.B6RVideDef.Value,
                    B6VVideDef = o.B6VVideDef.Value,
                    B3VideDef = o.B3VideDef.Value,
                    B35VideDef = o.B35VideDef.Value,

                    B12PleinRemp = o.B12PleinRemp.Value,
                    B9PleinRemp = o.B9PleinRemp.Value,
                    B6RPleinRemp = o.B6RPleinRemp.Value,
                    B6VPleinRemp = o.B6VPleinRemp.Value,
                    B3PleinRemp = o.B3PleinRemp.Value,
                    B35PleinRemp = o.B35PleinRemp.Value,

                    B12PleinDef = o.B12PleinDef.Value,
                    B9PleinDef = o.B9PleinDef.Value,
                    B6RPleinDef = o.B6RPleinDef.Value,
                    B6VPleinDef = o.B6VPleinDef.Value,
                    B3PleinDef = o.B3PleinDef.Value,
                    B35PleinDef = o.B35PleinDef.Value,

                    B12DefAremp = o.B12DefAremp.Value,
                    B9DefAremp = o.B9DefAremp.Value,
                    B6RDefAremp = o.B6RDefAremp.Value,
                    B6VDefAremp = o.B6VDefAremp.Value,
                    B3DefAremp = o.B3DefAremp.Value,
                    B35DefAremp = o.B35DefAremp.Value,

                    B12DefRemp = o.B12DefRemp.Value,
                    B9DefRemp = o.B9DefRemp.Value,
                    B6RDefRemp = o.B6RDefRemp.Value,
                    B6VDefRemp = o.B6VDefRemp.Value,
                    B3DefRemp = o.B3DefRemp.Value,
                    B35DefRemp = o.B35DefRemp.Value,

                    B12Afact = o.B12Afact.Value,
                    B9Afact = o.B9Afact.Value,
                    B6RAfact = o.B6RAfact.Value,
                    B6VAfact = o.B6VAfact.Value,
                    B3Afact = o.B3Afact.Value,
                    B35Afact = o.B35Afact.Value,

                    Commentaire = o.Commentaire,
                    etat = o.etat.Value,
                    Tonnage = (decimal)o.Tonnage



                };


                return dto;

            }

        }



        public void Modifier(FicheVenteBouteilleDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);
                var employe = _context.Employes.Find(dto.matriculeEmploye);
              


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }

                if (employe == null)
                {

                    employe = new Employe
                    {
                        matricule = dto.matriculeEmploye,
                        nomComplet = dto.NomEmploye,

                    };
                }


                var entity = _context.FicheVenteBouteilles.Find(dto.id);
                if (entity != null)

                {

                    entity.Reference = dto.Reference;
                    entity.Date = dto.Date;
                    entity.Client = client;
                    entity.VehiculeImmatricule = dto.VehiculeImmatricule;
                    entity.NbrePersonnes = dto.NbrePersonnes;
                    entity.idTypeFiche = dto.idTypeFiche;
                    entity.Employe = employe;


                    entity.B12Client = dto.B12Client;
                    entity.B9Client = dto.B9Client;
                    entity.B6RClient = dto.B6RClient;
                    entity.B6VClient = dto.B6VClient;
                    entity.B3Client = dto.B3Client;
                    entity.B35Client = dto.B35Client;


                    entity.B12VideRec = dto.B12VideRec;
                    entity.B9VideRec = dto.B9VideRec;
                    entity.B6RVideRec = dto.B6RVideRec;
                    entity.B6VVideRec = dto.B6VVideRec;
                    entity.B3VideRec = dto.B3VideRec;
                    entity.B35VideRec = dto.B35VideRec;

                    entity.B12VideDef = dto.B12VideDef;
                    entity.B9VideDef = dto.B9VideDef;
                    entity.B6RVideDef = dto.B6RVideDef;
                    entity.B6VVideDef = dto.B6VVideDef;
                    entity.B3VideDef = dto.B3VideDef;
                    entity.B35VideDef = dto.B35VideDef;

                    entity.B12PleinRemp = dto.B12PleinRemp;
                    entity.B9PleinRemp = dto.B9PleinRemp;
                    entity.B6RPleinRemp = dto.B6RPleinRemp;
                    entity.B6VPleinRemp = dto.B6VPleinRemp;
                    entity.B3PleinRemp = dto.B3PleinRemp;
                    entity.B35PleinRemp = dto.B35PleinRemp;

                    entity.B12PleinDef = dto.B12PleinDef;
                    entity.B9PleinDef = dto.B9PleinDef;
                    entity.B6RPleinDef = dto.B6RPleinDef;
                    entity.B6VPleinDef = dto.B6VPleinDef;
                    entity.B3PleinDef = dto.B3PleinDef;
                    entity.B35PleinDef = dto.B35PleinDef;

                    entity.B12DefAremp = dto.B12DefAremp;
                    entity.B9DefAremp = dto.B9DefAremp;
                    entity.B6RDefAremp = dto.B6RDefAremp;
                    entity.B6VDefAremp = dto.B6VDefAremp;
                    entity.B3DefAremp = dto.B3DefAremp;
                    entity.B35DefAremp = dto.B35DefAremp;

                    entity.B12DefRemp = dto.B12DefRemp;
                    entity.B9DefRemp = dto.B9DefRemp;
                    entity.B6RDefRemp = dto.B6RDefRemp;
                    entity.B6VDefRemp = dto.B6VDefRemp;
                    entity.B3DefRemp = dto.B3DefRemp;
                    entity.B35DefRemp = dto.B35DefRemp;

                    entity.B12Afact = dto.B12Afact;
                    entity.B9Afact = dto.B9Afact;
                    entity.B6RAfact = dto.B6RAfact;
                    entity.B6VAfact = dto.B6VAfact;
                    entity.B3Afact = dto.B3Afact;
                    entity.B35Afact = dto.B35Afact;

                    entity.Commentaire = dto.Commentaire;

                    entity.Tonnage = (decimal)(dto.B12Afact * 12.5 + dto.B9Afact * 9 + dto.B6RAfact * 6 + dto.B6VAfact * 6 + dto.B3Afact * 2.75 + dto.B35Afact * 35) / 1000;

                   
                    if (dto.idEntite == 4)
                        entity.etat = entity.etat;
                    else if (dto.idEntite == 5)
                        entity.etat = 2;
                    else
                        entity.etat = entity.etat;

                    


                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }


        public void EnInstance(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                if (idEntite == 5)
                {
                    var entity = _context.FicheVenteBouteilles.Find(id);
                    entity.etat = 2;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                if (idEntite == 4)
                {
                    var entity = _context.FicheVenteBouteilles.Find(id);
                    entity.etat = 1;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
                }
               

            }

        }




        public void Valider(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.FicheVenteBouteilles.Find(id);
                entity.etat = 3;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }



        public void CloturerParDate(int idEntite, DateTime date)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var entity = _context.FicheVenteBouteilles.Where(x => x.Date == date.Date).Select(x => x.id).ToArray();

                foreach (int id in entity)
                {

                    FicheVenteBouteille fiche = _context.FicheVenteBouteilles.Find(id);
                    fiche.etat = 4;
                    //var sortie = _context.Sorties.Where(x => x.id == item.id).SingleOrDefault();
                    _context.Entry(fiche).State = EntityState.Modified;
                    _context.SaveChanges();

                }


            }
        }




        public int AredpParClient(int codeClient , DateTime date)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var NbreAredp = _context.AREDPs.Where(x=> x.CodeClient == codeClient && x.Date == date).Count();

                return NbreAredp;

            }

        }


        public void Cloturer(int[] details)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                if (details == null) return;

                foreach (int id in details)
                {


                    FicheVenteBouteille fiche = _context.FicheVenteBouteilles.Find(id);
                    fiche.etat = 4;
                    _context.Entry(fiche).State = EntityState.Modified;
                    _context.SaveChanges();

                }
            }
        }


       
        public List<TypeFicheDto> GetListeTypeFiche()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            { 

                var list = _context.TypeFicheVentes.ToList();
                var listeDto = new List<TypeFicheDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new TypeFicheDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }

            
        }

        public TypeFicheDto GetTypeFicheParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.TypeFicheVentes.Find(id);

                var dto = new TypeFicheDto
                {
                    id = entity.id,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public EmployeDto GetEmployeParId(int matricule)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Employes.Find(matricule);
                if (entity == null) return null;
                var dto = new EmployeDto
                {
                    matricule = entity.matricule,
                    nomComplet = entity.nomComplet,

                };

                return dto;
            }



        }

        public int VerifierExistFiche(string reference)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var nbreFiche = _context.FicheVenteBouteilles.Where(x => x.Reference == reference).Count();

                return nbreFiche;
            }

        }


        public int TotalInstance()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.FicheVenteBouteilles.Where(x => x.etat == 1).Count();

                return Total;
            }


        }

        public int TotalCloturer()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.FicheVenteBouteilles.Where(x => x.etat == 3).Count();

                return Total;
            }


        }


        public int TotalNbrePersonnes(string filtreMotCle)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                DateTime dateNull = DateTime.Now.Date;
                DateTime date = Convert.ToDateTime(filtreMotCle);
                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    var Total = _context.FicheVenteBouteilles.Where(x => x.etat != 2 && x.Date == dateNull ).Select(x => x.NbrePersonnes.Value).DefaultIfEmpty(0).Sum();

                    return Total;

                }

                else
                {
                    var Total = _context.FicheVenteBouteilles.Where(x => x.etat != 2 && x.Date == date.Date).Select(x => x.NbrePersonnes.Value).DefaultIfEmpty(0).Sum();

                    return Total;
                }

            }


        }


    }
}