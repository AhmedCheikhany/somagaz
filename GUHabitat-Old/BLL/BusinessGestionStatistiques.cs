﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using GUHabitat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionStatistiques
    {

        private static BusinessGestionStatistiques instance;
        private BusinessGestionStatistiques() { }

        public static BusinessGestionStatistiques Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionStatistiques();
                }
                return instance;
            }
        }


        public List<BonusClientsDto> ChercherListeBonusClients(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var FicheVenteClient = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0).Select(x => x.CodeClient).Distinct().ToList();
                var AredpClient = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0).Select(x=>x.CodeClient).Distinct().ToList();

                //Combiner le deux listes
                var AredpFicheClients = AredpClient.Except(FicheVenteClient).ToList();

                var Listdto = new List<BonusClientsDto>();
               
               
                // ajouter la diference de Aredp en FicheVenteBouteille
                foreach (var item in AredpFicheClients)
                {

                    FicheVenteClient.Add(item);

                }
                




                foreach (var codeClient in FicheVenteClient)
                {

                    BonusClientsDto dto = new BonusClientsDto();

                   
                    dto.dateDebut = dateDebut.Value;
                    dto.dateFin = dateFin.Value;
                    dto.CodeClient = codeClient.Value;
                    dto.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();
                    dto.B12Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum());
                    dto.B9Afact = (_context.FicheVenteBouteilles.Where(x =>  x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x =>  x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum());
                    dto.B6RAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum());
                    dto.B6VAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum());
                    dto.B3Afact = (_context.FicheVenteBouteilles.Where(x =>  x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x =>  x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum());
                    dto.B35Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x =>  x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum());

                    dto.Tonnage = (decimal)(dto.B12Afact * 12.5 + dto.B9Afact * 9 + dto.B6RAfact * 6 + dto.B6VAfact * 6 + dto.B3Afact * 2.75 + dto.B35Afact * 35) / 1000;

                    dto.MontantMru = (decimal)dto.Tonnage * 1900;
                    dto.MontantMro = (decimal)dto.Tonnage * 19000;

                    Listdto.Add(dto);
                    

                }


                return Listdto.OrderByDescending(x => x.Tonnage).ToList();



                


            };


        }





        public List<VenteClientsDto> ChercherListeVentesClients(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var FicheVenteClient = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0).Select(x => x.CodeClient).Distinct().ToList();
                var AredpClient = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0).Select(x => x.CodeClient).Distinct().ToList();

                //Combiner le deux listes
                var AredpFicheClients = AredpClient.Except(FicheVenteClient).ToList();

                var Listdto = new List<VenteClientsDto>();


                // ajouter la diference de Aredp en FicheVenteBouteille
                foreach (var item in AredpFicheClients)
                {

                    FicheVenteClient.Add(item);

                }





                foreach (var codeClient in FicheVenteClient)
                {

                    VenteClientsDto dto = new VenteClientsDto();


                    dto.dateDebut = dateDebut.Value;
                    dto.dateFin = dateFin.Value;
                    dto.CodeClient = codeClient.Value;
                    dto.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();
                    dto.B12Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum());
                    dto.B9Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum());
                    dto.B6RAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum());
                    dto.B6VAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum());
                    dto.B3Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum());
                    dto.B35Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum());

                    dto.Tonnage = (decimal)(dto.B12Afact * 12.5 + dto.B9Afact * 9 + dto.B6RAfact * 6 + dto.B6VAfact * 6 + dto.B3Afact * 2.75 + dto.B35Afact * 35) / 1000;

                    Listdto.Add(dto);
                }


                return Listdto.OrderByDescending(x => x.Tonnage).ToList();






            };


        }



        public List<BonusVracPartenairesDto> ChercherListeBonusVracPartenaires(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var BonChargementPartenaires = _context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 5).Select(x => x.CodeClient).Distinct().ToList();

                var LivraisonPartenaires = _context.Livraisons.Where(x => x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient != 0 && x.BonRetourCiterne.idTypeBonRetour == 2).Select(x => x.BonRetourCiterne.CodeClient).Distinct().ToList();


                //Combiner le deux listes
                var BonChargementEtLivraison = LivraisonPartenaires.Except(BonChargementPartenaires).ToList();


                var Listdto = new List<BonusVracPartenairesDto>();


                // ajouter la diference de Aredp en FicheVenteBouteille
                foreach (var item in BonChargementEtLivraison)
                {

                    BonChargementPartenaires.Add(item);

                }


               


                foreach (var codeClient in BonChargementPartenaires)
                {

                    BonusVracPartenairesDto dto = new BonusVracPartenairesDto();


                    dto.dateDebut = dateDebut.Value;
                    dto.dateFin = dateFin.Value;
                    dto.CodeClient = codeClient.Value;
                    dto.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();
                   
                    dto.Tonnage = (decimal)(_context.BonChargementCiternes.Where(x => x.CodeClient != 0 && x.idTypeBonChargement == 5 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum()) - (_context.Livraisons.Where(x => x.BonRetourCiterne.CodeClient != 0 && x.BonRetourCiterne.idTypeBonRetour == 2 && x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient == codeClient).Select(x => x.PoidsRetour.Value).DefaultIfEmpty(0).Sum());

                    dto.MontantMru = (decimal)dto.Tonnage * 1200;
                    dto.MontantMro = (decimal)dto.Tonnage * 12000;

                    Listdto.Add(dto);


                }

                return Listdto.OrderByDescending(x => x.Tonnage).ToList();

            

            };


        }



        public List<BonusVracPriveeDto> ChercherListeBonusVracPrivee(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var BonChargementPrivees = _context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 3).Select(x => x.CodeClient).Distinct().ToList();

                var Listdto = new List<BonusVracPriveeDto>();







                foreach (var codeClient in BonChargementPrivees)
                {

                    BonusVracPriveeDto dto = new BonusVracPriveeDto();


                    dto.dateDebut = dateDebut.Value;
                    dto.dateFin = dateFin.Value;
                    dto.CodeClient = codeClient.Value;
                    dto.NomClient = _context.Clients.Where(x => x.Code == codeClient).Select(x => x.nomComplet).SingleOrDefault();

                    dto.Tonnage = (decimal)(_context.BonChargementCiternes.Where(x => x.CodeClient != 0 && x.idTypeBonChargement == 3 && x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == codeClient).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum());

                    dto.MontantMru = (decimal)dto.Tonnage * 800;
                    dto.MontantMro = (decimal)dto.Tonnage * 8000;

                    Listdto.Add(dto);


                }


                return Listdto.OrderByDescending(x => x.Tonnage).ToList();
            };


        }


        //public ModelStatistiques GetStatistiqueGlobale(DateTime? dateDebut, DateTime? dateFin)
        //{
        //    using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
        //    {

        //        ModelStatistiques model = new ModelStatistiques();


        //        //ViewBag.dateDebut = dateDebut;
        //        //ViewBag.dateFin = dateFin;


        //        //Nombre d’emballage a facturer

        //        if (dateDebut == null) dateDebut = DateTime.Now.Date;
        //        if (dateFin == null) dateFin = DateTime.Now.Date;

        //        model.dateDebut = (DateTime)dateDebut;
        //        model.dateFin = (DateTime)dateFin;


        //        //Nombre d'emballage vide présenté par le client

        //        model.B12Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Client.Value).DefaultIfEmpty(0).Sum();
        //        model.B9Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Client.Value).DefaultIfEmpty(0).Sum();
        //        model.B6RClient = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RClient.Value).DefaultIfEmpty(0).Sum();
        //        model.B6VClient = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VClient.Value).DefaultIfEmpty(0).Sum();
        //        model.B3Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Client.Value).DefaultIfEmpty(0).Sum();
        //        model.B35Client = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Client.Value).DefaultIfEmpty(0).Sum();





        //        //Nombre d’emballage Vente

        //        model.B12Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
        //        model.B9Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
        //        model.B6RVente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B6VVente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B3Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
        //        model.B35Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();



        //        //Nombre d’emballage Vente Directe

        //        model.B12VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
        //        model.B9VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
        //        model.B6RVenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B6VVenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B3VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
        //        model.B35VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();





        //        //Nombre d’emballage dotation

        //        model.B12Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
        //        model.B9Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
        //        model.B6RDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B6VDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B3Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
        //        model.B35Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();




        //        //Nombre d’emballage a consigner

        //        model.B12Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B9Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B6RConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
        //        model.B6VConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
        //        model.B3Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B35Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();
        //        model.PresentoirConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.PresentoirDC.Value).DefaultIfEmpty(0).Sum();


        //        //Nombre d’emballage corps et charges a consigner

        //        model.B12CorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B9CorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B6RCorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
        //        model.B6VCorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
        //        model.B3CorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B35CorpsEtCharges = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();


        //        //Nombre d’emballage corps  a consigner

        //        model.B12Corps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B9Corps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B6RCorps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
        //        model.B6VCorps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
        //        model.B3Corps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B35Corps = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();



        //        //Nombre d’emballage Avoir

        //        model.B12Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B9Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B6RAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B6VAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B3Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum();
        //        model.B35Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum();





        //        //Nombre d’emballage Aredp

        //        model.B12Aredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum());
        //        model.B9Aredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum());
        //        model.B6RAredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum());
        //        model.B6VAredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum());
        //        model.B3Aredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum());
        //        model.B35Aredp = (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum());




        //        //Nombre d’emballage a facturer

        //        model.B12Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum());
        //        model.B9Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum());
        //        model.B6RAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum());
        //        model.B6VAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum());
        //        model.B3Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum());
        //        model.B35Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum());




        //        //Nombre d’emballage produisez

        //        model.B12Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum());
        //        model.B9Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum());
        //        model.B6RProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum());
        //        model.B6VProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum());
        //        model.B3Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum());
        //        model.B35Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum());



        //        // Tonnage Manquant et Restant Avoir



        //        decimal pourcentageB12 = (Convert.ToDecimal("12,5") * 40) / 100; ;
        //        decimal pourcentageB9 = (9 * 40) / 100; ;
        //        decimal pourcentageB6R = (6 * 40) / 100;
        //        decimal pourcentageB6V = (6 * 40) / 100;
        //        decimal pourcentageB3 = (Convert.ToDecimal("2,75") * 40) / 100;
        //        decimal pourcentageB35 = (35 * 40) / 100;


        //        model.B12PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 1 && x.poidsRestant >= pourcentageB12).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
        //        model.B9PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 2 && x.poidsRestant >= pourcentageB9).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
        //        model.B6RPoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 3 && x.poidsRestant >= pourcentageB6R).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
        //        model.B6VPoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 4 && x.poidsRestant >= pourcentageB6V).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
        //        model.B3PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 5 && x.poidsRestant >= pourcentageB3).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();
        //        model.B35PoidManquantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 6 && x.poidsRestant >= pourcentageB35).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum();


        //        model.B12PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 1 && x.poidsRestant >= pourcentageB12).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
        //        model.B9PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 2 && x.poidsRestant >= pourcentageB9).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
        //        model.B6RPoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 3 && x.poidsRestant >= pourcentageB6R).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
        //        model.B6VPoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 4 && x.poidsRestant >= pourcentageB6V).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
        //        model.B3PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 5 && x.poidsRestant >= pourcentageB3).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();
        //        model.B35PoidRestantAvoir = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin && x.idProduit == 6 && x.poidsRestant >= pourcentageB35).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum();


        //        model.TotalPoidsManquantAvoir = (model.B12PoidManquantAvoir + model.B9PoidManquantAvoir + model.B6RPoidManquantAvoir + model.B6VPoidManquantAvoir + model.B3PoidManquantAvoir + model.B35PoidManquantAvoir) / 1000;
        //        model.TotalPoidsRestantAvoir = (model.B12PoidRestantAvoir + model.B9PoidRestantAvoir + model.B6RPoidRestantAvoir + model.B6VPoidRestantAvoir + model.B3PoidRestantAvoir + model.B35PoidRestantAvoir) / 1000;





        //        model.TotalPoidsManquantAredp = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin).Select(x => x.poidsManquant).DefaultIfEmpty(0).Sum() / 1000;
        //        model.TotalPoidsRestantAredp = (decimal)_context.AredpDetails.Where(x => x.AREDP.Date >= dateDebut && x.AREDP.Date <= dateFin).Select(x => x.poidsRestant).DefaultIfEmpty(0).Sum() / 1000;



        //        model.TonnageSomagaz = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == 0 && x.idTypeBonChargement == 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

        //        model.TotalRetourLivrer = (decimal)_context.Livraisons.Where(x => x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient != 0).Select(x => x.PoidsRetour.Value).DefaultIfEmpty(0).Sum();


        //        // Vrac Facturation
        //        model.TonnagePrivee = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1 && x.idTypeBonChargement != 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - model.TotalRetourLivrer;

        //        // Tous les Vracs sauf somagaz



        //        //model.TonnagePartenaire = (float)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

        //        model.AutresVrac = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

        //        model.TotalProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000 + model.TonnageSomagaz + model.AutresVrac;

        //        model.TotalCommerciale = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000 + model.TonnagePrivee;

        //        model.TotalConditionneeProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000;
        //        model.TotalConditionneeCommerciale = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000;



        //        model.B12Afact = model.B12Afact + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B9Afact = model.B9Afact + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B6RAfact = model.B6RAfact + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
        //        model.B6VAfact = model.B6VAfact + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
        //        model.B3Afact = model.B3Afact + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
        //        model.B35Afact = model.B35Afact + _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 2).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();




        //        return model;

        //    }



        //}



        public ModelStatistiqueGlobale GetStatistiqueGlobale(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
               

                ModelStatistiqueGlobale model = new ModelStatistiqueGlobale();



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;

                var DateCollaborateurString = "2023-01-01";
                var DateCollaborateur = Convert.ToDateTime(DateCollaborateurString).Date;

                var dateStockInterieur = _context.StockCentres.Where(x => x.Date == dateDebut && x.CodeCentre != 0 && x.CodeCentre != 9).Select(x => x.Date).ToList();

                var dateStock = _context.StockCentres.Where(x => x.Date == dateDebut && x.CodeCentre == 9).Select(x => x.Date).SingleOrDefault();


                if (dateStock != null || dateStockInterieur.Count() != 0)
                {

                    //Nombre d’emballage dotation

                    model.B12Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B9Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B6RDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
                    model.B6VDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
                    model.B3Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B35Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();




                    //Nombre d’emballage a consigner

                    model.B12Consignation =  _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
                    model.B9Consignation =  _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                    model.B6RConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                    model.B6VConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                    model.B3Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                    model.B35Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();




                    //Nombre d’emballage Avoir

                    model.B12Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum();
                    model.B9Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum();
                    model.B6RAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum();
                    model.B6VAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum();
                    model.B3Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum();
                    model.B35Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum();




                    //Nombre d’emballage a facturer

                    model.B12Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum());
                    model.B9Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum());
                    model.B6RAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum());
                    model.B6VAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum());
                    model.B3Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum());
                    model.B35Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum());




                    //Nombre d’emballage produisez

                    model.B12Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum());
                    model.B9Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum());
                    model.B6RProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum());
                    model.B6VProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum());
                    model.B3Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum());
                    model.B35Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum());



                    // Emballage  

                    // model.B12Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum(); 
                    // model.B9Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()     + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()     + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()    - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                    // model.B6REmballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                    // model.B6VEmballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                    // model.B3Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()     + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()     + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()    -  _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                    // model.B35Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 &&  x.Date  < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();

                    model.B12Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum(); 
                    model.B9Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()  - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()  - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                    model.B6REmballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                    model.B6VEmballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                    model.B3Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()  -  _context.FicheConsignations.Where(x =>x.Date < dateDebut).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()     - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()  -  _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                    model.B35Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();



                    model.StockNKTT = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum();  /*+ (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date > dateDebut && x.Date <= dateFin).Select(x => x.Depotage.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();*/

                    model.StockTheorieNKTT = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date == dateDebut).Select(x => x.StockTheorie.Value).DefaultIfEmpty(0).Sum();

                    model.StockFinNKTT = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date == dateDebut).Select(x => x.StockFin.Value).DefaultIfEmpty(0).Sum();

                    model.Depotage = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.Depotage.Value).DefaultIfEmpty(0).Sum();

                    model.GainPerteNktt = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.GainPerte.Value).DefaultIfEmpty(0).Sum();

                    model.TotalRetourLivrer = (decimal)_context.Livraisons.Where(x => x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient != 0).Select(x => x.PoidsRetour.Value).DefaultIfEmpty(0).Sum();


                    model.TonnageSomagaz = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == 0 && x.idTypeBonChargement == 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                    model.TotalRetour = (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();

                    model.TotalRetourSomagaz = (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin && x.idTypeBonRetour == 1 && x.CodeClient == 0).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();


                    // Vrac Facturation
                    model.TonnagePrivee = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1 && x.idTypeBonChargement != 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - model.TotalRetourLivrer;



                    // Tous les Vracs sauf somagaz

                    //model.TonnagePartenaire = (float)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();
                    model.TonnageCollaborateurEnvoiePipes = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();


                    model.AutresVrac = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                    model.TotalProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000 + model.TonnageSomagaz + model.AutresVrac + model.TonnageCollaborateurEnvoiePipes;

                    model.TotalFacturation = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000 + model.TonnagePrivee;

                    model.TotalConditionneeProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000;
                    model.TotalConditionneeCommerciale = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000;


                   
                    model.TonnageAvoirNktt = (decimal)(model.B12Avoir * 12.5 + model.B9Avoir * 9 + model.B6RAvoir * 6 + model.B6VAvoir * 6 + model.B3Avoir * 2.75 + model.B35Avoir * 35) / 1000;

                    


                    model.TonnageCollaborateurEnvoie = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();
                    model.TonnageCollaborateurReception = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idMouvementBonEnlvement == 1).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();

                    model.TonnageCollaborateurEnvoieInventaire = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();
                    model.TonnageCollaborateurReceptionInventaire = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idMouvementBonEnlvement == 1).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();


                    model.TonnageCollaborateurActuel = model.TonnageCollaborateurEnvoieInventaire - model.TonnageCollaborateurReceptionInventaire;

                    model.StockActuelNktt = model.StockNKTT > 0 ? (model.StockNKTT - model.TotalProduction + model.Depotage + model.TotalRetour + model.TonnageCollaborateurReception) : 0;


                    // Stock Centres



                    model.StockInterieur = (decimal)_context.StockCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum();

                    model.GainPerteInterieur = (decimal)_context.StockCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.GainPerte.Value).DefaultIfEmpty(0).Sum();


                    // Vente 

                    model.B12VenteInterieur = _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B9VenteInterieur = _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B6RVenteInterieur = _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B6VVenteInterieur = _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B3VenteInterieur = _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B35VenteInterieur = _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();


                    // Consignation 

                    model.B12ConsignationInterieur = _context.ConsignationCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                    model.B9ConsignationInterieur = _context.ConsignationCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                    model.B6RConsignationInterieur = _context.ConsignationCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                    model.B6VConsignationInterieur = _context.ConsignationCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                    model.B3ConsignationInterieur = _context.ConsignationCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                    model.B35ConsignationInterieur = _context.ConsignationCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                    // Dotation 

                    model.B12DotationInterieur = _context.DotationCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                    model.B9DotationInterieur = _context.DotationCentres.Where(x => x.CodeCentre != 0  && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                    model.B6RDotationInterieur = _context.DotationCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                    model.B6VDotationInterieur = _context.DotationCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                    model.B3DotationInterieur = _context.DotationCentres.Where(x => x.CodeCentre != 0  && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                    model.B35DotationInterieur = _context.DotationCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();





                    //Emballage


                    //model.B12EmballageInterieur =   _context.EmballageCentres.Where(x =>x.CodeCentre  < 9 && x.idDestinationSource > 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() +  _context.EmballageCentres.Where(x =>x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.CodeCentre  < 9 &&  x.idTypeMouvement == 1 && x.idDestinationSource   > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  +  _context.EmballageCentres.Where(x =>x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()        - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                    //model.B9EmballageInterieur =    _context.EmballageCentres.Where(x => x.CodeCentre  < 9 && x.idDestinationSource > 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x =>  x.CodeCentre  < 9 &&  x.idTypeMouvement == 1 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x => x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()          - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                    //model.B6REmballageInterieur =   _context.EmballageCentres.Where(x =>x.CodeCentre  < 9 && x.idDestinationSource > 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() +  _context.EmballageCentres.Where(x =>x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.CodeCentre  < 9 &&  x.idTypeMouvement == 1 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   +  _context.EmballageCentres.Where(x =>x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()        - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                    //model.B6VEmballageInterieur =   _context.EmballageCentres.Where(x =>x.CodeCentre  < 9 && x.idDestinationSource > 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() +  _context.EmballageCentres.Where(x =>x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.CodeCentre  < 9 &&  x.idTypeMouvement == 1 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   +  _context.EmballageCentres.Where(x =>x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()        - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                    //model.B3EmballageInterieur =   _context.EmballageCentres.Where(x => x.CodeCentre  < 9 && x.idDestinationSource > 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x =>  x.CodeCentre  < 9 &&  x.idTypeMouvement == 1 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()     - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()          - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                    //model.B35EmballageInterieur =  _context.EmballageCentres.Where(x => x.CodeCentre < 9 && x.idDestinationSource > 8 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.CodeCentre  > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre    < 9 && x.idTypeMouvement == 1 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x => x.CodeCentre > 8 && x.idDestinationSource < 9 && x.idTypeMouvement == 2 && x.Date >= dateDebut  && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && x.idDestinationSource < 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre < 9 && x.idDestinationSource > 8 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()        - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();



                    model.B12EmballageInterieur = _context.EmballageCentres.Where(x => x.CodeCentre  < 9  && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9  && (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && (x.idDestinationSource < 9 && x.idDestinationSource != 0)  && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.ConsignationCentres.Where(x => x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre  < 9 && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && (x.idDestinationSource < 9  && x.idDestinationSource != 0) && x.idTypeMouvement == 2 &&  x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 &&  (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()      - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                    model.B9EmballageInterieur  =  _context.EmballageCentres.Where(x => x.CodeCentre < 9  && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.CodeCentre  == 9 && (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 &&  (x.idDestinationSource < 9  && x.idDestinationSource != 0)  && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.ConsignationCentres.Where(x => x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.CodeCentre  < 9 && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 &&x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.CodeCentre  == 9 && (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.idTypeMouvement == 2 &&  x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()       - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                    model.B6REmballageInterieur = _context.EmballageCentres.Where(x => x.CodeCentre  < 9  && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 && x.Date  < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && (x.idDestinationSource < 9 && x.idDestinationSource != 0)  && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.ConsignationCentres.Where(x => x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() +  _context.EmballageCentres.Where(x => x.CodeCentre < 9 && (x.idDestinationSource > 8 || x.idDestinationSource == 0)  && x.idTypeMouvement == 1 &&x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && (x.idDestinationSource < 9  && x.idDestinationSource != 0) && x.idTypeMouvement == 2 &&  x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()      - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                    model.B6VEmballageInterieur = _context.EmballageCentres.Where(x => x.CodeCentre  < 9  && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 && x.Date  < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && (x.idDestinationSource < 9 && x.idDestinationSource != 0)  && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.ConsignationCentres.Where(x => x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre  < 9 && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && (x.idDestinationSource < 9  && x.idDestinationSource != 0) && x.idTypeMouvement == 2 &&  x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 &&  (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()      - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                    model.B3EmballageInterieur  =  _context.EmballageCentres.Where(x => x.CodeCentre < 9  && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.CodeCentre  == 9 && (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 &&  (x.idDestinationSource < 9  && x.idDestinationSource != 0)  && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.ConsignationCentres.Where(x => x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.CodeCentre  < 9 && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 &&x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.CodeCentre  == 9 && (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.idTypeMouvement == 2 &&  x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 &&  (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()       - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                    model.B35EmballageInterieur = _context.EmballageCentres.Where(x => x.CodeCentre  < 9  && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 && x.Date  < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && (x.idDestinationSource < 9 && x.idDestinationSource != 0) && x.idTypeMouvement == 2 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 && (x.idDestinationSource < 9 && x.idDestinationSource != 0)  && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  - _context.ConsignationCentres.Where(x => x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() +  _context.EmballageCentres.Where(x => x.CodeCentre < 9 && (x.idDestinationSource > 8 || x.idDestinationSource == 0) && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && (x.idDestinationSource < 9  && x.idDestinationSource != 0)  && x.idTypeMouvement == 2 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre > 8 &&  (x.idDestinationSource  < 9 && x.idDestinationSource != 0) && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()      - _context.ConsignationCentres.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();






                 

                    // Facturations Centres par Bouteilles


                    model.B12AfactInterieur = (model.B12VenteInterieur + model.B12ConsignationInterieur);
                    model.B9AfactInterieur = (model.B9VenteInterieur + model.B9ConsignationInterieur);
                    model.B6RAfactInterieur = (model.B6RVenteInterieur + model.B6RConsignationInterieur);
                    model.B6VAfactInterieur = (model.B6VVenteInterieur + model.B6VConsignationInterieur);
                    model.B3AfactInterieur = (model.B3VenteInterieur + model.B3ConsignationInterieur);
                    model.B35AfactInterieur = (model.B35VenteInterieur + model.B35ConsignationInterieur);




                    // Productions  Centres par Bouteilles





                    model.B12ProductionInterieur = (model.B12VenteInterieur + model.B12ConsignationInterieur + model.B12DotationInterieur);
                    model.B9ProductionInterieur = (model.B9VenteInterieur + model.B9ConsignationInterieur + model.B9DotationInterieur);
                    model.B6RProductionInterieur = (model.B6RVenteInterieur + model.B6RConsignationInterieur + model.B6RDotationInterieur);
                    model.B6VProductionInterieur = (model.B6VVenteInterieur + model.B6VConsignationInterieur + model.B6VDotationInterieur);
                    model.B3ProductionInterieur = (model.B3VenteInterieur + model.B3ConsignationInterieur + model.B3DotationInterieur);
                    model.B35ProductionInterieur = (model.B35VenteInterieur + model.B35ConsignationInterieur + model.B35DotationInterieur);






                    // Facturations  Centres
                    model.TotalFacturationInterieur = (decimal)((model.B12AfactInterieur) * 12.5 + (model.B9AfactInterieur) * 9 + (model.B6RAfactInterieur) * 6 + (model.B6VAfactInterieur) * 6 + (model.B3AfactInterieur) * 2.75 + (model.B35AfactInterieur) * 35) / 1000;


                    // Productions  Centres
                    model.TotalProductionInterieur = (decimal)((model.B12ProductionInterieur) * 12.5 + (model.B9ProductionInterieur) * 9 + (model.B6RProductionInterieur) * 6 + (model.B6VProductionInterieur) * 6 + (model.B3ProductionInterieur) * 2.75 + (model.B35ProductionInterieur) * 35) / 1000;

                   
                    model.StockTheorieInterieur = (decimal)_context.StockCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date == dateDebut).Select(x => x.StockTheorie.Value).DefaultIfEmpty(0).Sum();

                    model.StockFinInterieur = (decimal)_context.StockCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.Date == dateDebut).Select(x => x.StockFin.Value).DefaultIfEmpty(0).Sum();



                    var DateRetard = "2023-04-01";

                    if (model.dateDebut.Date < Convert.ToDateTime(DateRetard).Date)
                    {
                        model.StockActuelInterieur = model.StockInterieur > 0 ? (model.StockInterieur - model.TotalProductionInterieur) : 0;

                    }
                    else
                    {
                        model.StockActuelInterieur = model.StockInterieur > 0 ? (model.StockInterieur - model.TotalProductionInterieur + model.TonnageSomagaz - model.TotalRetourSomagaz) : 0;

                    }




                    model.TotalStockGlobale = model.StockNKTT + model.StockInterieur;

                    model.TotalFacturationGlobale = model.TotalFacturation + model.TotalFacturationInterieur;
                    model.TotalProductionGlobale = model.TotalProduction + model.TotalProductionInterieur;

                    model.TotalStockGloalTheorique = model.StockTheorieNKTT + model.StockTheorieInterieur;
                    model.TotalStockGloalFin = model.StockFinNKTT + model.StockFinInterieur;
                    model.TotalStockGloalActuel = model.StockActuelNktt + model.StockActuelInterieur;

                    model.GainPerteGlobale = (decimal)_context.StockCentres.Where(x => x.CodeCentre != 0 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.GainPerte.Value).DefaultIfEmpty(0).Sum();






                    return model;



                }


                model.TonnageCollaborateurEnvoieInventaire = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();
                model.TonnageCollaborateurReceptionInventaire = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idMouvementBonEnlvement == 1).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();


                model.TonnageCollaborateurActuel = model.TonnageCollaborateurEnvoieInventaire - model.TonnageCollaborateurReceptionInventaire;




                return model;

            }




        }




        public ModelStatistiqueCentreNKTT SituationCentreNktt(DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                ModelStatistiqueCentreNKTT model = new ModelStatistiqueCentreNKTT();



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                var DateCollaborateurString = "2023-01-01";
                var DateCollaborateur = Convert.ToDateTime(DateCollaborateurString).Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;


               
                var dateStock = _context.StockCentres.Where(x => x.Date == dateDebut && x.CodeCentre == 9).Select(x => x.Date).SingleOrDefault();


                if (dateStock != null)
                {



                    //Nombre d’emballage Vente

                    model.B12Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B9Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B6RVente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
                    model.B6VVente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
                    model.B3Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B35Vente = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 1).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();



                    //Nombre d’emballage Vente Directe

                    model.B12VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B9VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B6RVenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
                    model.B6VVenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
                    model.B3VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B35VenteDirecte = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche == 3).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();



                    //Nombre d’emballage dotation

                    model.B12Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B9Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B6RDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum();
                    model.B6VDotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum();
                    model.B3Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum();
                    model.B35Dotation = _context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.matriculeEmploye != 0 && x.idTypeFiche == 2).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum();




                    //Nombre d’emballage a consigner

                    model.B12Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
                    model.B9Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                    model.B6RConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                    model.B6VConsignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                    model.B3Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                    model.B35Consignation = _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();




                    //Nombre d’emballage Avoir

                    model.B12Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum();
                    model.B9Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum();
                    model.B6RAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum();
                    model.B6VAvoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum();
                    model.B3Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum();
                    model.B35Avoir = _context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum();




                    //Nombre d’emballage a facturer

                    model.B12Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum());
                    model.B9Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum());
                    model.B6RAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum());
                    model.B6VAfact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum());
                    model.B3Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum());
                    model.B35Afact = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeFiche != 2).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum());




                    //Nombre d’emballage produisez

                    model.B12Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B12DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Nonfact.Value).DefaultIfEmpty(0).Sum());
                    model.B9Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B9DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Nonfact.Value).DefaultIfEmpty(0).Sum());
                    model.B6RProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6RDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RNonfact.Value).DefaultIfEmpty(0).Sum());
                    model.B6VProduction = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B6VDE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VAfact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VNonfact.Value).DefaultIfEmpty(0).Sum());
                    model.B3Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B3DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Nonfact.Value).DefaultIfEmpty(0).Sum());
                    model.B35Production = (_context.FicheVenteBouteilles.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.idTypeFicheConsignation == 1).Select(x => x.B35DE.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Afact.Value).DefaultIfEmpty(0).Sum()) + (_context.AREDPs.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Nonfact.Value).DefaultIfEmpty(0).Sum());



                    // Emballage  

                    // model.B12Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum(); 
                    // model.B9Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()     + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()     + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()    - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                    // model.B6REmballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                    // model.B6VEmballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                    // model.B3Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()     + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()     + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()    -  _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                    // model.B35Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 &&  x.Date  < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()    + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre != 9 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();

                    model.B12Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12DC.Value).DefaultIfEmpty(0).Sum();
                    model.B9Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()  - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9DC.Value).DefaultIfEmpty(0).Sum();
                    model.B6REmballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RDC.Value).DefaultIfEmpty(0).Sum();
                    model.B6VEmballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VDC.Value).DefaultIfEmpty(0).Sum();
                    model.B3Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()  - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3DC.Value).DefaultIfEmpty(0).Sum();
                    model.B35Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date < dateDebut).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.CodeCentre == 9 && x.idTypeMouvement == 1 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.FicheConsignations.Where(x => x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35DC.Value).DefaultIfEmpty(0).Sum();



                    model.StockNKTT = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum();  /*+ (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date > dateDebut && x.Date <= dateFin).Select(x => x.Depotage.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();*/

                    model.StockTheorieNKTT = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date == dateDebut).Select(x => x.StockTheorie.Value).DefaultIfEmpty(0).Sum();

                    model.StockFinNKTT = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date == dateDebut).Select(x => x.StockFin.Value).DefaultIfEmpty(0).Sum();



                    model.Depotage = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.Depotage.Value).DefaultIfEmpty(0).Sum();

                    model.GainPerteNktt = (decimal)_context.StockCentres.Where(x => x.CodeCentre == 9 && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.GainPerte.Value).DefaultIfEmpty(0).Sum();

                    model.TotalRetourLivrer = (decimal)_context.Livraisons.Where(x => x.DateLivraison >= dateDebut && x.DateLivraison <= dateFin && x.BonRetourCiterne.CodeClient != 0).Select(x => x.PoidsRetour.Value).DefaultIfEmpty(0).Sum();


                    model.TonnageSomagaz = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient == 0 && x.idTypeBonChargement == 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                    model.TotalRetour = (decimal)_context.BonRetourCiternes.Where(x => x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();


                    // Vrac Facturation
                    model.TonnagePrivee = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1 && x.idTypeBonChargement != 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() - model.TotalRetourLivrer;



                    // Tous les Vracs sauf somagaz

                    //model.TonnagePartenaire = (float)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                    model.TonnageCollaborateurEnvoiePipes = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();


                    model.AutresVrac = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement != 1).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();

                    model.TotalProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000 + model.TonnageSomagaz + model.AutresVrac + model.TonnageCollaborateurEnvoiePipes;

                    model.TotalFacturation = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000 + model.TonnagePrivee;

                    model.TotalConditionneeProduction = (decimal)(model.B12Production * 12.5 + model.B9Production * 9 + model.B6RProduction * 6 + model.B6VProduction * 6 + model.B3Production * 2.75 + model.B35Production * 35) / 1000;
                    model.TotalConditionneeCommerciale = (decimal)(model.B12Afact * 12.5 + model.B9Afact * 9 + model.B6RAfact * 6 + model.B6VAfact * 6 + model.B3Afact * 2.75 + model.B35Afact * 35) / 1000;


                   
                    model.TonnageAvoirNktt = (decimal)(model.B12Avoir * 12.5 + model.B9Avoir * 9 + model.B6RAvoir * 6 + model.B6VAvoir * 6 + model.B3Avoir * 2.75 + model.B35Avoir * 35) / 1000;



                    model.TonnageCollaborateurEnvoie = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();
                    model.TonnageCollaborateurReception = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= dateDebut && x.Date <= dateFin && x.CodeClient != 0 && x.idMouvementBonEnlvement == 1).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();

                    model.TonnageCollaborateurEnvoieInventaire = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();
                    model.TonnageCollaborateurReceptionInventaire = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idMouvementBonEnlvement == 1).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();


                    model.TonnageCollaborateurActuel = model.TonnageCollaborateurEnvoieInventaire - model.TonnageCollaborateurReceptionInventaire;


                    model.StockActuelNktt = model.StockNKTT > 0 ? (model.StockNKTT - model.TotalProduction + model.Depotage + model.TotalRetour + model.TonnageCollaborateurReception) : 0;


                    return model;



                }


                model.TonnageCollaborateurEnvoieInventaire = (decimal)_context.BonChargementCiternes.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idTypeBonChargement == 2).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum() + (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idMouvementBonEnlvement == 2).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();
                model.TonnageCollaborateurReceptionInventaire = (decimal)_context.BonEnlevementGazs.Where(x => x.Date >= DateCollaborateur && x.CodeClient != 0 && x.idMouvementBonEnlvement == 1).Select(x => x.Tonnage.Value).DefaultIfEmpty(0).Sum();


                model.TonnageCollaborateurActuel = model.TonnageCollaborateurEnvoieInventaire - model.TonnageCollaborateurReceptionInventaire;




                return model;

            }


        }





        public ModelStatistiqueInterieur SituationInterieurParCentre(int? CodeCentre, DateTime? dateDebut, DateTime? dateFin)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                ModelStatistiqueInterieur model = new ModelStatistiqueInterieur();



                if (dateDebut == null) dateDebut = DateTime.Now.Date;
                if (dateFin == null) dateFin = DateTime.Now.Date;

                model.dateDebut = (DateTime)dateDebut;
                model.dateFin = (DateTime)dateFin;
                model.LibelleCentre = _context.Centres.Where(x => x.Code == CodeCentre).Select(x => x.libelle).SingleOrDefault();



                var dateStock = _context.StockCentres.Where(x => x.Date == dateDebut && x.CodeCentre != 0 && x.CodeCentre == CodeCentre && x.CodeCentre != 9).Select(x => x.Date).SingleOrDefault();

                if (dateStock != null)
                {

                    


                    // Stock Centres


                    model.StockTheorie = (decimal)_context.StockCentres.Where(x => x.CodeCentre == CodeCentre && x.Date == dateDebut).Select(x => x.StockTheorie.Value).DefaultIfEmpty(0).Sum();
                    model.Stock = (decimal)_context.StockCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre == CodeCentre && x.Date == dateDebut).Select(x => x.Stock.Value).DefaultIfEmpty(0).Sum();
                    model.StockFin = (decimal)_context.StockCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre == CodeCentre && x.Date == dateDebut).Select(x => x.StockFin.Value).DefaultIfEmpty(0).Sum();
                    model.Reception = (decimal)_context.BonChargementCiternes.Where(x => x.CodeClient == 0 && x.codeDestination == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.TonnageCharger.Value).DefaultIfEmpty(0).Sum();
                    model.Retour = (decimal)_context.BonRetourCiternes.Where(x =>x.idTypeBonRetour == 1 && x.CodeCentre == CodeCentre && x.DateRetour >= dateDebut && x.DateRetour <= dateFin).Select(x => x.PoidsGazRestant.Value).DefaultIfEmpty(0).Sum();
                    model.GainPerte = (decimal)_context.StockCentres.Where(x => x.CodeCentre != 0 && x.CodeCentre != 9 && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.GainPerte.Value).DefaultIfEmpty(0).Sum();
                    

                    // Vente 

                    model.B12Vente = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B9Vente = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B6RVente = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B6VVente = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B3Vente = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B35Vente = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum() + _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();


                    // Vente Consommateur

                    model.B12VenteConsomateur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Consommateur.Value).DefaultIfEmpty(0).Sum();
                    model.B9VenteConsomateur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Consommateur.Value).DefaultIfEmpty(0).Sum();
                    model.B6RVenteConsomateur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RConsommateur.Value).DefaultIfEmpty(0).Sum();
                    model.B6VVenteConsomateur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VConsommateur.Value).DefaultIfEmpty(0).Sum();
                    model.B3VenteConsomateur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Consommateur.Value).DefaultIfEmpty(0).Sum();
                    model.B35VenteConsomateur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Consommateur.Value).DefaultIfEmpty(0).Sum();


                    // Vente Revendeur

                    model.B12VenteRevendeur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12Revendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B9VenteRevendeur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9Revendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B6RVenteRevendeur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6RRevendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B6VVenteRevendeur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6VRevendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B3VenteRevendeur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3Revendeur.Value).DefaultIfEmpty(0).Sum();
                    model.B35VenteRevendeur = _context.VenteCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35Revendeur.Value).DefaultIfEmpty(0).Sum();




                    // Consignation 

                    model.B12Consignation =  _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                    model.B9Consignation =   _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum() ;
                    model.B6RConsignation =  _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                    model.B6VConsignation =  _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                    model.B3Consignation =  _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                    model.B35Consignation = _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();


                    // Dotation 

                    model.B12Dotation = _context.DotationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum();
                    model.B9Dotation = _context.DotationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                    model.B6RDotation = _context.DotationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                    model.B6VDotation = _context.DotationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                    model.B3Dotation = _context.DotationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                    model.B35Dotation = _context.DotationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();

                    // Emballage 

                    model.B12Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == CodeCentre && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()   - _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre  && x.Date < dateDebut).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum()  - _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B12.Value).DefaultIfEmpty(0).Sum(); 
                    model.B9Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == CodeCentre && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()    -  _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date < dateDebut).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()     - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum()   -  _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B9.Value).DefaultIfEmpty(0).Sum();
                    model.B6REmballage = _context.EmballageCentres.Where(x => x.CodeCentre == CodeCentre && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()   -  _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date < dateDebut).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum()  -  _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6R.Value).DefaultIfEmpty(0).Sum();
                    model.B6VEmballage = _context.EmballageCentres.Where(x => x.CodeCentre == CodeCentre && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()   - _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre  && x.Date < dateDebut).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum()  - _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B6V.Value).DefaultIfEmpty(0).Sum();
                    model.B3Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == CodeCentre && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()    -  _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date < dateDebut).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()     - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum()   -  _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B3.Value).DefaultIfEmpty(0).Sum();
                    model.B35Emballage = _context.EmballageCentres.Where(x => x.CodeCentre == CodeCentre && x.idTypeMouvement == 1 && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum() - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2   && x.CodeCentre == CodeCentre && x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   - _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre &&  x.Date < dateDebut).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()   + _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  + _context.EmballageCentres.Where(x => x.idTypeMouvement == 2 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()    - _context.EmballageCentres.Where(x => x.idTypeMouvement == 1 && x.idDestinationSource == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  - _context.EmballageCentres.Where(x => x.idTypeMouvement == 2  && x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum()  - _context.ConsignationCentres.Where(x => x.CodeCentre == CodeCentre && x.Date >= dateDebut && x.Date <= dateFin).Select(x => x.B35.Value).DefaultIfEmpty(0).Sum();



                    // Facturations  Centres par Bouteilles


                    model.B12Afact = (model.B12Vente + model.B12Consignation);
                    model.B9Afact = (model.B9Vente + model.B9Consignation);
                    model.B6RAfact = (model.B6RVente + model.B6RConsignation);
                    model.B6VAfact = (model.B6VVente + model.B6VConsignation);
                    model.B3Afact = (model.B3Vente + model.B3Consignation);
                    model.B35Afact = (model.B35Vente + model.B35Consignation);




                    // Productions  Centres par Bouteilles





                    model.B12Production = (model.B12Vente + model.B12Consignation + model.B12Dotation);
                    model.B9Production = (model.B9Vente + model.B9Consignation + model.B9Dotation);
                    model.B6RProduction = (model.B6RVente + model.B6RConsignation + model.B6RDotation);
                    model.B6VProduction = (model.B6VVente + model.B6VConsignation + model.B6VDotation);
                    model.B3Production = (model.B3Vente + model.B3Consignation + model.B3Dotation);
                    model.B35Production = (model.B35Vente + model.B35Consignation + model.B35Dotation);






                    // Facturations  Centres
                    model.TotalFacturation = (decimal)((model.B12Vente + model.B12Consignation) * 12.5 + (model.B9Vente + model.B9Consignation) * 9 + (model.B6RVente + model.B6RConsignation) * 6 + (model.B6VVente + model.B6VConsignation) * 6 + (model.B3Vente + model.B3Consignation) * 2.75 + (model.B35Vente + model.B35Consignation) * 35) / 1000;


                    // Productions  Centres
                    model.TotalProduction = (decimal)((model.B12Vente + model.B12Consignation + model.B12Dotation) * 12.5 + (model.B9Vente + model.B9Consignation + model.B9Dotation) * 9 + (model.B6RVente + model.B6RConsignation + model.B6RDotation) * 6 + (model.B6VVente + model.B6VConsignation + model.B6VDotation) * 6 + (model.B3Vente + model.B3Consignation + model.B3Dotation) * 2.75 + (model.B35Vente + model.B35Consignation + model.B35Dotation) * 35) / 1000;

                    var DateRetard = "2023-04-01";

                    if (model.dateDebut.Date < Convert.ToDateTime(DateRetard).Date)
                    {
                        model.StockActuel = model.Stock > 0 ? (model.Stock - model.TotalProduction) : 0;
                    }
                    else
                    {
                        model.StockActuel = model.Stock > 0 ? (model.Stock - model.TotalProduction + model.Reception - model.Retour) : 0;

                    }






                    return model;
                }

              
                return model;

            }




        }




    }
}