﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionBonRetourCiterne
    {


        private static BusinessGestionBonRetourCiterne instance;
        private BusinessGestionBonRetourCiterne() { }

        public static BusinessGestionBonRetourCiterne Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionBonRetourCiterne();
                }
                return instance;
            }
        }



        public List<BonRetourCiterneDto> ChercherListeBon(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                IEnumerable<BonRetourCiterne> baseQuery = Enumerable.Empty<BonRetourCiterne>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

               
                    if (string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.BonRetourCiternes.AsEnumerable()
                                    select o;

                    }



                if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                {
                    baseQuery = from o in _context.BonRetourCiternes.AsEnumerable()
                                  where o.Reference.Contains(filtreMotCle) || o.ReferenceBonChargement.Contains(filtreMotCle)
                                 select o;


                }

                    else if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() == 1)
                    {
                        baseQuery = from o in _context.BonRetourCiternes.AsEnumerable()
                                    where o.CodeCentre  == Convert.ToInt32(filtreMotCle)
                                    select o;

                    }

                    else if (!string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.BonRetourCiternes.AsEnumerable()
                                    where o.Client.Code == Convert.ToInt32(filtreMotCle)

                                    select o;

                    }



                    var query = from o in baseQuery
                                select new BonRetourCiterneDto
                                {
                                    id = o.id,
                                    Reference = o.Reference,
                                    idTypeBon = o.idTypeBonRetour.Value,
                                    TypeBon = o.TypeBonRetour.libelle,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,
                                    CodeCentre = o.CodeCentre.Value,
                                    LibelleCentre = o.Centre.libelle,
                                    ImmtriculeCamion = o.ImmatriculeCamion,

                                    DateEnvoie = o.DateEnvoie.Value,
                                    ReferenceBonChargement = o.ReferenceBonChargement,
                                    PoidsVide = o.PoidsVide.Value,
                                    PoidsPlein = o.PoidsPlein.Value,
                                    PoidsGaz = o.PoidsGaz.Value,
                                    ReferencePVPesee = o.ReferencePVPesee,
                                    DateRetour = o.DateRetour.Value,
                                    PoidsRetour = o.PoidsRetour.Value,

                                    PoidsVideRetour = o.PoidsVideRetour.Value,
                                    PoidsPleinRetour = o.PoidsPleinRetour.Value,

                                    PoidsGazRestant = o.PoidsGazRestant.Value,
                                    PoidsReceptionnerCentre = o.PoidsReceptionnerCentre.Value,
                                    Commentaire = o.Commentaire,
                                
                                    etat = o.etat.Value

                                };


                    return query.OrderByDescending(x => x.id).ToList();

            }

        }



        public List<BonRetourCiterneDto> ChercherListeBonAcloturer(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                IEnumerable<BonRetourCiterne> baseQuery = Enumerable.Empty<BonRetourCiterne>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.BonRetourCiternes.AsEnumerable()
                                where o.etat == 4
                                select o;

                }



                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.BonRetourCiternes.AsEnumerable()
                                where o.etat == 4 && (o.DateRetour == Convert.ToDateTime(filtreMotCle))
                                select o;

                }

                else if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() == 1)
                {
                    baseQuery = from o in _context.BonRetourCiternes.AsEnumerable()
                                where o.etat == 4 && (o.CodeCentre == Convert.ToInt32(filtreMotCle))
                                select o;

                }

                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.BonRetourCiternes.AsEnumerable()
                                where o.etat == 4 && (o.Client.Code == Convert.ToInt32(filtreMotCle))
                                select o;

                }



                var query = from o in baseQuery
                            select new BonRetourCiterneDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                idTypeBon = o.idTypeBonRetour.Value,
                                TypeBon = o.TypeBonRetour.libelle,
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,
                                CodeCentre = o.CodeCentre.Value,
                                LibelleCentre = o.Centre.libelle,
                                ImmtriculeCamion = o.ImmatriculeCamion,

                                DateEnvoie = o.DateEnvoie.Value,
                                ReferenceBonChargement = o.ReferenceBonChargement,
                                PoidsVide = o.PoidsVide.Value,
                                PoidsPlein = o.PoidsPlein.Value,
                                PoidsGaz = o.PoidsGaz.Value,
                                ReferencePVPesee = o.ReferencePVPesee,
                                DateRetour = o.DateRetour.Value,
                                PoidsRetour = o.PoidsRetour.Value,

                                PoidsVideRetour = o.PoidsVideRetour.Value,
                                PoidsPleinRetour = o.PoidsPleinRetour.Value,

                                PoidsGazRestant = o.PoidsGazRestant.Value,
                                PoidsReceptionnerCentre = o.PoidsReceptionnerCentre.Value,
                                Commentaire = o.Commentaire,

                                etat = o.etat.Value


                            };


                return query.OrderByDescending(x => x.id).ToList();

            }

        }





        public BonRetourCiterneDto GetBonParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.BonRetourCiternes.Find(id);

                var dto = new BonRetourCiterneDto
                {

                    id = o.id,
                    Reference = o.Reference,
                    idTypeBon = o.idTypeBonRetour.Value,
                    TypeBon = o.TypeBonRetour.libelle,
                    CodeClient = o.CodeClient.Value,
                    NomClient = o.Client.nomComplet,
                    CodeCentre = o.CodeCentre.Value,
                    LibelleCentre = o.Centre.libelle,
                    ImmtriculeCamion = o.ImmatriculeCamion,

                    DateEnvoie = o.DateEnvoie.Value,
                    ReferenceBonChargement = o.ReferenceBonChargement,
                    PoidsVide = o.PoidsVide.Value,
                    PoidsPlein = o.PoidsPlein.Value,
                    PoidsGaz = o.PoidsGaz.Value,
                    ReferencePVPesee = o.ReferencePVPesee,
                    DateRetour = o.DateRetour.Value,
                    PoidsRetour = o.PoidsRetour.Value,

                    PoidsVideRetour = o.PoidsVideRetour.Value,
                    PoidsPleinRetour = o.PoidsPleinRetour.Value,

                    PoidsGazRestant = o.PoidsGazRestant.Value,
                    PoidsReceptionnerCentre = o.PoidsReceptionnerCentre.Value,
                    Commentaire = o.Commentaire,

                    etat = o.etat.Value,
                   


                    


                };
                return dto;

            }
        }



        public BonRetourCiterneDto GetBonImprimerParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.BonRetourCiternes.Find(id);

                var dto = new BonRetourCiterneDto
                {

                    id = o.id,
                    Reference = o.Reference,
                    idTypeBon = o.idTypeBonRetour.Value,
                    TypeBon = o.TypeBonRetour.libelle,
                    CodeConcerne = o.idTypeBonRetour == 1 ? o.CodeCentre.Value : o.CodeClient.Value,
                    Concrne = o.idTypeBonRetour == 1 ? o.Centre.libelle : o.Client.nomComplet,
                    CodeClient = o.CodeClient.Value,
                    NomClient = o.Client.nomComplet,
                    CodeCentre = o.CodeCentre.Value,
                    LibelleCentre = o.Centre.libelle,
                    ImmtriculeCamion = o.ImmatriculeCamion,

                    DateEnvoie = o.DateEnvoie.Value,
                    ReferenceBonChargement = o.ReferenceBonChargement,
                    PoidsVide = o.PoidsVide.Value,
                    PoidsPlein = o.PoidsPlein.Value,
                    PoidsGaz = o.PoidsGaz.Value,
                    ReferencePVPesee = o.ReferencePVPesee,
                    DateRetour = o.DateRetour.Value,
                    PoidsRetour = o.PoidsRetour.Value,

                    PoidsVideRetour = o.PoidsVideRetour.Value,
                    PoidsPleinRetour = o.PoidsPleinRetour.Value,

                    PoidsGazRestant = o.PoidsGazRestant.Value,
                    PoidsReceptionnerCentre = o.PoidsReceptionnerCentre.Value,
                    Commentaire = o.Commentaire,

                    etat = o.etat.Value





                };
                return dto;

            }
        }







        public void Ajouter(BonRetourCiterneDto dto)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);
                var centre = _context.Centres.Find(dto.CodeCentre);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }

                if (centre == null)
                {

                    centre = new Centre
                    {
                        Code = dto.CodeCentre,
                        libelle = dto.LibelleCentre,

                    };
                }



                var entity = new BonRetourCiterne
                {

                  

                    Reference = dto.Reference,
                    idTypeBonRetour = dto.idTypeBon,
                    Client = client,
                    Centre = centre,
                    ImmatriculeCamion = dto.ImmtriculeCamion,
                    DateEnvoie = dto.DateEnvoie,
                    ReferenceBonChargement = dto.ReferenceBonChargement,
                    PoidsVide = dto.PoidsVide,
                    PoidsPlein = dto.PoidsPlein,
                    PoidsGaz = dto.PoidsGaz,
                    ReferencePVPesee = dto.ReferencePVPesee,
                    DateRetour = dto.DateRetour,
                    PoidsRetour = dto.PoidsRetour,
                    PoidsVideRetour = dto.PoidsVideRetour,
                    PoidsPleinRetour = dto.PoidsPleinRetour,

                    PoidsGazRestant = dto.PoidsGazRestant,
                    PoidsReceptionnerCentre = (dto.PoidsGaz - dto.PoidsGazRestant),
                    Commentaire = dto.Commentaire,
                    etat = dto.idEntite == 4 ? 1 : 2,




                };
                _context.BonRetourCiternes.Add(entity);
                try
                {
                    _context.SaveChanges();

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }



        public void Modifier(BonRetourCiterneDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);
                var centre = _context.Centres.Find(dto.CodeCentre);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }

                if (centre == null)
                {

                    centre = new Centre
                    {
                        Code = dto.CodeCentre,
                        libelle = dto.LibelleCentre,

                    };
                }


                var entity = _context.BonRetourCiternes.Find(dto.id);
                if (entity != null)

                {

                    entity.Reference = dto.Reference;
                    entity.idTypeBonRetour = dto.idTypeBon;
                    entity.Client = client;
                    entity.Centre = centre;
                    entity.ImmatriculeCamion = dto.ImmtriculeCamion;
                    entity.DateEnvoie = dto.DateEnvoie;
                    entity.ReferenceBonChargement = dto.ReferenceBonChargement;
                    entity.PoidsVide = dto.PoidsVide;
                    entity.PoidsPlein = dto.PoidsPlein;
                    entity.PoidsGaz = dto.PoidsGaz;
                    entity.ReferencePVPesee = dto.ReferencePVPesee;
                    entity.DateRetour = dto.DateRetour;
                    entity.PoidsRetour = dto.PoidsRetour;

                    entity.PoidsVideRetour = dto.PoidsVideRetour;
                    entity.PoidsPleinRetour = dto.PoidsPleinRetour;

                    entity.PoidsGazRestant = dto.PoidsGazRestant;
                    entity.PoidsReceptionnerCentre = (dto.PoidsGaz - dto.PoidsGazRestant);
                    entity.Commentaire = dto.Commentaire;
                    entity.etat = dto.idEntite == 4 ? 1 : 2;




                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }










        public TypeBonRetourDto GetTypeBonParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.TypeBonRetours.Find(id);

                var dto = new TypeBonRetourDto
                {
                    id = entity.id,
                    libelle = entity.libelle
                };
                return dto;

            }




        }




        public List<TypeBonRetourDto> GetListeTypeBon()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                //var list = _context.TypeBonChargements.Where(x=>x.id != 1).ToList();
                var list = _context.TypeBonRetours.ToList();
                var listeDto = new List<TypeBonRetourDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new TypeBonRetourDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }

        public CentreDto GetCentreParId(int Code)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Centres.Find(Code);
                if (entity == null) return null;
                var dto = new CentreDto
                {
                    Code = entity.Code,
                    libelle = entity.libelle,

                };

                return dto;
            }



        }



        public BonChargementCiterneDto GetBonChargementParId(string Reference)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.BonChargementCiternes.SingleOrDefault(x=>x.Reference == Reference);
                if (entity == null) return null;
                var dto = new BonChargementCiterneDto
                {
                    ImmtriculeCamion = entity.ImmatriculeCamion,
                    DateString = entity.Date.Value.ToString("dd/MM/yyyy"),
                    PoidDebutString = entity.PoidDebut.Value.ToString("#.000"),
                    PoidsApresChargementString = entity.PoidsApresChargement.Value.ToString("#.000"),
                    TonnageChargerString = entity.TonnageCharger.Value.ToString("#.000"),


                };

                return dto;
            }



        }



        public void Valider(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.BonRetourCiternes.Find(id);
                entity.etat = 3;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }

      


        public string GetBonRetourByRefPV(string RefPV,string RefBonRetour)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.BonRetourCiternes.Where(x => x.ReferencePVPesee == RefPV && x.Reference != RefBonRetour).Select(x => x.ReferencePVPesee).SingleOrDefault();

                return entity;
            }


        }


        public void EnInstance(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                if (idEntite == 5)
                {
                    var entity = _context.BonRetourCiternes.Find(id);
                    entity.etat = 2;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                if (idEntite == 4)
                {
                    var entity = _context.BonRetourCiternes.Find(id);
                    entity.etat = 1;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
                }


            }

        }



        public int TotalCloturer()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.BonRetourCiternes.Where(x => x.etat == 4).Count();

                return Total;
            }


        }



        public void CloturerParDate(int idEntite, DateTime date)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            { 


                var entity = _context.BonRetourCiternes.Where(x => x.DateRetour == date.Date).Select(x => x.id).ToArray();

                foreach (int id in entity)
                {

                    BonRetourCiterne fiche = _context.BonRetourCiternes.Find(id);
                    fiche.etat = 4;
                    //var sortie = _context.Sorties.Where(x => x.id == item.id).SingleOrDefault();
                    _context.Entry(fiche).State = EntityState.Modified;
                    _context.SaveChanges();

                }


            }
        }



        public DateTime GetDateLivraison(int idBonRetour)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var entity = _context.Livraisons.Where(x => x.idBonRetour == idBonRetour).Select(x=>x.DateLivraison).SingleOrDefault();

                return entity == null ? DateTime.Now : entity.Value;

            }
        }


        public void EnregistrerLivraisonRetour(LivraisonDto dto)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var  livraison = _context.Livraisons.Where(x=>x.idBonRetour == dto.idBonRetour).SingleOrDefault();

                if(livraison == null)
                {
                    BonRetourCiterne BonRetour = _context.BonRetourCiternes.Find(dto.idBonRetour);
                    BonRetour.etat = 4;
                    var entity = new Livraison
                    {
                        idBonRetour = dto.idBonRetour,
                        DateLivraison = dto.DateLivraison,
                        PoidsRetour = BonRetour.PoidsGazRestant,
                        Commentaire = dto.commentaire

                    };
                    _context.Livraisons.Add(entity);
                    _context.SaveChanges();

                }
                else
                {
                    BonRetourCiterne BonRetour = _context.BonRetourCiternes.Find(dto.idBonRetour);
                    BonRetour.etat = 4;

                    var entity = _context.Livraisons.Find(livraison.id);

                    entity.DateLivraison = dto.DateLivraison;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();

                }



            }



            
        }



    }
}