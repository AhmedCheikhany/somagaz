﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionBonEnlevementGaz
    {

        private static BusinessGestionBonEnlevementGaz instance;
        private BusinessGestionBonEnlevementGaz() { }

        public static BusinessGestionBonEnlevementGaz Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionBonEnlevementGaz();
                }
                return instance;
            }
        }




        public List<BonEnlevementGazDto> ChercherListeBonEnlevement(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<BonEnlevementGaz> baseQuery = Enumerable.Empty<BonEnlevementGaz>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                //if (string.IsNullOrEmpty(filtreMotCle)) 
                //_context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.BonEnlevementGazs.AsEnumerable()
                                select o;

                }

                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.BonEnlevementGazs.AsEnumerable()
                                where o.CodeClient == Convert.ToInt32(filtreMotCle) || o.Reference == filtreMotCle
                                select o;

                }

                var query = from o in baseQuery
                            select new BonEnlevementGazDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,
                                idMouvementBon = o.idMouvementBonEnlvement.Value,
                                MouvementBon = o.MouvementBonEnlevement.libelle,
                                idTypeBonEnlevement = o.idTypeBonEnlevement.Value,
                                TypeBonEnlevement = o.TypeBonEnlevement.libelle,
                                ImmatriculeCamion = o.ImmatriculeCamion,
                                ChauffeurCamion = o.ChauffeurCamion,
                                ReferenceBonPesee = o.ReferenceBonPesee,
                                ReferenceDepotage = o.ReferenceDepotage,
                                Tonnage = o.Tonnage.Value,
                                Commentaires = o.Commentaires,

                                etat = o.etat.Value

                            };

                return query.OrderByDescending(x => x.id).ToList();


            }

        }


        public BonEnlevementGazDto GetBonEnlevementParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.BonEnlevementGazs.Find(id);

                var dto = new BonEnlevementGazDto
                {

                    id = o.id,
                    Reference = o.Reference,
                    Date = o.Date.Value,
                    CodeClient = o.CodeClient.Value,
                    NomClient = o.Client.nomComplet,
                    idMouvementBon = o.idMouvementBonEnlvement.Value,
                    MouvementBon = o.MouvementBonEnlevement.libelle,
                    idTypeBonEnlevement = o.idTypeBonEnlevement.Value,
                    TypeBonEnlevement = o.TypeBonEnlevement.libelle,
                    ImmatriculeCamion = o.ImmatriculeCamion,
                    ChauffeurCamion = o.ChauffeurCamion,
                    ReferenceBonPesee = o.ReferenceBonPesee,
                    ReferenceDepotage = o.ReferenceDepotage,
                    Tonnage = o.Tonnage.Value,
                    Commentaires = o.Commentaires,

                    etat = o.etat.Value

                };


                return dto;

            }

        }




        public void Ajouter(BonEnlevementGazDto dto)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);

                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }





                var entity = new BonEnlevementGaz
                {

                    Reference = dto.Reference,
                    Date = dto.Date,
                    Client = client,

                    idMouvementBonEnlvement = dto.idMouvementBon,
                    idTypeBonEnlevement = dto.idTypeBonEnlevement,
                    ReferenceBonPesee = dto.ReferenceBonPesee,
                    ReferenceDepotage = dto.ReferenceDepotage,
                    ImmatriculeCamion = dto.ImmatriculeCamion,
                    ChauffeurCamion = dto.ChauffeurCamion,
                    Tonnage = dto.Tonnage,

                    Commentaires = dto.Commentaires,
                    etat = 1



                };
                _context.BonEnlevementGazs.Add(entity);
                try
                {
                    _context.SaveChanges();

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }


        public void Modifier(BonEnlevementGazDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var client = _context.Clients.Find(dto.CodeClient);

                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }




                var entity = _context.BonEnlevementGazs.Find(dto.id);
                if (entity != null)

                {


                    entity.Reference = dto.Reference;
                    entity.Date = dto.Date;
                    entity.Client = client;

                    entity.idMouvementBonEnlvement = dto.idMouvementBon;
                    entity.idTypeBonEnlevement = dto.idTypeBonEnlevement;
                    entity.ReferenceBonPesee = dto.ReferenceBonPesee;
                    entity.ReferenceDepotage = dto.ReferenceDepotage;
                    entity.ImmatriculeCamion = dto.ImmatriculeCamion;
                    entity.ChauffeurCamion = dto.ChauffeurCamion;
                    entity.Tonnage = dto.Tonnage;

                    entity.Commentaires = dto.Commentaires;
                    entity.etat = 1;




                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }



        public MouvementBonEnlevementDto GetMouvementParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.MouvementBonEnlevements.Find(id);

                var dto = new MouvementBonEnlevementDto
                {
                    id = entity.id,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public List<MouvementBonEnlevementDto> GetListeMouvementBonEnlevement()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.MouvementBonEnlevements.ToList();
                var listeDto = new List<MouvementBonEnlevementDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new MouvementBonEnlevementDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }


        public TypeBonEnlevementDto GetTypeBonEnlevementParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.TypeBonEnlevements.Find(id);

                var dto = new TypeBonEnlevementDto
                {
                    id = entity.id,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public List<TypeBonEnlevementDto> GetListeTypeBonEnlevement()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.TypeBonEnlevements.ToList();
                var listeDto = new List<TypeBonEnlevementDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new TypeBonEnlevementDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }

        public List<TypeBonEnlevementDto> GetListeTypeBonEnlevement(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.TypeBonEnlevements.Where(x=>x.id == id).ToList();
                var listeDto = new List<TypeBonEnlevementDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new TypeBonEnlevementDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }



    }
}