﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionAredpDetails
    {


        private static BusinessGestionAredpDetails instance;
        private BusinessGestionAredpDetails() { }

        public static BusinessGestionAredpDetails Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionAredpDetails();
                }
                return instance;
            }
        }




        public List<AredpDetailsDto> ChercherListeAredpDetails(int? idEntite, string login,int idProduit)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            { 

                IEnumerable<AredpDetail> baseQuery = Enumerable.Empty<AredpDetail>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                //if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);



                if (idProduit > 0)
                {
                    baseQuery = from o in _context.AredpDetails.AsEnumerable()
                                where o.idAredp == idProduit
                                select o;
                }

                var query = from o in baseQuery
                            select new AredpDetailsDto
                            {
                                id = o.id,
                                idAredp = o.idAredp.Value,
                                idProduit = o.idProduit.Value,
                                NomProduit = o.Produit.libelle,
                                poidsLu = (decimal)o.poidsLu,
                                Tare =  (decimal)o.Tare,
                                poidsManquant = (decimal)o.poidsManquant,
                                poidsRestant = (decimal) o.poidsRestant,

                               


                            };


                return query.OrderByDescending(x => x.id).ToList();



            }

        }



        public AredpDetailsDto GetAredpDetailsParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.AredpDetails.Find(id);

                var dto = new AredpDetailsDto
                {
                    id = o.id,
                    idAredp = o.idAredp.Value,
                    idProduit = o.idProduit.Value,
                    NomProduit = o.Produit.libelle,
                    poidsLu = (decimal)o.poidsLu,
                    Tare = (decimal)o.Tare,
                    poidsManquant = (decimal)o.poidsManquant,
                    poidsRestant = (decimal)o.poidsRestant,


                };


                return dto;

            }

        }





        public List<ProduitDto> GetListeProduit()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.Produits.ToList();
                var listeDto = new List<ProduitDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new ProduitDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }


        public void Ajouter(AredpDetailsDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Aredp = _context.AREDPs.Find(dto.idAredp);



                decimal poidsNormal = 0;
                decimal poidsRestant = 0;
                decimal pourcentageB12 = (Convert.ToDecimal("12,5") * 40) / 100;
                decimal pourcentageB9 = (9 * 40) / 100;
                decimal pourcentageB6R = (6 * 40) / 100;
                decimal pourcentageB6V = (6 * 40) / 100;
                decimal pourcentageB3 = (Convert.ToDecimal("2,75") * 40) / 100;
                decimal pourcentageB35 = (35 * 40) / 100;


                if (dto.idProduit == 1)
                {
                    poidsNormal = Convert.ToDecimal("12,5");
                    poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                   

                }
                if (dto.idProduit == 2)
                {
                    poidsNormal = 9;
                    poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                }
                if (dto.idProduit == 3)
                {
                    poidsNormal = 6;
                    poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                    
                }
                if (dto.idProduit == 4)
                {
                    poidsNormal = 6;
                    poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                   

                }
                if (dto.idProduit == 5)
                {
                    poidsNormal = Convert.ToDecimal("2,75");
                    poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                   
                }
                if (dto.idProduit == 6)
                {
                    poidsNormal = 35;
                    poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                    
                }


                var entity = new AredpDetail
                {
                    idAredp = dto.idAredp,
                    idProduit = dto.idProduit,
                    poidsLu = (dto.poidsLu > 0) ? dto.poidsLu : 0,
                    Tare = (dto.Tare > 0) ? dto.Tare : 0,
                    poidsRestant = (dto.Tare < dto.poidsLu && dto.poidsLu > 0 && dto.Tare > 0) ? (dto.poidsLu - dto.Tare) : 0,
                    poidsManquant = (poidsRestant < poidsNormal && poidsRestant > 0) ? (poidsNormal - poidsRestant) : 0

                };





                _context.AredpDetails.Add(entity);
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }



                Aredp.B12Nonfact = 0;
                Aredp.B9Nonfact = 0;
                Aredp.B6RNonfact = 0;
                Aredp.B6VNonfact = 0;
                Aredp.B3Nonfact = 0;
                Aredp.B35Nonfact = 0;

                Aredp.B12Afact = 0;
                Aredp.B9Afact = 0;
                Aredp.B6RAfact = 0;
                Aredp.B6VAfact = 0;
                Aredp.B3Afact = 0;
                Aredp.B35Afact = 0;

                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }




                var ListeAredpDetails = _context.AredpDetails.Where(x => x.idAredp == dto.idAredp).ToList();

                // Nbre Nonfacturer par Type
                Aredp.B12Nonfact = ListeAredpDetails.Where(x => x.idProduit == 1 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB12).Count();
                Aredp.B9Nonfact = ListeAredpDetails.Where(x => x.idProduit == 2 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB9).Count();
                Aredp.B6RNonfact = ListeAredpDetails.Where(x => x.idProduit == 3 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB6R).Count();
                Aredp.B6VNonfact = ListeAredpDetails.Where(x => x.idProduit == 4 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB6V).Count();
                Aredp.B3Nonfact = ListeAredpDetails.Where(x => x.idProduit == 5 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB3).Count();
                Aredp.B35Nonfact = ListeAredpDetails.Where(x => x.idProduit == 6 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB35).Count();


                // Nbre Afacturer par Type
                Aredp.B12Afact = ListeAredpDetails.Where(x => x.idProduit == 1 && x.poidsLu > 0 && x.poidsRestant < pourcentageB12).Count();
                Aredp.B9Afact = ListeAredpDetails.Where(x => x.idProduit == 2 && x.poidsLu > 0 && x.poidsRestant < pourcentageB9).Count();
                Aredp.B6RAfact = ListeAredpDetails.Where(x => x.idProduit == 3 && x.poidsLu > 0 && x.poidsRestant < pourcentageB6R).Count();
                Aredp.B6VAfact = ListeAredpDetails.Where(x => x.idProduit == 4 && x.poidsLu > 0 && x.poidsRestant < pourcentageB6V).Count();
                Aredp.B3Afact = ListeAredpDetails.Where(x => x.idProduit == 5 && x.poidsLu > 0 && x.poidsRestant < pourcentageB3).Count();
                Aredp.B35Afact = ListeAredpDetails.Where(x => x.idProduit == 6 && x.poidsLu > 0 && x.poidsRestant < pourcentageB35).Count();

                Aredp.TonnageFacturer = (decimal)(Aredp.B12Afact * 12.5 + Aredp.B9Afact * 9 + Aredp.B6RAfact * 6 + Aredp.B6VAfact * 6 + Aredp.B3Afact * 2.75 + Aredp.B35Afact * 35) / 1000;

                Aredp.TonnageNonFacturer = (decimal)(Aredp.B12Nonfact * 12.5 + Aredp.B9Nonfact * 9 + Aredp.B6RNonfact * 6 + Aredp.B6VNonfact * 6 + Aredp.B3Nonfact * 2.75 + Aredp.B35Nonfact * 35) / 1000;




                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }






        public void Modifier(AredpDetailsDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Aredp = _context.AREDPs.Find(dto.idAredp);



                decimal poidsNormal = 0;
                decimal poidsRestant = 0;
                decimal pourcentageB12 = (Convert.ToDecimal("12,5") * 40) / 100;
                decimal pourcentageB9 = (9 * 40) / 100;
                decimal pourcentageB6R = (6 * 40) / 100;
                decimal pourcentageB6V = (6 * 40) / 100;
                decimal pourcentageB3 = (Convert.ToDecimal("2,75") * 40) / 100;
                decimal pourcentageB35 = (35 * 40) / 100;


                if (dto.idProduit == 1)
                    {
                        poidsNormal = Convert.ToDecimal("12,5");
                        poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                       
                       
                    }
                    if (dto.idProduit == 2)
                    {
                        poidsNormal = 9;
                        poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                        
                    }
                    if (dto.idProduit == 3)
                    {
                        poidsNormal = 6;
                        poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                       
                    }
                    if (dto.idProduit == 4)
                    {
                        poidsNormal = 6;
                        poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                       

                    }
                    if (dto.idProduit == 5)
                    {
                        poidsNormal = Convert.ToDecimal("2,75");
                        poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                        

                    }
                    if (dto.idProduit == 6)
                    {
                        poidsNormal = 35;
                        poidsRestant = (dto.poidsLu > 0 && dto.Tare > 0 && dto.Tare < dto.poidsLu) ? (dto.poidsLu - dto.Tare) : 0;
                       
                    }


                var entity = _context.AredpDetails.Find(dto.id);
                if (entity != null)
                {

                    entity.idAredp = dto.idAredp;
                    entity.idProduit = dto.idProduit;
                    entity.poidsLu = (dto.poidsLu > 0) ? dto.poidsLu : 0;
                    entity.Tare = (dto.Tare > 0) ? dto.Tare : 0;
                    entity.poidsRestant = (dto.Tare < dto.poidsLu && dto.poidsLu > 0 && dto.Tare > 0) ? (dto.poidsLu - dto.Tare) : 0;
                    entity.poidsManquant = (poidsRestant < poidsNormal && poidsRestant > 0) ? (poidsNormal - poidsRestant) : 0;

                };



                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }



                Aredp.B12Nonfact = 0;
                Aredp.B9Nonfact = 0;
                Aredp.B6RNonfact = 0;
                Aredp.B6VNonfact = 0;
                Aredp.B3Nonfact = 0;
                Aredp.B35Nonfact = 0;

                Aredp.B12Afact = 0;
                Aredp.B9Afact = 0;
                Aredp.B6RAfact = 0;
                Aredp.B6VAfact = 0;
                Aredp.B3Afact = 0;
                Aredp.B35Afact = 0;

                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }





                var ListeAredpDetails = _context.AredpDetails.Where(x=> x.idAredp ==  dto.idAredp).ToList();

                // Nbre Nonfacturer par Type
                 Aredp.B12Nonfact =  ListeAredpDetails.Where(x => x.idProduit == 1 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB12).Count();
                 Aredp.B9Nonfact =  ListeAredpDetails.Where(x => x.idProduit == 2 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB9).Count();
                 Aredp.B6RNonfact = ListeAredpDetails.Where(x => x.idProduit == 3 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB6R).Count();
                 Aredp.B6VNonfact = ListeAredpDetails.Where(x => x.idProduit == 4 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB6V).Count();
                 Aredp.B3Nonfact =  ListeAredpDetails.Where(x => x.idProduit == 5 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB3).Count();
                 Aredp.B35Nonfact = ListeAredpDetails.Where(x => x.idProduit == 6 && x.poidsLu > 0 && x.poidsRestant >= pourcentageB35).Count();


                // Nbre Afacturer par Type
                Aredp.B12Afact = ListeAredpDetails.Where(x => x.idProduit == 1 && x.poidsLu > 0  && x.poidsRestant < pourcentageB12).Count();
                Aredp.B9Afact =  ListeAredpDetails.Where(x => x.idProduit == 2 && x.poidsLu > 0 && x.poidsRestant < pourcentageB9).Count();
                Aredp.B6RAfact = ListeAredpDetails.Where(x => x.idProduit == 3 && x.poidsLu > 0 && x.poidsRestant < pourcentageB6R).Count();
                Aredp.B6VAfact = ListeAredpDetails.Where(x => x.idProduit == 4 && x.poidsLu > 0 && x.poidsRestant < pourcentageB6V).Count();
                Aredp.B3Afact =  ListeAredpDetails.Where(x => x.idProduit == 5 && x.poidsLu > 0 && x.poidsRestant < pourcentageB3).Count();
                Aredp.B35Afact = ListeAredpDetails.Where(x => x.idProduit == 6 && x.poidsLu > 0 && x.poidsRestant < pourcentageB35).Count();


                Aredp.TonnageFacturer = (decimal)(Aredp.B12Afact * 12.5 + Aredp.B9Afact * 9 + Aredp.B6RAfact * 6 + Aredp.B6VAfact * 6 + Aredp.B3Afact * 2.75 + Aredp.B35Afact * 35) / 1000;

                Aredp.TonnageNonFacturer = (decimal)(Aredp.B12Nonfact * 12.5 + Aredp.B9Nonfact * 9 + Aredp.B6RNonfact * 6 + Aredp.B6VNonfact * 6 + Aredp.B3Nonfact * 2.75 + Aredp.B35Nonfact * 35) / 1000;



                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }


    }
}