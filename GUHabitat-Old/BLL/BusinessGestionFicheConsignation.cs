﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionFicheConsignation
    {

        private static BusinessGestionFicheConsignation instance;
        private BusinessGestionFicheConsignation() { }

        public static BusinessGestionFicheConsignation Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionFicheConsignation();
                }
                return instance;
            }
        }


        public List<FicheConsignationDto> ChercherListeConsignation(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<FicheConsignation> baseQuery = Enumerable.Empty<FicheConsignation>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

                if (idEntite == 5)
                {
                    if (string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                    where o.etat != 1
                                    select o;

                    }



                    if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                    {
                        baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                    where o.etat != 1 && (o.Reference.Contains(filtreMotCle))
                                    select o;

                    }
                    else if (!string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                    where o.etat != 1 && (o.Client.Code == Convert.ToInt32(filtreMotCle))
                                    select o;

                    }


                    var query = from o in baseQuery
                                select new FicheConsignationDto
                                {
                                    id = o.id,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    idTypeFicheConsignation = o.idTypeFicheConsignation.Value,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,

                                    NumeroSerie = o.NumeroSerie,
                                    TypeFicheConsignation = o.TypeFicheConsignation.libelle,
                                    VehiculeImmatricule = o.VehiculeImmatricule,

                                    B12DC = o.B12DC.Value,
                                    B9DC = o.B9DC.Value,
                                    B6RDC = o.B6RDC.Value,
                                    B6VDC = o.B6VDC.Value,
                                    B3DC = o.B3DC.Value,
                                    B35DC = o.B35DC.Value,
                                    PresentoirDC = o.PresentoirDC.Value,


                                    B12Prix = (float)o.B12Prix,
                                    B9Prix = (float)o.B9Prix,
                                    B6RPrix = (float)o.B6RPrix,
                                    B6VPrix = (float)o.B6VPrix,
                                    B3Prix = (float)o.B3Prix,
                                    B35Prix = (float)o.B35Prix,
                                    PresentoirPrix = (float)o.PresentoirPrix,


                                    TotalPrixB12 = (float)o.TotalPrixB12,
                                    TotalPrixB9 = (float)o.TotalPrixB9,
                                    TotalPrixB6R = (float)o.TotalPrixB6R,
                                    TotalPrixB6V = (float)o.TotalPrixB6V,
                                    TotalPrixB3 = (float)o.TotalPrixB3,
                                    TotalPrixB35 = (float)o.TotalPrixB35,
                                    TotalPrixPresentoir = (float)o.TotalPrixPresentoir,

                                    B12DE = o.B12DE.Value,
                                    B9DE = o.B9DE.Value,
                                    B6RDE = o.B6RDE.Value,
                                    B6VDE = o.B6VDE.Value,
                                    B3DE = o.B3DE.Value,
                                    B35DE = o.B35DE.Value,
                                    PresentoirDE = o.PresentoirDE.Value,

                                    Commentaire = o.Commentaire,
                                    etat = o.etat.Value,
                                    Tonnage = (decimal)o.Tonnage

                                };


                    return query.OrderByDescending(x => x.id).ToList();
                }
                else
                {
                    if (string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.FicheConsignations.AsEnumerable()

                                    select o;
                    }

                   


                    if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                    {
                        baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                    where  o.Reference.Contains(filtreMotCle)
                                    select o;

                    }
                    else if (!string.IsNullOrEmpty(filtreMotCle))
                    {
                        baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                    where o.Client.Code == Convert.ToInt32(filtreMotCle)
                                    select o;

                    }

                    var query = from o in baseQuery
                                select new FicheConsignationDto
                                {
                                    id = o.id,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    idTypeFicheConsignation = o.idTypeFicheConsignation.Value,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,

                                    NumeroSerie = o.NumeroSerie,
                                    TypeFicheConsignation = o.TypeFicheConsignation.libelle,
                                    VehiculeImmatricule = o.VehiculeImmatricule,

                                    B12DC = o.B12DC.Value,
                                    B9DC = o.B9DC.Value,
                                    B6RDC = o.B6RDC.Value,
                                    B6VDC = o.B6VDC.Value,
                                    B3DC = o.B3DC.Value,
                                    B35DC = o.B35DC.Value,
                                    PresentoirDC = o.PresentoirDC.Value,

                                    B12Prix = (float)o.B12Prix,
                                    B9Prix = (float)o.B9Prix,
                                    B6RPrix = (float)o.B6RPrix,
                                    B6VPrix = (float)o.B6VPrix,
                                    B3Prix = (float)o.B3Prix,
                                    B35Prix = (float)o.B35Prix,
                                    PresentoirPrix = (float)o.PresentoirPrix,

                                    TotalPrixB12 = (float)o.TotalPrixB12,
                                    TotalPrixB9 = (float)o.TotalPrixB9,
                                    TotalPrixB6R = (float)o.TotalPrixB6R,
                                    TotalPrixB6V = (float)o.TotalPrixB6V,
                                    TotalPrixB3 = (float)o.TotalPrixB3,
                                    TotalPrixB35 = (float)o.TotalPrixB35,
                                    TotalPrixPresentoir = (float)o.TotalPrixPresentoir,

                                    B12DE = o.B12DE.Value,
                                    B9DE = o.B9DE.Value,
                                    B6RDE = o.B6RDE.Value,
                                    B6VDE = o.B6VDE.Value,
                                    B3DE = o.B3DE.Value,
                                    B35DE = o.B35DE.Value,
                                    PresentoirDE = o.PresentoirDE.Value,

                                    Commentaire = o.Commentaire,
                                    etat = o.etat.Value,
                                    Tonnage = (decimal)o.Tonnage

                                };


                    return query.OrderByDescending(x => x.id).ToList();


                }
            }

        }


        public List<FicheConsignationDto> ChercherListeConsignationParClient(string filtreMotCle, DateTime? dateDebut, DateTime? dateFin, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<FicheConsignation> baseQuery = Enumerable.Empty<FicheConsignation>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);





                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                where o.Client.Code == Convert.ToInt32(filtreMotCle) && o.Date >= dateDebut && o.Date <= dateFin

                                select o;

                }



                var query = from o in baseQuery
                                select new FicheConsignationDto
                                {
                                    id = o.id,
                                    Reference = o.Reference,
                                    Date = o.Date.Value,
                                    idTypeFicheConsignation = o.idTypeFicheConsignation.Value,
                                    CodeClient = o.CodeClient.Value,
                                    NomClient = o.Client.nomComplet,

                                    NumeroSerie = o.NumeroSerie,
                                    TypeFicheConsignation = o.TypeFicheConsignation.libelle,
                                    VehiculeImmatricule = o.VehiculeImmatricule,

                                    B12DC = o.B12DC.Value,
                                    B9DC = o.B9DC.Value,
                                    B6RDC = o.B6RDC.Value,
                                    B6VDC = o.B6VDC.Value,
                                    B3DC = o.B3DC.Value,
                                    B35DC = o.B35DC.Value,
                                    PresentoirDC = o.PresentoirDC.Value,

                                    B12Prix = (float)o.B12Prix,
                                    B9Prix = (float)o.B9Prix,
                                    B6RPrix = (float)o.B6RPrix,
                                    B6VPrix = (float)o.B6VPrix,
                                    B3Prix = (float)o.B3Prix,
                                    B35Prix = (float)o.B35Prix,
                                    PresentoirPrix = (float)o.PresentoirPrix,

                                    TotalPrixB12 = (float)o.TotalPrixB12,
                                    TotalPrixB9 = (float)o.TotalPrixB9,
                                    TotalPrixB6R = (float)o.TotalPrixB6R,
                                    TotalPrixB6V = (float)o.TotalPrixB6V,
                                    TotalPrixB3 = (float)o.TotalPrixB3,
                                    TotalPrixB35 = (float)o.TotalPrixB35,
                                    TotalPrixPresentoir = (float)o.TotalPrixPresentoir,

                                    B12DE = o.B12DE.Value,
                                    B9DE = o.B9DE.Value,
                                    B6RDE = o.B6RDE.Value,
                                    B6VDE = o.B6VDE.Value,
                                    B3DE = o.B3DE.Value,
                                    B35DE = o.B35DE.Value,
                                    PresentoirDE = o.PresentoirDE.Value,

                                    Commentaire = o.Commentaire,
                                    etat = o.etat.Value,
                                    Tonnage = (decimal)o.Tonnage

                                };


                    return query.OrderByDescending(x => x.id).ToList();


                
            }

        }




        public List<FicheConsignationDto> ChercherListeConsignationInstance(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                IEnumerable<FicheConsignation> baseQuery = Enumerable.Empty<FicheConsignation>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                where o.etat == 1
                                select o;
                }
                   


                if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                {
                    baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                where o.etat == 1 && (o.Reference.Contains(filtreMotCle))
                                select o;

                }
                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                where o.etat == 1 && (o.Client.Code == Convert.ToInt32(filtreMotCle))
                                select o;

                }

                var query = from o in baseQuery
                            select new FicheConsignationDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                idTypeFicheConsignation = o.idTypeFicheConsignation.Value,
                                CodeClient = o.CodeClient.Value,

                                NumeroSerie = o.NumeroSerie,
                                TypeFicheConsignation = o.TypeFicheConsignation.libelle,
                                NomClient = o.Client.nomComplet,
                                VehiculeImmatricule = o.VehiculeImmatricule,

                                B12DC = o.B12DC.Value,
                                B9DC = o.B9DC.Value,
                                B6RDC = o.B6RDC.Value,
                                B6VDC = o.B6VDC.Value,
                                B3DC = o.B3DC.Value,
                                B35DC = o.B35DC.Value,
                                PresentoirDC = o.PresentoirDC.Value,

                                B12Prix = (float)o.B12Prix,
                                B9Prix = (float)o.B9Prix,
                                B6RPrix = (float)o.B6RPrix,
                                B6VPrix = (float)o.B6VPrix,
                                B3Prix = (float)o.B3Prix,
                                B35Prix = (float)o.B35Prix,
                                PresentoirPrix = (float)o.PresentoirPrix,

                                TotalPrixB12 = (float)o.TotalPrixB12,
                                TotalPrixB9 = (float)o.TotalPrixB9,
                                TotalPrixB6R = (float)o.TotalPrixB6R,
                                TotalPrixB6V = (float)o.TotalPrixB6V,
                                TotalPrixB3 = (float)o.TotalPrixB3,
                                TotalPrixB35 = (float)o.TotalPrixB35,
                                TotalPrixPresentoir = (float)o.TotalPrixPresentoir,

                                B12DE = o.B12DE.Value,
                                B9DE = o.B9DE.Value,
                                B6RDE = o.B6RDE.Value,
                                B6VDE = o.B6VDE.Value,
                                B3DE = o.B3DE.Value,
                                B35DE = o.B35DE.Value,
                                PresentoirDE = o.PresentoirDE.Value,

                                Commentaire = o.Commentaire,
                                etat = o.etat.Value,
                                Tonnage = (decimal)o.Tonnage

                            };


                return query.OrderByDescending(x => x.id).ToList();
            }

        }



        public List<FicheConsignationDto> ChercherListeConsignationCloturer(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<FicheConsignation> baseQuery = Enumerable.Empty<FicheConsignation>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);

                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                where o.etat == 3
                                select o;


                }



                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                where o.etat == 3 && (o.Date == Convert.ToDateTime(filtreMotCle))
                                select o;

                }
                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.FicheConsignations.AsEnumerable()
                                where o.etat == 3 && (o.Client.Code == Convert.ToInt32(filtreMotCle))
                                select o;

                }

                var query = from o in baseQuery
                            select new FicheConsignationDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                idTypeFicheConsignation = o.idTypeFicheConsignation.Value,
                                CodeClient = o.CodeClient.Value,
                                NomClient = o.Client.nomComplet,

                                NumeroSerie = o.NumeroSerie,
                                TypeFicheConsignation = o.TypeFicheConsignation.libelle,
                                VehiculeImmatricule = o.VehiculeImmatricule,

                                B12DC = o.B12DC.Value,
                                B9DC = o.B9DC.Value,
                                B6RDC = o.B6RDC.Value,
                                B6VDC = o.B6VDC.Value,
                                B3DC = o.B3DC.Value,
                                B35DC = o.B35DC.Value,
                                PresentoirDC = o.PresentoirDC.Value,


                                B12Prix = (float)o.B12Prix,
                                B9Prix = (float)o.B9Prix,
                                B6RPrix = (float)o.B6RPrix,
                                B6VPrix = (float)o.B6VPrix,
                                B3Prix = (float)o.B3Prix,
                                B35Prix = (float)o.B35Prix,
                                PresentoirPrix = (float)o.PresentoirPrix,

                                TotalPrixB12 = (float)o.TotalPrixB12,
                                TotalPrixB9 = (float)o.TotalPrixB9,
                                TotalPrixB6R = (float)o.TotalPrixB6R,
                                TotalPrixB6V = (float)o.TotalPrixB6V,
                                TotalPrixB3 = (float)o.TotalPrixB3,
                                TotalPrixB35 = (float)o.TotalPrixB35,
                                TotalPrixPresentoir = (float)o.TotalPrixPresentoir,

                                B12DE = o.B12DE.Value,
                                B9DE = o.B9DE.Value,
                                B6RDE = o.B6RDE.Value,
                                B6VDE = o.B6VDE.Value,
                                B3DE = o.B3DE.Value,
                                B35DE = o.B35DE.Value,
                                PresentoirDE = o.PresentoirDE.Value,

                                Commentaire = o.Commentaire,
                                etat = o.etat.Value,
                                Tonnage = (decimal)o.Tonnage

                            };


                return query.OrderByDescending(x => x.id).ToList();
            }

        }



        public FicheConsignationDto GetFicheConsignationParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.FicheConsignations.Find(id);

                var dto = new FicheConsignationDto
                {
                    id = entity.id,
                    Reference = entity.Reference,
                    Date = entity.Date.Value,
                    idTypeFicheConsignation = entity.idTypeFicheConsignation.Value,
                    CodeClient = entity.CodeClient.Value,
                    NomClient = entity.Client.nomComplet,

                    NumeroSerie = entity.NumeroSerie,
                    TypeFicheConsignation = entity.TypeFicheConsignation.libelle,
                    VehiculeImmatricule = entity.VehiculeImmatricule,

                    B12DC = entity.B12DC.Value,
                    B9DC = entity.B9DC.Value,
                    B6RDC = entity.B6RDC.Value,
                    B6VDC = entity.B6VDC.Value,
                    B3DC = entity.B3DC.Value,
                    B35DC = entity.B35DC.Value,
                    PresentoirDC = entity.PresentoirDC.Value,

                    B12Prix = (float)entity.B12Prix,
                    B9Prix = (float)entity.B9Prix,
                    B6RPrix = (float)entity.B6RPrix,
                    B6VPrix = (float)entity.B6VPrix,
                    B3Prix = (float)entity.B3Prix,
                    B35Prix = (float)entity.B35Prix,
                    PresentoirPrix = (float)entity.PresentoirPrix,


                    TotalPrixB12 = (float)entity.TotalPrixB12,
                    TotalPrixB9 = (float)entity.TotalPrixB9,
                    TotalPrixB6R = (float)entity.TotalPrixB6R,
                    TotalPrixB6V = (float)entity.TotalPrixB6V,
                    TotalPrixB3 = (float)entity.TotalPrixB3,
                    TotalPrixB35 = (float)entity.TotalPrixB35,
                    TotalPrixPresentoir = (float)entity.TotalPrixPresentoir,

                    B12DE = entity.B12DE.Value,
                    B9DE = entity.B9DE.Value,
                    B6RDE = entity.B6RDE.Value,
                    B6VDE = entity.B6VDE.Value,
                    B3DE = entity.B3DE.Value,
                    B35DE = entity.B35DE.Value,
                    PresentoirDE = entity.PresentoirDE.Value,

                    Commentaire = entity.Commentaire,
                    etat = entity.etat.Value,
                    Tonnage = (decimal)entity.Tonnage,
                    Total = (float) (entity.TotalPrixB12 + entity.TotalPrixB9 + entity.TotalPrixB6R + entity.TotalPrixB6V + entity.TotalPrixB3 + entity.TotalPrixB35)
                   
                };


                return dto;

            }

        }



        public void Ajouter(FicheConsignationDto dto)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }



                var entity = new FicheConsignation
                {
                    Date = dto.Date,
                    Reference = dto.Reference,
                    idTypeFicheConsignation = dto.idTypeFicheConsignation,
                    NumeroSerie = dto.NumeroSerie,
                    VehiculeImmatricule = dto.VehiculeImmatricule,

                    B12DC = dto.B12DC,
                    B9DC = dto.B9DC,
                    B6RDC = dto.B6RDC,
                    B6VDC = dto.B6VDC,
                    B3DC = dto.B3DC,
                    B35DC = dto.B35DC,
                    PresentoirDC = dto.PresentoirDC,

                    B12Prix = (float)dto.B12Prix,
                    B9Prix = (float)dto.B9Prix,
                    B6RPrix = (float)dto.B6RPrix,
                    B6VPrix = (float)dto.B6VPrix,
                    B3Prix = (float)dto.B3Prix,
                    B35Prix = (float)dto.B35Prix,
                    PresentoirPrix = (float)dto.PresentoirPrix,

                    TotalPrixB12 = (float)dto.TotalPrixB12,
                    TotalPrixB9 = (float)dto.TotalPrixB9,
                    TotalPrixB6R = (float)dto.TotalPrixB6R,
                    TotalPrixB6V = (float)dto.TotalPrixB6V,
                    TotalPrixB3 = (float)dto.TotalPrixB3,
                    TotalPrixB35 = (float)dto.TotalPrixB35,
                    TotalPrixPresentoir = (float)dto.TotalPrixPresentoir,

                    B12DE = dto.B12DE,
                    B9DE = dto.B9DE,
                    B6RDE = dto.B6RDE,
                    B6VDE = dto.B6VDE,
                    B3DE = dto.B3DE,
                    B35DE = dto.B35DE,
                    PresentoirDE = dto.PresentoirDE,


                    Client = client,
                    Commentaire = dto.Commentaire,
                    etat = 1,
                    Tonnage = (decimal)(dto.B12DE * 12.5 + dto.B9DE * 9 + dto.B6RDE * 6 + dto.B6VDE * 6 + dto.B3DE * 2.75 + dto.B35DE * 35) / 1000




            };
                _context.FicheConsignations.Add(entity);
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }



        public void Modifier(FicheConsignationDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var client = _context.Clients.Find(dto.CodeClient);


                if (client == null)
                {

                    client = new Client
                    {
                        Code = dto.CodeClient,
                        nomComplet = dto.NomClient,

                    };
                }



                var entity = _context.FicheConsignations.Find(dto.id);
                if (entity != null)

                {
                    entity.Date = dto.Date;
                    entity.Reference = dto.Reference;
                    entity.idTypeFicheConsignation = dto.idTypeFicheConsignation;
                    entity.NumeroSerie = dto.NumeroSerie;

                    entity.VehiculeImmatricule = dto.VehiculeImmatricule;

                    entity.B12DC = dto.B12DC;
                    entity.B9DC = dto.B9DC;
                    entity.B6RDC = dto.B6RDC;
                    entity.B6VDC = dto.B6VDC;
                    entity.B3DC = dto.B3DC;
                    entity.B35DC = dto.B35DC;
                    entity.PresentoirDC = dto.PresentoirDC;

                    entity.B12Prix = (float)dto.B12Prix;
                    entity.B9Prix = (float)dto.B9Prix;
                    entity.B6RPrix = (float)dto.B6RPrix;
                    entity.B6VPrix = (float)dto.B6VPrix;
                    entity.B3Prix = (float)dto.B3Prix;
                    entity.B35Prix = (float)dto.B35Prix;
                    entity.PresentoirPrix = dto.PresentoirPrix;

                    entity.TotalPrixB12 = (float)dto.TotalPrixB12;
                    entity.TotalPrixB9 = (float)dto.TotalPrixB9;
                    entity.TotalPrixB6R = (float)dto.TotalPrixB6R;
                    entity.TotalPrixB6V = (float)dto.TotalPrixB6V;
                    entity.TotalPrixB3 = (float)dto.TotalPrixB3;
                    entity.TotalPrixB35 = (float)dto.TotalPrixB35;
                    entity.TotalPrixPresentoir = dto.TotalPrixPresentoir;

                    entity.B12DE = dto.B12DE;
                    entity.B9DE = dto.B9DE;
                    entity.B6RDE = dto.B6RDE;
                    entity.B6VDE = dto.B6VDE;
                    entity.B3DE = dto.B3DE;
                    entity.B35DE = dto.B35DE;
                    entity.PresentoirDE = dto.PresentoirDE;


                    entity.Client = client;
                    entity.Commentaire = dto.Commentaire;

                    entity.Tonnage = (decimal)(dto.B12DE * 12.5 + dto.B9DE * 9 + dto.B6RDE * 6 + dto.B6VDE * 6 + dto.B3DE * 2.75 + dto.B35DE * 35) / 1000;


                    if (dto.idEntite == 4)
                        entity.etat = entity.etat;
                    else if (dto.idEntite == 5)
                        entity.etat = 2;
                    else
                        entity.etat = entity.etat;
                    


                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }



        public void EnInstance(int idEntite ,int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                if (idEntite == 5)
                { 
                    var entity = _context.FicheConsignations.Find(id);
                    entity.etat = 2;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                if (idEntite == 4)
                {
                    var entity = _context.FicheConsignations.Find(id);
                    entity.etat = 1;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
                }
               

            }

        }


        public void Valider(int idEntite,int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            { 
                var entity = _context.FicheConsignations.Find(id);
                entity.etat = 3;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }


        public void CloturerParDate(int idEntite, DateTime date)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var entity = _context.FicheConsignations.Where(x => x.Date == date.Date).Select(x => x.id).ToArray();

                foreach (int id in entity)
                {

                    FicheConsignation fiche = _context.FicheConsignations.Find(id);
                    fiche.etat = 4;
                    //var sortie = _context.Sorties.Where(x => x.id == item.id).SingleOrDefault();
                    _context.Entry(fiche).State = EntityState.Modified;
                    _context.SaveChanges();

                }


            }
        }




        public void Cloturer(int[] details)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                if (details == null) return;

                foreach (int id in details)
                {
                   

                    FicheConsignation fiche = _context.FicheConsignations.Find(id);
                    fiche.etat = 4;
                    _context.Entry(fiche).State = EntityState.Modified;
                    _context.SaveChanges();

                }
            }
        }

        public int VerifierExistFiche(string reference)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var nbreFiche = _context.FicheConsignations.Where(x => x.Reference == reference).Count();

                return nbreFiche;
            }

        }

        public int TotalInstance()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.FicheConsignations.Where(x => x.etat == 1).Count();

                return Total;
            }


        }

        public int TotalCloturer()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.FicheConsignations.Where(x => x.etat == 3).Count();

                return Total;
            }


        }



        public List<TypeFicheConsignationDto> GetListeTypeFiche()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.TypeFicheConsignations.ToList();
                var listeDto = new List<TypeFicheConsignationDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new TypeFicheConsignationDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }

        public TypeFicheConsignationDto GetTypeFicheParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.TypeFicheConsignations.Find(id);

                var dto = new TypeFicheConsignationDto
                {
                    id = entity.id,
                    libelle = entity.libelle
                };
                return dto;

            }

        }



    }
}