﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using GUHabitat.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class BusinessGestionUtilisateurs
    {
        private static BusinessGestionUtilisateurs instance;
        private BusinessGestionUtilisateurs() { }

        public static BusinessGestionUtilisateurs Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionUtilisateurs();
                }
                return instance;
            }
        }


        GUHabitatDBEntities _context = new GUHabitatDBEntities();


        public List<EntiteDto> GetListeEntites()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var list = _context.Entites.ToList();
                var listeDto = new List<EntiteDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new EntiteDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }
            
        }


        public List<RoleDto> GetListeRoles()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.Roles.ToList();
                var listeDto = new List<RoleDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new RoleDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;
            }
           
        }


        public void EnregistrerNouvelleUtilisateur(UtilisateurDto dto)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                

                var entity = new Utilisateur
                {

                    login = dto.login,
                    password = dto.password,
                    Nom = dto.Nom,
                    idEntite = dto.idEntite,
                    idRole = dto.idRole,
                    signature = dto.signature
                };

                

                _context.Utilisateurs.Add(entity);
              
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                
            }

        }

        





        public List<UtilisateurDto> ChercherListeUtilisateurs(string filtreMotCle, int? idEntite)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                //requête linq to entity : jointure + projection => max performance

                if (string.IsNullOrEmpty(filtreMotCle)) filtreMotCle = "";
                _context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


                var baseQuery = from o in _context.Utilisateurs
                                where o.login.Contains(filtreMotCle)
                                select o;


                var query = from o in baseQuery
                            select new UtilisateurDto
                            {
                                id = o.id,
                                login = o.login,
                                role = o.Role.libelle,
                                Nom = o.Nom,
                                entite = o.Entite.libelle,
                            };

                //exécution différé (l'execution est forcé par la méthode ToList)

                return query.OrderByDescending(x => x.login).ToList();
            }
        }




            public bool SupprimerUtilisateur(string login)
            {
                try
                {
                    var entity = _context.Utilisateurs.Find(login);
                    _context.Utilisateurs.Remove(entity);
                    _context.SaveChanges();
                    return true;

                }
                catch (Exception)
                {

                    return false;
                }


            }


        public UtilisateurDto GetUtilisateurParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Utilisateurs.Find(id);
                var dto = new UtilisateurDto
                {
                    login = entity.login,
                    password = entity.password,
                    Nom = entity.Nom,
                    idEntite = entity.idEntite == null ? 0 : entity.idEntite.Value,
                    idRole = entity.idRole == null ? 0 : entity.idRole.Value,
                    signature = entity.signature
                };
                return dto;
            }


        }




        public UtilisateurDto GetUtilisateurLogin(string login )
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Utilisateurs.Where(x => x.login == login).SingleOrDefault();
                var dto = new UtilisateurDto
                {
                    login = entity.login,
                    password = entity.password,
                    Nom = entity.Nom,
                    idEntite = entity.idEntite == null ? 0 : entity.idEntite.Value,
                    idRole = entity.idRole == null ? 0 : entity.idRole.Value,
                    signature = entity.signature
                };
                return dto;
            }


        }


        public string GetPasswordParlogin(string login )
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Utilisateurs.Where(x=> x.login == login).Select(x=>x.password).SingleOrDefault();
                
                return entity;
            }


        }





        public void ModifierUtilisateur(UtilisateurDto dto)
        {
            var entity = _context.Utilisateurs.Find(dto.id);

            entity.login = dto.login;
            entity.password = dto.password;
            entity.Nom = dto.Nom;
            entity.idEntite = dto.idEntite;
            entity.idRole = dto.idRole;
            if (dto.signature != null) entity.signature = dto.signature;

            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }



        }




     

        public void ModifierPasswordUtilisateur(UtilisateurDto dto)
        {
            var entity = _context.Utilisateurs.Where(x => x.login == dto.login).SingleOrDefault();

            //entity.login = dto.login;
            entity.password = dto.password;
            //entity.Nom = dto.Nom;
            //entity.idEntite = dto.idEntite;
            //if (dto.signature != null) entity.signature = dto.signature;

            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }



        }




    }
}
