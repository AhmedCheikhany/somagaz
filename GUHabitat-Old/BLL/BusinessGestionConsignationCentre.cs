﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionConsignationCentre
    {



        private static BusinessGestionConsignationCentre instance;
        private BusinessGestionConsignationCentre() { }

        public static BusinessGestionConsignationCentre Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionConsignationCentre();
                }
                return instance;
            }
        }


        public List<ConsignationCentreDto> ChercherListeConsignationCentre(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<ConsignationCentre> baseQuery = Enumerable.Empty<ConsignationCentre>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                //if (string.IsNullOrEmpty(filtreMotCle)) 
                //_context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.ConsignationCentres.AsEnumerable()
                                select o;

                }




                if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                {
                    baseQuery = from o in _context.ConsignationCentres.AsEnumerable()
                                where o.Reference.Contains(filtreMotCle)
                                select o;

                }


                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.ConsignationCentres.AsEnumerable()
                                where o.CodeCentre == Convert.ToInt32(filtreMotCle)
                                select o;

                }

                var query = from o in baseQuery
                            select new ConsignationCentreDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                Date = o.Date.Value,
                                CodeCentre = o.CodeCentre.Value,
                                LibelleCentre = o.Centre.libelle,

                                B12 = o.B12.Value,
                                B9 = o.B9.Value,
                                B6R = o.B6R.Value,
                                B6V = o.B6V.Value,
                                B3 = o.B3.Value,
                                B35 = o.B35.Value,

                                B12Prix = o.B12Prix.Value,
                                B9Prix = o.B9Prix.Value,
                                B6RPrix = o.B6RPrix.Value,
                                B6VPrix = o.B6VPrix.Value,
                                B3Prix = o.B3Prix.Value,
                                B35Prix = o.B35Prix.Value,

                            
                                TotalPrixB12 = o.TotalPrixB12.Value,
                                TotalPrixB9 = o.TotalPrixB9.Value,
                                TotalPrixB6R = o.TotalPrixB6R.Value,
                                TotalPrixB6V = o.TotalPrixB6V.Value,
                                TotalPrixB3 = o.TotalPrixB3.Value,
                                TotalPrixB35 = o.TotalPrixB35.Value,

                            

                                Commentaire = o.Commentaire,

                                etat = o.etat.Value

                            };

                return query.OrderByDescending(x => x.id).ToList();


            }

        }


        public ConsignationCentreDto GetConsignationCentreParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.ConsignationCentres.Find(id);

                var dto = new ConsignationCentreDto
                {

                    id = o.id,
                    Reference = o.Reference,
                    Date = o.Date.Value,
                    CodeCentre = o.CodeCentre.Value,
                    LibelleCentre = o.Centre.libelle,

                    B12 = o.B12.Value,
                    B9 = o.B9.Value,
                    B6R = o.B6R.Value,
                    B6V = o.B6V.Value,
                    B3 = o.B3.Value,
                    B35 = o.B35.Value,

                    B12Prix = o.B12Prix.Value,
                    B9Prix = o.B9Prix.Value,
                    B6RPrix = o.B6RPrix.Value,
                    B6VPrix = o.B6VPrix.Value,
                    B3Prix = o.B3Prix.Value,
                    B35Prix = o.B35Prix.Value,


                    TotalPrixB12 = o.TotalPrixB12.Value,
                    TotalPrixB9 = o.TotalPrixB9.Value,
                    TotalPrixB6R = o.TotalPrixB6R.Value,
                    TotalPrixB6V = o.TotalPrixB6V.Value,
                    TotalPrixB3 = o.TotalPrixB3.Value,
                    TotalPrixB35 = o.TotalPrixB35.Value,



                    Commentaire = o.Commentaire,

                    etat = o.etat.Value


                };


                return dto;

            }

        }








        public void Ajouter(ConsignationCentreDto dto)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var centre = _context.Centres.Find(dto.CodeCentre);

                if (centre == null)
                {

                    centre = new Centre
                    {
                        Code = dto.CodeCentre,
                        libelle = dto.LibelleCentre,

                    };
                }





                var entity = new ConsignationCentre
                {

                    Reference = dto.Reference,
                    Date = dto.Date,
                    Centre = centre,


                    B12 = dto.B12,
                    B9= dto.B9,
                    B6R = dto.B6R,
                    B6V = dto.B6V,
                    B3= dto.B3,
                    B35 = dto.B35,

                    B12Prix = dto.B12Prix,
                    B9Prix = dto.B9Prix,
                    B6RPrix= dto.B6RPrix,
                    B6VPrix = dto.B6VPrix,
                    B3Prix = dto.B3Prix,
                    B35Prix = dto.B35Prix,


                    TotalPrixB12 = dto.TotalPrixB12,
                    TotalPrixB9 = dto.TotalPrixB9,
                    TotalPrixB6R = dto.TotalPrixB6R,
                    TotalPrixB6V = dto.TotalPrixB6V,
                    TotalPrixB3 = dto.TotalPrixB3,
                    TotalPrixB35 = dto.TotalPrixB35,

                  


                    Commentaire = dto.Commentaire,

                    etat = 1



                };
                _context.ConsignationCentres.Add(entity);
                try
                {
                    _context.SaveChanges();

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }


        public void Modifier(ConsignationCentreDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var centre = _context.Centres.Find(dto.CodeCentre);

                if (centre == null)
                {

                    centre = new Centre
                    {
                        Code = dto.CodeCentre,
                        libelle = dto.LibelleCentre,

                    };
                }




                var entity = _context.ConsignationCentres.Find(dto.id);
                if (entity != null)

                {


                    entity.Reference = dto.Reference;
                    entity.Date = dto.Date;
                    entity.Centre = centre;

                    entity.B12 = dto.B12;
                    entity.B9 = dto.B9;
                    entity.B6R = dto.B6R;
                    entity.B6V = dto.B6V;
                    entity.B3 = dto.B3;
                    entity.B35 = dto.B35;

                    entity.B12Prix = dto.B12Prix;
                    entity.B9Prix = dto.B9Prix;
                    entity.B6RPrix= dto.B6RPrix;
                    entity.B6VPrix = dto.B6VPrix;
                    entity.B3Prix = dto.B3Prix;
                    entity.B35Prix = dto.B35Prix;

                    entity.TotalPrixB12 = dto.TotalPrixB12;
                    entity.TotalPrixB9= dto.TotalPrixB9;
                    entity.TotalPrixB6R = dto.TotalPrixB6R;
                    entity.TotalPrixB6V = dto.TotalPrixB6V;
                    entity.TotalPrixB3 = dto.TotalPrixB3;
                    entity.TotalPrixB35 = dto.TotalPrixB35;

                    entity.Commentaire = dto.Commentaire;





                    if (dto.idEntite == 4)
                        entity.etat = entity.etat;
                    else if (dto.idEntite == 5)
                        entity.etat = 2;
                    else
                        entity.etat = entity.etat;




                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }









        public CentreDto GetCentreParCode(int code)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.Centres.Find(code);

                var dto = new CentreDto
                {
                    Code = entity.Code,
                    libelle = entity.libelle
                };
                return dto;

            }

        }

        public List<CentreDto> GetListeCentre()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.Centres.Where(x => x.Code != 0 && x.Code != 9).ToList();
                var listeDto = new List<CentreDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new CentreDto
                    {
                        Code = item.Code,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }


        public void EnInstance(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                     var entity = _context.ConsignationCentres.Find(id);
                     entity.etat = 1;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();
                


            }

        }


        public void Valider(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.ConsignationCentres.Find(id);
                entity.etat = 2;
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

            }

        }



    }
}