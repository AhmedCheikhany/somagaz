﻿using GUHabitat.DAL;
using GUHabitat.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GUHabitat.BLL
{
    public class BusinessGestionSortie
    {

        private static BusinessGestionSortie instance;
        private BusinessGestionSortie() { }

        public static BusinessGestionSortie Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessGestionSortie();
                }
                return instance;
            }
        }


        public List<SortieDto> ChercherListeSortie(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<Sortie> baseQuery = Enumerable.Empty<Sortie>();

                //Utilisateur user = _context.Utilisateurs.Find(login);
                //if (string.IsNullOrEmpty(filtreMotCle)) 
                //_context.Database.Log = (msg) => System.Diagnostics.Debug.WriteLine(msg);


                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.Sorties.AsEnumerable()
                                select o;

                }




                if (!string.IsNullOrEmpty(filtreMotCle) && filtreMotCle.Count() >= 6)
                {
                    baseQuery = from o in _context.Sorties.AsEnumerable()
                                where  o.ReferenceTypeSortie.Contains(filtreMotCle)
                                select o;

                }


                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.Sorties.AsEnumerable()
                                where o.Client == Convert.ToInt32(filtreMotCle) || o.Employe == Convert.ToInt32(filtreMotCle)
                                select o;

                }

                var query = from o in baseQuery
                                select new SortieDto
                                {
                                    id = o.id,
                                    Reference = o.Reference,
                                    ReferenceTypeSortie = o.ReferenceTypeSortie,
                                    NumeroFacture = o.NumeroFacture,
                                    Client = (int)o.Client,
                                    Employe = (int)o.Employe,
                                    Date = o.Date.Value,
                                    idTypeSortie = o.idTypeSortie.Value,
                                    TypeSortie = o.TypeSortie.libelle,
                                    B12 = o.B12.Value,
                                    B9 = o.B9.Value,
                                    B6R = o.B6R.Value,
                                    B6V = o.B6V.Value,
                                    B3 = o.B3.Value,
                                    B35 = o.B35.Value,
                                    Presentoir = o.Presentoir.Value,
                                    Commentaires = o.Commentaires,

                                    etat = o.etat.Value

                                };

                    return query.OrderByDescending(x => x.id).ToList();
                
              
            }

        }


        public List<SortieDto> ChercherListeSortieValider(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<Sortie> baseQuery = Enumerable.Empty<Sortie>();


                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.Sorties.AsEnumerable()
                                where o.etat == 1
                                select o;

                }



                if (!string.IsNullOrEmpty(filtreMotCle) /*&& filtreMotCle.Count() >= 6*/)
                {
                    baseQuery = from o in _context.Sorties.AsEnumerable()
                                where o.etat == 1 && (o.Date == Convert.ToDateTime(filtreMotCle))
                                select o;

                }

                //else if (!string.IsNullOrEmpty(filtreMotCle))
                //{
                //    baseQuery = from o in _context.Sorties.AsEnumerable()
                //                where o.etat == 1 && (o.Client == Convert.ToInt32(filtreMotCle) || o.Employe == Convert.ToInt32(filtreMotCle))
                //                select o;

                //}


                var query = from o in baseQuery
                            select new SortieDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                ReferenceTypeSortie = o.ReferenceTypeSortie,
                                NumeroFacture = o.NumeroFacture,
                                Client = (int)o.Client,
                                Employe = (int)o.Employe,
                                Date = o.Date.Value,
                                idTypeSortie = o.idTypeSortie.Value,
                                TypeSortie = o.TypeSortie.libelle,
                                B12 = o.B12.Value,
                                B9 = o.B9.Value,
                                B6R = o.B6R.Value,
                                B6V = o.B6V.Value,
                                B3 = o.B3.Value,
                                B35 = o.B35.Value,
                                Presentoir = o.Presentoir.Value,
                                Commentaires = o.Commentaires,

                                etat = o.etat.Value

                            };

                return query.OrderByDescending(x => x.id).ToList();


            }

        }



        public List<SortieDto> ChercherListeSortieCloturer(string filtreMotCle, int? idEntite, string login)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                IEnumerable<Sortie> baseQuery = Enumerable.Empty<Sortie>();

               
                if (string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.Sorties.AsEnumerable()
                                where o.etat == 2
                                select o;

                }



                if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.Sorties.AsEnumerable()
                                where o.etat == 2 && (o.Date == Convert.ToDateTime(filtreMotCle))
                                select o;

                }

                else if (!string.IsNullOrEmpty(filtreMotCle))
                {
                    baseQuery = from o in _context.Sorties.AsEnumerable()
                                where o.etat == 2 && (o.Client == Convert.ToInt32(filtreMotCle) || o.Employe == Convert.ToInt32(filtreMotCle))
                                select o;

                }

                var query = from o in baseQuery
                            select new SortieDto
                            {
                                id = o.id,
                                Reference = o.Reference,
                                ReferenceTypeSortie = o.ReferenceTypeSortie,
                                NumeroFacture = o.NumeroFacture,
                                Client = (int)o.Client,
                                Employe = (int)o.Employe,
                                Date = o.Date.Value,
                                idTypeSortie = o.idTypeSortie.Value,
                                TypeSortie = o.TypeSortie.libelle,
                                B12 = o.B12.Value,
                                B9 = o.B9.Value,
                                B6R = o.B6R.Value,
                                B6V = o.B6V.Value,
                                B3 = o.B3.Value,
                                B35 = o.B35.Value,
                                Presentoir = o.Presentoir.Value,
                                Commentaires = o.Commentaires,

                                etat = o.etat.Value

                            };

                return query.OrderByDescending(x => x.id).ToList();


            }

        }



        public SortieDto GetSortieParId(int id)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var o = _context.Sorties.Find(id);

                var dto = new SortieDto
                {

                    id = o.id,
                    Reference = o.Reference,
                    ReferenceTypeSortie = o.ReferenceTypeSortie,
                    NumeroFacture = o.NumeroFacture,
                    Client =(int) o.Client,
                    Employe = (int)o.Employe,
                    Date = o.Date.Value,
                    idTypeSortie = o.idTypeSortie.Value,
                    TypeSortie = o.TypeSortie.libelle,
                    B12 = o.B12.Value,
                    B9 = o.B9.Value,
                    B6R = o.B6R.Value,
                    B6V = o.B6V.Value,
                    B3 = o.B3.Value,
                    B35 = o.B35.Value,
                    Presentoir = o.Presentoir.Value,
                    Commentaires = o.Commentaires,

                    etat = o.etat.Value

                };


                return dto;

            }

        }



        public void Ajouter(SortieDto dto)
        {




            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                


                var entity = new Sortie
                {
                   
                    Reference = dto.Reference,
                    ReferenceTypeSortie = dto.ReferenceTypeSortie,
                    NumeroFacture = dto.NumeroFacture,
                    Client = dto.Client,
                    Employe = dto.Employe,
                    Date = dto.Date,
                    idTypeSortie = dto.idTypeSortie,

                    B12 = dto.B12,
                    B9 = dto.B9,
                    B6R = dto.B6R,
                    B6V = dto.B6V,
                    B3 = dto.B3,
                    B35 = dto.B35,
                    Presentoir = dto.Presentoir,

                    Commentaires = dto.Commentaires,
                    etat = 1,
                   



                };
                _context.Sorties.Add(entity);
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                dto.id = entity.id;

            }


        }




        public void Modifier(SortieDto dto)
        {


            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var entity = _context.Sorties.Find(dto.id);
                if (entity != null)

                {
                    
                    entity.Reference = dto.Reference;
                    entity.ReferenceTypeSortie = dto.ReferenceTypeSortie;
                    entity.NumeroFacture = dto.NumeroFacture;
                    entity.Client = dto.Client;
                    entity.Employe = dto.Employe;
                    entity.Date = dto.Date;
                    entity.idTypeSortie = dto.idTypeSortie;

                    entity.B12 = dto.B12;
                    entity.B9  = dto.B9;
                    entity.B6R = dto.B6R;
                    entity.B6V = dto.B6V;
                    entity.B3 = dto.B3;
                    entity.B35 = dto.B35;
                    entity.Presentoir = dto.Presentoir;

                    entity.etat = 1;
                    entity.Commentaires = dto.Commentaires;
                 



                };
                _context.Entry(entity).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }


            }


        }




        public int VerifierExistFiche(string reference)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var nbreFiche = _context.Sorties.Where(x => x.Reference == reference).Count();

                return nbreFiche;
            }

        }



        public TypeSortieDto GetTypeSortieParId(int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                var entity = _context.TypeSorties.Find(id);

                var dto = new TypeSortieDto
                {
                    id = entity.id,
                    libelle = entity.libelle
                };
                return dto;

            }

        }



        public List<TypeSortieDto> GetListeTypeSortie()
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var list = _context.TypeSorties.ToList();
                var listeDto = new List<TypeSortieDto>();
                foreach (var item in list)
                {
                    listeDto.Add(new TypeSortieDto
                    {
                        id = item.id,
                        libelle = item.libelle,
                    });

                }
                return listeDto;

            }


        }


        public void Valider(int idEntite, int[] details)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                if (details == null) return;

                foreach (int id in details)
                {


                    Sortie sortie = _context.Sorties.Find(id);
                    sortie.etat = 2;
                    _context.Entry(sortie).State = EntityState.Modified;
                    _context.SaveChanges();

                }
            }
        }


        public void Valider(int idEntite, DateTime date)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                

                var entity = _context.Sorties.Where(x=> x.Date == date.Date ).Select(x=>x.id).ToArray();

                foreach (int id in entity)
                {

                    Sortie sortie = _context.Sorties.Find(id);
                    sortie.etat = 2;
                    //var sortie = _context.Sorties.Where(x => x.id == item.id).SingleOrDefault();
                    _context.Entry(sortie).State = EntityState.Modified;
                    _context.SaveChanges();

                }


            }
        }

        public void CloturerParDate(int idEntite, DateTime date)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {


                var entity = _context.Sorties.Where(x => x.Date == date.Date).Select(x => x.id).ToArray();

                foreach (int id in entity)
                {

                    Sortie sortie = _context.Sorties.Find(id);
                    sortie.etat = 3;
                    //var sortie = _context.Sorties.Where(x => x.id == item.id).SingleOrDefault();
                    _context.Entry(sortie).State = EntityState.Modified;
                    _context.SaveChanges();

                }


            }
        }




        public void Cloturer(int[] details)
        {
            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                if (details == null) return;

                foreach (int id in details)
                {


                    Sortie sortie = _context.Sorties.Find(id);
                    sortie.etat = 3;
                    _context.Entry(sortie).State = EntityState.Modified;
                    _context.SaveChanges();

                }
            }
        }


        public void EnInstance(int idEntite, int id)
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {
                    var entity = _context.Sorties.Find(id);
                    entity.etat = 1;
                    _context.Entry(entity).State = EntityState.Modified;
                    _context.SaveChanges();

            }

        }


        public int TotalCloturer()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.Sorties.Where(x => x.etat == 2).Count();

                return Total;
            }


        }


        public int TotalValider()
        {

            using (var _context = new GUHabitat.DAL.GUHabitatDBEntities())
            {

                var Total = _context.Sorties.Where(x => x.etat == 1).Count();

                return Total;
            }


        }

       


    }
}